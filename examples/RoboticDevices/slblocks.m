function blkStruct = slblocks
  % Specify that the product should appear in the library browser
  % and be cached in its repository
  Browser.Library = 'robdev';
 Browser.Name    = 'Robotics Laboratory: Experimental Dynamical Systems';
 % Browser.Name    = 'experimental robotic devices';
  blkStruct.Browser = Browser;