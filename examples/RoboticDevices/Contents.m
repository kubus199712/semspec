% RoboticDevices: a toolbox designed for design, verification and deployment
% of model based control algorithms intended for robotic devices available 
% in the Robotics Laboratory at the Department of Cybernetics and Robotics 
% at the Wroclaw University of Science and Technology
%
%
% (C) Krzysztof Arent, 2019
% This toolbox is under development. Currently it is designed for two
% courses: "Control Theory for Embedded Systems" and Sterowanie adaptacyje 
% i odporne" at Wroclaw University of Science and Technology, in the
% academic year 2020/2021. Dissemination and distribution of this software 
% (both the whole one as well as its parts) in any form and use of it 
% for other purposes is not permitted.
%
% release date: 2020-10-01
%
%
% DCmotor - software tools for model's paremeters identification and
% controller design, intended for a DC motor
% 2Rmanipulator - software tools for controller design, intended for 
% a 2DOF manipulator
% PendulumOnCart - software tools for model's paremeters identification and
% controller design, intended for a pendulum on a cart
% robdev - a Simulink library that includes models and drivers for 
% experimental dynamical systems such as: a DC motor, a 2DOF manipulator, 
% a pendulum on a cart
