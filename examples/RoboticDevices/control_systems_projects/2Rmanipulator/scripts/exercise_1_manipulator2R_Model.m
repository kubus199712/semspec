% 2R manipulator: control
% launching Simulink diagrams designed for the exercise
% on 2R manipulator control (the simulation part)

% author: Krzysztof Arent, 2018

manipulator2R_feedback_components;
manipulator2R_Control_model;

