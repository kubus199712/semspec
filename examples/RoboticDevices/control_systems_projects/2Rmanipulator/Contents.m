% 2Rmanipulator: a set of block diagrams, data sets, scripts 
%   and a notebook designed for verification and further development 
%   of a driver for the 2DOF manipulator in the Robotics Laboratory
% - experimental verification of model based control algorithms 
% - analysis and development of identification algorithms and modelling 
%   techniques.
%
% (C) Krzysztof Arent, 2018
%
% the contents
%
% bin/ 
% data/
% deployment/
%  manipulator2R_Control_intel.mdl - a Simulink diagram of a controller
%   for 2DOF manipulator in Robotics Laboratory, that implements
%   a predefined trajectory tracking in the manipulator's workspace. 
%   The diagram is configured for automatic code generation that 
%   is intended to be deployed at a board with an Intel processor
%  manipulator2R_ControlE_intel.mdl - a Simulink diagram of a controller
%   for 2DOF manipulator in Robotics Laboratory, that implements
%   a trajectory tracking along a straight line in the manipulator's 
%   workspace. The final point of  the trajectory is set manually, in a ROS
%   environment. The diagram is configured for automatic code generation 
%   that is intended to be deployed at a board with an Intel processor.
% functions/
% lib/
%  manipulator2R_feedback_components.mdl
% models/ 
%  manipulator2R_Control_model.mdl: a Simulink diagram of a control system
%   consisting of a manipulator's model and a hierarchical controller that
%   implements a predefined trajectory tracking in a workspace of the
%   manipulator; the diagram in the version designed for HIL      
% notebook
%  2DOFmanipulator.nb - step by step derivation of motion equations
%   for 2DOF manipulator in Mathematica
% scripts/                             
%  exercise_1_manipulator2R_Model.m - Model based control of 
%   a 2DOF manipulator.
%
% projectPaths.m
%