/*
 * File: manipulator2R_Control_intel.h
 *
 * Real-Time Workshop code generated for Simulink model manipulator2R_Control_intel.
 *
 * Model version                        : 1.970
 * Real-Time Workshop file version      : 7.6  (R2010b)  03-Aug-2010
 * Real-Time Workshop file generated on : Tue Feb 04 19:12:04 2020
 * TLC version                          : 7.6 (Jul 13 2010)
 * C/C++ source code generated on       : Tue Feb 04 19:12:04 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Intel->x86/Pentium
 * Code generation objective: Execution efficiency
 * Validation result: Passed (5), Warnings (4), Error (0)
 */

#ifndef RTW_HEADER_manipulator2R_Control_intel_h_
#define RTW_HEADER_manipulator2R_Control_intel_h_
#ifndef manipulator2R_Control_intel_COMMON_INCLUDES_
# define manipulator2R_Control_intel_COMMON_INCLUDES_
#include <math.h>
#include <string.h>
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "rt_nonfinite.h"
#include "rtGetInf.h"
#include "rtGetNaN.h"
#include "rt_pow_snf.h"
#endif                                 /* manipulator2R_Control_intel_COMMON_INCLUDES_ */

#include "manipulator2R_Control_intel_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  (rtmGetTPtr((rtm))[0])
#endif

/* Block signals and states (auto storage) for system '<Root>' */
typedef struct {
  real_T qp[2];                        /* '<S4>/Inv J' */
  struct {
    real_T TimeStampA;
    real_T LastUAtTimeA[2];
    real_T TimeStampB;
    real_T LastUAtTimeB[2];
  } Derivative_RWORK;                  /* '<S4>/Derivative' */

  struct {
    real_T TimeStampA;
    real_T LastUAtTimeA[2];
    real_T TimeStampB;
    real_T LastUAtTimeB[2];
  } Derivative1_RWORK;                 /* '<S4>/Derivative1' */

  real_T singularity;                  /* '<S4>/Inv J' */
  real_T disableIref;                  /* '<Root>/supervisory controller: safety module' */
  struct {
    int_T IcNeedsLoading;
  } Integrator_IWORK;                  /* '<S4>/Integrator' */

  uint8_T is_active_c10_manipulator2R_Con;/* '<Root>/supervisory controller: safety module' */
  uint8_T is_c10_manipulator2R_Control_in;/* '<Root>/supervisory controller: safety module' */
} D_Work_manipulator2R_Control_in;

/* Continuous states (auto storage) */
typedef struct {
  real_T Integrator_CSTATE[2];         /* '<S4>/Integrator' */
} ContinuousStates_manipulator2R_;

/* State derivatives (auto storage) */
typedef struct {
  real_T Integrator_CSTATE[2];         /* '<S4>/Integrator' */
} StateDerivatives_manipulator2R_;

/* State disabled  */
typedef struct {
  boolean_T Integrator_CSTATE[2];      /* '<S4>/Integrator' */
} StateDisabled_manipulator2R_Con;

/* Invariant block signals (auto storage) */
typedef const struct tag_ConstBlockIO_manipulator2R_ {
  real_T q1t0;                         /* '<S5>/q1(t0)' */
  real_T q2t0;                         /* '<S5>/q2(t0)' */
} ConstBlockIO_manipulator2R_Cont;

#ifndef ODE5_INTG
#define ODE5_INTG

/* ODE5 Integration Data */
typedef struct {
  real_T *y;                           /* output */
  real_T *f[6];                        /* derivatives */
} ODE5_IntgData;

#endif

/* External inputs (root inport signals with auto storage) */
typedef struct {
  real_T q1p;                          /* '<Root>/q1p' */
  real_T q1v;                          /* '<Root>/q1v' */
  real_T q2p;                          /* '<Root>/q2p' */
  real_T q2v;                          /* '<Root>/q2v' */
  real_T relinfo;                      /* '<Root>/relinfo' */
} ExternalInputs_manipulator2R_Co;

/* External outputs (root outports fed by signals with auto storage) */
typedef struct {
  real_T Out1;                         /* '<Root>/Out1' */
  real_T Out2;                         /* '<Root>/Out2' */
} ExternalOutputs_manipulator2R_C;

/* Real-time Model Data Structure */
struct RT_MODEL_manipulator2R_Control_ {
  const char_T * volatile errorStatus;
  RTWSolverInfo solverInfo;

  /*
   * ModelData:
   * The following substructure contains information regarding
   * the data used in the model.
   */
  struct {
    real_T *contStates;
    real_T *derivs;
    boolean_T *contStateDisabled;
    boolean_T zCCacheNeedsReset;
    boolean_T derivCacheNeedsReset;
    boolean_T blkStateChange;
    real_T odeY[2];
    real_T odeF[6][2];
    ODE5_IntgData intgData;
  } ModelData;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    int_T numContStates;
    int_T numSampTimes;
  } Sizes;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    uint32_T clockTick0;
    time_T stepSize0;
    uint32_T clockTick1;
    boolean_T firstInitCondFlag;
    SimTimeStep simTimeStep;
    boolean_T stopRequestedFlag;
    time_T *t;
    time_T tArray[2];
  } Timing;
};

extern const ConstBlockIO_manipulator2R_Cont manipulator2R_Control_in_ConstB;/* constant block i/o */

/* Class declaration for model manipulator2R_Control_intel */
class manipulator2R_Control_intelModelClass {
  /* public data and function members */
 public:
  /* External inputs */
  ExternalInputs_manipulator2R_Co manipulator2R_Control_intel_U;

  /* External outputs */
  ExternalOutputs_manipulator2R_C manipulator2R_Control_intel_Y;

  /* Model entry point functions */

  /* model initialize function */
  void initialize();

  /* model step function */
  void step();

  /* Constructor */
  manipulator2R_Control_intelModelClass();

  /* Destructor */
  ~manipulator2R_Control_intelModelClass();

  /* Real-Time Model get method */
  RT_MODEL_manipulator2R_Control_ * getRTM() const;

  /* private data and function members */
 private:
  /* Block singals and states */
  D_Work_manipulator2R_Control_in manipulator2R_Control_int_DWork;
  ContinuousStates_manipulator2R_ manipulator2R_Control_intel_X;/* Block continuous states */

  /* Real-Time Model */
  RT_MODEL_manipulator2R_Control_ manipulator2R_Control_intel_M_;

  /* Real-Time Model pointer */
  RT_MODEL_manipulator2R_Control_ *manipulator2R_Control_intel_M;

  /* private member function(s) for subsystem '<Root>' */
  real_T manipulator2R_Control_intel_det(const real_T x[4]);
  void manipulator2R_Control_in_mpower(const real_T a[4], real_T c[4]);

  /* Continuous states update member function*/
  void rt_ertODEUpdateContinuousStates(RTWSolverInfo *si );

  /* Derivatives member function */
  void manipulator2R_Control_intel_derivatives();
};

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : manipulator2R_Control_intel
 * '<S1>'   : manipulator2R_Control_intel/computed torque control
 * '<S2>'   : manipulator2R_Control_intel/friction compensation
 * '<S3>'   : manipulator2R_Control_intel/supervisory controller: safety module
 * '<S4>'   : manipulator2R_Control_intel/trajectory generator in the joints space
 * '<S5>'   : manipulator2R_Control_intel/trajectory generator in the task space: a composed chain
 * '<S6>'   : manipulator2R_Control_intel/computed torque control/C(q)*qv
 * '<S7>'   : manipulator2R_Control_intel/computed torque control/D(q)
 * '<S8>'   : manipulator2R_Control_intel/computed torque control/Q(q)*v
 * '<S9>'   : manipulator2R_Control_intel/computed torque control/qaref-Kp*ep-Kd*ep
 * '<S10>'  : manipulator2R_Control_intel/friction compensation/kompensacja tarcia  Coulombowskiego, silnik #1
 * '<S11>'  : manipulator2R_Control_intel/friction compensation/kompensacja tarcia  Coulombowskiego, silnik #2
 * '<S12>'  : manipulator2R_Control_intel/friction compensation/kompensacja tarcia  wiskotycznego, silnik #1
 * '<S13>'  : manipulator2R_Control_intel/friction compensation/kompensacja tarcia  wiskotycznego, silnik #2
 * '<S14>'  : manipulator2R_Control_intel/trajectory generator in the joints space/Inv J
 * '<S15>'  : manipulator2R_Control_intel/trajectory generator in the joints space/init_traj
 * '<S16>'  : manipulator2R_Control_intel/trajectory generator in the task space: a composed chain/kinematics
 * '<S17>'  : manipulator2R_Control_intel/trajectory generator in the task space: a composed chain/ref_traj
 */
#endif                                 /* RTW_HEADER_manipulator2R_Control_intel_h_ */

/*
 * File trailer for Real-Time Workshop generated code.
 *
 * [EOF]
 */
