/*
 * File: rtGetInf.h
 *
 * Real-Time Workshop code generated for Simulink model manipulator2R_Control_intel.
 *
 * Model version                        : 1.970
 * Real-Time Workshop file version      : 7.6  (R2010b)  03-Aug-2010
 * Real-Time Workshop file generated on : Tue Feb 04 19:12:04 2020
 * TLC version                          : 7.6 (Jul 13 2010)
 * C/C++ source code generated on       : Tue Feb 04 19:12:04 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Intel->x86/Pentium
 * Code generation objective: Execution efficiency
 * Validation result: Passed (5), Warnings (4), Error (0)
 */

#ifndef RTW_HEADER_rtGetInf_h_
#define RTW_HEADER_rtGetInf_h_
#include <stddef.h>
#include "rtwtypes.h"
#include "rt_nonfinite.h"
#ifdef __cplusplus

extern "C" {

#endif

  extern real_T rtGetInf(void);
  extern real32_T rtGetInfF(void);
  extern real_T rtGetMinusInf(void);
  extern real32_T rtGetMinusInfF(void);

#ifdef __cplusplus

}                                      /* extern "C" */
#endif
#endif                                 /* RTW_HEADER_rtGetInf_h_ */

/*
 * File trailer for Real-Time Workshop generated code.
 *
 * [EOF]
 */
