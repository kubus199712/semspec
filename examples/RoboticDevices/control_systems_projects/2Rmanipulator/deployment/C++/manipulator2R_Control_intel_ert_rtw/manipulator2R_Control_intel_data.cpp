/*
 * File: manipulator2R_Control_intel_data.cpp
 *
 * Real-Time Workshop code generated for Simulink model manipulator2R_Control_intel.
 *
 * Model version                        : 1.970
 * Real-Time Workshop file version      : 7.6  (R2010b)  03-Aug-2010
 * Real-Time Workshop file generated on : Tue Feb 04 19:12:04 2020
 * TLC version                          : 7.6 (Jul 13 2010)
 * C/C++ source code generated on       : Tue Feb 04 19:12:04 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Intel->x86/Pentium
 * Code generation objective: Execution efficiency
 * Validation result: Passed (5), Warnings (4), Error (0)
 */

#include "manipulator2R_Control_intel.h"
#include "manipulator2R_Control_intel_private.h"

/* Invariant block signals (auto storage) */
const ConstBlockIO_manipulator2R_Cont manipulator2R_Control_in_ConstB = {
  -0.78539816339744828
  ,                                    /* '<S5>/q1(t0)' */
  1.5707963267948966
  /* '<S5>/q2(t0)' */
};

/*
 * File trailer for Real-Time Workshop generated code.
 *
 * [EOF]
 */
