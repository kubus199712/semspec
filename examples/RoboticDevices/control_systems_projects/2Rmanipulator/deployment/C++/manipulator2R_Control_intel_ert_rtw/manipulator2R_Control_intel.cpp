/*
 * File: manipulator2R_Control_intel.cpp
 *
 * Real-Time Workshop code generated for Simulink model manipulator2R_Control_intel.
 *
 * Model version                        : 1.970
 * Real-Time Workshop file version      : 7.6  (R2010b)  03-Aug-2010
 * Real-Time Workshop file generated on : Tue Feb 04 19:12:04 2020
 * TLC version                          : 7.6 (Jul 13 2010)
 * C/C++ source code generated on       : Tue Feb 04 19:12:04 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Intel->x86/Pentium
 * Code generation objective: Execution efficiency
 * Validation result: Passed (5), Warnings (4), Error (0)
 */

#include "manipulator2R_Control_intel.h"
#include "manipulator2R_Control_intel_private.h"

/* Named constants for Stateflow: '<Root>/supervisory controller: safety module' */
#define manipulator2R_Control_in_IN_off (1U)
#define manipulator2R_Control_int_IN_on (2U)

/*
 * This function updates continuous states using the ODE5 fixed-step
 * solver algorithm
 */
void manipulator2R_Control_intelModelClass::rt_ertODEUpdateContinuousStates
  (RTWSolverInfo *si )
{
  /* Solver Matrices */
  static const real_T rt_ODE5_A[6] = {
    1.0/5.0, 3.0/10.0, 4.0/5.0, 8.0/9.0, 1.0, 1.0
  };

  static const real_T rt_ODE5_B[6][6] = {
    { 1.0/5.0, 0.0, 0.0, 0.0, 0.0, 0.0 },

    { 3.0/40.0, 9.0/40.0, 0.0, 0.0, 0.0, 0.0 },

    { 44.0/45.0, -56.0/15.0, 32.0/9.0, 0.0, 0.0, 0.0 },

    { 19372.0/6561.0, -25360.0/2187.0, 64448.0/6561.0, -212.0/729.0, 0.0, 0.0 },

    { 9017.0/3168.0, -355.0/33.0, 46732.0/5247.0, 49.0/176.0, -5103.0/18656.0,
      0.0 },

    { 35.0/384.0, 0.0, 500.0/1113.0, 125.0/192.0, -2187.0/6784.0, 11.0/84.0 }
  };

  time_T t = rtsiGetT(si);
  time_T tnew = rtsiGetSolverStopTime(si);
  time_T h = rtsiGetStepSize(si);
  real_T *x = rtsiGetContStates(si);
  ODE5_IntgData *id = (ODE5_IntgData *)rtsiGetSolverData(si);
  real_T *y = id->y;
  real_T *f0 = id->f[0];
  real_T *f1 = id->f[1];
  real_T *f2 = id->f[2];
  real_T *f3 = id->f[3];
  real_T *f4 = id->f[4];
  real_T *f5 = id->f[5];
  real_T hB[6];
  int_T i;
  int_T nXc = 2;
  rtsiSetSimTimeStep(si,MINOR_TIME_STEP);

  /* Save the state values at time t in y, we'll use x as ynew. */
  (void) memcpy(y, x,
                nXc*sizeof(real_T));

  /* Assumes that rtsiSetT and ModelOutputs are up-to-date */
  /* f0 = f(t,y) */
  rtsiSetdX(si, f0);
  manipulator2R_Control_intel_derivatives();

  /* f(:,2) = feval(odefile, t + hA(1), y + f*hB(:,1), args(:)(*)); */
  hB[0] = h * rt_ODE5_B[0][0];
  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0]);
  }

  rtsiSetT(si, t + h*rt_ODE5_A[0]);
  rtsiSetdX(si, f1);
  this->step();
  manipulator2R_Control_intel_derivatives();

  /* f(:,3) = feval(odefile, t + hA(2), y + f*hB(:,2), args(:)(*)); */
  for (i = 0; i <= 1; i++) {
    hB[i] = h * rt_ODE5_B[1][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1]);
  }

  rtsiSetT(si, t + h*rt_ODE5_A[1]);
  rtsiSetdX(si, f2);
  this->step();
  manipulator2R_Control_intel_derivatives();

  /* f(:,4) = feval(odefile, t + hA(3), y + f*hB(:,3), args(:)(*)); */
  for (i = 0; i <= 2; i++) {
    hB[i] = h * rt_ODE5_B[2][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1] + f2[i]*hB[2]);
  }

  rtsiSetT(si, t + h*rt_ODE5_A[2]);
  rtsiSetdX(si, f3);
  this->step();
  manipulator2R_Control_intel_derivatives();

  /* f(:,5) = feval(odefile, t + hA(4), y + f*hB(:,4), args(:)(*)); */
  for (i = 0; i <= 3; i++) {
    hB[i] = h * rt_ODE5_B[3][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1] + f2[i]*hB[2] +
                   f3[i]*hB[3]);
  }

  rtsiSetT(si, t + h*rt_ODE5_A[3]);
  rtsiSetdX(si, f4);
  this->step();
  manipulator2R_Control_intel_derivatives();

  /* f(:,6) = feval(odefile, t + hA(5), y + f*hB(:,5), args(:)(*)); */
  for (i = 0; i <= 4; i++) {
    hB[i] = h * rt_ODE5_B[4][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1] + f2[i]*hB[2] +
                   f3[i]*hB[3] + f4[i]*hB[4]);
  }

  rtsiSetT(si, tnew);
  rtsiSetdX(si, f5);
  this->step();
  manipulator2R_Control_intel_derivatives();

  /* tnew = t + hA(6);
     ynew = y + f*hB(:,6); */
  for (i = 0; i <= 5; i++) {
    hB[i] = h * rt_ODE5_B[5][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1] + f2[i]*hB[2] +
                   f3[i]*hB[3] + f4[i]*hB[4] + f5[i]*hB[5]);
  }

  rtsiSetSimTimeStep(si,MAJOR_TIME_STEP);
}

/* Function for Embedded MATLAB: '<S4>/Inv J' */
real_T manipulator2R_Control_intelModelClass::manipulator2R_Control_intel_det(
  const real_T x[4])
{
  real_T y;
  boolean_T isodd;
  int32_T jpiv_offset;
  real_T b_x_idx;
  real_T b_x_idx_0;
  real_T b_x_idx_1;
  real_T b_x_idx_2;
  int8_T ipiv_idx;
  b_x_idx_0 = x[0];
  b_x_idx = x[1];
  b_x_idx_1 = x[2];
  b_x_idx_2 = x[3];
  ipiv_idx = 1;
  jpiv_offset = 1;
  if (fabs(x[1]) > fabs(x[0])) {
    jpiv_offset = 2;
  }

  jpiv_offset--;
  if (x[jpiv_offset] != 0.0) {
    if (jpiv_offset != 0) {
      ipiv_idx = 2;
      b_x_idx_0 = x[1];
      b_x_idx = x[0];
      b_x_idx_1 = x[3];
      b_x_idx_2 = x[2];
    }

    b_x_idx /= b_x_idx_0;
  }

  if (b_x_idx_1 != 0.0) {
    b_x_idx_2 += (-b_x_idx_1) * b_x_idx;
  }

  y = b_x_idx_0 * b_x_idx_2;
  isodd = FALSE;
  if (ipiv_idx > 1) {
    isodd = TRUE;
  }

  if (isodd) {
    return -y;
  }

  return y;
}

/* Function for Embedded MATLAB: '<S4>/Inv J' */
void manipulator2R_Control_intelModelClass::manipulator2R_Control_in_mpower(
  const real_T a[4], real_T c[4])
{
  real_T d;
  c[0] = a[0];
  c[1] = a[1];
  c[2] = a[2];
  c[3] = a[3];
  d = a[0] * a[3] - a[1] * a[2];
  c[0] = a[3] / d;
  c[3] = a[0] / d;
  c[1] = (-c[1]) / d;
  c[2] = (-c[2]) / d;
}

/* Model step function */
void manipulator2R_Control_intelModelClass::step()
{
  /* local block i/o variables */
  real_T rtb_Clock;
  real_T rtb_Switch[2];
  real_T rtb_Derivative[2];
  real_T rtb_Integrator[2];
  real_T rtb_Clock_l;
  real_T rtb_Derivative1[2];
  real_T I_v;
  real_T DT;
  real_T J[4];
  real_T rtb_Ifv_l;
  real_T rtb_Ifc_b;
  real_T rtb_Add1;
  real_T tmp[4];
  real_T J_0[4];
  real_T rtb_TmpSignalConversionAtSFun_0;
  real_T rtb_x_idx;
  real_T rtb_x_idx_0;
  real_T rtb_TmpSignalConversionAtSFun_1;
  real_T rtb_q_idx;
  if (rtmIsMajorTimeStep(manipulator2R_Control_intel_M)) {
    /* set solver stop time */
    rtsiSetSolverStopTime(&manipulator2R_Control_intel_M->solverInfo,
                          ((manipulator2R_Control_intel_M->Timing.clockTick0+1)*
      manipulator2R_Control_intel_M->Timing.stepSize0));
  }                                    /* end MajorTimeStep */

  /* Update absolute time of base rate at minor time step */
  if (rtmIsMinorTimeStep(manipulator2R_Control_intel_M)) {
    manipulator2R_Control_intel_M->Timing.t[0] = rtsiGetT
      (&manipulator2R_Control_intel_M->solverInfo);
  }

  /* Embedded MATLAB: '<S2>/kompensacja tarcia  wiskotycznego, silnik #1' incorporates:
   *  Inport: '<Root>/q1v'
   */
  /* Embedded MATLAB Function 'friction compensation/kompensacja tarcia  wiskotycznego, silnik #1': '<S12>:1' */
  /*  I_v = k_M * T_v */
  if (manipulator2R_Control_intel_U.q1v > 0.0) {
    /* '<S12>:1:4' */
    /* '<S12>:1:5' */
    I_v = 1.12;
  } else {
    /* '<S12>:1:7' */
    I_v = 1.0;
  }

  /* '<S12>:1:10' */
  rtb_Ifv_l = I_v * manipulator2R_Control_intel_U.q1v;

  /* Embedded MATLAB: '<S2>/kompensacja tarcia  Coulombowskiego, silnik #1' incorporates:
   *  Inport: '<Root>/q1v'
   */
  /* Embedded MATLAB Function 'friction compensation/kompensacja tarcia  Coulombowskiego, silnik #1': '<S10>:1' */
  /*  I_C = k_M * T_C */
  /* '<S10>:1:10' */
  rtb_Ifc_b = tanh(35.0 * manipulator2R_Control_intel_U.q1v) * 1.65;

  /* Clock: '<S5>/Clock' */
  rtb_Clock = manipulator2R_Control_intel_M->Timing.t[0];

  /* Embedded MATLAB: '<S5>/ref_traj' */
  /* Embedded MATLAB Function 'trajectory generator in the task space: a composed chain/ref_traj': '<S17>:1' */
  /* '<S17>:1:3' */
  /* '<S17>:1:4' */
  I_v = rtb_Clock - floor(rtb_Clock / 165.0) * 165.0;

  /* '<S17>:1:5' */
  /* '<S17>:1:6' */
  DT = (rtb_Clock - I_v) / 165.0 * 165.0;

  /* '<S17>:1:8' */
  /* '<S17>:1:9' */
  /* '<S17>:1:10' */
  /* '<S17>:1:11' */
  /* '<S17>:1:12' */
  /* '<S17>:1:13' */
  /* '<S17>:1:15' */
  /* '<S17>:1:16' */
  /* '<S17>:1:17' */
  /* '<S17>:1:18' */
  /* '<S17>:1:20' */
  rtb_x_idx = 0.0;
  rtb_x_idx_0 = 0.0;
  if ((I_v >= 5.0) && (I_v < 25.0)) {
    /* '<S17>:1:24' */
    /* '<S17>:1:25' */
    rtb_x_idx = 0.0;
    rtb_x_idx_0 = ((rtb_Clock - DT) - 5.0) * 0.02;
  }

  if ((I_v >= 25.0) && (I_v < 45.0)) {
    /* '<S17>:1:27' */
    /* '<S17>:1:28' */
    rtb_x_idx = ((rtb_Clock - DT) - 25.0) * -0.03;
    rtb_x_idx_0 = 0.4;
  }

  if ((I_v >= 45.0) && (I_v < 85.0)) {
    /* '<S17>:1:30' */
    /* '<S17>:1:31' */
    rtb_x_idx = sin(((rtb_Clock - DT) - 45.0) * 0.075) * -0.6 + -0.6;
    rtb_x_idx_0 = cos(((rtb_Clock - DT) - 45.0) * 0.075) * 0.4;
  }

  if ((I_v >= 85.0) && (I_v < 125.0)) {
    /* '<S17>:1:33' */
    /* '<S17>:1:34' */
    rtb_x_idx = sin((125.0 - (rtb_Clock - DT)) * 0.075) * -0.6 + -0.6;
    rtb_x_idx_0 = cos((125.0 - (rtb_Clock - DT)) * 0.075) * 0.4;
  }

  if ((I_v >= 125.0) && (I_v < 145.0)) {
    /* '<S17>:1:36' */
    /* '<S17>:1:37' */
    rtb_x_idx = (145.0 - (rtb_Clock - DT)) * -0.03;
    rtb_x_idx_0 = 0.4;
  }

  if ((I_v >= 145.0) && (I_v < 165.0)) {
    /* '<S17>:1:39' */
    /* '<S17>:1:40' */
    rtb_x_idx = 0.0;
    rtb_x_idx_0 = (165.0 - (rtb_Clock - DT)) * 0.02;
  }

  /* Embedded MATLAB: '<S5>/kinematics' incorporates:
   *  SignalConversion: '<S16>/TmpSignal ConversionAt SFunction Inport1'
   */
  /* Embedded MATLAB Function 'trajectory generator in the task space: a composed chain/kinematics': '<S16>:1' */
  /* '<S16>:1:4' */
  /* '<S16>:1:15' */
  rtb_Switch[0] = 0.0;
  rtb_Switch[1] = 0.0;

  /* '<S16>:1:16' */
  rtb_Switch[0] = (0.4175 * cos(manipulator2R_Control_in_ConstB.q1t0) * cos
                   (manipulator2R_Control_in_ConstB.q2t0) + 0.56 * cos
                   (manipulator2R_Control_in_ConstB.q1t0)) - 0.4175 * sin
    (manipulator2R_Control_in_ConstB.q1t0) * sin
    (manipulator2R_Control_in_ConstB.q2t0);

  /* '<S16>:1:17' */
  rtb_Switch[1] = (0.4175 * cos(manipulator2R_Control_in_ConstB.q2t0) * sin
                   (manipulator2R_Control_in_ConstB.q1t0) + 0.56 * sin
                   (manipulator2R_Control_in_ConstB.q1t0)) + 0.4175 * cos
    (manipulator2R_Control_in_ConstB.q1t0) * sin
    (manipulator2R_Control_in_ConstB.q2t0);

  /* Switch: '<S5>/Switch' incorporates:
   *  Sum: '<S5>/Add'
   */
  if (rtb_Clock >= 5.0) {
    rtb_Switch[0] = rtb_x_idx + rtb_Switch[0];
    rtb_Switch[1] = rtb_x_idx_0 + rtb_Switch[1];
  }

  /* Derivative: '<S4>/Derivative' */
  {
    real_T t = manipulator2R_Control_intel_M->Timing.t[0];
    real_T timeStampA =
      manipulator2R_Control_int_DWork.Derivative_RWORK.TimeStampA;
    real_T timeStampB =
      manipulator2R_Control_int_DWork.Derivative_RWORK.TimeStampB;
    real_T *lastU =
      &manipulator2R_Control_int_DWork.Derivative_RWORK.LastUAtTimeA[0];
    if (timeStampA >= t && timeStampB >= t) {
      rtb_Derivative[0] = 0.0;
      rtb_Derivative[1] = 0.0;
    } else {
      real_T deltaT;
      real_T lastTime = timeStampA;
      if (timeStampA < timeStampB) {
        if (timeStampB < t) {
          lastTime = timeStampB;
          lastU =
            &manipulator2R_Control_int_DWork.Derivative_RWORK.LastUAtTimeB[0];
        }
      } else if (timeStampA >= t) {
        lastTime = timeStampB;
        lastU = &manipulator2R_Control_int_DWork.Derivative_RWORK.LastUAtTimeB[0];
      }

      deltaT = t - lastTime;
      rtb_Derivative[0] = (rtb_Switch[0] - *lastU++) / deltaT;
      rtb_Derivative[1] = (rtb_Switch[1] - *lastU++) / deltaT;
    }
  }

  /* Integrator: '<S4>/Integrator' */
  if (manipulator2R_Control_int_DWork.Integrator_IWORK.IcNeedsLoading) {
    manipulator2R_Control_intel_X.Integrator_CSTATE[0] =
      manipulator2R_Control_in_ConstB.q1t0;
    manipulator2R_Control_intel_X.Integrator_CSTATE[1] =
      manipulator2R_Control_in_ConstB.q2t0;
    manipulator2R_Control_int_DWork.Integrator_IWORK.IcNeedsLoading = 0;
  }

  rtb_Integrator[0] = manipulator2R_Control_intel_X.Integrator_CSTATE[0];
  rtb_Integrator[1] = manipulator2R_Control_intel_X.Integrator_CSTATE[1];

  /* Embedded MATLAB: '<S4>/Inv J' */
  /* Embedded MATLAB Function 'trajectory generator in the joints space/Inv J': '<S14>:1' */
  /* '<S14>:1:5' */
  J[0] = 0.0;
  J[1] = 0.0;
  J[2] = 0.0;
  J[3] = 0.0;

  /* '<S14>:1:7' */
  I_v = (0.4175 * cos(rtb_Integrator[0]) * cos(rtb_Integrator[1]) + 0.56 * cos
         (rtb_Integrator[0])) - 0.4175 * sin(rtb_Integrator[0]) * sin
    (rtb_Integrator[1]);
  DT = (0.4175 * cos(rtb_Integrator[1]) * sin(rtb_Integrator[0]) + 0.56 * sin
        (rtb_Integrator[0])) + 0.4175 * cos(rtb_Integrator[0]) * sin
    (rtb_Integrator[1]);

  /* '<S14>:1:18' */
  J[0] = -((0.4175 * cos(rtb_Integrator[1]) * sin(rtb_Integrator[0]) + 0.56 *
            sin(rtb_Integrator[0])) + 0.4175 * cos(rtb_Integrator[0]) * sin
           (rtb_Integrator[1]));

  /* '<S14>:1:20' */
  J[2] = -(0.4175 * cos(rtb_Integrator[1]) * sin(rtb_Integrator[0]) + 0.4175 *
           cos(rtb_Integrator[0]) * sin(rtb_Integrator[1]));

  /* '<S14>:1:22' */
  J[1] = (0.4175 * cos(rtb_Integrator[0]) * cos(rtb_Integrator[1]) + 0.56 * cos
          (rtb_Integrator[0])) - 0.4175 * sin(rtb_Integrator[0]) * sin
    (rtb_Integrator[1]);

  /* '<S14>:1:24' */
  J[3] = 0.4175 * cos(rtb_Integrator[0]) * cos(rtb_Integrator[1]) - 0.4175 * sin
    (rtb_Integrator[0]) * sin(rtb_Integrator[1]);
  if (fabs(manipulator2R_Control_intel_det(J)) < 0.0001) {
    /* '<S14>:1:27' */
    /* '<S14>:1:28' */
    J_0[0] = J[0] + 0.0001;
    J_0[1] = J[1];
    J_0[2] = J[2];
    J_0[3] = J[3] + 0.0001;
    manipulator2R_Control_in_mpower(J_0, tmp);
    rtb_q_idx = rtb_Derivative[0] - (I_v - rtb_Switch[0]) * 25.0;
    I_v = rtb_Derivative[1] - (DT - rtb_Switch[1]) * 25.0;
    manipulator2R_Control_int_DWork.qp[0] = 0.0;
    manipulator2R_Control_int_DWork.qp[0] = tmp[0] * rtb_q_idx +
      manipulator2R_Control_int_DWork.qp[0];
    manipulator2R_Control_int_DWork.qp[0] = tmp[2] * I_v +
      manipulator2R_Control_int_DWork.qp[0];
    manipulator2R_Control_int_DWork.qp[1] = 0.0;
    manipulator2R_Control_int_DWork.qp[1] = tmp[1] * rtb_q_idx +
      manipulator2R_Control_int_DWork.qp[1];
    manipulator2R_Control_int_DWork.qp[1] = tmp[3] * I_v +
      manipulator2R_Control_int_DWork.qp[1];
  } else {
    /* '<S14>:1:31' */
    manipulator2R_Control_in_mpower(J, tmp);
    rtb_q_idx = rtb_Derivative[0] - (I_v - rtb_Switch[0]) * 25.0;
    I_v = rtb_Derivative[1] - (DT - rtb_Switch[1]) * 25.0;
    manipulator2R_Control_int_DWork.qp[0] = 0.0;
    manipulator2R_Control_int_DWork.qp[0] = tmp[0] * rtb_q_idx +
      manipulator2R_Control_int_DWork.qp[0];
    manipulator2R_Control_int_DWork.qp[0] = tmp[2] * I_v +
      manipulator2R_Control_int_DWork.qp[0];
    manipulator2R_Control_int_DWork.qp[1] = 0.0;
    manipulator2R_Control_int_DWork.qp[1] = tmp[1] * rtb_q_idx +
      manipulator2R_Control_int_DWork.qp[1];
    manipulator2R_Control_int_DWork.qp[1] = tmp[3] * I_v +
      manipulator2R_Control_int_DWork.qp[1];
  }

  if (fabs(manipulator2R_Control_intel_det(J)) < 0.01) {
    /* '<S14>:1:35' */
    /* '<S14>:1:36' */
    manipulator2R_Control_int_DWork.singularity = 1.0;
  } else {
    /* '<S14>:1:38' */
    manipulator2R_Control_int_DWork.singularity = 0.0;
  }

  /* Clock: '<S4>/Clock' */
  rtb_Clock_l = manipulator2R_Control_intel_M->Timing.t[0];

  /* SignalConversion: '<S15>/TmpSignal ConversionAt SFunction Inport3' incorporates:
   *  Constant: '<S4>/ddqc1'
   *  Constant: '<S4>/ddqc2'
   */
  rtb_TmpSignalConversionAtSFun_1 = 0.1;
  rtb_x_idx_0 = 0.1;

  /* Embedded MATLAB: '<S4>/init_traj' incorporates:
   *  Constant: '<S4>/tf'
   *  Constant: '<S4>/ti'
   *  SignalConversion: '<S15>/TmpSignal ConversionAt SFunction Inport2'
   */
  /* Embedded MATLAB Function 'trajectory generator in the joints space/init_traj': '<S15>:1' */
  /* '<S15>:1:2' */
  /* '<S15>:1:3' */
  /* '<S15>:1:4' */
  rtb_Derivative1[0] = 0.0;
  rtb_Derivative1[1] = 0.0;
  if (0.1 < fabs(manipulator2R_Control_in_ConstB.q1t0 - -1.5707963267948966) *
      4.0 / 25.0) {
    /* '<S15>:1:5' */
    /* '<S15>:1:6' */
    I_v = manipulator2R_Control_in_ConstB.q1t0 - -1.5707963267948966;
    if (rtIsNaN(I_v)) {
      I_v = (rtNaN);
    } else if (I_v > 0.0) {
      I_v = 1.0;
    } else if (I_v < 0.0) {
      I_v = -1.0;
    } else {
      I_v = 0.0;
    }

    rtb_TmpSignalConversionAtSFun_1 = I_v * 4.0 * fabs
      (manipulator2R_Control_in_ConstB.q1t0 - -1.5707963267948966) / 25.0;
  }

  if (0.1 < 4.0 * fabs(manipulator2R_Control_in_ConstB.q2t0) / 25.0) {
    /* '<S15>:1:8' */
    /* '<S15>:1:9' */
    if (rtIsNaN(manipulator2R_Control_in_ConstB.q2t0)) {
      I_v = (rtNaN);
    } else if (manipulator2R_Control_in_ConstB.q2t0 > 0.0) {
      I_v = 1.0;
    } else if (manipulator2R_Control_in_ConstB.q2t0 < 0.0) {
      I_v = -1.0;
    } else {
      I_v = 0.0;
    }

    rtb_x_idx_0 = I_v * 4.0 * fabs(manipulator2R_Control_in_ConstB.q2t0) / 25.0;
  }

  /* '<S15>:1:12' */
  DT = 2.5 - sqrt((25.0 * rtb_x_idx_0 - 4.0 *
                   manipulator2R_Control_in_ConstB.q2t0) / rtb_x_idx_0) * 0.5;
  if ((0.0 <= rtb_Clock_l) && (rtb_Clock_l <= 2.5 - sqrt((25.0 *
         rtb_TmpSignalConversionAtSFun_1 - (manipulator2R_Control_in_ConstB.q1t0
          - -1.5707963267948966) * 4.0) / rtb_TmpSignalConversionAtSFun_1) * 0.5))
  {
    /* '<S15>:1:14' */
    /* '<S15>:1:15' */
    rtb_q_idx = 0.5 * rtb_TmpSignalConversionAtSFun_1 * rt_pow_snf(rtb_Clock_l,
      2.0) + -1.5707963267948966;

    /* '<S15>:1:16' */
    rtb_Derivative1[0] = rtb_TmpSignalConversionAtSFun_1 * rtb_Clock_l;

    /* '<S15>:1:17' */
  } else if ((2.5 - sqrt((25.0 * rtb_TmpSignalConversionAtSFun_1 -
                          (manipulator2R_Control_in_ConstB.q1t0 -
      -1.5707963267948966) * 4.0) / rtb_TmpSignalConversionAtSFun_1) * 0.5 <
              rtb_Clock_l) && (rtb_Clock_l <= 5.0 - (2.5 - sqrt((25.0 *
      rtb_TmpSignalConversionAtSFun_1 - (manipulator2R_Control_in_ConstB.q1t0 -
      -1.5707963267948966) * 4.0) / rtb_TmpSignalConversionAtSFun_1) * 0.5))) {
    /* '<S15>:1:18' */
    /* '<S15>:1:19' */
    rtb_q_idx = (rtb_Clock_l - (2.5 - sqrt((25.0 *
      rtb_TmpSignalConversionAtSFun_1 - (manipulator2R_Control_in_ConstB.q1t0 -
      -1.5707963267948966) * 4.0) / rtb_TmpSignalConversionAtSFun_1) * 0.5) *
                 0.5) * ((2.5 - sqrt((25.0 * rtb_TmpSignalConversionAtSFun_1 -
      (manipulator2R_Control_in_ConstB.q1t0 - -1.5707963267948966) * 4.0) /
      rtb_TmpSignalConversionAtSFun_1) * 0.5) * rtb_TmpSignalConversionAtSFun_1)
      + -1.5707963267948966;

    /* '<S15>:1:20' */
    rtb_Derivative1[0] = (2.5 - sqrt((25.0 * rtb_TmpSignalConversionAtSFun_1 -
      (manipulator2R_Control_in_ConstB.q1t0 - -1.5707963267948966) * 4.0) /
      rtb_TmpSignalConversionAtSFun_1) * 0.5) * rtb_TmpSignalConversionAtSFun_1;

    /* '<S15>:1:21' */
  } else if ((5.0 - (2.5 - sqrt((25.0 * rtb_TmpSignalConversionAtSFun_1 -
      (manipulator2R_Control_in_ConstB.q1t0 - -1.5707963267948966) * 4.0) /
                rtb_TmpSignalConversionAtSFun_1) * 0.5) < rtb_Clock_l) &&
             (rtb_Clock_l <= 5.0)) {
    /* '<S15>:1:22' */
    /* '<S15>:1:23' */
    rtb_q_idx = manipulator2R_Control_in_ConstB.q1t0 - 0.5 *
      rtb_TmpSignalConversionAtSFun_1 * rt_pow_snf(5.0 - rtb_Clock_l, 2.0);

    /* '<S15>:1:24' */
    rtb_Derivative1[0] = (2.5 - sqrt((25.0 * rtb_TmpSignalConversionAtSFun_1 -
      (manipulator2R_Control_in_ConstB.q1t0 - -1.5707963267948966) * 4.0) /
      rtb_TmpSignalConversionAtSFun_1) * 0.5) * rtb_TmpSignalConversionAtSFun_1
      - rtb_TmpSignalConversionAtSFun_1 * rtb_Clock_l;

    /* '<S15>:1:25' */
  } else {
    /* '<S15>:1:27' */
    rtb_q_idx = manipulator2R_Control_in_ConstB.q1t0;

    /* '<S15>:1:28' */
    rtb_Derivative1[0] = 0.0;

    /* '<S15>:1:29' */
    rtb_TmpSignalConversionAtSFun_1 = 0.0;
  }

  if ((0.0 <= rtb_Clock_l) && (rtb_Clock_l <= DT)) {
    /* '<S15>:1:32' */
    /* '<S15>:1:33' */
    I_v = 0.5 * rtb_x_idx_0 * rt_pow_snf(rtb_Clock_l, 2.0);

    /* '<S15>:1:34' */
    rtb_Derivative1[1] = rtb_x_idx_0 * rtb_Clock_l;

    /* '<S15>:1:35' */
  } else if ((DT < rtb_Clock_l) && (rtb_Clock_l <= 5.0 - DT)) {
    /* '<S15>:1:36' */
    /* '<S15>:1:37' */
    I_v = (rtb_Clock_l - 0.5 * DT) * (rtb_x_idx_0 * DT);

    /* '<S15>:1:38' */
    rtb_Derivative1[1] = rtb_x_idx_0 * DT;

    /* '<S15>:1:39' */
  } else if ((5.0 - DT < rtb_Clock_l) && (rtb_Clock_l <= 5.0)) {
    /* '<S15>:1:40' */
    /* '<S15>:1:41' */
    I_v = manipulator2R_Control_in_ConstB.q2t0 - 0.5 * rtb_x_idx_0 * rt_pow_snf
      (5.0 - rtb_Clock_l, 2.0);

    /* '<S15>:1:42' */
    rtb_Derivative1[1] = rtb_x_idx_0 * DT - rtb_x_idx_0 * rtb_Clock_l;

    /* '<S15>:1:43' */
  } else {
    /* '<S15>:1:45' */
    I_v = manipulator2R_Control_in_ConstB.q2t0;

    /* '<S15>:1:46' */
    rtb_Derivative1[1] = 0.0;

    /* '<S15>:1:47' */
    rtb_x_idx_0 = 0.0;
  }

  /* Switch: '<S4>/Switch1' */
  if (rtb_Clock_l > 5.0) {
    rtb_Derivative1[0] = manipulator2R_Control_int_DWork.qp[0];
    rtb_Derivative1[1] = manipulator2R_Control_int_DWork.qp[1];
  }

  /* Sum: '<S1>/Add1' incorporates:
   *  Inport: '<Root>/q1v'
   */
  rtb_Add1 = manipulator2R_Control_intel_U.q1v - rtb_Derivative1[0];

  /* Sum: '<S1>/Add2' incorporates:
   *  Inport: '<Root>/q2v'
   */
  rtb_x_idx = manipulator2R_Control_intel_U.q2v - rtb_Derivative1[1];

  /* Switch: '<S4>/Switch' */
  if (rtb_Clock_l > 5.0) {
    rtb_Derivative1[0] = rtb_Integrator[0];
    rtb_Derivative1[1] = rtb_Integrator[1];
  } else {
    rtb_Derivative1[0] = rtb_q_idx;
    rtb_Derivative1[1] = I_v;
  }

  /* Sum: '<S1>/Add3' incorporates:
   *  Inport: '<Root>/q1p'
   */
  DT = manipulator2R_Control_intel_U.q1p - rtb_Derivative1[0];

  /* Sum: '<S1>/Add6' incorporates:
   *  Inport: '<Root>/q2p'
   */
  I_v = manipulator2R_Control_intel_U.q2p - rtb_Derivative1[1];

  /* Derivative: '<S4>/Derivative1' */
  {
    real_T t = manipulator2R_Control_intel_M->Timing.t[0];
    real_T timeStampA =
      manipulator2R_Control_int_DWork.Derivative1_RWORK.TimeStampA;
    real_T timeStampB =
      manipulator2R_Control_int_DWork.Derivative1_RWORK.TimeStampB;
    real_T *lastU =
      &manipulator2R_Control_int_DWork.Derivative1_RWORK.LastUAtTimeA[0];
    if (timeStampA >= t && timeStampB >= t) {
      rtb_Derivative1[0] = 0.0;
      rtb_Derivative1[1] = 0.0;
    } else {
      real_T deltaT;
      real_T lastTime = timeStampA;
      if (timeStampA < timeStampB) {
        if (timeStampB < t) {
          lastTime = timeStampB;
          lastU =
            &manipulator2R_Control_int_DWork.Derivative1_RWORK.LastUAtTimeB[0];
        }
      } else if (timeStampA >= t) {
        lastTime = timeStampB;
        lastU = &manipulator2R_Control_int_DWork.Derivative1_RWORK.LastUAtTimeB
          [0];
      }

      deltaT = t - lastTime;
      rtb_Derivative1[0] = (manipulator2R_Control_int_DWork.qp[0] - *lastU++) /
        deltaT;
      rtb_Derivative1[1] = (manipulator2R_Control_int_DWork.qp[1] - *lastU++) /
        deltaT;
    }
  }

  /* Switch: '<S4>/Switch2' */
  if (rtb_Clock_l > 5.0) {
    rtb_TmpSignalConversionAtSFun_1 = rtb_Derivative1[0];
    rtb_x_idx_0 = rtb_Derivative1[1];
  }

  /* Embedded MATLAB: '<S1>/qaref-Kp*ep-Kd*ep' incorporates:
   *  SignalConversion: '<S9>/TmpSignal ConversionAt SFunction Inport1'
   *  SignalConversion: '<S9>/TmpSignal ConversionAt SFunction Inport2'
   */
  /* Embedded MATLAB Function 'computed torque control/qaref-Kp*ep-Kd*ep': '<S9>:1' */
  /* Kp=[7500,0;0,10000]; */
  /* Kd=[750,0;0,750]; */
  /* '<S9>:1:10' */

  /* Embedded MATLAB: '<S1>/Q(q)*v' incorporates:
   *  Inport: '<Root>/q2p'
   */
  /* Embedded MATLAB Function 'computed torque control/Q(q)*v': '<S8>:1' */
  /* '<S8>:1:3' */
  rtb_TmpSignalConversionAtSFun_0 = (rtb_TmpSignalConversionAtSFun_1 - (150.0 *
    rtb_Add1 + 0.0 * rtb_x_idx)) - (2000.0 * DT + 0.0 * I_v);
  DT = (rtb_x_idx_0 - (0.0 * rtb_Add1 + 150.0 * rtb_x_idx)) - (0.0 * DT + 2000.0
    * I_v);

  /* '<S8>:1:5' */
  rtb_x_idx = ((0.195326 * cos(manipulator2R_Control_intel_U.q2p) *
                rtb_TmpSignalConversionAtSFun_0 + 5.28984 *
                rtb_TmpSignalConversionAtSFun_0) + 0.0523754 * DT) + 0.097663 *
    cos(manipulator2R_Control_intel_U.q2p) * DT;

  /* '<S8>:1:6' */
  rtb_x_idx_0 = (0.097663 * cos(manipulator2R_Control_intel_U.q2p) *
                 rtb_TmpSignalConversionAtSFun_0 + 0.0523754 *
                 rtb_TmpSignalConversionAtSFun_0) + 0.2300004 * DT;

  /* '<S8>:1:10' */

  /* Embedded MATLAB: '<S1>/C(q)*qv' incorporates:
   *  Inport: '<Root>/q1v'
   *  Inport: '<Root>/q2p'
   *  Inport: '<Root>/q2v'
   */
  /* Embedded MATLAB Function 'computed torque control/C(q)*qv': '<S6>:1' */
  /* '<S6>:1:4' */
  /* '<S6>:1:6' */
  rtb_TmpSignalConversionAtSFun_0 = -0.195326 * sin
    (manipulator2R_Control_intel_U.q2p) * manipulator2R_Control_intel_U.q1v *
    manipulator2R_Control_intel_U.q2v - 0.097663 * sin
    (manipulator2R_Control_intel_U.q2p) * manipulator2R_Control_intel_U.q2v *
    manipulator2R_Control_intel_U.q2v;

  /* '<S6>:1:7' */
  DT = 0.097663 * sin(manipulator2R_Control_intel_U.q2p) *
    manipulator2R_Control_intel_U.q1v * manipulator2R_Control_intel_U.q1v;

  /* '<S6>:1:12' */
  rtb_TmpSignalConversionAtSFun_1 = 0.060277275467148887 *
    rtb_TmpSignalConversionAtSFun_0 + -0.0 * DT;
  rtb_Add1 = -0.0 * rtb_TmpSignalConversionAtSFun_0 + 0.272108843537415 * DT;

  /* Embedded MATLAB: '<S1>/D(q)' incorporates:
   *  Inport: '<Root>/q1p'
   *  Inport: '<Root>/q2p'
   */
  /* Embedded MATLAB Function 'computed torque control/D(q)': '<S7>:1' */
  /* '<S7>:1:3' */
  /* '<S7>:1:5' */
  rtb_TmpSignalConversionAtSFun_0 = (1.71085 * cos
    (manipulator2R_Control_intel_U.q1p) * cos(manipulator2R_Control_intel_U.q2p)
    + 29.2149 * cos(manipulator2R_Control_intel_U.q1p)) - 1.71085 * sin
    (manipulator2R_Control_intel_U.q1p) * sin(manipulator2R_Control_intel_U.q2p);

  /* '<S7>:1:6' */
  DT = 1.71085 * cos(manipulator2R_Control_intel_U.q1p) * cos
    (manipulator2R_Control_intel_U.q2p) - 1.71085 * sin
    (manipulator2R_Control_intel_U.q1p) * sin(manipulator2R_Control_intel_U.q2p);

  /* '<S7>:1:11' */
  if (rtmIsMajorTimeStep(manipulator2R_Control_intel_M)) {
    /* Stateflow: '<Root>/supervisory controller: safety module' incorporates:
     *  Inport: '<Root>/relinfo'
     */
    /* Gateway: supervisory controller:
       safety module */
    /* During: supervisory controller:
       safety module */
    if (manipulator2R_Control_int_DWork.is_active_c10_manipulator2R_Con == 0) {
      /* Entry: supervisory controller:
         safety module */
      manipulator2R_Control_int_DWork.is_active_c10_manipulator2R_Con = 1U;

      /* Transition: '<S3>:4' */
      /* Entry 'on': '<S3>:2' */
      manipulator2R_Control_int_DWork.is_c10_manipulator2R_Control_in =
        manipulator2R_Control_int_IN_on;
      manipulator2R_Control_int_DWork.disableIref = 1.0;
    } else {
      switch (manipulator2R_Control_int_DWork.is_c10_manipulator2R_Control_in) {
       case manipulator2R_Control_in_IN_off:
        break;

       case manipulator2R_Control_int_IN_on:
        /* During 'on': '<S3>:2' */
        if (manipulator2R_Control_intel_U.relinfo == 0.0) {
          /* Transition: '<S3>:5' */
          /* Exit 'on': '<S3>:2' */
          /* Entry 'off': '<S3>:1' */
          manipulator2R_Control_int_DWork.is_c10_manipulator2R_Control_in =
            manipulator2R_Control_in_IN_off;
          manipulator2R_Control_int_DWork.disableIref = 0.0;
        } else {
          if (manipulator2R_Control_int_DWork.singularity == 1.0) {
            /* Transition: '<S3>:13' */
            /* Exit 'on': '<S3>:2' */
            /* Entry 'off': '<S3>:1' */
            manipulator2R_Control_int_DWork.is_c10_manipulator2R_Control_in =
              manipulator2R_Control_in_IN_off;
            manipulator2R_Control_int_DWork.disableIref = 0.0;
          }
        }
        break;

       default:
        /* Transition: '<S3>:4' */
        /* Entry 'on': '<S3>:2' */
        manipulator2R_Control_int_DWork.is_c10_manipulator2R_Control_in =
          manipulator2R_Control_int_IN_on;
        manipulator2R_Control_int_DWork.disableIref = 1.0;
        break;
      }
    }
  }

  /* Outport: '<Root>/Out1' incorporates:
   *  Gain: '<S1>/magicGain1'
   *  Product: '<S1>/Product'
   *  Saturate: '<Root>/Saturation1'
   *  Sum: '<S1>/Add4'
   *  Sum: '<S2>/Sum1'
   *  Sum: '<S2>/Sum3'
   */
  I_v = (((0.060277275467148887 * rtb_x_idx + -0.0 * rtb_x_idx_0) +
          rtb_TmpSignalConversionAtSFun_1) + (0.060277275467148887 *
          rtb_TmpSignalConversionAtSFun_0 + -0.0 * DT) * 1.5) *
    manipulator2R_Control_int_DWork.disableIref + (rtb_Ifv_l + rtb_Ifc_b);
  manipulator2R_Control_intel_Y.Out1 = I_v >= 12.0 ? 12.0 : I_v <= -12.0 ? -12.0
    : I_v;

  /* Embedded MATLAB: '<S2>/kompensacja tarcia  Coulombowskiego, silnik #2' incorporates:
   *  Inport: '<Root>/q2v'
   */
  /* Embedded MATLAB Function 'friction compensation/kompensacja tarcia  Coulombowskiego, silnik #2': '<S11>:1' */
  /*  I_C = k_M * T_C */
  if (manipulator2R_Control_intel_U.q2v > 0.0) {
    /* '<S11>:1:4' */
    /* '<S11>:1:5' */
    rtb_q_idx = 4.8;
  } else {
    /* '<S11>:1:7' */
    rtb_q_idx = 5.4;
  }

  /* '<S11>:1:10' */

  /* Embedded MATLAB: '<S2>/kompensacja tarcia  wiskotycznego, silnik #2' incorporates:
   *  Inport: '<Root>/q2v'
   */
  /* Embedded MATLAB Function 'friction compensation/kompensacja tarcia  wiskotycznego, silnik #2': '<S13>:1' */
  /*  I_v = k_M * T_v */
  if (manipulator2R_Control_intel_U.q2v > 0.0) {
    /* '<S13>:1:4' */
    /* '<S13>:1:5' */
    I_v = 0.27;
  } else {
    /* '<S13>:1:7' */
    I_v = 0.0;
  }

  /* '<S13>:1:10' */

  /* Outport: '<Root>/Out2' incorporates:
   *  Gain: '<S1>/magicGain2'
   *  Inport: '<Root>/q2v'
   *  Product: '<S1>/Product1'
   *  Saturate: '<Root>/Saturation'
   *  Sum: '<S1>/Add5'
   *  Sum: '<S2>/Sum2'
   *  Sum: '<S2>/Sum4'
   */
  I_v = (((-0.0 * rtb_TmpSignalConversionAtSFun_0 + 0.272108843537415 * DT) *
          1.5 + rtb_Add1) + (-0.0 * rtb_x_idx + 0.272108843537415 * rtb_x_idx_0))
    * manipulator2R_Control_int_DWork.disableIref + (tanh(35.0 *
    manipulator2R_Control_intel_U.q2v) * rtb_q_idx + I_v *
    manipulator2R_Control_intel_U.q2v);
  manipulator2R_Control_intel_Y.Out2 = I_v >= 12.0 ? 12.0 : I_v <= -12.0 ? -12.0
    : I_v;
  if (rtmIsMajorTimeStep(manipulator2R_Control_intel_M)) {
    /* Update for Derivative: '<S4>/Derivative' */
    {
      real_T timeStampA =
        manipulator2R_Control_int_DWork.Derivative_RWORK.TimeStampA;
      real_T timeStampB =
        manipulator2R_Control_int_DWork.Derivative_RWORK.TimeStampB;
      real_T* lastTime =
        &manipulator2R_Control_int_DWork.Derivative_RWORK.TimeStampA;
      real_T* lastU =
        &manipulator2R_Control_int_DWork.Derivative_RWORK.LastUAtTimeA[0];
      if (timeStampA != rtInf) {
        if (timeStampB == rtInf) {
          lastTime =
            &manipulator2R_Control_int_DWork.Derivative_RWORK.TimeStampB;
          lastU =
            &manipulator2R_Control_int_DWork.Derivative_RWORK.LastUAtTimeB[0];
        } else if (timeStampA >= timeStampB) {
          lastTime =
            &manipulator2R_Control_int_DWork.Derivative_RWORK.TimeStampB;
          lastU =
            &manipulator2R_Control_int_DWork.Derivative_RWORK.LastUAtTimeB[0];
        }
      }

      *lastTime = manipulator2R_Control_intel_M->Timing.t[0];
      *lastU++ = rtb_Switch[0];
      *lastU++ = rtb_Switch[1];
    }

    /* Update for Derivative: '<S4>/Derivative1' */
    {
      real_T timeStampA =
        manipulator2R_Control_int_DWork.Derivative1_RWORK.TimeStampA;
      real_T timeStampB =
        manipulator2R_Control_int_DWork.Derivative1_RWORK.TimeStampB;
      real_T* lastTime =
        &manipulator2R_Control_int_DWork.Derivative1_RWORK.TimeStampA;
      real_T* lastU =
        &manipulator2R_Control_int_DWork.Derivative1_RWORK.LastUAtTimeA[0];
      if (timeStampA != rtInf) {
        if (timeStampB == rtInf) {
          lastTime =
            &manipulator2R_Control_int_DWork.Derivative1_RWORK.TimeStampB;
          lastU =
            &manipulator2R_Control_int_DWork.Derivative1_RWORK.LastUAtTimeB[0];
        } else if (timeStampA >= timeStampB) {
          lastTime =
            &manipulator2R_Control_int_DWork.Derivative1_RWORK.TimeStampB;
          lastU =
            &manipulator2R_Control_int_DWork.Derivative1_RWORK.LastUAtTimeB[0];
        }
      }

      *lastTime = manipulator2R_Control_intel_M->Timing.t[0];
      *lastU++ = manipulator2R_Control_int_DWork.qp[0];
      *lastU++ = manipulator2R_Control_int_DWork.qp[1];
    }
  }                                    /* end MajorTimeStep */

  if (rtmIsMajorTimeStep(manipulator2R_Control_intel_M)) {
    rt_ertODEUpdateContinuousStates(&manipulator2R_Control_intel_M->solverInfo);

    /* Update absolute time for base rate */
    /* The "clockTick0" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick0"
     * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
     * overflow during the application lifespan selected.
     */
    ++manipulator2R_Control_intel_M->Timing.clockTick0;
    manipulator2R_Control_intel_M->Timing.t[0] = rtsiGetSolverStopTime
      (&manipulator2R_Control_intel_M->solverInfo);

    {
      /* Update absolute timer for sample time: [0.001s, 0.0s] */
      /* The "clockTick1" counts the number of times the code of this task has
       * been executed. The resolution of this integer timer is 0.001, which is the step size
       * of the task. Size of "clockTick1" ensures timer will not overflow during the
       * application lifespan selected.
       */
      manipulator2R_Control_intel_M->Timing.clockTick1++;
    }
  }                                    /* end MajorTimeStep */
}

/* Derivatives for root system: '<Root>' */
void manipulator2R_Control_intelModelClass::
  manipulator2R_Control_intel_derivatives()
{
  /* Derivatives for Integrator: '<S4>/Integrator' */
  {
    ((StateDerivatives_manipulator2R_ *)
      manipulator2R_Control_intel_M->ModelData.derivs)->Integrator_CSTATE[0] =
      manipulator2R_Control_int_DWork.qp[0];
    ((StateDerivatives_manipulator2R_ *)
      manipulator2R_Control_intel_M->ModelData.derivs)->Integrator_CSTATE[1] =
      manipulator2R_Control_int_DWork.qp[1];
  }
}

/* Model initialize function */
void manipulator2R_Control_intelModelClass::initialize()
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&manipulator2R_Control_intel_M->solverInfo,
                          &manipulator2R_Control_intel_M->Timing.simTimeStep);
    rtsiSetTPtr(&manipulator2R_Control_intel_M->solverInfo, &rtmGetTPtr
                (manipulator2R_Control_intel_M));
    rtsiSetStepSizePtr(&manipulator2R_Control_intel_M->solverInfo,
                       &manipulator2R_Control_intel_M->Timing.stepSize0);
    rtsiSetdXPtr(&manipulator2R_Control_intel_M->solverInfo,
                 &manipulator2R_Control_intel_M->ModelData.derivs);
    rtsiSetContStatesPtr(&manipulator2R_Control_intel_M->solverInfo,
                         &manipulator2R_Control_intel_M->ModelData.contStates);
    rtsiSetNumContStatesPtr(&manipulator2R_Control_intel_M->solverInfo,
      &manipulator2R_Control_intel_M->Sizes.numContStates);
    rtsiSetErrorStatusPtr(&manipulator2R_Control_intel_M->solverInfo, ((const
      char_T **)(&rtmGetErrorStatus(manipulator2R_Control_intel_M))));
    rtsiSetRTModelPtr(&manipulator2R_Control_intel_M->solverInfo,
                      manipulator2R_Control_intel_M);
  }

  rtsiSetSimTimeStep(&manipulator2R_Control_intel_M->solverInfo, MAJOR_TIME_STEP);
  manipulator2R_Control_intel_M->ModelData.intgData.y =
    manipulator2R_Control_intel_M->ModelData.odeY;
  manipulator2R_Control_intel_M->ModelData.intgData.f[0] =
    manipulator2R_Control_intel_M->ModelData.odeF[0];
  manipulator2R_Control_intel_M->ModelData.intgData.f[1] =
    manipulator2R_Control_intel_M->ModelData.odeF[1];
  manipulator2R_Control_intel_M->ModelData.intgData.f[2] =
    manipulator2R_Control_intel_M->ModelData.odeF[2];
  manipulator2R_Control_intel_M->ModelData.intgData.f[3] =
    manipulator2R_Control_intel_M->ModelData.odeF[3];
  manipulator2R_Control_intel_M->ModelData.intgData.f[4] =
    manipulator2R_Control_intel_M->ModelData.odeF[4];
  manipulator2R_Control_intel_M->ModelData.intgData.f[5] =
    manipulator2R_Control_intel_M->ModelData.odeF[5];
  manipulator2R_Control_intel_M->ModelData.contStates = ((real_T *)
    &manipulator2R_Control_intel_X);
  rtsiSetSolverData(&manipulator2R_Control_intel_M->solverInfo, (void *)
                    &manipulator2R_Control_intel_M->ModelData.intgData);
  rtsiSetSolverName(&manipulator2R_Control_intel_M->solverInfo,"ode5");
  rtmSetTPtr(manipulator2R_Control_intel_M,
             &manipulator2R_Control_intel_M->Timing.tArray[0]);
  manipulator2R_Control_intel_M->Timing.stepSize0 = 0.001;
  rtmSetFirstInitCond(manipulator2R_Control_intel_M, 1);

  /* InitializeConditions for Derivative: '<S4>/Derivative' */
  manipulator2R_Control_int_DWork.Derivative_RWORK.TimeStampA = rtInf;
  manipulator2R_Control_int_DWork.Derivative_RWORK.TimeStampB = rtInf;

  /* InitializeConditions for Integrator: '<S4>/Integrator' */
  if (rtmIsFirstInitCond(manipulator2R_Control_intel_M)) {
    manipulator2R_Control_intel_X.Integrator_CSTATE[0] = -0.78539816339744828;
    manipulator2R_Control_intel_X.Integrator_CSTATE[1] = 1.5707963267948966;
  }

  manipulator2R_Control_int_DWork.Integrator_IWORK.IcNeedsLoading = 1;

  /* InitializeConditions for Derivative: '<S4>/Derivative1' */
  manipulator2R_Control_int_DWork.Derivative1_RWORK.TimeStampA = rtInf;
  manipulator2R_Control_int_DWork.Derivative1_RWORK.TimeStampB = rtInf;

  /* set "at time zero" to false */
  if (rtmIsFirstInitCond(manipulator2R_Control_intel_M)) {
    rtmSetFirstInitCond(manipulator2R_Control_intel_M, 0);
  }
}

/* Constructor */
manipulator2R_Control_intelModelClass::manipulator2R_Control_intelModelClass()
{
  /* Real-Time Model */
  manipulator2R_Control_intel_M = &manipulator2R_Control_intel_M_;
}

/* Destructor */
manipulator2R_Control_intelModelClass::~manipulator2R_Control_intelModelClass()
{
  /* Currently there is no destructor body generated.*/
}

/* Real-Time Model get method */
RT_MODEL_manipulator2R_Control_ * manipulator2R_Control_intelModelClass::getRTM()
  const
{
  return manipulator2R_Control_intel_M;
}

/*
 * File trailer for Real-Time Workshop generated code.
 *
 * [EOF]
 */
