# Copyright 1994-2009 The MathWorks, Inc.
#
# File    : ert_watc.tmf $Revision: 1.30.4.29 $
# Abstract:
#       Real-Time Workshop template makefile for building a Windows-based 
#       stand-alone embedded real-time version of Simulink model using
#       generated C code and the Open Watcom Compiler
#
#       The following defines (macro names) can be used to modify the behavior
#       of the build:
#         OPT_OPTS       - Optimization option. Default is -oaxt. To enable
#                          debugging specify as "OPT_OPTS=-d2".
#         OPTS           - User specific options.
#         CPP_OPTS       - C++ Compiler Options
#         USER_OBJS      - Additional user objects, such as files needed by
#                          S-functions.
#         USER_PATH      - The directory path to the source (.c) files which 
#                          are used to create any .obj files specified in 
#                          USER_OBJS. Multiple paths must be separated
#                          with a semicolon. For example:
#                          "USER_PATH=path1;path2"
#         USER_INCLUDES  - Additional include paths (i.e. 
#                          "USER_INCLUDES=-Iinclude-path1 -Iinclude-path2")
#       In particular, note how the quotes are before the name of the macro
#       name.
#
#       This template makefile is designed to be used with a system target
#       file that contains 'rtwgensettings.BuildDirSuffix' see ert.tlc


#------------------------ Macros read by make_rtw -----------------------------
#
# The following macros are read by the Real-Time Workshop build procedure:
#       MAKECMD - This is the command used to invoke the make utility
#       HOST    - What platform this template makefile is targeted for
#                 (i.e. PC or UNIX)
#       BUILD   - Invoke make from the Real-Time Workshop build procedure
#                 (yes/no)?
#       TARGET  - Type of target Real-Time (RT) or Nonreal-Tie (NRT)
#
MAKECMD         = "%WATCOM%\binnt\wmake"
HOST            = PC
BUILD           = yes
SYS_TARGET_FILE = any
BUILD_SUCCESS	= All targets are up to date.
COMPILER_TOOL_CHAIN = watc

#---------------------- Tokens expanded by make_rtw ---------------------------
#
# The following tokens, when wrapped with "|>" and "<|" are expanded by the
# Real-Time Workshop build procedure.
#
#  MODEL_NAME          - Name of the Simulink block diagram
#  MODEL_MODULES_OBJ   - Object file name for any additional generated source 
#                        modules
#  MAKEFILE_NAME       - Name of makefile created from template makefile 
#                        <model>.mk
#  MATLAB_ROOT         - Path to where MATLAB is installed. 
#  MATLAB_BIN          - Path to MATLAB executable.
#  S_FUNCTIONS_LIB     - List of S-functions libraries to link.
#  S_FUNCTIONS_OBJ     - List of additional S-function .obj modules.
#  NUMST               - Number of sample times
#  NCSTATES            - Number of continuous states
#  BUILDARGS           - Options passed in at the command line.
#  MULTITASKING        - yes (1) or no (0): Is solver mode multitasking
#  INTEGER_CODE        - yes (1) or no (0): Is generated code purely integer
#  MAT_FILE            - yes (1) or no (0): Should mat file logging be done,
#                        if 0, the generated code runs indefinitely
#  EXT_MODE            - yes (1) or no (0): Build for external mode
#  TMW_EXTMODE_TESTING - yes (1) or no (0): Build ext_test.c for external mode
#                        testing.
#  EXTMODE_TRANSPORT   - Index of transport mechanism (e.g. tcpip, serial) for extmode
#  EXTMODE_STATIC      - yes (1) or no (0): Use static instead of dynamic mem alloc.
#  EXTMODE_STATIC_SIZE - Size of static memory allocation buffer.
#  MULTI_INSTANCE_CODE - Is the generated code multi instantiable (1/0)?
#  PORTABLE_WORDSIZES  - Is this build intended for SIL simulation with portable word sizes (1/0)?
#  SHRLIBTARGET        - Is this build intended for generation of a shared library instead 
#                        of executable (1/0)?

MODEL                   = manipulator2R_Control_intel
MODULES_OBJ             = manipulator2R_Control_intel_data.obj rtGetInf.obj rtGetNaN.obj rt_nonfinite.obj rt_pow_snf.obj 
MAKEFILE                = manipulator2R_Control_intel.mk
MATLAB_ROOT             = C:\MATLAB
ALT_MATLAB_ROOT         = C:\MATLAB
MATLAB_BIN              = C:\MATLAB\bin
ALT_MATLAB_BIN          = C:\MATLAB\bin
MASTER_ANCHOR_DIR       = 
START_DIR               = C:\Users\root_ka\Documents\MATLAB\RoboticDevices\control_systems_diagrams\2Rmanipulator\deployment
S_FUNCTIONS             = 
S_FUNCTIONS_LIB         = 
NUMST                   = 2
NCSTATES                = 2
BUILDARGS               =  GENERATE_REPORT=1 GENERATE_ASAP2=0 OPTS="-DTID01EQ=1"
MULTITASKING            = 0
INTEGER_CODE            = 0
MAT_FILE                = 0
ONESTEPFCN              = 1
TERMFCN                 = 0
B_ERTSFCN               = 0
MEXEXT                  = mexw32
EXT_MODE                = 0
TMW_EXTMODE_TESTING     = 0
EXTMODE_TRANSPORT       = 0
EXTMODE_STATIC          = 0
EXTMODE_STATIC_SIZE     = 1000000

MODELREFS               = 
SHARED_SRC              = 
SHARED_SRC_DIR          = 
SHARED_BIN_DIR          = 
SHARED_LIB              = 
TARGET_LANG_EXT         = cpp
MEX_OPT_FILE            = -f C:\Users\root_ka\AppData\Roaming\MATHWO~1\MATLAB\R2010b\mexopts.bat

MULTI_INSTANCE_CODE     = 1
GEN_SAMPLE_MAIN         = 1
TARGET_LANG_EXT         = cpp
MEX_OPT_FILE            = -f C:\Users\root_ka\AppData\Roaming\MATHWO~1\MATLAB\R2010b\mexopts.bat
PORTABLE_WORDSIZES      = 0
SHRLIBTARGET            = 0
OPTIMIZATION_FLAGS      = -ox
ADDITIONAL_LDFLAGS      = 

#--------------------------- Model and reference models -----------------------
MODELLIB                  = manipulator2R_Control_intellib.lib
MODELREF_LINK_LIBS        = 
MODELREF_LINK_RSPFILE     = manipulator2R_Control_intel_ref.rsp
MODELREF_INC_PATH         = 
RELATIVE_PATH_TO_ANCHOR   = ..
MODELREF_TARGET_TYPE      = NONE
MODELREF_SFCN_SUFFIX      = _msf

#-- In the case when directory name contains space ---
!ifneq MATLAB_ROOT $(ALT_MATLAB_ROOT)
MATLAB_ROOT = $(ALT_MATLAB_ROOT)
!endif
!ifneq MATLAB_BIN $(ALT_MATLAB_BIN)
MATLAB_BIN = $(ALT_MATLAB_BIN)
!endif

#--------------------------------- Tool Locations -----------------------------
#
# Modify the following macro to reflect where you have installed
# the Watcom C/C++ Compiler.
#
!ifndef %WATCOM
!error WATCOM environmental variable must be defined
!else
WATCOM = $(%WATCOM)
!endif

#---------------------------- Tool Definitions ---------------------------

!include $(MATLAB_ROOT)\rtw\c\tools\watctools.mak


# Determine if we are generating an s-function
SFCN = 0
!if "$(MODELREF_TARGET_TYPE)" == "SIM"
SFCN = 1
!endif
!if $(B_ERTSFCN)==1
SFCN = 1
!endif


#------------------------ External mode ---------------------------------------
# Uncomment -DVERBOSE to have information printed to stdout
# To add a new transport layer, see the comments in
#   <matlabroot>/toolbox/simulink/simulink/extmode_transports.m
!ifeq EXT_MODE 1
EXT_CC_OPTS = -DEXT_MODE -DWIN32 # -DVERBOSE
EXTMODE_PATH  = $(MATLAB_ROOT)\rtw\c\src\ext_mode\common;$(MATLAB_ROOT)\rtw\c\src\rtiostream\rtiostreamtcpip;$(MATLAB_ROOT)\rtw\c\src\ext_mode\serial;$(MATLAB_ROOT)\rtw\c\src\ext_mode\custom;
!ifeq EXTMODE_TRANSPORT 0
EXT_OBJ = ext_svr.obj updown.obj ext_work.obj rtiostream_interface.obj rtiostream_tcpip.obj
EXT_LIB = $(WATCOM)\lib386\nt\wsock32.lib
!endif
!ifeq EXTMODE_TRANSPORT 1
EXT_OBJ  = ext_svr.obj updown.obj ext_work.obj ext_svr_serial_transport.obj
EXT_OBJ += ext_serial_pkt.obj ext_serial_win32_port.obj
EXT_LIB  = 
!endif
!ifeq TMW_EXTMODE_TESTING 1
EXT_OBJ     += ext_test.obj
EXT_CC_OPTS += -DTMW_EXTMODE_TESTING
!endif
!ifeq EXTMODE_STATIC 1
EXT_OBJ     += mem_mgr.obj
EXT_CC_OPTS += -DEXTMODE_STATIC -DEXTMODE_STATIC_SIZE=$(EXTMODE_STATIC_SIZE)
!endif
!else
EXT_OBJ     =
EXT_CC_OPTS =
EXT_LIB     = 
!endif

#------------------------------ Include Path -----------------------------

MATLAB_INCLUDES = $(MATLAB_ROOT)\extern\include;&
$(MATLAB_ROOT)\simulink\include;&
$(MATLAB_ROOT)\rtw\c\src;&
$(MATLAB_ROOT)\rtw\c\src\ext_mode\common;

# Additional includes

ADD_INCLUDES = &
$(START_DIR)\manipulator2R_Control_intel_ert_rtw;&
$(START_DIR);&


COMPILER_INCLUDES = $(WATCOM)\h;$(WATCOM)\h\nt;.

INCLUDES = .;..;$(RELATIVE_PATH_TO_ANCHOR);$(MATLAB_INCLUDES)$(ADD_INCLUDES)$(COMPILER_INCLUDES)

!ifneq SHARED_SRC_DIR ""
INCLUDES = $(INCLUDES);$(SHARED_SRC_DIR)
!endif


#-------------------------------- C Flags --------------------------------

# Required Options
REQ_OPTS = $(WATC_STD_OPTS)

!ifeq SHRLIBTARGET 1
REQ_OPTS += -bd
!endif

# Optimization Options.
#   -oaxt : maximum optimization
#   -d2   : debugging options
#
OPT_OPTS = $(DEFAULT_OPT_OPTS)

!if "$(OPTIMIZATION_FLAGS)" != ""
CC_OPTS = $(REQ_OPTS) $(OPTS) $(EXT_CC_OPTS) $(OPTIMIZATION_FLAGS)
MEX_OPT_OPTS = OPTIMFLAGS="$(OPTIMIZATION_FLAGS)"
!else
CC_OPTS = $(REQ_OPTS) $(OPT_OPTS) $(OPTS) $(EXT_CC_OPTS)
MEX_OPT_OPTS = OPTIMFLAGS="$(OPT_OPTS)"
!endif

CPP_REQ_DEFINES = -DMODEL=$(MODEL) -DNUMST=$(NUMST) -DNCSTATES=$(NCSTATES) &
		  -DMT=$(MULTITASKING) &
		  -DMAT_FILE=$(MAT_FILE) -DINTEGER_CODE=$(INTEGER_CODE) &
		  -DONESTEPFCN=$(ONESTEPFCN) -DTERMFCN=$(TERMFCN) &
		  -DHAVESTDIO -DMULTI_INSTANCE_CODE=$(MULTI_INSTANCE_CODE)

!if "$(MODELREF_TARGET_TYPE)" == "SIM"
CPP_REQ_DEFINES = $(CPP_REQ_DEFINES) -DMDL_REF_SIM_TGT=1
!else
CPP_REQ_DEFINES = $(CPP_REQ_DEFINES) -DMT=$(MULTITASKING)
!endif

!ifeq PORTABLE_WORDSIZES 1
CPP_REQ_DEFINES += -DPORTABLE_WORDSIZES
!endif

CFLAGS = $(MODELREF_INC_PATH) $(CC_OPTS) $(CPP_REQ_DEFINES) $(USER_INCLUDES)
CPPFLAGS = $(MODELREF_INC_PATH) $(CPP_OPTS) $(CC_OPTS) $(CPP_REQ_DEFINES) $(USER_INCLUDES)

!if "$(ADDITIONAL_LDFLAGS)" != ""
MEX_LDFLAGS = LINKFLAGS="$$LINKFLAGS $(ADDITIONAL_LDFLAGS)"
!else
MEX_LDFLAGS =
!endif

MEX_FLAGS = -win32 $(MEX_OPTS) $(MEX_OPT_OPTS) $(MEX_LDFLAGS) $(MEX_OPT_FILE)

#------------------------------- Source Files ---------------------------------

!ifeq INTEGER_CODE 1
ADD_SRCS = 
!else
ADD_SRCS =
!endif


!ifeq SFCN 0
!ifeq MODELREF_TARGET_TYPE NONE
!ifeq SHRLIBTARGET 1
PRODUCT   = $(RELATIVE_PATH_TO_ANCHOR)\$(MODEL)_win32.dll
REQ_OBJS  = $(MODEL).obj $(MODULES_OBJ) $(ADD_SRCS)
!else
PRODUCT   = $(RELATIVE_PATH_TO_ANCHOR)\$(MODEL).exe
REQ_OBJS  = $(MODEL).obj $(MODULES_OBJ) ert_main.obj $(ADD_SRCS)
!endif
!else
PRODUCT   = $(MODELLIB)
REQ_OBJS  = $(MODULES_OBJ)
!endif
!else
MEX  = $(MATLAB_BIN)\mex
!ifeq MODELREF_TARGET_TYPE SIM
PRODUCT      = $(RELATIVE_PATH_TO_ANCHOR)\$(MODEL)$(MODELREF_SFCN_SUFFIX).$(MEXEXT)
RTW_SFUN_SRC = $(MODEL)$(MODELREF_SFCN_SUFFIX).$(TARGET_LANG_EXT)
!else
PRODUCT      = $(RELATIVE_PATH_TO_ANCHOR)\$(MODEL)_sf.$(MEXEXT)
RTW_SFUN_SRC = $(MODEL)_sf.$(TARGET_LANG_EXT)
!endif
REQ_OBJS  = $(MODULES_OBJ) 
!if $(B_ERTSFCN)==1
REQ_OBJS  = $(MODEL).obj $(REQ_OBJS) 
!endif
REQ_OBJS     = $(REQ_OBJS) $(ADD_SRCS)
!endif

USER_OBJS =

OBJS = $(REQ_OBJS) $(USER_OBJS) $(S_FUNCTIONS) $(EXT_OBJ)
SHARED_OBJS = $(SHARED_SRC:.c*=.obj)

#--------------------------- Additional Libraries -----------------------------

!ifeq INTEGER_CODE 0
INTLIBEXT = 
!else
INTLIBEXT = int_
!endif

LIBS = 



!if $(SFCN) == 1
LIBFIXPT = $(MATLAB_ROOT)\extern\lib\win32\watcom\libfixedpoint.lib
LIBS     = $(LIBS) $(LIBFIXPT)
!endif

!ifeq MODELREF_TARGET_TYPE SIM
LIBMWMATHUTIL = $(MATLAB_ROOT)\extern\lib\win32\watcom\libmwmathutil.lib
LIBS     = $(LIBS) $(LIBMWMATHUTIL)
!endif

!ifeq MODELREF_TARGET_TYPE SIM
LIBMWSL_FILEIO = $(MATLAB_ROOT)\extern\lib\win32\watcom\libmwsl_fileio.lib
LIBS     = $(LIBS) $(LIBMWSL_FILEIO)
!endif

LIBS += $(EXT_LIB)

!ifeq MODELREF_TARGET_TYPE SIM
LIBMWIPP = $(MATLAB_ROOT)\lib\win32\libippmwipt.lib
LIBS += $(LIBMWIPP)
!endif

#-------------------------- Source Path ---------------------------------------

# User source path

!ifdef USER_PATH
EXTRA_PATH = ;$(USER_PATH)
!else
EXTRA_PATH = 
!endif

# Additional sources

ADD_SOURCES = $(MATLAB_ROOT)\rtw\c\src;

# Source Path

!ifeq GEN_SAMPLE_MAIN 0
STATIC_MAIN_PATH = $(MATLAB_ROOT)\rtw\c\ert
!else
STATIC_MAIN_PATH =
!endif

.c : ..;$(STATIC_MAIN_PATH);$(MATLAB_ROOT)\rtw\c\src;$(EXTMODE_PATH)$(ADD_SOURCES);$(EXTRA_PATH);$(MATLAB_ROOT)/simulink/src
.cpp : ..;$(STATIC_MAIN_PATH);$(MATLAB_ROOT)\rtw\c\src;$(EXTMODE_PATH)$(ADD_SOURCES);$(EXTRA_PATH);$(MATLAB_ROOT)/simulink/src
#----------------------- Exported Environment Variables -----------------------
#
# Because of the 128 character command line length limitations in DOS, we
# use environment variables to pass additional information to the WATCOM
# Compiler and Linker
#
CCPATH = $(WATCOM)\binnt;$(WATCOM)\binw
PATH = $(CCPATH);$(%windir)\system32;$(%windir)

#--------------------------------- Rules --------------------------------------
SHARED_MODULES_LNK = $(SHARED_SRC_DIR)\shared_modules.lnk

.ERASE

.BEFORE
	@set path=$(PATH)
	@set INCLUDE=$(INCLUDES)
	@set WATCOM=$(WATCOM)
	@set MATLAB=$(MATLAB_ROOT)
	@if exist $(MODEL).lnk @del $(MODEL).lnk

!ifeq SHRLIBTARGET 1
	@echo SYSTEM NT_DLL >> $(MODEL).lnk
!else
	@echo DEBUG ALL >> $(MODEL).lnk
!endif
	@for %i in ($(OBJS)) do @echo FILE %i >> $(MODEL).lnk
	@for %i in ($(LIBS)) do @echo LIBRARY %i >> $(MODEL).lnk
	@for %i in ($(S_FUNCTIONS_LIB)) do @echo LIBRARY %i >> $(MODEL).lnk
	@for %i in ($(MODELREF_LINK_LIBS)) do @echo LIBRARY %i >> $(MODEL).lnk
	@for %i in ($(SHARED_LIB)) do @echo LIBRARY %i >> $(MODEL).lnk

all : $(PRODUCT) .symbolic
	@echo $#$#$# $(BUILD_SUCCESS)


!ifeq SFCN 0
!ifeq MODELREF_TARGET_TYPE NONE
!ifeq SHRLIBTARGET 1
# Shared library target (.dll) $(MODEL)_win32.dll------------------------------
$(PRODUCT) : $(OBJS) $(LIBS)
	$(PERL) $(MATLAB_ROOT)\rtw\c\tools\dlldef_conv_tool.pl $(MODEL).def WATC
	$(LD) $(LDFLAGS) $(ADDITIONAL_LDFLAGS) NAME $@ @$(MODEL).lnk @$(MODEL).deflnk
	@echo $#$#$# Successfully created dynamically linked library: $(MODEL)_win32.dll
	@del $(MODEL).lnk $(MODEL).deflnk
!else
#  Stand-alone model $(MODEL).exe----------------------------------------------
$(PRODUCT) : $(OBJS) $(LIBS) $(SHARED_LIB)
	$(LD) NAME $@ $(LDFLAGS) $(ADDITIONAL_LDFLAGS) @$(MODEL).lnk
	@echo $#$#$# Successfully created executable: $(MODEL).exe
	@del $(MODEL).lnk
!endif
!else
# Model reference RTW Target $(MODELLIB)---------------------------------------
$(PRODUCT) : $(OBJS) $(SHARED_LIB)
	@echo $#$#$# Linking $(PRODUCT) ...
	$(LIBCMD) /n $@ $(OBJS) $(S_FUNCTIONS_LIB)
	@if exist $(MODEL).lnk @del $(MODEL).lnk
	@echo Successfully created static library $(MODELLIB)
!endif
!else
# Model reference SIM Target $(MODEL)$(MODELREF_SFCN_SUFFIX).$(MEXEXT)-----------------------------
$(PRODUCT) : $(OBJS) $(SHARED_LIB) $(LIBS) $(RTW_SFUN_SRC) $(MODELREF_LINK_LIBS)
	@echo $#$#$# building S-Function: $(PRODUCT)
	$(LIBCMD) /n $(MODELLIB) $(OBJS) $(S_FUNCTIONS_LIB)
	$(MEX) $(MEX_FLAGS) $(MODELREF_INC_PATH) -outdir $(RELATIVE_PATH_TO_ANCHOR) $(RTW_SFUN_SRC) $(MODELLIB) @$(MODELREF_LINK_RSPFILE) $(SHARED_LIB) $(LIBS)
	@if exist $(MODEL).lnk @del $(MODEL).lnk
	@echo $#$#$# Successfully created S-Function: $(PRODUCT)
!endif

!ifeq GEN_SAMPLE_MAIN 0
!ifeq TARGET_LANG_EXT cpp
ert_main.obj : $(MATLAB_ROOT)\rtw\c\ert\ert_main.c
	@echo $#$#$# Compiling $[@
	$(CPP) $(CPPFLAGS) $[@
!else
ert_main.obj : $(MATLAB_ROOT)\rtw\c\ert\ert_main.c
	@echo $#$#$# Compiling $[@
	$(CC) $(CFLAGS) $[@
!endif
!endif

.c.obj:
	@echo $#$#$# Compiling $[@
	$(CC) $(CFLAGS) $[@

.cpp.obj:
	@echo $#$#$# Compiling $[@
	$(CPP) $(CPPFLAGS) $[@

$(OBJS) : $(MAKEFILE) rtw_proj.tmw .AUTODEPEND

# shared utilities library --------------------------------------------
!ifneq SHARED_LIB ""
$(SHARED_LIB) PHONY_TARGET: $(SHARED_SRC)
	@echo $#$#$# Creating $@
	@for %i in ($<) do @$(CC) $(CFLAGS) -Fo$(SHARED_BIN_DIR)\ %i
#       the lnk file can't be pre-generated becasue it uses a *.obj directive
#       and the OBJ files don't exist until they are compiled
	@if exist $(SHARED_MODULES_LNK) @del $(SHARED_MODULES_LNK)
	@for %i in ($(SHARED_OBJS)) do @echo %i >> $(SHARED_MODULES_LNK)
	$(LIBCMD) /n $@ @$(SHARED_MODULES_LNK)
	@del $(SHARED_MODULES_LNK)
	@echo $#$#$# $@ Created
!endif

# Libraries:




