% This script add all the subdirectories of this project to the Matlab
% search path. It should be launched first but only in a local directory.

currentpath=pwd;
 if  length(pwd)-strfind(pwd, '2Rmanipulator')+1 == length('2Rmanipulator'),
    TwoRmanipulatorPathRoot=pwd;
    addpath(TwoRmanipulatorPathRoot);
    addpath([TwoRmanipulatorPathRoot, '\data']);
    addpath([TwoRmanipulatorPathRoot, '\deployment']);
    addpath([TwoRmanipulatorPathRoot, '\models']);
    addpath([TwoRmanipulatorPathRoot, '\scripts']);
    addpath([TwoRmanipulatorPathRoot, '\bin']);
    addpath([TwoRmanipulatorPathRoot, '\lib']);
    clear TwoRmanipulatorPathRoot
 else
    disp('error: the current working directory is not suitable');
    disp('       for running the script projectPaths.m');
 end;
 clear currentpath;