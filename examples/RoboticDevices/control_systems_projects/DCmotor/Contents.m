% DCmotor: a set of block diagrams, data sets and scripts designed for 
% - verification and further development of a driver for a DC motor 
%   (Groupner Speed 280),  
% - experimental verification of model based control algorithms analysis and 
% - development of identification algorithms and modelling techniques.
%
% (C) Krzysztof Arent, 2018
%
% The contents of DCmotor folder:
% bin/
% data/
%  data1.mat - a data file (friction coefficients identification)
%  data2.mat - a data file (model parameters estimation (one directional 
%   rotor rotation))
%  data3.mat - a data file (model parameters estimation (bidirectional 
%   rotor rotation)
%  data4.mat - a data file (model parameters estimation (one directional 
%   rotor rotation, in opposite direction in compare to the data2.mat
%   case))
%  data5.mat - a data file (model verification (staircaise input signal))
%  data6.mat - a data file (model verificatio (sinusoidal input signal))
% deployment/
%  DCmotor_Controller_atmega32.mdl - a Simulink diagram of a set point 
%   controller, configured for automatic code generation that is intended 
%   to be deployed at  at a board with an ATmega32 processor.
%  DCmotor_Controller_dsPIC.mdl - a Simulink diagram of a set point 
%   controller, configured for automatic code generation that is intended 
%   to be deployed at  at a board with an dsPIC processor.
%  DCmotor_Controller_stm.mdl - a Simulink diagram of a set point 
%   controller, configured for automatic code generation that is intended 
%   to be deployed at an STM board.
% functions/
%  DCmotor_PPTracking_controller.m - a tracking controller for a DC motor
%  DCmotor_PPsetPointRegulation_controller.m - a set point controller for a
%  DC motor
%  DCmotor_identP2tfparameters - model parameters converter
% lib/
%  DCmotor_feedback_components.mdl: a library associated with the
%    DCmotor_Control_Model.mdl diagram. It includes various forward
%    compensators and prefilters to be applied in a 2DOF feedback control
%    system for simulation analysis.
% models/
%  DCmotor_Control_Model.mdl: a Simulink diagram for DC motor controllers
%    analysis
%  DCmotor_Identification_ModelVerificationManual.mdl: a Simulink diagram
%    for DC motor model's parameters verification and tuning.
% scripts/
%  exercise_2_DCmotor_IdentificationPD.m - model parameters derivation on
%    the basis of the acquired data
%  exercise_3_DCmotor_IdentificationMV.m - model parameteres verification
%  exercise_4_DCmotor_Control_Model.m - simulation
%  DCmotor_init.m - initialisation of the plant parameters
%  DCmotor_Control_init.m - initialisation of the controllers' parameters
%  DCmotor_dataProcessing.m - loading data stored in .mat files in the
%                             data/ directory
% projectPaths.m
