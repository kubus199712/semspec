% This script add all the subdirectories of this project to the Matlab
% search path. It should be launched first but only in a local directory.

currentpath=pwd;
if  length(pwd)-strfind(pwd, 'DCmotor')+1 == ...
        length('DCmotor'),
    DCmotorPathRoot=pwd;
    addpath(DCmotorPathRoot);
    addpath([DCmotorPathRoot, '\data']);
    addpath([DCmotorPathRoot, '\deployment']);
    addpath([DCmotorPathRoot, '\functions']);
    addpath([DCmotorPathRoot, '\models']);
    addpath([DCmotorPathRoot, '\scripts']);
    addpath([DCmotorPathRoot, '\bin']);
    addpath([DCmotorPathRoot, '\lib']);
    clear DCmotorPathRoot;
else
    disp('error: the current working directory is not suitable');
    disp('       for running the script DCmotorPath_projectPaths.m');
end;
clear currentpath;