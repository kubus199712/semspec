% DC motor: control
% launching Matlab scripts and Simulink diagrams designed for the exercise
% on DC motor control (the simulation part)

% author: Krzysztof Arent, 2018

%DCmotor_init %run if the DC motor parameters have not been defined beforehand
DCmotor_Control_init
edit DCmotor_Control_init
warning('off','MATLAB:dispatcher:InexactCaseMatch')
DCmotor_feedback_components
DCmotor_Control_Model
sisotool(g);

% COMMENTS
% This script together with the above Matlab scripts and Simulink 
% diagrams allow for model based controller synthesis and analysis 
% for a DC motor. The set of considered controllers includes
% - pole placement tracking control
% - pole placement set point control
% - PID control  (with use of sisotool)
% - IMC (with use of sisotool)
% - LQG control  (with use of sisotool)
% - Loop shaping (with use of sisotool)
% - Optimization baset tuning (with use of sisotool)
% The sisotool is based on a linear part of a DC motor model. Full 
% simulation analysis with a nonlinear model of a DC motor can be carried
% out using the associated Simulink diagram.
% Attention! The systems C (a controller) and F(a prefilter) exported to
% the workspace from sisotool has to be converted to tf form in order that
% they could be used in a simulation analysis by Simulink diagrams.
% C=tf(C)
% F=tf(F)