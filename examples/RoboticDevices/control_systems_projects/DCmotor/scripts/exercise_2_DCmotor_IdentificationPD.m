% DC motor: identification
% launching Matlab scripts and Simulink diagrams designed for the exercise
% on DC motor model identification (the simulation part concerning
% parameters derivation)

% (C) Krzysztof Arent, 2018

DCmotor_dataProcessing
edit DCmotor_dataProcessing
ident

% COMMENT
% the ident tool is used to convert the data  
% Dv.signals.values and rpm.signals.values (with the sampling time = 0.01) 
% stored in the workspace onto a process model P.
% Next, P is used to determine tf model parameteres that are used by other
% DCmotor scripts and diagrams:
% [b0,a1,a0,g]=DCmotor_identP2tfparameters(P)
% sf=stepinfo(g);
% bw=bandwidth(g);