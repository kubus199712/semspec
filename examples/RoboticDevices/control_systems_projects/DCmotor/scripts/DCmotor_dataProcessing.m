% DC motor: data processing
% Loading to the workspace the stored data for further model and data 
% analysis. The data are assigned to the following variables: 
% D  - an input signal with a friction compensation ingredient
% Dv - an input signal without a friction compensation ingredient, 
% rpm - on output signal
% Fc - a Coulomb friction coefficient (in the PWM space)

% clear all
% load('data1.mat')
% load('data2.mat')
% load('data3.mat')
load('data4.mat')
% load('data5.mat')
% load('data6.mat')

Fc=10; % setting the value of Fc for purposes of data acquisition 
       % (designed for DC motor linear model identification)
