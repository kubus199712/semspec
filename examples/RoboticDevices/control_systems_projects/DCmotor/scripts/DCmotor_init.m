% initialisation of parameters for the following block diagrams: 
%  DCmotor_Identification_ModelVerification.mdl
%  DCmotor_Control_Model.mdl

% author: Krzysztof Arent, 2018

% DC motor model parameters
a0=28.75;
a1=19.02;
b0=5021.3*1.35;
Fc=10;

g=tf([0, 0, b0], [1, a1, a0]);