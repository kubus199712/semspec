% DC motor: control
% launching Matlab scripts and Simulink diagrams designed for the exercise
% on DC motor control (C code generation)

% author: Krzysztof Arent, 2018

DCmotor_init
DCmotor_Control_init

warning('off','MATLAB:dispatcher:InexactCaseMatch')
DCmotor_feedback_components
DCmotor_Controller_stm
DCmotor_Controller_dsPIC
DCmotor_Controller_atmega32


