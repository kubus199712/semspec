% initialisation of parameters for the following block diagrams: 
%  DCmotor_Control.mdl
%  DCmotor_Control_Model.mdl

% author: Krzysztof Arent, 2018

% DCmotor_init;

% pole placement tracking controller parameters
x0=[0,300,0];
q=poly([0, 0.1*i, -0.1*i]);
mu=poly([-5, -5, -5 -5, -5, -5]);
[c4,c3,c2,c1,c0,d4,d3,d2,d1,d0,q2,q1,q0]=...
    DCmotor_PPTracking_controller(mu,q,a1,a0,b0);

% pole placement set poing controller parameters
MU=poly([-20, -23, -26, -30]);
[C1, C0, D1, D0, F0]=DCmotor_PPsetPointRegulation_controller(MU,a1,a0,b0);