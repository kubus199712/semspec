% DC motor: identification
% launching Matlab scripts and Simulink diagrams designed for the exercise
% on DC motor model identification (the simulation part concerning on-line
% model verification)

% author: Krzysztof Arent, 2018

% clear all
% DCmotor_init
DCmotor_dataProcessing
edit DCmotor_dataProcessing
edit DCmotor_init
DCmotor_Identification_ModelVerificationManual

% COMMENT
% This script together with the above Matlab scripts and the Simulink 
% diagram allow manual tuning the values of a1, a0, b0, Fc