function [ b0, a1, a0, g ] = DCmotor_identP2tfparameters( P )
%DCMOTOR_IDENTP2TFPARAMETERS(P) extracts DC motor parameters a0, a1, b0 and
%the related transfer function g from the model P derived by the ident tool.
%   
gP=tf(P);
b0=gP.num{1}(3);
a0=gP.den{1}(3);
a1=gP.den{1}(2);
g=tf([0,0,b0],[1, a1, a0]);
end

