function [C1, C0, D1, D0, F0]=DCmotor_PPsetPointRegulation_controller( ...
    MU, a1, a0, b0, varargin)
%
% DCmotor_PPsetPointRegulation_controller returns the values of
% coefficients of two transfer functions, C(s) and F(s), 
% where C represents a feedforward controller while F a prefilter 
% in a 2DOF feedback control system.
%
% The meaning if the input arguments:
% a0, a1, b0 are coefficients of a transfer function P(s) representing a
% plant (P(s)=b0/(s^2+a1*s+a0).
% MU is a vector of coefficients of the desired denominator in the
% closed-loop transfer function. It has to be a strictly stable, fourth
% order polynomial. MU be obtained using a poly function, e.g
% MU=poly([-20, -23, -26, -30])
%
% The meaning of the output arguments:
% C1, C0, D1, D0 are coefficient of C (C(s)=(D1*s+D0)/(s^2+C1*s+C0))
% F0 is a coefficient of a prefilter F ( F(s)=F0 )
% 
% funcion call
% [C1, C0, D1, D0, F0]=DCmotor_PPsetPointRegulation_controller(MU,a1,a0,b0)
%

%
% (C) Krzysztof Arent, 2018
%
if nargin < 1
  error('DCmotor_PPsetPointRegulation :  MU is a required input')
end

if nargin >=1 && nargin < 4
    a0=28.75;
    a1=19.02;
    b0=5021.3*1.35;
end

S=[  1  0  0  0; ...
    a1  1  0  0; ...
    a0 a1 b0  0; ...
     0 a0  0 b0];
m3=MU(2);
m2=MU(3);
m1=MU(4);
m0=MU(5);
M=[ m3-a1; ...
    m2-a0; ...
    m1; ...
    m0];
cd=S\M;
C1=cd(1);
C0=cd(2);
D1=cd(3);
D0=cd(4);
F0=(a0*C0+b0*D0)/(b0*D0);