/*
 * File: PendulumOnCart_Controller_intel_types.h
 *
 * Real-Time Workshop code generated for Simulink model PendulumOnCart_Controller_intel.
 *
 * Model version                        : 1.72
 * Real-Time Workshop file version      : 7.6  (R2010b)  03-Aug-2010
 * Real-Time Workshop file generated on : Tue Feb 04 19:04:24 2020
 * TLC version                          : 7.6 (Jul 13 2010)
 * C/C++ source code generated on       : Tue Feb 04 19:04:25 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Intel->x86/Pentium
 * Code generation objective: Execution efficiency
 * Validation result: Passed (7), Warnings (2), Error (0)
 */

#ifndef RTW_HEADER_PendulumOnCart_Controller_intel_types_h_
#define RTW_HEADER_PendulumOnCart_Controller_intel_types_h_
#endif                                 /* RTW_HEADER_PendulumOnCart_Controller_intel_types_h_ */

/*
 * File trailer for Real-Time Workshop generated code.
 *
 * [EOF]
 */
