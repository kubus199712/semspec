/*
 * File: PendulumOnCart_Controller_intel.h
 *
 * Real-Time Workshop code generated for Simulink model PendulumOnCart_Controller_intel.
 *
 * Model version                        : 1.72
 * Real-Time Workshop file version      : 7.6  (R2010b)  03-Aug-2010
 * Real-Time Workshop file generated on : Tue Feb 04 19:04:24 2020
 * TLC version                          : 7.6 (Jul 13 2010)
 * C/C++ source code generated on       : Tue Feb 04 19:04:25 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Intel->x86/Pentium
 * Code generation objective: Execution efficiency
 * Validation result: Passed (7), Warnings (2), Error (0)
 */

#ifndef RTW_HEADER_PendulumOnCart_Controller_intel_h_
#define RTW_HEADER_PendulumOnCart_Controller_intel_h_
#ifndef PendulumOnCart_Controller_intel_COMMON_INCLUDES_
# define PendulumOnCart_Controller_intel_COMMON_INCLUDES_
#include <math.h>
#include "rtwtypes.h"
#endif                                 /* PendulumOnCart_Controller_intel_COMMON_INCLUDES_ */

#include "PendulumOnCart_Controller_intel_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((void*) 0)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((void) 0)
#endif

/* Block signals and states (auto storage) for system '<Root>' */
typedef struct {
  uint16_T Memory_PreviousInput;       /* '<S2>/Memory' */
} D_Work_PendulumOnCart_Controlle;

/* External inputs (root inport signals with auto storage) */
typedef struct {
  real_T x[4];                         /* '<Root>/x' */
} ExternalInputs_PendulumOnCart_C;

/* External outputs (root outports fed by signals with auto storage) */
typedef struct {
  real_T Iref;                         /* '<Root>/Iref' */
} ExternalOutputs_PendulumOnCart_;

/* Class declaration for model PendulumOnCart_Controller_intel */
class PendulumOnCart_Controller_intelModelClass {
  /* public data and function members */
 public:
  /* External inputs */
  ExternalInputs_PendulumOnCart_C PendulumOnCart_Controller_int_U;

  /* External outputs */
  ExternalOutputs_PendulumOnCart_ PendulumOnCart_Controller_int_Y;

  /* Model entry point functions */

  /* model initialize function */
  void initialize();

  /* model step function */
  void step();

  /* Constructor */
  PendulumOnCart_Controller_intelModelClass();

  /* Destructor */
  ~PendulumOnCart_Controller_intelModelClass();

  /* private data and function members */
 private:
  /* Block singals and states */
  D_Work_PendulumOnCart_Controlle PendulumOnCart_Controller_DWork;
};

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : PendulumOnCart_Controller_intel
 * '<S1>'   : PendulumOnCart_Controller_intel/controller linear state feedback
 * '<S2>'   : PendulumOnCart_Controller_intel/controller linear state feedback/decision making system: selection of control system algorithm
 * '<S3>'   : PendulumOnCart_Controller_intel/controller linear state feedback/friction compensation
 * '<S4>'   : PendulumOnCart_Controller_intel/controller linear state feedback/decision making system: selection of control system algorithm/Compare To Constant
 * '<S5>'   : PendulumOnCart_Controller_intel/controller linear state feedback/decision making system: selection of control system algorithm/Compare To Constant1
 * '<S6>'   : PendulumOnCart_Controller_intel/controller linear state feedback/decision making system: selection of control system algorithm/Compare To Constant2
 * '<S7>'   : PendulumOnCart_Controller_intel/controller linear state feedback/decision making system: selection of control system algorithm/Compare To Constant3
 * '<S8>'   : PendulumOnCart_Controller_intel/controller linear state feedback/friction compensation/Coulomb friction 1
 * '<S9>'   : PendulumOnCart_Controller_intel/controller linear state feedback/friction compensation/viscous friction 1
 */
#endif                                 /* RTW_HEADER_PendulumOnCart_Controller_intel_h_ */

/*
 * File trailer for Real-Time Workshop generated code.
 *
 * [EOF]
 */
