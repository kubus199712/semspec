/*
 * File: PendulumOnCart_Controller_intel.cpp
 *
 * Real-Time Workshop code generated for Simulink model PendulumOnCart_Controller_intel.
 *
 * Model version                        : 1.72
 * Real-Time Workshop file version      : 7.6  (R2010b)  03-Aug-2010
 * Real-Time Workshop file generated on : Tue Feb 04 19:04:24 2020
 * TLC version                          : 7.6 (Jul 13 2010)
 * C/C++ source code generated on       : Tue Feb 04 19:04:25 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Intel->x86/Pentium
 * Code generation objective: Execution efficiency
 * Validation result: Passed (7), Warnings (2), Error (0)
 */

#include "PendulumOnCart_Controller_intel.h"
#include "PendulumOnCart_Controller_intel_private.h"

/* Model step function */
void PendulumOnCart_Controller_intelModelClass::step()
{
  real_T F_v;
  real_T F_C;
  real_T rtb_Switch;
  real32_T u;

  /* Embedded MATLAB: '<S3>/viscous friction 1' incorporates:
   *  Inport: '<Root>/x'
   */
  /* Embedded MATLAB Function 'controller linear state feedback/friction compensation/viscous friction 1': '<S9>:1' */
  /*  I_v = k_M * T_v */
  if (PendulumOnCart_Controller_int_U.x[2] > 0.0) {
    /* '<S9>:1:4' */
    /* '<S9>:1:5' */
    F_v = 7.1071;
  } else {
    /* '<S9>:1:7' */
    F_v = 8.5171;
  }

  /* '<S9>:1:9' */

  /* Embedded MATLAB: '<S3>/Coulomb friction 1' incorporates:
   *  Inport: '<Root>/x'
   */
  /* Embedded MATLAB Function 'controller linear state feedback/friction compensation/Coulomb friction 1': '<S8>:1' */
  /*  I_C = k_M * T_C */
  if (PendulumOnCart_Controller_int_U.x[2] > 0.0) {
    /* '<S8>:1:4' */
    /* '<S8>:1:5' */
    F_C = 5.5947;

    /*     F_C = 5.1; */
  } else {
    /* '<S8>:1:8' */
    F_C = 4.5736;

    /*     F_C = 4.3; */
  }

  /* '<S8>:1:12' */

  /* Sum: '<S2>/Sum' incorporates:
   *  Abs: '<S2>/Abs'
   *  Abs: '<S2>/Abs1'
   *  Abs: '<S2>/Abs2'
   *  Abs: '<S2>/Abs3'
   *  Constant: '<S4>/Constant'
   *  Constant: '<S5>/Constant'
   *  Constant: '<S6>/Constant'
   *  Constant: '<S7>/Constant'
   *  Inport: '<Root>/x'
   *  Memory: '<S2>/Memory'
   *  Product: '<S2>/Product'
   *  RelationalOperator: '<S4>/Compare'
   *  RelationalOperator: '<S5>/Compare'
   *  RelationalOperator: '<S6>/Compare'
   *  RelationalOperator: '<S7>/Compare'
   */
  PendulumOnCart_Controller_DWork.Memory_PreviousInput = (uint16_T)((uint32_T)
    (fabs(PendulumOnCart_Controller_int_U.x[0]) <= 1.5) * (uint32_T)(fabs
    (PendulumOnCart_Controller_int_U.x[1]) <= 0.20943951023931953) * (uint32_T)
    (fabs(PendulumOnCart_Controller_int_U.x[2]) <= 0.2) * (uint32_T)(fabs
    (PendulumOnCart_Controller_int_U.x[3]) <= 0.2) + (uint32_T)
    PendulumOnCart_Controller_DWork.Memory_PreviousInput);

  /* Switch: '<S1>/Switch' incorporates:
   *  Gain: '<S1>/cart & pendulum stabilisation'
   *  Gain: '<S1>/cart positioning'
   *  Inport: '<Root>/x'
   *  Saturate: '<S2>/Saturation'
   */
  u = (real32_T)PendulumOnCart_Controller_DWork.Memory_PreviousInput;
  if ((u <= 1.0F ? u : 1.0F) >= 0.5F) {
    rtb_Switch = ((5.6682944910302364 * PendulumOnCart_Controller_int_U.x[0] +
                   13.754402272940357 * PendulumOnCart_Controller_int_U.x[1]) +
                  3.7788629940201575 * PendulumOnCart_Controller_int_U.x[2]) +
      3.5156261250528913 * PendulumOnCart_Controller_int_U.x[3];
  } else {
    rtb_Switch = -1.1196631093393059 * PendulumOnCart_Controller_int_U.x[0] +
      -1.1196631093393059 * PendulumOnCart_Controller_int_U.x[2];
  }

  /* Outport: '<Root>/Iref' incorporates:
   *  Gain: '<S3>/rw//(km*i)'
   *  Inport: '<Root>/x'
   *  Sum: '<S1>/Sum2'
   *  Sum: '<S3>/Sum3'
   */
  PendulumOnCart_Controller_int_Y.Iref = (tanh(35.0 *
    PendulumOnCart_Controller_int_U.x[2]) * F_C + F_v *
    PendulumOnCart_Controller_int_U.x[2]) * 0.04 + rtb_Switch;
}

/* Model initialize function */
void PendulumOnCart_Controller_intelModelClass::initialize()
{
  /* (no initialization code required) */
}

/* Constructor */
PendulumOnCart_Controller_intelModelClass::
  PendulumOnCart_Controller_intelModelClass()
{
}

/* Destructor */
PendulumOnCart_Controller_intelModelClass::
  ~PendulumOnCart_Controller_intelModelClass()
{
  /* Currently there is no destructor body generated.*/
}

/*
 * File trailer for Real-Time Workshop generated code.
 *
 * [EOF]
 */
