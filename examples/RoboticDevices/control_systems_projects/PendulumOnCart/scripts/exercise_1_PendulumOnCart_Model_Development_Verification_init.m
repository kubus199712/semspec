% Pendulum on Cart: modelling
% Development and verification of the Simulink model for the pendulum on 
% the cart. Step by step from 3D model in Autodesk Inventor to
% a uniform model in the form of a Simulink diagram.

% (C) Krzysztof Arent, 2018

disp('-->> PendulumOnCart: model development and verification');
disp('-->> ');

clear all;

q1p0=0;
q1v0=0;
q2p0=0;
q2v0=0;

disp('-->> Comparison of mechanical parts of nonlinear models of a pendulum')
disp('     on a cart that were developed using Autodesk Inventor and Mathematica respectively')
disp('-->> launching the diagram PendulumOnCart_ModelsMechComparison')
PendulumOnCart_ModelsMechComparison
%disp('-->> press a key')
%pause
% 
disp('-->> Comparison of uniform nonlinear models of a pendulum')
disp('     on a cart that were developed using Autodesk Inventor and Mathematica respectively')
disp('-->> launching the diagram PendulumOnCart_ModelsComparison')
PendulumOnCart_ModelsComparison
%disp('-->> press a key')
%pause

disp('-->> Computation of matrices of a linear approximation of a state'); 
disp('     space model of the mechanical part of a pendulum on a cart');
disp('     at the unstable equilibrium point on the basis of two') ;
disp('     Simulink mechanical models');
disp('-->> A1,B1,C1,D1 are associated to a model obtained on the basis a 3D model developed in Autodesk Inventor');
disp('-->> A1,B1,C1,D1 are associated to a model obtained on the basis of symbolic computations in Mathematica');

[A1,B1,C1,D1]=linmod('PendulumOnCart_ModelMechAI',[0,0,0,0]', [0,0]');
[A2,B2,C2,D2]=linmod('PendulumOnCart_ModelMechM', [0,0,0,0]', [0,0]');
if ~all(all(C1(1:4,:)==eye(4))),
    S=C1(1:4,:);
    A1=inv(S)*A1*S;
    B1=inv(S)*B1;
    C1=C1*S;
end;
if ~all(all(C2(1:4,:)==eye(4))),
    S=C2(1:4,:);
    A2=inv(S)*A2*S;
    B2=inv(S)*B2;
    C2=C2*S;
end;
disp('-->> relative differences of the resulted matrices ')
disp(['    A1-A2: ', num2str(norm(A1-A2,2)/norm(A2,2)*100), ' %'])
disp(['    B1-B2: ', num2str(norm(B1-B2,2)/norm(B2,2)*100), ' %'])
disp(['    C1-C2: ', num2str(norm(C1-C2,2)/norm(C2,2)*100), ' %'])


disp('')
disp('-->> Computation of matrices of a linear approximation of a uniform')
disp('     state space model of the mechanical part of a pendulum on a cart');
disp('     at the unstable equilibrium point on the basis of two') ;
disp('     Simulink mechanical models');
disp('')
disp('-->> A3,B3,C3,D3 are associated to a model obtained on the basis of symbolic computations in Mathematica');
disp('-->> A4,B4,C4,D4 are associated to a model obtained on the basis of a Simulink subsystem representing the mechanical part based on a 3D model developed in Autodesk Inventor');
disp('-->> A5,B5,C5,D5 are associated to a model obtained on the basis of a Simulink subsystem representing the mechanical part based on symbolic computations in Mathematica');

[A3,B3,C3,D3]=linmod('PendulumOnCart_ModelForLinmod',[0,0,0,0]', [0]');
if ~all(all(C3(1:4,:)==eye(4))),
    S=C3(1:4,:);
    A3=inv(S)*A3*S;
    B3=inv(S)*B3;
    C3=C3*S;
end;

[A4,B4,C4,D4]=linmod('PendulumOnCart_ModelForLinmod_mechAI',[0,0,0,0]', [0]');
if ~all(all(C4(1:4,:)==eye(4))),
    S=C4(1:4,:);
    A4=inv(S)*A4*S;
    B4=inv(S)*B4;
    C4=C4*S;
end;

[A5,B5,C5,D5]=linmod('PendulumOnCart_ModelForLinmod_mechM',[0,0,0,0]', [0]');
if ~all(all(C4(1:4,:)==eye(4))),
    S=C5(1:4,:);
    A5=inv(S)*A5*S;
    B5=inv(S)*B5;
    C5=C5*S;
end;

disp('-->> relative differences of the resulted matrices ')
disp(['     A3-A4: ', num2str(norm(A3-A4,2)/norm(A3,2)*100), ' %'])
disp(['     B3-B4: ', num2str(norm(B3-B4,2)/norm(B3,2)*100), ' %'])
disp(['     C3-C4: ', num2str(norm(C3-C4,2)/norm(C3,2)*100), ' %'])
disp(['     A4-A5: ', num2str(norm(A4-A5,2)/norm(A4,2)*100), ' %'])
disp(['     B4-B5: ', num2str(norm(B4-B5,2)/norm(B4,2)*100), ' %'])
disp(['     C4-C5: ', num2str(norm(C4-C5,2)/norm(C4,2)*100), ' %'])


