% Pendulum on Cart: identification
% Cart model friction parameters estimatinon with use of Simulink 
% Parameters Estimation Tool and their off-line verification)

% author: Krzysztof Arent, 2018

clear all
disp(' ');
disp('-->> PendulumOnCart: identification of friction coefficients in the model of a cart');
disp('     F1_vp, F1_vn, F1_Cp, F1_Cn');
disp(' ');
disp('-->> setting the values of the model parameters');
disp(' ');

km = 0.039821; % A/Nm
Js = 7.5*10^(-6); %kg m^2
i = 14;
rw = 0.0223; %m
m=1.006; %kg

a2=m+(i/rw)^2*(Js/(2*pi));
b0=i*km/rw;

disp('-->> loading data from a .mat file');
load('data/data3_pc.mat')

disp('-->> setting the values of variables used for parameters estimation ');
disp(' ');

x1_0=q1p.signals.values(1);
x2_0=q1v.signals.values(1);
x1=q1p.signals.values;
x2=q1v.signals.values;
I=Iref.signals.values;
t=Iref.time;

F1_vp=7.1071; 
F1_vn=8.5171; 
F1_Cp=5.5947; 
F1_Cn=4.5736; 

It=[t,I];
x1t=[t,x1];
x2t=[t,x2];

disp(' ');
disp('-->> PendulumOnCart: launching a Simulink model for identification');
spetool PendulumOnCart_CartModel_Verification
disp(' ');
disp('-->> PendulumOnCart: launching a Simulink model for verification');
PendulumOnCart_CartModel_VerificationManual
disp('-->> PendulumOnCart: the end');