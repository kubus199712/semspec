% Pendulum on Cart: model based control
% Stabilisation of the inverted pendulum on a cart.
% deployment

% author: Krzysztof Arent, 2018

clear all;

q1p0=0;
q1v0=0;
q2p0=0;
q2v0=0;

[A3,B3,C3,D3]=linmod('PendulumOnCart_ModelForLinmod',[0,0,0,0]', [0]');
if ~all(all(C3(1:4,:)==eye(4))),
    S=C3(1:4,:);
    A3=inv(S)*A3*S;
    B3=inv(S)*B3;
    C3=C3*S;
end;

clear S;

polyrootsF3=[-6, -6, -6, -6];
polyrootsK3=[-4, -4, -4, -4];

F=-[0,0,0,1]*inv([B3, A3*B3, A3*A3*B3, A3*A3*A3*B3])* polyvalm(poly(polyrootsF3),A3);
K=-[0,0,0,1]*inv([B3, A3*B3, A3*A3*B3, A3*A3*A3*B3])* polyvalm(poly(polyrootsK3),A3);
K=K.*[1,0,1,0];

disp(['      F=', num2str(F)])
disp(['      K=', num2str(K)])

PendulumOnCart_Controller_intel;
PendulumOnCart_Controller_stm;
