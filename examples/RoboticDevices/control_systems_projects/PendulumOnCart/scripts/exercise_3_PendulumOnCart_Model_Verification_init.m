% Pendulum on Cart: identification
% Pendulum model friction parameters estimatinon with use of Simulink 
% Parameters Estimation Tool and their off-line verification)

% author: Krzysztof Arent, 2018

clear all
disp(' ');
disp('-->> PendulumOnCart: identification of friction coefficients in the model of a cart');
disp('     F2_vp, F2_vn, F2_Cp, F2_Cn');
disp(' ');
disp('-->> PendulumOnCart: loading a .mat file');
disp(' ');

% load('data/data1_pc.mat')
% range=[14.46/0.01+1:150/0.01+1]; % eksperyment1.mat
load('data/data2_pc.mat')
range=[36/0.01+1:102/0.01+1]; % eksperyment2.mat


disp(' ');
disp('-->> PendulumOnCart: arranging data sets');
disp(' ');

x1_0=q1p.signals.values(1);
x2_0=q2p.signals.values(1);
x3_0=q1v.signals.values(1);
x4_0=q2v.signals.values(1);
x1=q1p.signals.values;
x2=q2p.signals.values;
x3=q1v.signals.values;
x4=q2v.signals.values;
I=Iref.signals.values;
t=Iref.time;

disp(' ');
disp('-->> PendulumOnCart: setting initial values for');
disp('     F2_vp, F2_vn, F2_Cp, F2_Cn');
disp(' ');

F1_vp=7.1071; 
F1_vn=8.5171; 
F1_Cp=5.5947; 
F1_Cn=4.5736;
F2_vp=0.0065;
F2_vn=0.0065;
F2_Cp=0.0005;
F2_Cn=0.0005;

disp('-->> PendulumOnCart: launching a Simulink model for identification');
spetool PendulumOnCart_Model_Verification

disp('press any key')
pause
%clear all

x1=x1(range);
x2=x2(range);
x3=x3(range);
x4=x4(range);
x1_0=x1(1);
x2_0=x2(1);
x3_0=x3(1);
x4_0=x4(1);

t=t(range);
I=I(range);

x1t=[t,x1];
x2t=[t,x2];
x3t=[t,x3];
x4t=[t,x4];
It=[t,I];

disp('-->> PendulumOnCart: launching a Simulink model for verification');
PendulumOnCart_Model_VerificationManual