% Pendulum on Cart: model based control
% Stabilisation of the inverted pendulum on a cart.

% author: Krzysztof Arent, 2018


disp('-->> PendulumOnCart: model based control');
disp('-->> ');

clear all;

disp('-->> Computation of matrices of a linear approximation of a state'); 
disp('     space model of the mechanical part of a pendulum on a cart');
disp('     at the unstable equilibrium point on the basis of two') ;
disp('     a Simulink mechanical model');
disp('-->> A1,B1,C1,D1 are associated to a model obtained on the basis a 3D model developed in Autodesk Inventor');

q1p0=0;
q1v0=0;
q2p0=0;
q2v0=0;

[A1,B1,C1,D1]=linmod('PendulumOnCart_ModelMechAI',[0,0,0,0]', [0,0]');

if ~all(all(C1(1:4,:)==eye(4))),
    S=C1(1:4,:);
    A1=inv(S)*A1*S;
    B1=inv(S)*B1;
    C1=C1*S;
end;

disp('')
disp('-->> Computation of matrices of a linear approximation of a uniform')
disp('     state space model of the mechanical part of a pendulum on a cart');
disp('     at the unstable equilibrium point on the basis of two') ;
disp('     a Simulink mechanical model');
disp('')
disp('-->> A3,B3,C3,D3 are associated to a model obtained on the basis of symbolic computations in Mathematica');

[A3,B3,C3,D3]=linmod('PendulumOnCart_ModelForLinmod',[0,0,0,0]', [0]');
if ~all(all(C3(1:4,:)==eye(4))),
    S=C3(1:4,:);
    A3=inv(S)*A3*S;
    B3=inv(S)*B3;
    C3=C3*S;
end;

clear S;

disp('')
disp('-->> computation of a gain F of a stabilising linear state feedback')
disp('-->> computation of a gain K of a cart positioning linear state feedback')
polyRoots=-5:0.333333:-4;
polyRootsK=-4:0.333333:-3;
F=-acker(A1, B1(:,1), polyRoots);
K=-acker(A1, B1(:,1), polyRootsK).*[1,0,1,0];

disp('')
disp('-->> computation of a gain F3 of a stabilising linear state feedback')
disp('-->> computation of a gain K3 of a cart positioning linear state feedback')

polyrootsF3=[-6, -6, -6, -6];
polyrootsK3=[-4, -4, -4, -4];

F3=-[0,0,0,1]*inv([B3, A3*B3, A3*A3*B3, A3*A3*A3*B3])* polyvalm(poly(polyrootsF3),A3);
K3=-[0,0,0,1]*inv([B3, A3*B3, A3*A3*B3, A3*A3*A3*B3])* polyvalm(poly(polyrootsK3),A3);
K3=K3.*[1,0,1,0];


%F3=-place(A3, B3, polyRoots);
%K3=-acker(A3, B3(:,1), polyRootsK).*[1,0,1,0];

disp(['      F=', num2str(F)])
disp(['      K=', num2str(K)])
disp(['      F3=', num2str(F3)])
disp(['      K3=', num2str(K3)])

q1p0=0;
q1v0=0;
q2p0=pi/9;
q2v0=0;

disp('launch PendulumOnCart_Model_Control_1_Mech')
PendulumOnCart_Model_Control_1_Mech
disp('launch PendulumOnCart_Model_2_Control_M')
PendulumOnCart_Model_Control_2_M
disp('launch PendulumOnCart_Model_Control_3')
PendulumOnCart_Model_Control_3
disp('launch PendulumOnCart_Model_Control_4_SwingingUP')
PendulumOnCart_Model_Control_4_SwingingUP
disp('launch PendulumOnCart_Model_Control_5')
PendulumOnCart_Model_Control_5

