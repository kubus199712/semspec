close all;
clear all;

sim('PendulumOnCart_ModelsComparison');

figure(1);
subplot(2,2,1);  hold on; grid on;
plot(rvq1);
title("R�nica wzgl�dna pozycji w�zka");
ylabel(" ");
xlabel(" ");

subplot(2,2,2);  hold on; grid on;
plot(rvq2);
title("R�nica wzgl�dna pozycji wahad�a");
ylabel(" ");
xlabel(" ");

subplot(2,2,3);  hold on; grid on;
plot(rvdq1);
title("R�nica wzgl�dna pr�dko�ci w�zka");
xlabel("t[s]"); 
ylabel(" ");

subplot(2,2,4);  hold on; grid on;
plot(rvdq2);
title("R�nica wzgl�dna pr�dko�ci k�owej wahad�a");
xlabel("t[s]"); 
ylabel(" ");

figure(2);
hold on; grid on;
subplot(2,2,1);  hold on; grid on;
plot(q1);
title("Pozycja w�zka");
ylabel(" ");
xlabel(" ");

subplot(2,2,2);  hold on; grid on;
plot(q2);
title("Pozycja wahad�a");
ylabel(" ");
xlabel(" ");

subplot(2,2,3);  hold on; grid on;
plot(dq1);
title("Pr�dko�� w�zka");
xlabel("t[s]"); 
ylabel(" ");

subplot(2,2,4);  hold on; grid on;
plot(dq2);
title("Pr�dko�� k�towa wahad�a");
xlabel("t[s]"); 
ylabel(" ");
