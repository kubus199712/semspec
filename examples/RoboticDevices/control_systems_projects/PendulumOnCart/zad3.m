close all;
clear all;

i = [0 0 0 0];
j = [0 1 0 0];
k = [10 10 10 10];
l = [5 5 5 5];
b = k;
sim('PendulumOnCart_ModelsMechComparison');

% figure(1);
% subplot(2,2,1);  hold on; grid on;
% plot(rvq1);
% title("R�nica pozycji w�zka");
% ylabel(" ");
% xlabel(" ");
% 
% subplot(2,2,2);  hold on; grid on;
% plot(rvq2);
% title("R�nica pozycji wahad�a");
% ylabel(" ");
% xlabel(" ");
% 
% subplot(2,2,3);  hold on; grid on;
% plot(rvdq1);
% title("R�nica pr�dko�ci w�zka");
% xlabel("t[s]"); 
% ylabel(" ");
% 
% subplot(2,2,4);  hold on; grid on;
% plot(rvdq2);
% title("R�nica pr�dko�ci k�towej wahad�a");
% xlabel("t[s]"); 
% ylabel(" ");
% 
% figure(2);
% hold on; grid on;
% subplot(2,2,1);  hold on; grid on;
% plot(q1);
% title("Pozycja w�zka");
% ylabel(" ");
% xlabel(" ");
% 
% subplot(2,2,2);  hold on; grid on;
% plot(q2);
% title("Pozycja wahad�a");
% ylabel(" ");
% xlabel(" ");
% 
% subplot(2,2,3);  hold on; grid on;
% plot(dq1);
% title("Pr�dko�� w�zka");
% xlabel("t[s]"); 
% ylabel(" ");
% 
% subplot(2,2,4);  hold on; grid on;
% plot(dq2);
% title("Pr�dko�� k�towa wahad�a");
% xlabel("t[s]"); 
% ylabel(" ");

% figure(1);
% hold on; grid on;
% plot(q1);
% plot(q2);
% plot(dq1);
% plot(dq2);
% title("");
% legend("q_1(t)", "q_2(t)", "q_1'(t)", "q_2'(t)");
% xlabel("t[s]"); 

figure(2);
subplot(2,1,1);
hold on; grid on;
plot(vq2);
legend( "diff q_2(t)");
title("");
ylabel("");
xlabel("");

subplot(2,1,2);
hold on; grid on;
plot(vq1);
plot(dvq1);
plot(dvq2);
title("");
legend("diff q_1(t)", "diff q_1'(t)", "diff q_2'(t)");
xlabel("t[s]");
ylabel("");
