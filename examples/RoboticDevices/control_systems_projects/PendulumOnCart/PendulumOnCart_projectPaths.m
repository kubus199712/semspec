% This script add all the subdirectories of this project to the Matlab
% search path. It should be launched first but only in a local directory.

currentpath=pwd;
if  length(pwd)-strfind(pwd, 'PendulumOnCart')+1 == ...
        length('PendulumOnCart'),
    PendulumOnCartPathRoot=pwd;
    addpath(PendulumOnCartPathRoot);
    addpath([PendulumOnCartPathRoot, '\data']);
    addpath([PendulumOnCartPathRoot, '\deployment']);
    addpath([PendulumOnCartPathRoot, '\functions']);
    addpath([PendulumOnCartPathRoot, '\models']);
    addpath([PendulumOnCartPathRoot, '\prj']);
    addpath([PendulumOnCartPathRoot, '\scripts']);
    addpath([PendulumOnCartPathRoot, '\bin']);
    addpath([PendulumOnCartPathRoot, '\lib']);
    clear PendulumOnCartPathRoot;
 else
    disp('error: the current working directory is not suitable');
    disp('       for running the script PendulumOnCart_projectPaths.m');
 end;
 clear currentpath;