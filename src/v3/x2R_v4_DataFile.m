% Simscape(TM) Multibody(TM) version: 6.0

% This is a model data file derived from a Simscape Multibody Import XML file using the smimport function.
% The data in this file sets the block parameter values in an imported Simscape Multibody model.
% For more information on this file, see the smimport function help page in the Simscape Multibody documentation.
% You can modify numerical values, but avoid any other changes to this file.
% Do not add code to this file. Do not edit the physical units shown in comments.

%%%VariableName:smiData


%============= RigidTransform =============%

%Initialize the RigidTransform structure array by filling in null values.
smiData.RigidTransform(9).translation = [0.0 0.0 0.0];
smiData.RigidTransform(9).angle = 0.0;
smiData.RigidTransform(9).axis = [0.0 0.0 0.0];
smiData.RigidTransform(9).ID = '';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(1).translation = [62.731092414965474 -458.8535412846386 -4.2188474935755949e-14];  % mm
smiData.RigidTransform(1).angle = 2.0943951023931957;  % rad
smiData.RigidTransform(1).axis = [-0.57735026918962584 -0.57735026918962573 0.57735026918962573];
smiData.RigidTransform(1).ID = 'B[Link_drive:2:-:Link_drive:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(2).translation = [-37.268907585034526 441.1464587153618 5.6843418860808015e-14];  % mm
smiData.RigidTransform(2).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(2).axis = [-0.57735026918962595 -0.57735026918962584 0.57735026918962562];
smiData.RigidTransform(2).ID = 'F[Link_drive:2:-:Link_drive:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(3).translation = [62.731092414965431 -458.8535412846386 -4.2188474935755949e-14];  % mm
smiData.RigidTransform(3).angle = 2.0943951023931957;  % rad
smiData.RigidTransform(3).axis = [-0.57735026918962584 -0.57735026918962573 0.57735026918962573];
smiData.RigidTransform(3).ID = 'B[Link_drive:1:-:base:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(4).translation = [89.069434774125597 100.00000000000011 -88.032378705016924];  % mm
smiData.RigidTransform(4).angle = 2.0943951023931957;  % rad
smiData.RigidTransform(4).axis = [-0.57735026918962573 -0.57735026918962562 -0.57735026918962573];
smiData.RigidTransform(4).ID = 'F[Link_drive:1:-:base:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(5).translation = [12.73109241496547 -508.8535412846386 -2.886579864025407e-14];  % mm
smiData.RigidTransform(5).angle = 4.4408920985006262e-16;  % rad
smiData.RigidTransform(5).axis = [0 -1 0];
smiData.RigidTransform(5).ID = 'AssemblyGround[Link_drive:1:Link:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(6).translation = [52.731092414965481 441.1464587153614 -3.3306690738754696e-14];  % mm
smiData.RigidTransform(6).angle = 1.5707963267948966;  % rad
smiData.RigidTransform(6).axis = [0 0 1];
smiData.RigidTransform(6).ID = 'AssemblyGround[Link_drive:1:drive:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(7).translation = [12.731092414965488 -508.8535412846386 -2.886579864025407e-14];  % mm
smiData.RigidTransform(7).angle = 4.4408920985006262e-16;  % rad
smiData.RigidTransform(7).axis = [0 -1 0];
smiData.RigidTransform(7).ID = 'AssemblyGround[Link_drive:2:Link:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(8).translation = [52.731092414965488 441.1464587153614 -3.3306690738754696e-14];  % mm
smiData.RigidTransform(8).angle = 1.5707963267948966;  % rad
smiData.RigidTransform(8).axis = [0 0 1];
smiData.RigidTransform(8).ID = 'AssemblyGround[Link_drive:2:drive:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(9).translation = [-96.741407190300464 -69.552276765472513 -50.000000000000135];  % mm
smiData.RigidTransform(9).angle = 1.5707963267948966;  % rad
smiData.RigidTransform(9).axis = [1 0 0];
smiData.RigidTransform(9).ID = 'RootGround[base:1]';


%============= Solid =============%
%Center of Mass (CoM) %Moments of Inertia (MoI) %Product of Inertia (PoI)

%Initialize the Solid structure array by filling in null values.
smiData.Solid(3).mass = 0.0;
smiData.Solid(3).CoM = [0.0 0.0 0.0];
smiData.Solid(3).MoI = [0.0 0.0 0.0];
smiData.Solid(3).PoI = [0.0 0.0 0.0];
smiData.Solid(3).color = [0.0 0.0 0.0];
smiData.Solid(3).opacity = 0.0;
smiData.Solid(3).ID = '';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(1).mass = 27.495840372908869;  % kg
smiData.Solid(1).CoM = [89.06943477361915 50 -88.032378705016967];  % mm
smiData.Solid(1).MoI = [113638.93688549021 181451.47327845692 113638.93701448146];  % kg*mm^2
smiData.Solid(1).PoI = [0 0 0];  % kg*mm^2
smiData.Solid(1).color = [0.019607843137254902 0.019607843137254902 0.019607843137254902];
smiData.Solid(1).opacity = 1;
smiData.Solid(1).ID = 'base.ipt_{4D369A74-44CE-9AEC-9444-238B7AB31D12}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(2).mass = 0.0092199114857512594;  % kg
smiData.Solid(2).CoM = [3.1566279678185291e-14 500.00000000000006 1.9239647463853933e-10];  % mm
smiData.Solid(2).MoI = [675.25935458054516 15.546898912896371 675.07897481056784];  % kg*mm^2
smiData.Solid(2).PoI = [0 0 9.8953023552894625e-14];  % kg*mm^2
smiData.Solid(2).color = [0.8666666666666667 0.8666666666666667 0.050980392156862744];
smiData.Solid(2).opacity = 1;
smiData.Solid(2).ID = 'Link.ipt_{CCABF4DF-4297-A74C-40AB-C5B875A4A642}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(3).mass = 22.619467105847484;  % kg
smiData.Solid(3).CoM = [3.1370415521323871e-09 39.999999999999993 0];  % mm
smiData.Solid(3).MoI = [17153.09489842294 10178.759536093499 17153.096217241167];  % kg*mm^2
smiData.Solid(3).PoI = [0 4.5474735088646404e-12 0];  % kg*mm^2
smiData.Solid(3).color = [0.039215686274509803 0.039215686274509803 0.039215686274509803];
smiData.Solid(3).opacity = 1;
smiData.Solid(3).ID = 'drive.ipt_{29C07E9D-414E-FFB5-41F7-78A04D5DE0D5}';


%============= Joint =============%
%X Revolute Primitive (Rx) %Y Revolute Primitive (Ry) %Z Revolute Primitive (Rz)
%X Prismatic Primitive (Px) %Y Prismatic Primitive (Py) %Z Prismatic Primitive (Pz) %Spherical Primitive (S)
%Constant Velocity Primitive (CV) %Lead Screw Primitive (LS)
%Position Target (Pos)

%Initialize the RevoluteJoint structure array by filling in null values.
smiData.RevoluteJoint(2).Rz.Pos = 0.0;
smiData.RevoluteJoint(2).ID = '';

smiData.RevoluteJoint(1).Rz.Pos = 0.0;  % deg
smiData.RevoluteJoint(1).ID = '[Link_drive:2:-:Link_drive:1]';

smiData.RevoluteJoint(2).Rz.Pos = -pi;  % deg
smiData.RevoluteJoint(2).ID = '[Link_drive:1:-:base:1]';

