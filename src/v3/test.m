close all;
f = diff(simout.Data);
t = diff(simout.Time);

a= f./t;
a = [[0]; [a]];

p = polyfit(simout.Time, simout.Data,7);
x1 = linspace(0, 1, 187);
y1 = polyval(p,x1);

df = diff(y1);

figure
hold on
% plot(simout.Time, a);
% plot(x1,y1);
% plot(simout.Time, simout.Data);
plot(df);
