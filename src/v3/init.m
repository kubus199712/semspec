clear all;
close all;

l1 = 0.89;       % [m]
l2 = 0.89;       % [m]
m1 = 22.619467105847484;    % [kg]
m2 = 22.619467105847484;    % [kg]
g = 9.80665 ;       % [m/s^2]

Kd = 1000;
Kp = 100000;

Ke = 500000;
Be = 0.1;

% --- Impedance Controller ---
M_t = 2;
D_t = 800;
K_t = 8;

Xe = 1.3;
% --- Warto�ci pocz�tkowe (rad) dla DM ---
q1_0 = pi;
q2_0 = 0;

% --- warto�ci pocz�tkowe (rad) dla AI ---
q0_0ai = pi;
q1_0ai = 0;

ts = 0.1;
t2 = 1;
t3 = 2;
t1 = t2 + t3;
time = t1/ts;

% x_d=linspace(0, 1.0, time );
% y_d = linspace(1.8, 0.6, (t2/ts) );
% y_d = [ y_d, 0.6*ones(1, (t3/ts) )];
% x_tr = timeseries( x_d ); 
% y_tr = timeseries( y_d);

x_d = linspace(0.9, 1.5, time);
x_tr = timeseries(x_d);
y_d = 0.8*ones(1, time);
y_tr = timeseries(y_d);
x2R_v4_DataFile;

sim('impedance_control_AI')
% sim('tracking_performance')

figure
hold on
grid on
plot(x_d, y_d)
plot(x_r, y_r)

