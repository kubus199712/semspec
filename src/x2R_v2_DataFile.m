% Simscape(TM) Multibody(TM) version: 6.0

% This is a model data file derived from a Simscape Multibody Import XML file using the smimport function.
% The data in this file sets the block parameter values in an imported Simscape Multibody model.
% For more information on this file, see the smimport function help page in the Simscape Multibody documentation.
% You can modify numerical values, but avoid any other changes to this file.
% Do not add code to this file. Do not edit the physical units shown in comments.

%%%VariableName:smiData


%============= RigidTransform =============%

%Initialize the RigidTransform structure array by filling in null values.
smiData.RigidTransform(5).translation = [0.0 0.0 0.0];
smiData.RigidTransform(5).angle = 0.0;
smiData.RigidTransform(5).axis = [0.0 0.0 0.0];
smiData.RigidTransform(5).ID = '';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(1).translation = [-50 50 8.8817841970012539e-15];  % mm
smiData.RigidTransform(1).angle = 1.570796326794897;  % rad
smiData.RigidTransform(1).axis = [0 1 0];
smiData.RigidTransform(1).ID = 'B[Link:2:-:Link:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(2).translation = [49.999999999999986 950 -2.6645352591003751e-14];  % mm
smiData.RigidTransform(2).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(2).axis = [0.70710678118654757 0 0.70710678118654746];
smiData.RigidTransform(2).ID = 'F[Link:2:-:Link:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(3).translation = [89.069434774125597 100 -88.032378705016967];  % mm
smiData.RigidTransform(3).angle = 3.1415926535897922;  % rad
smiData.RigidTransform(3).axis = [-4.3668196507142413e-16 0.70710678118654746 -0.70710678118654757];
smiData.RigidTransform(3).ID = 'B[base:1:-:Link:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(4).translation = [49.999999999999986 950 -2.6645352591003751e-14];  % mm
smiData.RigidTransform(4).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(4).axis = [0.70710678118654757 0 0.70710678118654746];
smiData.RigidTransform(4).ID = 'F[base:1:-:Link:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(5).translation = [49.921332309892385 700.00266543874909 89.584733773026613];  % mm
smiData.RigidTransform(5).angle = 1.5707963267948923;  % rad
smiData.RigidTransform(5).axis = [1.2088335980370731e-16 -1.242651419659962e-16 1];
smiData.RigidTransform(5).ID = 'RootGround[base:1]';


%============= Solid =============%
%Center of Mass (CoM) %Moments of Inertia (MoI) %Product of Inertia (PoI)

%Initialize the Solid structure array by filling in null values.
smiData.Solid(2).mass = 0.0;
smiData.Solid(2).CoM = [0.0 0.0 0.0];
smiData.Solid(2).MoI = [0.0 0.0 0.0];
smiData.Solid(2).PoI = [0.0 0.0 0.0];
smiData.Solid(2).color = [0.0 0.0 0.0];
smiData.Solid(2).opacity = 0.0;
smiData.Solid(2).ID = '';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(1).mass = 72.376305163147364;  % kg
smiData.Solid(1).CoM = [3.1566279678185291e-14 500.00000000000006 1.9239647463853933e-10];  % mm
smiData.Solid(1).MoI = [5300785.9334572796 122043.15646623647 5299369.9522629566];  % kg*mm^2
smiData.Solid(1).PoI = [3.1985109671923235e-10 6.8539520726047428e-11 7.7678123489022248e-10];  % kg*mm^2
smiData.Solid(1).color = [1 0.50196078431372548 0];
smiData.Solid(1).opacity = 1;
smiData.Solid(1).ID = 'Link.ipt_{CCABF4DF-4297-A74C-40AB-C5B875A4A642}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(2).mass = 27.495840372908869;  % kg
smiData.Solid(2).CoM = [89.06943477361915 50 -88.032378705016967];  % mm
smiData.Solid(2).MoI = [113638.93688549021 181451.47327845692 113638.93701448146];  % kg*mm^2
smiData.Solid(2).PoI = [0 0 0];  % kg*mm^2
smiData.Solid(2).color = [0.019607843137254902 0.019607843137254902 0.019607843137254902];
smiData.Solid(2).opacity = 1;
smiData.Solid(2).ID = 'base.ipt_{4D369A74-44CE-9AEC-9444-238B7AB31D12}';


%============= Joint =============%
%X Revolute Primitive (Rx) %Y Revolute Primitive (Ry) %Z Revolute Primitive (Rz)
%X Prismatic Primitive (Px) %Y Prismatic Primitive (Py) %Z Prismatic Primitive (Pz) %Spherical Primitive (S)
%Constant Velocity Primitive (CV) %Lead Screw Primitive (LS)
%Position Target (Pos)

%Initialize the RevoluteJoint structure array by filling in null values.
smiData.RevoluteJoint(2).Rz.Pos = 0.0;
smiData.RevoluteJoint(2).ID = '';

smiData.RevoluteJoint(1).Rz.Pos = -78.642652679687075;  % deg
smiData.RevoluteJoint(1).ID = '[Link:2:-:Link:1]';

smiData.RevoluteJoint(2).Rz.Pos = -50.553258522037268;  % deg
smiData.RevoluteJoint(2).ID = '[base:1:-:Link:2]';

