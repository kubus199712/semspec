(* Content-type: application/vnd.wolfram.cdf.text *)

(*** Wolfram CDF File ***)
(* http://www.wolfram.com/cdf *)

(* CreatedBy='Mathematica 11.3' *)

(***************************************************************************)
(*                                                                         *)
(*                                                                         *)
(*  Under the Wolfram FreeCDF terms of use, this file and its content are  *)
(*  bound by the Creative Commons BY-SA Attribution-ShareAlike license.    *)
(*                                                                         *)
(*        For additional information concerning CDF licensing, see:        *)
(*                                                                         *)
(*         www.wolfram.com/cdf/adopting-cdf/licensing-options.html         *)
(*                                                                         *)
(*                                                                         *)
(***************************************************************************)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[      1088,         20]
NotebookDataLength[    418787,       7901]
NotebookOptionsPosition[    389385,       7293]
NotebookOutlinePosition[    415828,       7857]
CellTagsIndexPosition[    415715,       7851]
WindowTitle->Robot Manipulator Workspaces
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 ButtonBox[
  GraphicsBox[RasterBox[CompressedData["
1:eJztvXmUXkeRL8iZ0UMt1JJbkqXa901Vqk1VqlW1qvaqT7IWy7a8gXfMYoNt
NhuwjVmMbfBuoIEGGrqb1TS8bppme9ONbbaemdfs58x7/aaB/vOdxzJ/cyf3
jIiMvN+tkizJdnJOcL+bGRnLLyIj0jctuenaW4/f+L+97GUvu/1PxP8dv+Yt
s7fdds3bTvyZeDl5y+0333TLDdev3HLHDTfdcNvotf+7GGwX9NWXv+xlm8Tz
2dm2/5noTFNrokSJEj3PdK7rXKJEiRIlSpToLNEfDZ1rOxIleqHRH59N+ydR
oiL0xxw617YlSnSuKG9fbGSvrFdeokQvBjqj+/L/nm/LEp19+r8SvSToXOdZ
ohfePi8uv/2c+5coUaJEiRIlSpQoUaJEiRIlSpQoUaJEzzf9t5W9ic44dZSn
5USJABXJmUQvKPp/ToNkHfnvhagjUaJEiRIlSpQoUaJEiRIlSpQoUaJEiV7U
9D9WOv5nokSJzj39W6IXDf13hv7bcj5B3n8Dzxid63xNlChRokSJEp01+qOh
c21HokQvNPrj/0j7J1GiIvTHHDrXtiVKdK4ob19sZK+sV16iRC8GOqP78veX
jmR59AdAvye/A7oEUEROEd7o/CUxGka8f2ConG9nkngsh3MoPwbrX0fn8nTn
yR1ep/6N6iknY714blT/6dhZLobF99lLk0ZzxvXc/4fGxsDcuaJiNeB3l3j6
rahVHEEev3a0bG6cbt7lkqqrIf1B2PgHpgb/wZDis/siIiNRokSJEiVKlChR
okSJEiVKlChRokSJilJvU6uglqxHPHvEs7fRUw8hxceNmzkpq0/Ja3Vj3Y3N
gqgcwdcseJpbnX6lr8nKs+OalF2C+uw6s0a+97e0ibF2RYq3UVKzo+4m+WzK
uhtaFPU0tCr5kq9bUYt+NjSD3y3md4vhs35YeVqm8qcBkOHtMTzdwA6pV9vW
Yp5AruRv8rJ7JUE9ak5j093UCvBsDsnIsu+9ys5mpNf63uP88zjY9VJ/r5Nr
8GgwPG6txUTa2GTi5+1DtsrfKpZmbVMLsrlbxqgJ2qmfvc7PJkMAe8lnsTdy
lT6JdYPR3eDzSj1djnmssR3GxibvZ3eTiSW0s6kloO7GFrdO+27Xm5ywfPa3
me8FcXE6G1tNrmofuhtbzb7we8iv93p7G7xMuKdcvJt8XsM9qf2G72CN298t
fk+SfLL1w8tqNTE3skB8um0tQfWk2dgM8kPu8fqmrFdQd3Nb1tnUlnU1tKn4
dzXVZvua6rJ9zQ2KupubdGwM6XyU8WrU1GTnzO8mz6OpKdun5vQ8/K1lQPnm
XcmBsvX7viatc1+jl7VPPhsaFHWBZ5fi088utwbY0IifWpbk1e9+XZPRaWyy
OoFMt74Rj2ubm5xMK3cfWqNldomx1pa6rKOxTvA0mLkGlavtgtpEnLoE9dTJ
uDWaGgH3bZOvU6Rm4ZrT7GsFyENfW2BOwZqv49oL5ducsHVQ1oaGVl+zXQ1v
NXXa1/OgxscI8QK7G1r48UbwDvXG5hEf3rtordPTim0LbKW+Qn9a8TrnS2sW
4IQI6ozoDnS2MvZF5NLxQCbEiVmL8Gjl+Rpaw3mqJ8/nmL3Q7yAO1DbKT3CH
mDF47KmozXbvEVRRp57yXY2hZx0a22P59tTp33vw+G4jE9tCcq6B2NRAcQS5
y+UB8quVYEflMLHncKT2BXKoP3nyYvnQSuzg4sX4Qfc61QPjyuZxSxZiRWxH
e5fmG4M1W0NI3eHqVaxuwvNinv1BnpM9RXGndZe1i8k5uo9pHUL4M090hqc+
MPGl+zyo661YLu1ByH6aM4zfAWa0TnB1KBYPuo7mDmc3tZfuIy4HWiPyaNzh
Oo6PyXF2P1FdMI/Jb1rb6D/HcbKC3OEwpzJaQplcXeVqEK2JXB8O9rvmb65t
UtRiqLm2OWupa3Ljdq65rhnz1MJ3w1dnxuv0by+30VOdfgZ7Sdgme43rW7YX
BX2rNuxboMehXgb6lu1ru21/g2vBmOI3vdHZs8eO1/p+6nTUYT0VIe02fjje
Pdwc8GuPl2dx0JjUOTv2AD6sry6wmWK0e09diCnCBp8bXBz21HmbKhjd9Kyx
J8TF+0TOIBFfcFwALiBPsA847ruJPOi7G3P664BtBD8m/6jdu4291CY+l+qI
/LrQR7gPbHw4G0gM4LnPykGYsvvJrNmDfbYyUt9Kfet86lusPvb8S+2hNhJf
kXwu1zgbcmILz6I05+g83QuBzbFcpvoLUCPBi+YRHaf7n57/g/rA5Cv1k9aK
IGYE8wAfJj+CfOTsZfZKbN9zMYnVBiqXjgcySb6wMWByHdneGs5TPYxt+rwm
z3X+bNdc12TObk3obOefzeRcB6kZrGsO5+uazLmPwaOR+EljEY2Vx27Tzsrs
P+2syDbtkE9BOyrUOyQ5t0n+tnOW142B3zvsvH/fpGRUgDG93usG+sAaZAeQ
vcnasIOMwzG0psLbDwnKRrbZOS9zk5JRiW2FawCfl13p56DNOz0mDhuHf8yP
SoJhiNOmwBcgGzw9fpWIb9POSoyBtWVnJdZFcPcybFwxPvSdjQN9ulzEtuPc
BGM7PP8mYpfPWRpzn6ebgD8WZ5y3HoNNYO2miDzkB/EP5pDDd6e3AeYq1L/J
rbc5VYnsDfdQpcsxZDubT35feR4as0on1/tWSTCFdlQSH4g8tx77D2MF1/r8
JLlJcwv6gXKEiUOwX/07xiuS92Cf4L3v4+t92KNp5x499mdyXr/LcWvvJoDP
JmafBnsA2sntJSgj2D94DwT+Avw2gXyP4RLaAWwP6ovFuDKwM6yj/vcmGNto
/vF5sQn6Q2stGPc+kvyhNc3yQltg/yM5FYxbn9yT7gWSc7SWcXEnsd+0g66p
DP1g6tR/IvuN6kH1aCeMpfFjB6w/MI9pnYTvXDwIXhE+HB9+z3ibsWx0piA5
j+ojzKudZD/CHANxhTke1DiUSxwepB/AXkzrGrSTYrQT60nnPsqfzn3p3Idj
BOWnc1869wX5WPjcB2TvILHb4fPqpX7uS32LyAtiwsQ59S1kb+pbWH7qW6lv
BfmY+hapN2RPgHzE9YXvW3wd9b/T94pYzlMdYB+gNZWhH+l7BfCxMvDxhfS9
wu9Vu5fo+YzDsBLpe/muKkGV2WZBL9+pn+q3HN9ZZcY1j+XV/FXoKdcqPrsG
ycXrN3PjO70Nbp1bU4X17axyfHTeynTrd3oeqMv7R9dWEX4haydcV4l0Uzv9
GJYBMfa4Ab+h3J1g7U5vp8eO+Oj4vE2boZ+7iP3Eh5cTmTY2m+Fah7u3C+lH
PoKcsDhb25yMqsBuhwebAziWzl6ylvoJY7EZ2VhJfCI5AnRvJusxD58ziN/u
DZBzYX4xvsJ821nJxBPHKNh3Edoc7AmA205fB/ye5vKy0sQylgOhvWivBXuH
xozuFYzRZmgv2fu+rnC5E+7FlxO73D4C8jejuhLmI6pjO+FaUWNlnVXv4LnD
jkMCPLsqvOyduF4424AfOM8pXjCmYa5vJnxuHOhwe4DpBSgvIA6BDXwswj0F
7aB1DvJWMfsb1AXoa+A3yZed/n3zTuxHWItIbTU1azMdA7WVy/XNARaRvY/y
jcSTJcZGhwH5Hchn6hrEB+0t2C/CuAU409yk/TZmC8QD8dH6A2xF/YXWAygX
943Nu7BNbh/TnI30z1gPges3o3MW2G9cnoO8pLjRPr+Z2ITrou0XGIN07mPy
0Pqazn1gT3m70rkPxz1cR/cujCXNL8bXdO5DuRnE5AVx7uPyhc7RGNB991I7
91Gs4N7BmKe+RWNK6wrcvzDfsR2pbzH5mfpW6lu7Ut8K7aFU5TFH++6l1rcw
Pul7BZMzHL55NjoMyO9APlPX0veKMGcj/TPWQ+D65/t7hdvnATY0p5lYGF1/
cmG1pt3kSUmO764C81W567bQte5ZFY7DsQvL8O6GOqq0TWDdZisv4K/i9URo
C2dPDJvAHwaP3UAmmtN2bXa+MLQ7PreF8nHrAR5bjP4t1BeEI11fSZ5a15bd
4DeLbZXTA/H0v2scNkG8oS0m97bk+LhlN7YtigOHXSSuW6LxJDHZzchk3rdQ
u6J7JxJT7jd8sn4TzIFOn5NwbSUjA4wbvs3QZxqb3RRfLl6AZzfNY2avERlb
ONuK7HGujhTc2xqvKrKe6iO+7jJkbRS/t+wC4zGya8vl8m6Cc04OlK1FuyHp
OGxhMavC/BFcy9XIQDbqLwVklavR4H0LqnlVfH2Hc5H4bjHzwXowxtd5Ml4E
J4JVvB6RPM2xDdV5ru7s9lhxcqL1hMsLKCfYz4wszgdm3RaqH46tN2eo7YI2
R+ITtcnOU8w4Plhznc2mTwY9OIJRRG40TnmYpHNf3H6Yy3nrNnzuM3bnYl/A
zgLxLXbuo1RFnkQG7Idk3Zk79+Xn8JacelY2xmfk3MfnzZaYPCgzd+9E/OL6
bY7f5c99NNaRHDB8m8m5KTwfVOXii/KCq5c5fSLEufi+fv7PfRGbQe3M66fl
coO1i9GRm/exWrQb0nlw7qM8G8Ep9a3Ut1LfAjalvhXkdupbcZtT3wpxD/I3
fa+I+ZK+V+C59L1ifXjy/LgmbQHvr9hTI6jWPO1vTVvRew14r2HWce8cQVkh
UZ28jpjecM1W8uTXh3NbGT+3sjr9O53fCrB8xR6B9W4xvrs6h4QO8Fs+txp6
BRnH/DX5svcAHoeHjX01JlZXTZmxamyHk1vjZXK6HI/BaLe1DWC2B8rHvm6N
2lVjsPF8IS+3thrZvdXhVG3eKU4YCxq7cvq2gvU4xnwc4nlA9UD/oS7w7vyI
YQnlcHHGT97+OM5bif1by8op5zvNeZiDcSq3p8v/hjkB/cW+bwH0uqvHBI1n
bV3N6p9nXmH6whYSo7x9vXV3GItYDrlxk9P59S2/pluera6+xWRA4sZqzf6K
68+PDR2neryfW6N8NcwaaBfFKq+/8fopZvH8w/qpDzE7wvXlYuvf+R6M125l
dfF25+816AO2aWuAWciLYxMf9/EO44rzFvd6Gu+tRDaWUS4n8zCI2V50fThe
LBZc/UrnPjp35s99xfYGjWGYE2HO5PskqILmXFhb8vOsXM5w9b5crjC4B+e+
WG7HY4R9hPs6VufjeRzWg3x84vUgr65xuVNkz8T2NvWf1lDeVh4fWudoDShn
fxxn2gO3lpVTzvdya3g6/XNfzN/YnozVqFpCeTJgXSifQ9wZ5vw+9+XhxmPI
17aYXxvPp9S3eP2pb6W+VWzPxPY29R/qonuZsyWWKzTO+Jn6FvW3XP2lNSqv
1vN2v3j7VpHYxPCsJTLS94owLul7Be0jL67vFZy+cF+EWPu1f1pRl/1pZV22
rdI8zbuiCvBewbxXkt8VtX4MjfuxbfBZQeRUhnwsRdawcxVY5zbKA22I+WYx
YmRT/DBfraYKQiIG28i7fG41BMe3EvpTGcMKMAbWB09D2/b45zZqi+OvCdZb
e5ANVDfjR/xd69gG9Ch7KE7mt7V1m5W1h9jG2RDBFPGWtRPbaGkb4PM41qD1
W/f4p8MrTxczF+QG9cX5U7MxuXswv8V3K9BD89H9JnGIyZa20bVbIU4UF5ir
e2pZ/z3VGB1Ejo0TxUPlV42x0e5POwfqmh2P1RipV8xvMxTYCX5rf2tMjHwf
lvTX95eyf/74yezfvnp1NjfTo+9Z1d7AT4cfxZylMBe2AUI22v0W9ZPUaKaW
lqvHqJ/QOs/VUa4e0x4UqetYZy14rw34txW2oZbhrQV9pDajvrL2c73N+AT7
UZ5dtCf7Z63JR8YORifbr8vFtyKc21YRGef0MnPRvKoAMXR7l/ps+Wo91rF8
LZOfUV8pZhW8zSFmtaFuknPs74K5E41v9NxSG6yj56507svDj8Emgh/Lx9hI
awWNzTYrh9A2uBdidkfydxvVizCrja6n+ysat7xaavOjgtlPkbxnc4XJu22E
orIL2xnmPa2/aK9HaljRGsT5QzHhaxTXmwrI5XAmeoJakZcLrOyw5gS9l9PF
xZjmM8WcwyqYr3WygxpWUVfw3Af7fk5OVYT864kPxCCW++E+rmXl5Z51yoyd
s3Of8zn1rdx8y6v7EKNYPafY5tnHxb8ijE3qW6lvoX0XiVnqWxGsgvnUt14w
fYvqjNjI51H6XhHTyeZnufgytTp9r+D9P+++VzC+5MYB+HFBVUO2XdAF1fXZ
9mr51O/bq+r9bzlepefsU82bue3uKWX4dUpmFZg3726+CjyrwdPyQr1gXttm
9Lhxo1vJqzf68Fq7xq7bDuy4AOi9oArIBrbANduBvO3VGBuln1JlfbbNPgX2
erxWkI7PBXLMxGq72evbzHO72QfbHQ/4XQV/1xl5oW5H6l3r1/x23K/bVqnn
tOxad0aVJG3Zbt/t/jJ6rY5tyGczT22QGJv3C8S8xqxOjV1Q5ce3V3nbNG6+
Lnt/DSbG5mDcYKp8qap1mNk4bDP+W10ek3qPiYsdHtdY1jo/rT4UP2uD4ql3
ZHGAeDl/nbw6977dyfYx3w54toM1MDbboB/IZ+gbzD2DlY13BdBn413pbUT2
VdVhn51cOA50VGE/nM1urN7Z7/B1eMM4WvsgtnU6t8z8BZUmv2xu2VxzvDbn
SC5Ueb4LIC+Kl7UPjIG9s93uH+C/vMv6yDuXxXiNHq8AGNt9Zvh9/tR53+2+
sraCfbwNxBruPeeH+X2BrdnmqWt0Pa6RsP7Begzqpa/LDaZ++vq9HZLtAbbH
VPn6fYHRbXm2u3pd7+t30HNAz6r2tZ/2AWq3k+N0NYB+Up9tp/Yb+6y9FwA7
bT9EPQza5zAGa6HdpN+gvgZxg70O4ONkxfoV6eN+DGKFMYDYuziRPgdzA/fB
+sg4PBeQ/l5VD/z3eeBjH/pwAbKr3mPJ4LvdjRE9IIbYL5IvsO/D/g9y3mNG
5FQR25kc9nPgDOX2F58Xbv9VY7tgPju/INFYIP/Sue+0zn1k3yEfq0A9M/L+
DO5dUEehfxfA3zDudN8yNQ7HvN6vJbGCMfe1GOc84qki/lcRnFBc8PwFAY54
j/t41QPMwV5GsaxHsQr3je1F3h6Y41AXzrXIfkP7Aca7Htvo6gPJO1IfUP4w
fXZ7NcEJrid1AdU6KwvuW2YNrgM+7nY/0fiiOIK89Xu7PrDP84AzFKopIGeq
QO6RGuPxIb7Q/HJ9w59t/D6g+V4PYgn2XxXxEeJf5fXQ+gbPArhXhDHfDvEN
MAN2o/yha+qRX/AcRvXDXn9envtgTaH1qwri5rFPfSv1rdS3YK7hvZ76Fs7n
1LcakD2pb4EYg7xaV99yPtQHvoU9B9ej9L2CxKsa44XrM8QKY5C+V5B8AfUJ
9a3z8XuFjS3Te1C9o/sA5NjO2qZsR01jtqPWkPotx5r0sxa827EaPa7WImr0
v51MvCbkbcQyapi1gna6cUYXWovnd8b8Iet21hA5yBY41oifTqbEA2Ao6M8U
NehntT77qacl+i7iEj7ryJPy13s5VXh+h6FAVw2k+vA3klUP9NSB9zpiq9XH
yHO/6zEe5vcOiFPwbPQ2BZjVl8ckIDtXb2yud3IVVs7GeoxTNfCB+M1hH8a5
Hr9XgdhAveX8ojhUNRB76jHFYgDzwNqOfPBx3oFk5thY1QB4IdYEC4pH1J8c
TKke89wR5LfJoWqfUztcTH1+qT0r1u60vxHpcZ+POn993gJ8qyCe3k57ZwTp
b95/WJG9a7vA3aVBvOtwnpJasYPLxep6krPe3x0GC1kXVW2UY7UNoJbD2svU
6KCOg7EaprYiPq4ncHq5dUyvqQG9wb03EnnUn4idNZzeJoJLpH8EfS/PL9sn
DG8NtTXSc53tTF+O9arA3ogeiCewfyfVjfTG5OTp5eO8M7YOxBPxUIyDuMfy
AWLF5EPgrz1DYNugLTvR2cnMQf4acAaJ5TfFB8Wd+kd57DmsXDxATjs707nv
zJ37IpjQWAb7M5K7OfslqAVkP+yEOVpTTicXP6KXxb+R6IvFqJGRBfcR5zuf
t/x+iWDCxQPVEx8f3g6av7Se8Njza3GeRutdnl/ReNJ40RpPY8nYHolv2Psi
NtY0+X/2QVgTLKJnB+pPDqZsnaM1KdZDaI54/2N7OBividTbaP9l3tG6SGxo
ngbjDJZIDsZgJ3juBDzn17kv9a3UtxifYjFLfSs/L1Pfyrcx9a3Ut6LxasqK
9y1uHYOdWZ++VzTl5F1ODaZzpMem7xWNDA4vhO8VMfkFYmJyclddc3ZhfYsg
/9wFSL+L8boW/KyH6zSP5Zc8+gnkGD16rScoy613MpvR2C6npyW0z603PtRZ
O1qMbs9n7eL8drrqsH/WJ8/Tkl0I7HF6pOxauV7kg6BdAu9dJpd2qe/FmuS3
Y/t93H4rV+/uWzogNdaYza6UFNl5+y3ay9M1R75rvfApSMa71tqkaSegXSbH
dhkZSod4Hrv08uzYZVcgu3Za/cAnq1fb1QTkYp0aDzNep21SWCnMmt247V27
3D/DNBgMvc3eFjyubWhwz+6+jmx5bn/W0NZq7Ia2WxubnP/wt3+K8RrvA42n
+u3i2Cj09Wc9Qq+3DWLUEMTB44Jt03VLx1fKlH6gnLExr4FxaED24jg3YZ9M
juwwdo2PdSs9ja0Gq2qAsbHF2WdzgPjo7n+qGwEmWvZBQS533fpGYnsDwQbr
3GF0NbS3Zsvz/dnB8W4UKxejukaTW80+v0yu2dzTc82eT+HT7NbsQmsa8W+H
q//nIqtf2iXjj/dMvX5W12efff9hRXJshxqvdzjL94MmDg6narzXLFbS3p7+
vULf/qxJ5Hdsj/v91uypztZKUMvqfC3dResqrImklvO1WPyug2uaUU3GPQCP
7WLnrY2xOkzkW/9AD7kwIG8/sqsOr9llxpBvTE/gfWB6SF0z6U1UhvUl7E+u
H9ZBWTQ2JF6k19t+eiHtda4HYhno6fSCeLg8ishDGLdkFC+IP+3PHr+w9zs/
XL7BHo59h+cR1+PtOaWuBesEPLtQDLwcjwm2geYhtteeUcLzBY4tyDeEN4kv
9AWco3YFdtHcTue+M3buozmC/Ob3LPKnDvu+i7MF7QsQs7qwToc+MHYEWDI1
BfnL1Tkgo87aEqsBuC/4eOA6pPPe5FxdS0bj7fIX9QBm75HcCXCL5iOtd1RX
6LvnY+o3wAPuU1pPwj0KaxvZAzA36jg7uDi3YJ+Y2KN8qcN+oH0K9jquOUyv
Q/aRODF1j7PLysRYwJrH9T5SJ+F8UC9tHQ1z6MLAdr4uxebDfoB5LqyntpB9
juKA19A6y9dRK8eTP+uB/XGOz32pb6W+lfpW6lthnFPfwvUz9a3zqW9Rvel7
RbhX2RqdvlfguL1Ev1dA22HPw3skrA3wuaexVVCbe1ao321grNWNBXMNreZp
5yx/a7AW64EUm4uvqWBlcHxUN+WLrbE68PqKqE7zW+Cxu6FFkHnWW2rOTpy6
KsuyP2bveu/9Io6NnmolNWWHVg6r+UeffArf+dQ0qTX/67f/S81Lkr+1nCYj
p0mRHPt///3fFc+JU1ea8Wagt1mP1Tepec13lXpH+0fw7Bb06FNPOZ2WHn3y
Q+p5r9G/2/jxT9/9riJriyWkR+Vuk8Pln777jFpjMZK0x/0Wzzo/Lv9bQhx9
9oEj2W3XTWZN7a0O090WE4HfxHh39t2/uAStke+XXjTk8RC0ujCg5tYW9huM
PBZShySLp32uCl65ZlWt0bqbOlqzD7x5gdcnYy3W7VbUpOak7bvrmpEtt103
peZsbC8Ta6V+6vtH71kxfjcbuXr9DafGnM/aNuKHwlTnAfRHrvvXz15O8D2c
/f0Tx/W6WpCz4jlxsDv7mpjTfkw5WdYvlweC/7ZrJ5Fs+dvzNJoY7DcxGAiw
9jHQ8ZVraVylzHted8jkWLPLqz3m9x431pLdft20WrPH5ZynPWANpNtNXKBs
+7T50tTRJuK/GMb/yBC+XxNPia2kC2sbFNn56y8by/5rEIcjCm/tv99fMq8o
Dh+9Z1XZcaGJsbSveW979tF7V7WsB4+o+iT9tjVrjyBX8xramPrZytRBMNcQ
1kK+luf1AFh3uRrs9eNe1AbGudqMbYQ6vJ2hDRXcugatO4YN30vDnoXeG1pB
/6T48r5YDLAtbWA9xGkj/TbmH8U+lFXBjmHbtM84b5S9IPeCPGuA5xAOI6iD
96nCnVnKnUO4s0Ps7JEXrxycG2iu072UL5/iUAGw8fnB5TeDC3M+Suc+uh9j
ecHnDl8j6J5vI/GG2BbIIRL3eN7k4V+uTvE12OcZ41+QV3l4kZwkeFQ0+nze
QzFr4Gzl8iWWj0zNaOCwyMvlGO4+1j5OcM9zeRnfkyhHGzjbw5qA4xWrjXn7
A/xuoHrKUSgrXpv5usTlZgXjGxcr7DO2jTtDBOeHhnJ7j8mr4DzBxTeiL8rL
xRa/x2pNeJZoVT0wrNtt58G5L/Wt4vsSY5L6Fod5iF/qW3GfuVj7OHme1Ldy
ciAnN1PfCt9fHH2rjfwOcyd9r0jfK7i9lF9bXjrfK2LYuFrWwNgJxqqaOzy1
tItnOx6z4y0dZs78tiTmq90Ys7a5A6y1Y+1iDZQB5DZ7uW68uYN5Wt2Et6Uj
1KOe7caPdqyjhZcb4kJtM7pbgM6mtqxSUrN5Nvladd/971f3Of/1xz/O9tQ3
iRg0u+/le+qas/veC+br9FhFXZMbv+9978/ae/Yruu9997sxub5C0MVX6Huy
x5/6cHZS/G7vGdDfqQUpXqFffquulN+rBV1i+CVvhfmGrfel/qYtZdt1Uqe0
d2H1sLIP6tZ+tJh7rGfUe0Vji6OTnB5TE/7pmWcUyd+VTYLkU+SlIomdxU/w
y+/vH3zrYlZaGsxKi4PZmqBTFw1nH3zLorvDGBjoMt/mtV37xbscl3ctUxM9
Cvf9g13qe74cb+nsUNjJNWsLg0pOaXFA26moWT0/9+ARRXBM+i5tsPcuNp5W
9o2nxtX7wGBn9jFzfzA10efwkiTH5H3KHoO5pubs9uv1fYl8b9nboeyX+q0e
aYP0/WtPHldz9i5GPqcmejVWAhdpX1tnu/PH+lHZCOLQoOMkbbPr9iscm9Tz
g+ZORq5TeWtzVtigsBU2SOwGBvZp+61fwgcbB+uPvAdq2duuyI7dcd2kiUGz
i4F8Op8MLtZ2OSZ9lLrvff0h9VuOSZxuvPygG7c5VqH2Icgrk2N3XK/vsSqa
dH2tNE/3bsZcXjaBNRK/Bp/H+qkxdvEXtlQIu2RO2vujqYO9bm/vsfeK8k7J
3BvvNneWKg4Cd7m2QuEyoHBW/tv7J4CXjNnA4D6l/9TREaX/Jqnf2Cb3/Mfu
XVPjp46NZDOT/a5WVTW1i2e7eq+CNdlQtamL1bAWwj7RYmqrq7+4zqN+0Rx5
Br/bmZ4Rqf2w/sb6V1QO7EPWTzHG9KFqVPcNL7B3vnRU0fD0bNaxf8jJHp46
lJ24/Kqso3/IyZgvXeSwkeOSB8qB9sh/D2Bv/5DTLWVLHRYrxW/s6eg/oP59
geHpQ8hWqU/pgPIBXnKN5ZH2VDk7jzrb9DqPm1yjeE2spE6p3/ZabZfm3bvf
2jWrMKhu0fMd+w84OzQmGlOpc3hqFuE7It4Vjz1ztHQo/VKunPf6DUYOUy1/
r+EdVnKOEvz07zAWFxlch4DudmevxdnuDfnu/abx7BA+HNI+A93V9MzUgn9b
2R4LcqYK9k+7kdmO1gf5ns59kdpAcQnjEtSdCD5oHtW1GFa4vrgaDGsxY0e1
4alm4u3rOMWtA+mI2gPjxM63B7pyY9tC1xDZXJ4TH8JcaM/xBdoB8cA60Tu1
h+Yh6lO87uoWkHs0l1hq93vX7S+Mm4uvw4LgyPRttBdaiD9BnpJc5fYw8bua
YgRzgFtra5Szi8mjPOzzcjbQxcUUYAX1In98XqI9xdUDaqPhp3s1sIPuK5Q7
UGcMC4YIb3Ug259jgr1E4nYuzn2pb6W+RetI6lsUj9S3Ut8CPKlvsfmTvldA
ve0uPmFue9tp7KpR7Jh6h2JA9y3JZ7qHg5yCtkbmSRzD/UZjxfVKUgddr4XY
gVxp6UDv1I5qirnDEveRamQjV6uw3dWcvbS/BLFg/ITnCLqHgnpEa3EYu+fj
ewXGluZwXt3w8mpbOzW1Sdqrn2gMjKvn3pC3jaHYOLeO6uf4o+9wPG+O2rfX
620toicm21ONkFnT2pHV2N7tamZb9s/PPOP+TFNH34C6S6g090qV9S3ZV//u
7/1874Cbk3++Ss6p90ZPn/7rv1Zz+rt8a/bu+x9Qa6uaWgOS4+9+/wNgrC27
5MpXqnH5VN+w1Xds8xTy/tncMVn5Wn9zNjI1o+Xd/35gj76TkmugDq3naq3n
iquFXKDHYCKpuknmbBs417Z7EnxyXn6vf9MNM95WJUvrkt/l5fzDb1lS75UN
2uaH37qovt23dXUE+LV17dXrG7WMI8v6TkA+nQ6DxecevEiR5bXr7JrDi4NK
38Bgt76buX7a4WJteeYvLhH2SPtajIwW49N0EC85Jufse7uw1ceh1cm+/NiI
4puZ6vdrr8drYSycHyoGrToOBker0+kAeEnb7f2XtePwkvFdPCW/1SXnPAZa
1jOfuETdo+i1LS73JR5SNpWpY9CCfNC2H3HjFhPq57tumVMxrza9BOeSf8pc
krpU7jWbPJNP996OZcA1aN5gKXQPHugJYmp9kxjIfIQ56O4VG3wtcFijfG1V
+Wpl2fxD8QT7or270+0xaVu1GLf7x/oja1SN6AW6ZmkqWueCusjVz6CHrKde
E3lsr+B6U0Q3awfpQ5zcqA9Y50c/8ans69/8lnr+7ne/z06Kmirn3vjmO9X4
Bx57Invu+z/IugaHlTz528qSvB947HH3ruYET9fAcPbTn/8i+8Cjeq2SKcZP
XsHwi+c973lf9vkvPa3mpM43vuVONX7dza9V7ydFHaa6JX39G9puaeOvf/Mb
Y7tc9zo1Z33R6/YqH5Rdj1m7rlbj8t3ayNv1BLJL6lw6ckytHZ2Zz+T/FD5i
TvJLsvhKGdZOKUPHRdsk5Uoep99h+oTzEdpsY0TxU9i6dXtBLPZmS4ePqXVw
jcL5G9afvcLG92IbTX5AvLWNV7uzB42FJI1Jp8JE5kChHI7lNXeuY/dIOvet
X2+RWETwgPrLxSAajyI2xezLsZXaCzFS2Fm8C9jQmiczhj+wMYpdpHbnYU1t
zu1NxleON08/l+dBjq8jdjG9bF7nxTtmH9ezN2gf1VmuPpSrE8HYRutOJE/y
amOR9/XGcl1xLsPLxSh2fimM1Xly7kt9ix9PfSvf1tS3vK8cb+pbxXSmvnUa
cS7D+2LuWy+g7xX5uHSGesrtl9ZO3i43XhA3hHOBOhqNH6z31LZ11tOAL9Kb
y+VAa05eb4TY/hrTQ/GM4RupYa15uRWxqfBepf29SH8me1j8bujYd8aonh3v
2vB6Xt7ZonJ2k/n2rqxeUWdW1ybJxF/eFba0q7uc//z3X1PPG1/7+qy6qdVT
Y6uZ/3s3L79Vj03PMvxtim4SY3JubPpQVtPclr3n/foeS35Xr1Hf5PVTkhyX
8zXN7YqkPZdd9So1ftnVr1LvmrStkkfa+q8/+bGTUWP0Vpt7MSWvyeuwd1Ja
hx2XevR9mdRXg/R0CP5ns39+9tmszpwzJWZ1JpflGCT5Hf7NN84CW9uVPOvT
I29dUvcX0N5/ePKEGreY1UACNsrnRcv6z8HIpxoDsj//0EWK7Luli5YPmDUH
lIybr5xQ73u7O8V7K9DRlt1365y6y/A6/d0clftmc19CZVC68vio4js03e/8
8GvbnR8WL+sHjLWNt13XoWzH8f78gxcpgthR3yGpWEm/BO+Qudu58tioW29j
ccUxbf/QUA8rs7qZiwHUhfGRvtx367zKg9rWjiCHNOnxt4hckrokn+W1vyHZ
NfK3W2PyF1N79hoX/y4QTxP/W0z8QQ5aXO3e7tin8/zVVxxE+53LWalD8kqd
tWRfUbvkU/K+5cYZ57/cZ/WiTkmqM7VrPbV6I/W5Prqui5FZxJauyO/1yonZ
yK+l9v/s579wv7/w9JezS0Wtk2u/94Mfsuvh+CWC9+HHn3Ry7Zz8e1tvf+td
6vfBQ4vZP37zW+q3lP3wY0+ysiz1DI26cSlb2+P56yO2a95Xmt9PBOvidnWx
evLsgiRlSty0XI0b5KP4rhw9EeiA+i8FmNr3j3/yL1kMoAy/riviQ1d2w2tv
UXaWy4FyNobytc6Pf/JTSr7EVs5LzOTc+vYazdsuNm/PFL2kz32F8TgdLIuv
OzPYxXwsgk1XQd7Q3pjtG63lpx/rjchav9+nb9d6czbkPxt7rmiOlrcl3vfP
BKbPX76dKV+K8FO5Zy7mz9+5L/WtM5NHG/Mr9a3i8U9960zYlfrWmYx16lun
G5+icX9hf69Yf0x4WYXxbD+92BWrOeVrxXp9zeddX908XSqCQbE8PVMU+g9z
eD36zoRtzV09WYsg+Wwy1NzVq9499ZJnhDpz5szapmC8O5DbUkBuC7MupjNu
G7++qZOzozfXv6a9cl23eO7LGvd2ZY0yvu2dgvZml7/yWnWX8+rX35L9+6/+
PfvM33w2q21pUyS/SZ8yd0orR45mv/3tb8X836i5U+YO6JS5a6qz1Nquxuyc
fH/vAw+qd/2dGpMcl/Py+3V9m6bLX3mNGpfPevNNu6ENzmv5f/e1rykdkLy8
dnc/8N1nnxH0rNJX39ahZDSIp5Ujn1Z2fbsmyS+poaNT5X6ToEZR8yR2jbaH
SQzFvPwO/9abZvVaJdt+i9c+vuaqScXT1bNP+92i74kefdtydnRlSNBwdmx1
SP9e1TQy0msw6RDvw4r/2Nqws7/eyP/CQxcpsu9Wp10jn3XgnsNh3+JjgOf0
3Yi+W5h1MuuNLYi3xfNLPVedGFPz75b3NZ+7PPv6kydQrOHaejkGYvqFh44q
0jHodFhK2te7T8n7h6dOCB3jDqOjy0PqPlD6r+Jt8k/Oad+HtB5A/s4E8Anc
9fo2kzftAL8h7d8KxhNiZWMA9chYS14Z+7cKv2Ws5fp3v3HB5JS+p7f51Ohq
bqfIpUOKV+9RQ22GBC7uHcz7NXsRWRxlfsp5mDtBXFQ8tf/eJ42rxMhip3MW
5uuwetr8lnnq8xXvq3pinyS9fw65vabwELWqaW+3rluqdsVqai87HtZyWGvL
1eYi1CtqcC8ju3wNL9an4n2tKUc+9fv7P/yh433kiaeyU6Ley/df/+Y/1Luk
L375b7ObXn8r4e9RvHJey+11c05OJ9DR2Yv4mx1/bza1sKR0yPdvfOvbWen4
SSzH2fojJdP2eWnTX3zq04pPrtW8vcE6eacC5VkMvm/GHxXjl1v+Tm2v5Jle
WGbtglj+7Be/UL81Rm/I7rv/AWWT1W91ULsgjnAcYdTp11mScbFx5WLRYrH9
wQ+Brb1BXOD5IGaLHG9hbG8GsaYkcXrTnW9XWEls+kfGM/78EdurzFxnmPfp
3Ef29QbOfXJPYp8wzshvIKfJ1lROdlCfytueHxu+flt5Lbn64FxvzntOHMtQ
S4AjwKAz5IWYNpF3ui91D+HyIB9HuDdoDwpyuZPEkfuNemI3tpP6HvCHudjS
2RPPy86c3ly0ZlDZ7F4sINv53x3nW8ceZeV3cnGJ1bI8GXT/5vmahzHDE8sP
YB8vr5eXUZji+z4/1+zY2T73pb6VlyNsjqW+VQbT1LfYXE59Ky479a14ThD7
Ut/yel8I3ytOyxYkN75v2NpYqC/ztSPs8725sWvqxDJoLrZ0wTrmcQriVxZL
ph8W6kuQn8vp4nXAfl9o7ozx9zL9JU4taH3BsxSHy7q+V/Tk9gXIh3Ndj7d1
9wnqB9QXUk8/4eF4i4xH5NvxHvsEY2hNTAblJfLMeysnr4fTQcakHEusLb1Z
6z4hf5+uY7pX71N3Wk0d+7L7H/yAusvpPTCc/dVnP6vusurNPYOkpz7yEXV/
Vd/aru519Hy7uwO64upr1L2NvBeydIW5h5JP+Z36/gcfUu+Kr70zaxRjmjrV
uJxvVONd6s+OXfGq6/T6V10rbOxUd0jSVv3sVLyvufUN2TPCHvv3HULS8vYa
mZ2KT5J9tyTlWz3yvcncV0l65rnnFOmzwj6Fm8auW+HnaK/++/re9upDyEZt
Z5eSe2JN/x17J9aGDVb67iuP3nbTIYeJXz/iZCoSOr7wgaOK/LjVOQzW7FXy
5Lv33+MTznUCG8y9gpFLeXv6erKvP3VC/3fAPne5suUT95UUn5yDMulai5Ok
L4p1klC8VY5qnxYPHVBypQ6KlbxvaTB3nTLfjhu8jgsMGlG+gTsTwesxGlZz
7q6oba9aC+cgbwPJIx8D/S5ttXY++4lL1dxjd65kr3/ltMoXRZ2Q9vmn8Fnm
klyr82kfyKsusBckPl2O3vbqObIG83qZnW4PxeIi/bP3ihYTi0ce2fw8sab/
LNuJ0iiwZV9ou3wX/ur9M6d8lxi0dPpe3ipJ1K5Wpsa2B3Uxpw73wDmufpOa
bflhf+nxa1uRjFg/4HRQ+8B7D9Tfz/SEPFt5vT//xS/d+5f+9ivZlddcp96/
+e3vOJ5P/OVnzHi/uksaHJ9UMm++5Q1qztpoZck/7/qWu96pfs8srii5cl7K
sPytTnd/9tiTH1KyqN9y3Nrj5VM89W/Pa9dd72RJm7VdDwq73hHY5fX3ZYNj
E45fj78xGjspQ+HUo/GSNLO4qnRrP0N85Rpnk4mL1HOFtL0HYkRj1Y98sb+l
vXJOxeLTn3Eyld4evF76ov3HOeZx7TM2rga2S5s8RpPIfrv2yMWXGpsmlRyL
NX/mgecc5izDnm2493Tu29C5L7CX6surW8SGnn78uyfmN4kvsK2VxcxTa9H4
oVqXh3lebGPriM9RP+l4DAcup/Pmi6ztI/syhz+SA7m2Q597OL4yeVNuPxXB
vydiWx7WPeXW0jwqsm80taO9kxfvvoj+vDq1ntq03nwpEg9YG7j9E9fZzuhq
76FriMwgdyNxQHji8XNz7qPyYnrL4J76VupbZfdiLLaxdcTn1LfK2LyO/Zob
C2p3Xn2J4JX61gbjAWsDt3/iOl9afSuGEWdHgfwLcs7rPRPfK4rlDNlzPZzM
vLWhT/weWW++l8vzcj6vZ6/k2BnkThEscTxbXQ5z9hbMoR4vsz2ocUVxK5qT
JB/L4hyRF3yvKJCXaE3o396+AUH79bMXUF+M9uPfvfvNmv3hOk5WLyeH0SHl
QbvcOHgK6jC/O3o5mftDe/uAnbl+hr529BE7nBwx19uvSGPcm7XJ78Jd+t/D
eua5Z7Of/PSnWXNHV3bnO96p7nWm5hbcd/8f/+Qn2d997R/E747s/gcfdPOH
j51Qv6+85hr9bby9U8joVHKuukbfDx05cVK878ve/5C+K1PfqSV1GBK/5bic
l79bOrsE7cuuvkbfY8lny95uMdat7e3sVvPuuVdT894uTUK3l9fl9D3z7HPq
HkuvNeuErquvNXrEs9nIbe3U386ffe45RW3yPlXgpbDb5/Gz1LpP/910d948
Z+yyttlnV3ZxaUzxXHx4zNjUpe437nz1IWVzk7Fd0sHxAcV7zaUTam2zsPPk
EX0vIJ/O/7363sPe/1hdLeYu5OLD5i5BPOX7neaeA/pv8ZC2//hzVyB8nE97
Lb/n9XK6ssfvWs3+Vaxdnh/SftgYgzXNSlcXtsH44P045vxoNfcXMj89n7ej
GcTc+q/wM2PW94tLow5Xm3fKL3lnIn4vzw0hjDRPl8FPx0zywHf5dDh1ahyd
DQbXZz95afaPH7o46xvodTmh8mpft8gnnD8un9R4j8qnuwzGGpNjRr5+Stsn
Dg66PdEqnm2C7rp53mDrc9g+JY4+bmDvmN8yD138DcbeJ333dHB8v/F/xO01
fYe2T93PyTnlr3ifGB80OTzpbGy19nZ1G+px75JX+ix9b1Okz3ySOnr6Tf0C
9a6X1N7e/X4MzetnR+/+sH/04rqL5Uf6CZQd1Gg6zvQHWqNdH2L6U6xHcdTL
9Csz9qnP/FX2rW9/J/vUp/8q+/3vf59dfd0NSv9r33Bb9q3vfEfPf+e/6B4l
6Ac/+pfs6a98NXv8Qx/OfvHLX6rfkufpv/2KkiF72tDElJqTPJL/6mtvUGul
bDmu+b+q+OX40ZOnFJ/kf/orX1H8cytrRtdXjE0Dyj65VtJVYkzyy99Wj+SZ
Wyk5G+U62ffs3NDENLbLyLXvjz+ln9J3ZNdTHwZ2+Fx62zvuzt77wEPut5Rt
46V+C145/61v/xdlp7znsvGK2St9hxgNHZxS66UNyneJWZ+Xwcfiq4AP573F
WcqUNstxZaOIsVyjYm18/B3A+wc/+pHKE4QR2UuST2IsfXirkg3OPPR84/Ze
uE86glzN229F9kE69wXnPsDbwdjdgfiJb4Y62NhSHMhv4EdHkBPAvl5Yc8l6
ErcAR2g39IPlo31hP/AB2BjkRKzOl4kN6kkMDxjviOW3xAbhjm3poHbBvWZ8
C7B3/Dk53kt+x/aJw87GB/gE40bjQ2T7HCT9m9qQ12c5n6hM2vfhHuDiXXQf
BnblYM7tb2o/2EsdyFdmveDtpP7FagiqVcTnaN3Z72NJzjMdtu7A/cXmC9TH
1b0BPE/yK9ZTzsm5L/Utxs/Ut9D61LdS34ru/dS3LG/qW2exb73AvldE8Sc1
PfznSi6GYC/H+jLnF8Ea7wW+Lnf0YXuDs4Kh2Dj1JfAvsBnmX069QL1kP1qP
6l+eXbBGwl7H1YVYfnF5RfcxF5M+W2/349pic4TmItlLfu/TGPJ7lM8roCe3
33L5M5Dt239A08AB/7swDUZ+Y3ld6n0oXE90dkX1MGvXbfcg8z5I5AwCeUPg
OZirp6t/UFGnwLPTYiu/Dct/B6G7V93jfORjH1PfmQdHxtT7Aw99IGvp6Mqm
5xfdvZC8s7D3S3e98271vf63v/tt9pGPirVd+/y3akFSnpxrU9/t92UPfOCD
ap26E+rqdiS/YSt9Yl6NyW/YgueV192gxt9+9z1Z+75eTd2GzO+2bsNvycjU
8h72uvbpO6lf/epXeqy7x+l5+933Gj33Kluhnl/9+lfZP/zjPwqs+hRePpfF
7x6DoaJe9R3+7a+ZF+t6nL1aR6/Sc8kRfQdyyZFxg0l39sTbV9VdRyvAQ9Kt
r9J/x9vU5AEtQ8i85KJxNXbrNbPE5x51ZyLJY6DHrZzVhRGlUz7l+3WXTjoe
S9KOT77niLlT0bIlr7RR3+N5kmPyzsPi+6UPHlNkfQ0I4G3vWqysdnOXI59S
xrOfvAzEoA/F3Nlg5RkcvyjWfVHp73ZjHu8xj4mxQd2ZvGbOjUtf7r99KbBb
jnk/e1TsVAxeNetkWbkSf4VfF9AhfG0D+dCh7mR6Te4Ysjkl86vH3NmI59tf
O+9ySv6Wz7sMSV1y7g0iFzxWPYpHjk+LvFHY9Zi5Hq1/bVHf7V132ZTfUwZT
F/995s5WPH1cu12spW6NVTfCW+aEysGuHiVTypaxfOIda94OSmBc+fraBeW7
2l/yrNu339UsW8PYGjhAayetx0xthfUS/u6P1PcBq6dArR+AeocYHmoPI4fW
c26M6yEBn9Z14rLLBZ3KnvzwR7JX3XCTm19cO6zmoBz1LmS86oYbs9HpWT12
6vKAT5KUNTo163CSvxeEzMUSlKttkHOSX+q0PiF7BrRuy2f9kGOOx8RHr3t1
YHNg14Du7VLn6NQhxbe4etj3TWrXAM4va498HxFPZ7vBBON4CsXrhz/6F/cO
7R01cjBG2mY2FsifQRPLK/j8BbZBW6OxPnXK421ihWK0n5x70L4pk8d5eY5s
HoqMF8n5cnpfuue+uE1DEZwjWNFaOUDti+myfGXqJisnkjusfvPeD2MFsOmP
6Sijy45HcxXoyOtBHI55ONicHCCUi10Mr5y9gPjKxIdSf4HYcrjR/cdSyBvg
UTZ/InNla5blL+BTnh009xxOhpgeHcQxKj8W00Esm9ORS+XOTREcuTNT5ByS
GyuLOzt/npz7Ut+KY5v6FoNLnv7Ut1LfKodPLE4x/tS3Ut964X+viMcxp75C
nAr1F+1rV9GcK9fjOZ39ob5cflhHLX+//c3YNcDkl6GgTuX1KhavAvaWI5fr
OWel9cgO9k6IyfPyvWIj9gKe3sFh9ffdKTK/e+xv8953IOQJ1rn3EUxivEfN
j4B1I2TtiB8bBGNQ5iCj09omePuQHSNxe6GMA9Bm+u7t74M2OFu8vT0D0seh
rFvg2W3Om119g+rfibj2xpvUPc6tt92RdXTr79o//elPs699/evqz3jIeyQ5
P7dovl136nuiz37u84K3O/vzj31MvV9z/Q3ZXvWtvlf9/t3vfpv9+cc/nu3t
0WMPffARxbdXfstX1OtIjj/0wYfV772SevqFjBvVuLxLWlhdyzp75L/D0Z/d
/a53id/92cjEVPbc976Xnbj0lLsDkPTQw1oPHu9XvFrPI453+OBk9tOf/Qzr
ETokLne/6z41fvd978729Q1k3er8M+Cpf0CdibrEs6tP/zmVd7xuwaz3eq0N
p45OKB75VHPCz8uOHnTf7yVOnWK8tDyWPffJy7IvPXzc2bm316+XczPTQ1qu
wPGN1x5yf6fbO5QcKbs/OzA8kH3jQycVP8THjjkZYkyuk+tvODWtdGnqd3Kd
zb3SvlF1t/PU20tKv7RZ+i3HpO1Wv5Qt5Uo7lDw17nVB32Q8JWZfeviY88Pi
eGR5PDss5MrfT75jLXvjdYe8PIWrxXBeYWjzzWIrnzqn/BqIldT9/juWvZ/d
2k75W/r0wB0rznYUg6kh7acgGwP5tHGX8fvGh09mQyODLqeGR4eyd75+SZ+l
Zf7s18/ufpNP6r5G55PE1OMk1nf3IR8+9d4jyr4DUr7B7x2vW1Rr/vK9Fwm9
B9SY1H/D5dPaBsEnbZL2z84MK5skWV2QT+qQ8ZBkMbG5IvW6nBAk88ZhanyV
ct54nf7zXzJmnX36bkra87TAxsbUkt4/i1mX4Onqt/tM1CvRe7pFP+gRvV3W
sKBuuto4QurwCKqbfWBdHyfD1vhBXFtR3T9A5Yq6jnrQCNZNa33QV2hvGnby
omuC3kBocCTj+4X+/dRHPppde9OrI72R66VUFul5AS+1d4SXO6j7+DvueVd2
8tRVRNaIx2KQw9Ssu/xKx6t+sz2S9vwQxz4w3hfIYHIglhcErx/9y/9p4jmi
/j2NN9z+JuEzYwOby5F+H8M/WMedWTC++UTlc3mFx/tsnAfJ+jx98KziYszl
Vjr3rffch3+DWAR7jeb3iFvrdfB7x+3TiC99zlYgw83RNUxOD0YwGwzlhTVo
JGN9juJO1kq72dwtk885tSbURzHmag1ZQ3OOYsXkW5+ptyFf0X1u5VhcYOxh
7jFrY30rB5s+Nvf9777C8rgcofJDTGFPCGvKSMRXZgzx0dzKibH1M9rzSR2K
9Soml/pAHemha2L1cxDUX1Yu56emnrL9hsvBcjlCewZnx5k+9zF5gGxIfYvF
KvUt7FvqWzxWqW+RWIQ8qW+lvrX+vhXG5YXyvYLvuzDfKcaxmHG9megG62Pf
K9jaU+B7BeVl7aO5EanFz9f3Cr+3PW/6XsGNMXUnlltmfP/wmKFR/Byxv0fD
+ZExZh189+v60bunAfLej/Qwv0f82EAwz9EYQ4wviBfK5vyRdoY29g+NKOpT
dWxIYKu/C8s7rQ8++qi6rxmdmM66evW9xEc//nE1Jv/syOe+8AV1x7O3u0ff
BwiSd0JyTPKOHJxyd0TyTsjeCz33/e9lo5PT2b6+/eq7/Ace0fdLXb394l3T
deYODZJcJ9dcf+Or1fvnv/hF9fze97+f/frXv1a/77nv3dn41Iwak+8/+/nP
xLrvq7sz+f6xv/hEtq9/v7kvGFS/pVy53pJ8l7xyzclTV2A9v9F6vvClLymc
eiUNErLf1uVz/wH1Hf5udUcx4HRKPyxdfnxS8VwhnnpsQD1vM9/6n/vUZer7
vvwtnyNjQ0pOd7++Q7vixKSbs0+5Rv5+6p2l7KKVcXW/AOXI94tWxwEWA+J9
wq37MpDx4JtWlZ5uc68if8txee8h5cinJDs2MnbA+DqQjY4NK1l2zurXmCwa
/fvRuKUrTkw5vU+bux911yLs+vIjJxw2cv6BN60grKzt8vfw6AGXa+809zmQ
5JjGXP+djXerdx0bifXTwH7rp41Dt7J/QMUuLwYaD817FOF8wsmUWF518TTI
pQOabC4N6LtmmUuSH+aQu+sSv+dm9J+tu/36eXMX5td86J2HlR6pVz7l2Pzs
mMLw6OrBnPgPeNqv4yHJ5bPJaekrxUrGZh9cr2QMZg++edXFDMZTx31APWms
JJ/GZUjXLNEHVP0aojWa1MCRWJ0N6/EAW1/zxvxcf1Q+U89HuHVcb8rvU/F+
wfUmzi5ti7yfv+zKq8vYUB6DPNsHWLmwL8dwo7GJYRPa+Fd/81nT+/i+mR/n
/J7q8YM9VvvZD9eNYHnSJvt7am4++87/8U8C/3czuqnPXJ7xNtHY82vysAN8
I9QGGM9YDmwEZ+7MUiTvxtK5L5Kj3LkvanM5WQGOeWvL1ZByMc/TlYdNOSw5
nWVyMcgtXn5/oKfc3sDvA6yO/Lzne04MqyL7rmBNiOawf/L9pzwOsVzPt2V9
PP1Idv6+oLx4P3PPCH5BHS3ndxn8Rnjb+1l7Qr/wGsDDyR3h9kksnvw+w3uF
5slo8Dtmq+fFOs7VuY9S6lsxnMcY2XyupL4Vs4XbKxF5qW9FfIj5FNam8ms4
m2MxK4ZnHk/qW9RHwJP6ViQePJYv5O8Vcd9iOZNX13jbz8X3Cp/TebUktr5I
Ho+u+3tF3Ncwt1+a3yvy8p3mLuY/MHowOzAGiL4rGg95IMExxStodNy8T7j5
QchDfwNdg4we+d9idzJHqZ3j2PbRcd4XarezEfKHvweJfGnfILRNyBkYNfGV
34LdvdZw9r0f6Hubnv3m27O8Q7pJ3yHJuyR5z/P5L3wRfEvfn33wEX33tVw6
rL539/QPZpdefmX28GOPZg8/+lh26RVXmnueA+rZu/+AGNdr5J+v6JG0fzAb
n57Nbnj1ze7OSP6WcnrF3MTMbHbjzTeL56Fs9chF2bve857skccez05ddbW6
M+47MKTo1JVXZY88/riakzxrh4+quzp1X2d45O9TV16ZnbriqqxvYCi7481v
VfzyOTF7SN9VCR65Vsp41Ojpl/eoBiu35yx+B8SY/cY+qP8bS/fcsqz9HbR3
X4PO/ztu0H/mZeHQuLm3GNQk5hbmx7M7blxQ66++eEbYe0CRtF9/yx/KDh4c
VfcfBw+OZIuC/17BK/mPlSa0LkOSR87dfNVcNi7WaFkaA4uFfF59cia759bl
7E1C7+LCuJ8HOqUt0japW/JZuX3CJ4mjkg1kHj88qXg03yG1TvEY246VJrMP
331Y4XCVkC1J2thr7Dsu5iWPxPTqk7PZvcK+Nwu9Wo7WIX23tmj/JzXGCvdB
ha3E+J7XL5uYLGVXCkwW5sZdXK6y79KHgSGHj9Rv7Ze/nZ/mqWMwo2KwYGKg
eSe0jIEDKPd6Dc733rIifFnJrr5kVuUMrOGujsvxoWGVc9JP6Zu0X+aH9k/v
GUcDB1DOSfskNlKfxFzFQui8R+hemj9o/t2DERdjaYu06003Lqp1Pj/sv+eh
4+l8GxxysWSxAuslST8UCZ+WFg6qOEp7XnP1XHZwYlTbI/jkb2mz9OXDdx9R
dp04Mq322YCkEX0uHxwRdVfUsEFTPwfHYO0dxzWbq72ABmntpfUW9gzYG8Y4
+YwsrmdQmWNep+8nvt4PBvLGce1HvlJ91l64bpyMH3Q6LKbYPvE+DnkBjqPj
hH8cYxb4Os5gRHweBTYo/8fxWtefYTzBHBcbKhfM43jS/kxwtzFBPReMAZ3I
DtivuTMFPA+wZxWCNfeubIDnhHHPR2M25mPuzg3sOSnEOMhfwjcEzxzB+czn
tN974ZltEPqXzn1k/frPfTDG4d4BOYL2s/d7MLovGGzpHkA5grEbpP4HeY33
HF87mVpI9dN9B+Q4rKCdJE6DcM/YWu1qEbMv2VgRfBzmERztPoH+kL3s9wnI
Q6Af1niuPg8ibMaJHstDahLKjUgek9ixvWDU24h9hHxcXtKcorjRPBjHfT7Y
YwR/tF/HvZ5RrGcwl/8gqHHjUX2OB/rLPsP8tfqRDqYe4lo84TFl9zOs8WEd
CPo7c94Iax+JTaCfymTk0PPQeXHuS30ranfqWz5fae4g25l4Q/3Mvoe5lvpW
6ltBnBC2qW+F+Uvto7Iori+2vsXlTQyX9L2C32cE8/S9IpL7EeydDbAeg31L
Ywbrzdj58b0C1v9gPapDOGdGDk5qmpj0v2M0Aagc70YoJvf50rdeOxgaHp9w
JGM3ZPJK/newZuYW1B3Sl55+Ous/MKTuY/S36gPuzybJ5x1vfov6jq6/5x/I
brz5NWpc3vnINeq+x9KQpRFEjz7+uFqzH/Dqb91DalzeHTk5YN2ApOER/S1b
fc82NMzTgKHYeEgjnkagjrHsgCT3zyeU9Lz8zj44rP9bTO+6dYX4bbAQPn3k
niOKh+IygHwcQbaVtX+E2E/Wx7DQPo7l4piP2QZxFn5KjCQOgc8j1h8cg5id
UCbC0OD6ykv0fxtMPgOsSV6dMRphMAF+qJwpk0+DRobFCe8VTxMTPudiOYT2
ywiIOSWOt1AOjAFdY0TGWIR4+dKX+96wivadrFVDpucMH9Q17JzV3DNCU6dX
489FbzvtPrcOnzfEf57SxNnwpYD8c5kvZ0tPOvclOsu4Dqsn3H9np24Nn3Ye
nGf1NbpvT7NXnm0fzjWOZzXmZzHvKbbnPdbrsDP1rURnGdfUt84g9qlvPc+U
+tYLBv/0veKFTel7xRmh4cj4+OS0pqlp/xu+T3kamyRj5n1sipHB/Z6K6KFj
RG+gL2ZzESpnI5QfWwsxmZgS/FPqOSrwHFW4ym/CB7O33nWXukN6z/ve574p
D5pv/t/85jfd3/U3c2hO3ccMgPsAdf/15aezAyOj+ruzoaFRQSOA1Pfosezx
J55Uazy//p4tn3L8sSeeyA4M6zm0fmxckbR3eEzYLb9pAxqyTzk/7t89+TVD
ZF7/FrJHDRmeEfGUGI2MG5K4TRjs5H6w42MHjV3j7ju8vI8YHB0zfmq65Kj+
8yZ/fq/87xbpOwvl16gnZwf0idjK+jg6jsmOj4cYDY8TimDp8CS/h5g1Aa5U
L7BN+vnuN64pLJzfFgMrY9zLGQEyOflOLsFS0rWX6b+vUT5dHo6FvAp3qIdi
Mw58of47uybc2iHCq3JJ+iLvkkVOjZq65vciyCcbX4nTG/Tfxwfve9z9j3je
Z+bXlqbAPvOYBnnA2I5+Uz4y5vJvjMGIyRntt79Dt/EcYnilvdIXmRvDZp2k
UbPvVA2T9UtQtDYzddjWyjHbA2K11byP5dXrWP+J1WOqawrwFugx0BbYv8aA
HPRb8c6wdrD9CNpD9ZH3PN/Zd64HMxiN0d9Mn471umisaBynSP+nc7E+O8Xo
jsgt24/zYoLsmGHXBzhxGNB84c4CMXtjeE+FdowRmZxPNJdiZ6OAPyKXtTWd
+3IxiWIXqZGx3OLmongV9CmoxTGfHF9Y18aIrBCbGaQvWmtycgXyBfWGqelj
ufjP8H4z8aW2xn3MxzzYFxHcYzVlLOY7h2uRPGX0OhlMrYn2rrwxbrxcvk4x
OUnsoGtdv53yPsR6CVd3y+0nlAu0J5WJYYyX7es0V6dCfSgPON0R/+gZJbqP
iuZxzO4Ifs/XuS/1rTJ2lYtfDiZR7MrtkUgupr4Vz4/UtyJ6y1DqW3H7Ut/K
9+1c9q3cPI/gm75X8GPpe4V8vrS+V+RiBvcMsz/k3y83OT2rn4Imps3TjNvf
dh6+K95pvwb+niDrJqaxjMlA5ox4zoR6pj1BOdAGqiNqA/AV2k99mgC8EJPJ
GYyVnpvJDgqaELgetPlh7rT+8jOfVndIxy4+ab4pjwf3Tj//xS/U+/DomPvu
L+8CfiHGf/Mfv9HfpwWNynsf9Z3efoMWNK5Jjj/x1FNK3ojhh/cp3/r2t7Pf
//532dve/vbslddc6+RIGpNPc4ck7+PGJ6fwcwK/Qxo393f2Hm9cyLFkZY6a
32NS/sQkWD+p1ku8Dtqnwe/gJL4blDbK7/AffdfR7LrL5xVdf2o+u+WaRTF2
kZr7yeevzA6vzCDfRs29xhi4J6M+QPvHjO1jzocJZ7+VY+fHJggGE/r3OMCM
zo9NYL3jZGwcjFtC2Nt3Y9eYu4fwNl5/xWL23ttLeuzgZMA3JtZCfTSu48hm
Hz+sZyI7sjqbvee2UnZkbRbnkqCvPnYyu/O1q8iusQlCyB/ou8khziaOAO4H
QX07iGou4JdYiD3zntv0fd91Io8kXXtqTj1fr3LqqJp75M4jej8h/4z9KhfI
fpkIsWRtNc9H77pIEcw7Sz4PSa4CcnkySfKaxkzY+h6REzeI3LB2HQR4HZwy
NWx6JqiXvibO4LrL1NZgHVOvJ0ktd3V7mtTeMk9f52cC+bYus30gUu/ZnoPq
PNMvppkn8WGSzpEeFfhPKeY76JVB77brgM3UfuiDwpD0OIoF7a+5sY706HJr
uZ6P4sCdL6Z5vUHcSM8P18/gcZtX03zeTzJYIpw5TBhcJqlNdL/Q3INnLfok
Y/SMgzCBawn+6dxX4NzHYYVycQbLZXKawz62J+j+DvyhMaI4xWpJZJzyx+KK
MIpgiWMHesz0DKsT5ju3ryG+ofz89bFahW2YQXxczae1BdV6Mk/rH7UZxnRy
msHevc8gnIIaTfYK3t+4t3P7JJaLNAcpD5eLUdyJfch3Ylc0p4meSTrG6IW9
ENZpiUveHqT1DdbgYC9Qe4hdQc3h4hCp4ageOhvKnMmYPGD3Kbe/3PjZO/el
vhXimfoWwYPDkcEr9S0mxhADJkfofOpb8ZxMfYuxk8H0pdC3glxjnh7P9L0C
1Q4ab5Ln6XtFZE+9iL5X8H0S+8/2d2D/9KE5lmYOzYP3efVnhuK88Tm7Pm/d
TIQ3HOflxGSWt6uYrFw/Zg8pmpKk8D2kzzmCfvnLX2b/8R+/0d/VJ/0dj7xL
eNW116t7pyef+pD6Pq7uF8B37E9/5jNq/uJLL3PfnC1NgN/2/akPf1jxu7uU
Sf19Wz5PXHJp9u3vfEfN//BHPwpk6FyZcU9Fs7h/KL9m6BjskTOOJshvSJOm
/0qaUnTIPDmZs8pOSfJegaOvPn5S3dssLEy7uwzr38QUo9vaanxSeoG90H66
Vp8dZgJ/A9sJXuw8tSePoIyojaaGQf8d36yfJ3InGf/D2Jk6ad9J7tF8lHF5
3x2HcexnInEoS2F+IdtJnqpcmsU5NTU76/2RNk5PK/vycuoN1y27+zTv2wzA
ldo0m4Mr5+ts9p8fv0TRRNTvcL/QfTgFMZim+TGLY0D2oMPKkKxjru7PFa/x
M7Hfc+urtTOgxjo5Tka89p9enZ9newX2aZ7YWL6vcTbNMGv8WPnetq6+FGDq
dXC+0l7M6fBnAg4PBse5vNiENnP4hLbEcJpnYkbPLfPIP87X9eRSXs4gexkc
qK8x34vme/7+XE9upXPfevYXjB2/Z6jM4rHYWL5hPflr5sO8C2p2nr2hz0Xw
z891XWdoP6A+xXTEZRfL6xheupfRGhPHPTbv84HrA/n2lNs34dr19RXWl7m4
LYX2ybryietZEd7I2SL3/DG30foQ5mAxf7g9yOy5IGacT0y+zPF6ytV7Pi/P
j3NfOSziclLfSn2rvO+x/Eh9K/UtRKlvpb5VON5ATvpeUcDf9L0ifa8otiY/
z/3vQ/ML66DFbNb8ni3Dy83PMr/hs9waaks5no3YyPHk8c3OzTuamfP7RX8b
Nt+a5V0HvXsA3/8n0Z0DuB8xdyTwe/w0okOGZjHfDNbLybMy7Ddsu08o0doR
48mbj5LBbAZiCOXaO0LjC/+9P7xPmgLYFPUB1tCifsXmy41z9hRZE857W6dc
Lvh7Vfeeg0ERXFCNobLhPS4zHvaPfP0bzSW4D2dBXgV+uPvmWXxfZPahIpBr
eL/EMeV6TdE9FJcxXzhm0fwh+RDbf5aK9AFVDxfCGl6kdhap8UXlcuvy5Mfk
Fu1pp9ND1iOP9smN2Feub8XsW18PLsaTZ/9G9W3M78UNYVJ0PBa3jfgWiwl3
XioUn4XTwTed+4rycrUpL2Zlz5frsHe9+XW6eje6T2P1+0zHa306yufVRvMu
L9ZF1oTzcVu5GnQ6NZ2vM4sBT578In6fTs0vkgfh78Xo+o1hsv78i8vg+9RG
4hhbQ6kots/nuY+VlfrW+vBaZzxT3ypOqW+tb004n/rWevMg/J361vnWt4rK
LYozV2vXo2+9uG6EJ+ZDLMfWK6eofadTI8rNnalc3rjfL73vFUXs5XjmF5cA
LernwiIZz6EFun4xMk7XQl2LhJ+uz6OYrXk+LPpn4OtiuHaBk6ff58T6OVEj
9VOTxhZ8V5ffjGfB9+PZOXQXNXMIEvg2777P0+e8km9J64B0yNAckHnIyJtT
PDNmjZaBc8L7sqBp3rzPLyI/rd+H4Ng8xADSPFnLy6Jr/f2W92MG4XkI43VI
44T8mpsP9czn+GD8RvbPWWzo+tD30KcFJFfrW8ho3gTv83Qe5hiN2zyyE42h
3/MgFjTWXBzmIzJxDh4iOZmnPz/+nE0YuxD32Bq6J8G+nMW5M03uPmfM0+9B
ulfm/V7hciFmK7QH4Ipl5MeFzysoNx9/v8exjXw9jNRNrmbn1m6mrkbm5qL9
gspYjMheJE8pkx8v1jeMr8CvOY53gdrAyCrU17ydc2X7UFHsC/gYyChyDsjD
tAjeIc0FdlA8adw3cgbgeNbLz63n7AT+ROPP657L5aN7Nse23PNYOvet99xX
1j/OnwWqp2j+5vi0ACi6R6zPJp/KYkxiE/Cbs2+huJXLqaLzNL8KxCqox+H6
uUBXLH5cfufjw/WnOTbeeXaXw6vcvuN6ZH6vhPkSl0n2cdTGcjmcZ1MkJnnx
LUdBnqwHe8JfqNeWyZdCccvLK872onJgrAvWTa5ml8UjL/ZlamjqWxj/1LfK
4JT6Fqsv9S0sK/WtMvEpky+pb7n39L3C25m+V3B4rrdvFrF/I/zc+lh9XToH
3yticeVig+UuLq8IWs6WxHNJPBfVc8WM29/LgFbQGvjux5bBejG2RHkxuTVL
ZG7J2rQc2LYE1i0hH6A9K8ieJSRnGdhmifqBfYFzSwCThaWlbFHguShkLSzK
92UQC/ON2HwznjcEvzfLOU+LZo3nX5DExHBBkZmbX2TX4zsArcPKWgD5o3xY
WtY+gKccV3rI+KKZ83xmfNGsCeahjHBdIFvKcRguuG/0c/Q+xt6LmG/zEgfn
n/NpyemFuhYXeduw/9jecH4plEv0Qh3cukWAl+NfhHNLyA5Oto2HzgddLxbp
/OISsI/EdNHbhGMW4gFzR9m5sATyc9G9h3ZiHLBNyxjjxTAP4/mJ1y4u0fgZ
u62dYK+pvAnu5WwuGT5QL6ktNj68DxDPHD4w5uPNxcLvsUWKAYozzl31e2EJ
8UEdii+o/8uoDsMabGs15Ke1GNdNOgbWLYVji0ytpzK5vuTrOJaZ159wzzFy
lpjeJv21Pi+FPc7JWloOeo6KFbEz7H8UP9hj8PwS1et8WSZyGN+XqF4Oayp/
2cleWo5hH67H/Nh+Glvqq8fZ+8b3++WMxn2J4LdEfPC5sgL8IjFZ8mcD7hxB
c4Y7H2D9HK4cbstkX8CzUJgz8HxEY86dxbDN6dx3Oue+Regj9YFgjmxcIrYg
HwCGSxRXKMvmCe8ztJvGiMsTLg/xfqH7lfi5hO3kcAxzlJG9FMYyzF1Q462N
SzhncV5jP5aW4vJp/aO1Fu9TIpfoDfMRr2P7FKpHy8gOTjasn2G98TUO1ySM
d1g/wrpO42pxRJgvrUTspLkLbSK1bCnMw3h+4rVLyzR+sJ7A/RbvY37fcTWe
xiHmA19XuN5N5fGx8HuMq3V8b192a3D9IPsnyG16Njqb5z4qi9v7MA6pb6W+
xdWF1LdS3wpzBeKY+lbqW2eub4W1PogNEzP6z62x2h36RnIzfa8g8pez9L2C
xiXcl7TnhHU4jBtXz/C+wLUrXBfWChg3rmZyWNFYLK+uZSsrq9nKqqU19Vw2
tMKR4V+2T7HG8+P1y4YfybPvK4bIb8oPx6w+aM8ytWsV2reG+VawX8vEfujH
8gr2YcXYSO1bWlkR4yvqCWlxaRnFzT7tPcWivT8y9wE4vz3/spFvdejfq06P
/E3PBItLdL9AmSsGb2/r8rKhlRUzr9+hbzBOK0FMAAF7sc0rOKarJI8gn7NF
709/T2O/vfvngsXP8MEYYJxWfI5B3ShP1rwPtoYBe1B+BXtjjc0xz7uG8gfu
gxWAK81FaDPNMUfLYM7FchXhv8TEZYkZt2u5dUtS5jLPC3NpaRnrUTzL4F38
xnUH71GLh/Z9LZIvEbwM/5K1F9iH+llw37SE7njcPzcKsvtjCfkNxuA+sPYv
hzgjvJfzx6CftH7i+gXxXzUU2hqzg2JJZaO6v8LEKxIXvR/WApmwjkLMgloC
+ktgz0oYd1zj15DNvnaBPUj2YeDbSijfjdMetbJGbA5xXF5hcCAy6e8w/rZX
rfHrAN60ftDxgGcFxMphtobjuoLrmB8PeyjFlsaU1lEux2GvWSHygtwBdTx2
dlkmsoK9tELwg7kEMQC4BPbS/bqK+fB5ydcsamc4v4btX+WwIHxBfgB5KN/T
uQ/2aOhD7NwX7CGQM3Te4UX7P1NPOfz9GMY88Ckiz8eIxJnu4wBzHKfofK7N
IRa4T4Q55n/zdRXZROoxrpM494N9TM59zj+mJ3A+oLUr4RzMqdM593G1FdYr
KI/WQM4mNmYrGAccv7V4bEAuBT1udQ3HjtTcoI5Z7EkvQ3UnhheyZy2wL6hV
tPZDn1cw0VrD1YLlFewfrxuv5cagfYFN0XzwZy2uLrJ2ECyjNYiJWV5cNnru
S33L28PJ8XamvpX6VupbHN5h/FLfQvmZ+lY0Lul7Be8PthPLpL/D+KfvFTBW
KHeYmhetL0w+wzqKZb+wvldw9gf5RuK5QnBeXStla2trgkri95ojOLbmxu17
yY2pZwnOQRklJG81mIM6S0BfCenz9njyMkvu6deY3yXeFupn4GOpBPRQfEqI
d62k52wd1Jj6M8YqqSe+Jq44svcr6t2sWQWyVp3sNTcOz7urThfWB+WsrEla
Azat6TGrpxTiguJXWkP+r1GsUaxKDkeLkZNdisdi1eC4ivyyNQvgtIoxpHMY
e2+TjVUYV5o7eGwtwGWN5KrNmVLAvxr4GuapX8fxlpCNUeyA3ysgX5StJe8r
3pOlQBfem0RnCefiCs3NtTU/B/JzrRT6F+qg+x7WmjAmPqZ+bi2QbX6bXId7
ZxnsQ5s/KybPYF7hum4xJTlP6p/LjRLgK2Gs8f4Ma62uqxQfkI8luhfD+rqG
9rutAavAlhLAEOorOdzXAB+fy54viJ/FpQSxoLnn9aP1Je+n2x9AxpqtSSVQ
i4D9YR/wPtG9aPdHuD9x/q0xY2H9g/2o5OwPaybe21x/C3sbrqM0VjAWfP+k
umlt4OphWOftOtS3S3FcwvND2D9t/aT5BsdWgzmaT1y+4roQ1v8wn33cfI0J
/QqxWluDsbBxomvz8AnzGMd2DfTpkvuNawfOP1hPaP6lc9/pn/toHYnFlNvn
dI/T3oHjyGMT7nnSS0u+rvs6CeScJ+e+aEwQ5iViR4SP5uyL+NzH9y2uttM4
0ZqXf+7DPY3kZtB3be6E/p3Ncx/sSzTOXL2KnaWie3AD5z7MW0K6fV2l+IB8
LHjuQz6WYB0ANeM8O/elvsXbwu331LdCXFLfCmOY+hasS6lv+b0T+pH6lq8z
6XtF2Hdj/YDrCUEupu8VzN7l8hXXhbD+h/ns4+ZrTOhXiNXaGoyFjRNdm4dP
mMc4tmugT5fcb/68x+VqmN+rdh8heWtZ6XBJ0GFEh+1Y6XD4LAFe+tu8H1bP
UshDeUtSVzgm10oZhwU+gU7FmyOb6o9RKWe+ZObzeIz8wwQbv9d8PpXsb0hg
r5ZKOP88D3hauWg94S+RtSCvS6U1hm8tGtvDMfwQj3we4edBLCFebNxKeB32
H9btVb9/VvGZiOIhMc+Nf8xeLt8QXwnHHPnD7JsSyBGEG/xdis4HuFEb0Z6j
9pYYG8PYHA7WFdgvLk5ras7GDP9m7OLqiNzvkdgE4yVmLvAtlBfuKdATVmFd
ZvIJ5CGf20x8iuQel4M5eVcqlXJ4CuyznD13GMT4cJF9YfOOixmqz+X2YYgh
lwuHXU2O5Gqspjgb8NrDQb2LyIjmbBksmD0KbTh8GOBEfkdtIDmO8MmLdwnM
B/W7TJ5w8YJ2c77S3snVTBrbvD5CcS5qL5sfkT0U5HBEFox1qcxZCWFVAgQw
KxP3YvUT+BXtTTTu6dwX3VcbOPcFtkV7n+c7nNefDxO5cH25WlSCdYbzhehF
NeHcnfswXwnbG8s9NgcLULn+FvClcx/1OfTH85wv5z66HvXZWD6SmhTDj11X
JPe4HMzJu5fKua8cJqlv5cU09a3cPXY49a3Ut7SM1LeYdUVyj8vBnLx7qfSt
9L0ixy+Q4wifvHiXwHxQv8vkCRev9L3ivP9eUbams/van2mOHzuWHRN0HBB9
d2PHw3FNxz3f8eMsDyfzeK7MkI+163h8LtCRoyvm3zFo+/EIP/T5uB8/euxo
duyooWOGwPvRoznzR4+59xhWx4AuTsZRKIvoQrErEAOMz3HkJ5zLzxOKJZ8r
jlf6YPw5SvEqQ7HcQ3kCfMjLkby843w4xvkPx0S+RPdYuf1SLs9pjtp4BTaA
3I3kcblY5u+3/Niy8c7LmxxMrZ+sveD9KNl7R4/SPRcj3pYgRuvAKhZrKftY
WfwKYKviehzL53I8hiuK/3EgcwP5cMzbgXzL3SfHg/hyeHHro/HJi5ldQ9fG
8onFIfQrqDckdjgPeHwL9ba8MdqfYryMb8dI/GJy4j2SqXUGB5dfQC+LO9IV
r2XHcnFn8oT4V5RidSqaM8eYusrY52w5zq+L7j23PpJbMgbMuSyd+56fc98x
mpvkPXoGCeLP23+MkZnb91k55++5D2J4rFyMy9nO5WE6972kzn0w947F+Fk6
ztqSzn1FaSPnvtS3Ut9KfYvNw9S3Ut9KfSuaR+l7RU4+sTik7xVhj0zfK872
94poHT0e9tFjxyAOnkYnphIlSvQSpLHzwIZEiRIlSpQoUaJEzz+lc1+iRIkS
JXohUepbiRIlSvTip/XW+s27qs4OXViV/Ymi6o3Tbk9bWKrZIBm5QNcrKurV
c6O+5tGf5NBm8IzSWYoZjBfVG8aS+hiLY34ehDgxczl25K3n7cjxJeorXR/P
2TDm5fK8AE9Orr9ij6RaRFv2ED7EA/YAkl/N7IsN5OpZzNdEiRIleqnTy7bt
et7oXPuWKFGiRIlefPR89q1EiRJtnM51bUiUKFGiRIk4+v8B/UhR6g==
    "], {{0, 0}, {
    1714, 38}}, {0, 255},
    ColorFunction->RGBColor],
   ImageSize->{1714, 38},
   PlotRange->{{0, 1714}, {0, 38}}],
  Alignment->Left,
  BaseStyle->{"Hyperlink", "DemonstrationHeader"},
  ButtonData->{
    URL["http://demonstrations.wolfram.com"], None},
  ButtonNote->"http://demonstrations.wolfram.com"]], "DemonstrationHeader"],

Cell["Robot Manipulator Workspaces", "DemoTitle"],

Cell[BoxData[
 TagBox[
  StyleBox[
   DynamicModuleBox[{$CellContext`dof$$ = 3, $CellContext`params$$ = {0.5, 
    0.5, 0.5}, $CellContext`r1$$ = {0.5, 1.5}, $CellContext`r1old$$ = {0.5, 
    1.5}, $CellContext`r2$$ = {0.5, 1.5}, $CellContext`r2old$$ = {0.5, 
    1.5}, $CellContext`r3$$ = {0.5, 1.5}, $CellContext`r3old$$ = {0.5, 
    1.5}, $CellContext`Type$$ = {{"r", "r", "r"}, {0, 1, 1}, {
     Rational[1, 2] Pi, 0, 0}, {2, 0, 0}, {
      TextCell[
       RawBoxes[
        Cell[
         BoxData[
          FormBox[
           SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
       "InlineMath", $CellContext`ExpressionUUID -> 
       "ac4494d5-7022-4b04-9d21-ee2100588f50"], 
      TextCell[
       RawBoxes[
        Cell[
         BoxData[
          FormBox[
           SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
       "InlineMath", $CellContext`ExpressionUUID -> 
       "61f66885-76ae-4040-8977-edc2197c65ae"], 
      TextCell[
       RawBoxes[
        Cell[
         BoxData[
          FormBox[
           SubscriptBox["q", "3"], TraditionalForm]], "InlineMath"]], 
       "InlineMath", $CellContext`ExpressionUUID -> 
       "17945a6d-caf3-4555-ade9-6fdfb1395e36"]}}, $CellContext`TypeOld$$ = {{
     "r", "r", "r"}, {0, 1, 1}, {Rational[1, 2] Pi, 0, 0}, {2, 0, 0}, {
      TextCell[
       RawBoxes[
        Cell[
         BoxData[
          FormBox[
           SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
       "InlineMath", $CellContext`ExpressionUUID -> 
       "ac4494d5-7022-4b04-9d21-ee2100588f50"], 
      TextCell[
       RawBoxes[
        Cell[
         BoxData[
          FormBox[
           SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
       "InlineMath", $CellContext`ExpressionUUID -> 
       "61f66885-76ae-4040-8977-edc2197c65ae"], 
      TextCell[
       RawBoxes[
        Cell[
         BoxData[
          FormBox[
           SubscriptBox["q", "3"], TraditionalForm]], "InlineMath"]], 
       "InlineMath", $CellContext`ExpressionUUID -> 
       "17945a6d-caf3-4555-ade9-6fdfb1395e36"]}}, $CellContext`workspace3D$$ =
     Graphics3D[
     GraphicsComplex[CompressedData["
1:eJzt3FVYVG/YNnykRFrEIEQkJERBBQmBewhRRAERKUFSBEVCUFEBkZYWAREB
aVGR7rrpjiGGjqG7JCzEF97v857/zux8e8/xfFsex29jmHHNrHXOdV6zjpvY
aNwhJSEhuU5OQkK28y/PJP/TrJk1cFPgedTX8kmQU2176exrasysp12MT+g3
oKkyvfCeag55Ocd16VyKVXBYTM1BZ3sR+aCaN+UltyUwMnJAJbNxBfk9a/1E
bMQcYGd00zlr/A25J7PAaeNbUwArMbz2pXQN+bat7YdbPaMgd7h9on1gHbnK
icoYsvP9QFhpsYi9YAP5qmpLf+A9LHjctvdbl9om8tZzeONDXwsBrkLAiy2O
4E3N9rcbi1ZBiQmjLJvhOPgeupr9opIGY8oaOTLotwJMLQ5bMQlPI1+9ovXE
9PoS2KS14OnJnUMeYs8QqXlyHqSnNPNcmllEXiG374BL+jSQnlsW4729gtyZ
YjpuZGYceLzN8Hsauoq89b4fprF2BLjTJTdeffMN+Xbz+5KX2j1g9bT23Wfa
a8iPfc45Sy/TAvDxP/6GDxN8ukU09PVQHhjv2Pz7/cQ6cv3HMhbYT0ug4iRJ
rWE5HkgfzcCkttJi8nr1vk9FL4DbPK8fadFOIO+5F4dPEJ8DvwdC2fewTCPv
ku51j3SZBjnHOX+VDc4iT6aOfGv6fAI45Qv44VQXkHOO81K4iY+CMWcbFv+7
S8gpN30qXj0bAGXTYvRcjCvI/SK5i68Jd4Hz9zIZ6goJzmmaH5B2uQEYnlV6
FH16FXnDFZHEgZpsMHskXF/0AcG/7RtUk3aeBUGlhSUv+wYAR96qtGUWHcYq
9fOrooZpoPvB+bHqZzzyiMZBMo6mScAuqawie24c+dIeoeEs73HQMLfqFvRi
Erno8PqjGMpRwN6U/pHEZxq5Jsly6usPg2BDWzT4hNIscvKNcQ/vnm6gJO6V
4Jozhzy4v17FsA4LdLqVJXxb55E/yPh88Dl5DXhWHt1bH7yA3Npa8EyBTQY4
Pewq0/OD4BXUSU9Ok0wAtSskulr9OEAqYBw/fY8e0/PbQN/+8xignBawuM7X
j9xVQcy7gWUUXNPPLSw8Pox8tefbvELyMHAb6/3xYhiPfGyJSUqkoB/I+W15
4x+PIeebf2le/qgbuHrUntnoGkc+MXFa/O5CO2i9zZRqtT6B/Pqdk5+WNBvB
Te4K/eL2SeRKr2y4FpzLQYOarN+Je1PIj4x8nzZs+gRCpry3m4sJnnTfeGXD
exBcpbQP0hFqA0XqZ9bwG/SYjwqGc89i+8Hc3SJe+48dyK2kNqKWzHoBTlK5
qssZh3zfQ15/g0EckIxurLrF2Is8ndLPvI6+EzSLj1DMavUjt+7UqKagwAJx
ntxL164MIk+4ftfK6HUDYBkl7TacGEI+Fn3h5++6SvA8g257P+cIckvdP6xv
vfJBV4gWxYufBOdgDf05fCceLFHLS22dxyMvKfn2+lkuFvSaXfGNkKsE9zhO
W8ZcZ8B064t9+i3cBta/ZF407KtBbmUgivUNaQaV1z8YCgo1IDfrYBWeiGwA
qrqLac+Fm5FLfG0UMteoBes36b639bUiL/R5amVdVQnWGG463n6NRc64Scn3
8Fsp4Dgcs+j5sh059dlHcc/+5gKNUz3HLaQ6kFdaVWtyBH4BbScO6pcWEpx2
hTstyD8MrAqSvF37Q/CMn14pPwqywNVtXpNOh0SAjaMz3HZiwKzE5MkyP80E
BZGdTx8+T0Ee4dAkTUGaAUiUzhbUiKYin3mHDenJSAU+OQbSNGHpyGPP/5Rk
IPsEVpiy+FouZyIXpYgfVqVMApeflHm6BWchv4w5p3CqLhZY2CYxvo/ORi5S
WfecNzkCBIuFn66xykH+I5Ey+AI+EDgeJGt9+Ps/7uEkYMzrCFz3pD8evZSL
nP/i3lcHP5TCIgrOTymnM+CVa/NjT57sHBeVIw9+JhbDRDHx7VWDbOTlrcoR
YTcKYdFv3omD6nnIc0IfeFzIyoOOTgx6db8LCE49yCnZkA359U41vtUpRo7H
6UeWxWVAiZKtlP2GpchDKXwy3fw+Q6t0blIsFURu/SyXrfBFAow8rKepzF9O
eJwN4/Eyqwi4nel9h+8dwUmyz8gNsrjDZLp9A9E4gqu0s3ndNe+EN6VutV8/
WwMvSiSbs8vtvE8W9vQus3bAx9OHJy/cbkAudDPUTzUcC2PnhFnppVqQqxy4
JBdd1AKZ/Tk7m/WxyEndx4fK5xrgRki67+xMO3I2H8d1xoYaKI93D30m3Yn8
87uZiQiTCrjnbViv2o0u5P22gbbPfAvhVY1v2z8EcMh9g8kYwXY6pNE5q4It
JfhU2IoqHcU7KH4X/CCl60auIMXf+HmuD/B16ZAxXW8G2BaZBlZ2GoxYqpzS
gHAv4OvtD+tfxSLfdpcLSuPvBhO8ea6pnp3IDU/rtvoNdIKbcf1+AwM45Owc
Bxny1dpBoU4i697FHuSn77tEO7C2ALPpPw4pWX3IT2S+FYuQrAPt9rImfHwD
yD/xzrBbMFSAgC6qzvmLg8gfvjZgybudC8zE7ppY0Qwhb3M/JXYpLxYI0em4
Sj4iuE9xck8OWzvQeft0WzmzEpxi2tTQs6fBGCmKTjYutQF8ZR07rWEt8hRe
VRU9uRZggmfyF6ptQH4k8tZvP/FGsHXpzsrCQDPyE/Ymad/7a4H+j5i+oPA2
5MFnDOylzlcB0eGFiuJNLHJ+iTjdz5/KwIxSrWfsznXpn1+14nNh9MoDIfo1
I4ufOpBTkuK8s8+nggWyD9yqQp3IFcF3FzPGcJAseiOD9yHB2bkjRR6fKgcr
rp+u/U7NBOEOa0MPk2kwsT4ig6xny0BX4NdSN75c5AupLB5rn4oBj8vLwxjD
AuScjluFDo0FgF/o53vqK8XIBzmOxmVE5QJ80oJz6nAp8iOXLZ9nCGYB/Xzh
3y+oy5Fj+s3WPm3vPP+AaRfyNYJnDDyuu3s1CfgvFKRSJFcQnudnDqbWr5Eg
Jd/klurxSoJXS9hzDXmAwXfHVXVN/+PLUNHbKRiWK3i33iZzg1x/4OGqnJ2/
m9rFn14TCEnEeBKmAj2Rk2g+I+M/4w9jT/1YM+j2QR4rT+Yfo/AKlt8e17vX
5kd4HJGfUtcpvSBJmuDeT7aByPEFWobP3N0gZnBYeEonmPD43PUfDf2dIV71
2NGBi68Jfhm3/SLnEcREkQle2iJ4ucZfibMi9yEJDbvp3NMQ5K4Bed2sCjch
CfOloxKlBBcqkdQ7pFQN+c8psuafyIM5p162HY3Zeb9NT3dnzFdCZuc/N4vc
ipBb39SLDrxZATNs5cWp/cqQl8w0jpruh7C7Kq30EUsFcv3bPmVvDhZDK7PY
z5b5lQRfUYzGVOdBnqtKyxdZq5FXR/hepD2ZBcmHuAU/y9Qgv4r7O7KZ8Rl2
aZfrbx+tRV6W9WEkKD8WltNdVXQpILjOtAypJ68vTLGoOyhPU4d85UJg3lWO
TsijLqTwarwaamUleTSa0GDumbqTJvW0w1j617Lf/tYjvznQyJ2ig4Uefx/s
e9DSjNxBffgNx9UW+CBbmXHrGBa5j27vyQe+DTDwrFygTnI7chJGZv0jjjVw
86yZ9+/1DuQWQmb7fh+vgIwGZxyq93YhTzn6QMz/ZCGMKX5brTZA8CXdv1ua
3unQ/xjTmz0PcMgDL71vok2JgDm2BqN0xQQ3vTNXfmZoAKY5FL+osmuFx7b4
PT3IaDBO3JPW7Hv6YXB9S3CzeAfyWaOaPwsdPTAopiLu7J8u5HPXFCs8dXHQ
FMspqunagzzhiLZPXlQHPJljUClf2Ie8nC5y6tKVNuhr2Ub9K3YA+UMNCk2d
H/WQkWTu/OMzQ8hD6BKDQ7QrYab6eG/tnWHkPIc8HOt+5kE73jZ3DqkR5Af5
GUTh9zjYEbPZPPyF4KxZ43X0XGMw74fR4+W2TvjmmViM40tqzMMzh4alF/Aw
ol1Lni2vB/n+8Nu0v/aMQJF34ph71weQa10/JR3aMABVP+BuPA0fRk7Fn8Rh
L9kLM921e5+V4pEPFKp5dOt3wYTy1YLhX6PIH31fyFsGWBieVi7u8ncMeVZr
8MajkTr4LYfxOVf9OPLTpFhpvelSuDVtuk9DYwI5KX+zU6nJR8gd9+ydThTB
hUdG3iiyT8ET57912/D3QhJfW3O54X0Y0/ux4c7xE1C7oZE3YHYQebLAFefg
6TEoaCn2K+MjHvmRgpGctRU8PDRjMkyRPIY84BuvRM6rISia93XPM8sJ5O+P
XByT7eqFrs4Z1E4Lk8h/PTl48w62E/o/CjiiJDiN3Hos+vdPthYouZTVtcAx
gzyy5WWE3IVKqF+V0HG/juB+8rFCU+6psFA+61D68VnkgqnR3l+ws9BC08zu
s+ggLPRJPjXGtg9j3HPgXMThGWjL5q44gMcjt+lVv+jANAX5XIWuH344jlz+
mdLz2LpxmKP24/pSzSThcTLSw3VlR+HkaDHjXtw08k8Z81QKfwYh231l49GI
WeQGTHwCV7l6IFdp/Mc5mnnkD74HnFDZxsJ1d07bIIEF5GurBhZKyjVw/fKe
MeUlguN7/JPdkzKg3lblcKr2IvIaddKM3flVxe+9jrvzq18JKmd35zzOSmHt
u/OrMAPpid351T9vrrWw3J1faSZwe+7Or/75Y2pj9d35lYlb5e3d+dU/vzYj
0L87v0ren/pod371z18ttV7dnV8dvvjkSGrpGnIGDtfE3flVOm84fcfAOvJe
MtWHu/Mre33r6d351T83jJp9tDu/ErIo4sCpbSK3+eMxf+BrIcgnc8jYnV/9
cx9uy3bNgG/gJfgVw3NxAoSIRY0o7KHBQLdFKj31VbBySNPDWH4G+TJZ4tN7
hcugmUuwiHJtHvkfE++33x8sgNpP5bUPVJaRT/Z8NqusnwFnhV3vuHOuIqdd
1Ja9hpsA53g+LErf+YZcmi0iaH8sHkiTBYIxmzXkm1eihzKsdr5XJ6U5C8is
I5eTTEiTj2gFSkGLvr6NBJ+KiQg8tJgP1INI9106tIGcnYmuRYVjFXw9EKDD
kTMGqBx8S7P0dq4L2MfXY9eWgfa5FnIbkynkXmynN+suLgJx2z3XDzTOIle+
HDbrfmEObGmPYlTwC8h7ve/rPBieAkeoh+e03y8jp81fW+CSHAcVe95yXPu5
gtwRvxSomjYMqm/uZdNeXkUef/6Y6tNX3UD+TRJ7Reo35BjKEtwPqWbwSSmN
XUx4DTm1CO2L4gN5YL91x4XDjwiuIftyVhqzDEhwwbX+TKNgv+A9G7MoGkzy
1QgtZcFFEPb+WG669QTyezcpRjEFc8CoOPSQuu80ctX8QqbB1WnAnqq3FH1r
DnngpdDWoZEJoCmhNJ3dt4DcuZT643e/UcAYnmaZvbWEvPmOo5fWnkFwVWG7
cOn2CnIyEjZzjZAuMH3jw1onzSryj0kCVnnVDaB/Xs5t2o3gM8sZ0n0ncsCZ
hlG7zEqCp7pmtVMKL4C526YBQk3DYC9Vqt+rGhpMDk7q+vNrO587PxmLaw/G
kDMJVIRMHJkBGoKvhrqlJpFHNH05LhkxufP9Xey8bs00cgY6hjLDzjFw1d4k
rpV8DnnhfEGXUPIIwEwwy3N8m0dOHzIe/1WuDxhuOL89672I3MhSt8A2oAOI
fLuTRQuXkOs19d+Qk6kDD2OZf3eGLSM/yUG17C6UBe5c3hC0I19BTlrL8yCR
aRZsSx43or4zAGJD8gVjcTt5w4ZyCphPA6wVyd8oMzxyX3+XzF82k8Aicolp
bWwMeflG6TvK8+OAsaV5XwrvJHIKVkHW+xAPrG5fqIngnUZ+NHz+1jDfILD9
fWbNYXAGOW7qgscex24wlnX0T4fiHHJWwzgR0ZdY8CDFg0NObx55W9jB4rDU
ajAUc+HBDZYF5CMOrKK/BDNA/fBIctoLgpPn/WCfWZ8EUw1BstZ/eoA+H8PL
R800GKmgN0kVzhOgLOcajjlvEDnWKZZ2tmYMWJho5lH74pHjrydSNHXgAbuB
CFunxxjygo+5zRw3h4BNcGcQ98758J/Th3Zb06T0grGbiokONZPIM/VU7h77
2gl89vduUu2ZRu5smDnFN9MMNll4Tm6tEXzBy3mqmbUS6PWXnVN7P4Oc3N65
g+t6KvilSFG/uk7whbjHXikjY0Br4DTNkQ9dAAqlvzjxmQZThCtlE44eBTeY
SFuo0nqRi4dpsbBw40HvyOpvOptB5F8SGiw5FwZBKunp+9e6R5D/VRD/W3q+
DzQF2Z72sBpFfr6d2bT8DA7canuaP5w3htxVM+xW7ywWkHPOqXLXjiMfvpoy
faCvHgz9kX/xM3wCOQ+Jt+xeTgg+Z+svt5yYRN5ldVRHQTsFTDw55tFvT3BG
fX5KhcIRoPf2nvFnzXbAxJLmeMyGBhPp0JB44f0QSDWNvo4t7ULe2i1bV3Rs
AIxvfBx9OtmD3L1gbihctRfobc0yZ1b0IzenCyvMEcGBn5szXNbKQ8j1LTce
z7e1AzeKU7+yHUaQf+d/0d/4oBmUcHiUaMbgketFvCH7M1oNOBxuePVcG0Ue
suEg9JK/CGwHMkkW1BJc+JZu0rhDIkicahF5uncM+f+2eU65y0tjC/NOSPGd
z1TjbA108HlhvDvXetecl7jC2gHXuZNzpW83IPe6anFRLRwLlcMjeBikWpBz
RPh5xxS1QP2hiw9b9LHIOXXJVirmGmDOO1XM3Ew78oXMjOb9DTVwYqtQ/7l0
J3KWQ4Eu70wq4DOlE+nqN7qQT3MVHHvuWwgduPRGfgrgkEuS7DfYndeN0RaL
tJcSnP10QRkNxTuYuc08TEbXjby4ifWy2HwvrHSqvL0u2QSP1XjzcvbQY0yo
Sb0eyvXAD0YcsV1XsMgdVA5cVlDFQXipq0S2pgM5ObdZZNvhTqh+UcLzDjkO
+fg5vrkDSVgoOxfKfGZPD/JkCd2lsN9NMLJpz95bub3I1Wx/9X7WrIUlj/iU
3dj6kXd7SfXL3i2HMhWkzNLCA8g/KI3H7ffPgbffUo0cXyT4lLXWc//9sbBR
J559UGMQufCx8yqNzMPQlkn594Y1Fn69y7b1TJseMxFQxtjJNAiDmTNppz93
IjcM+HtJsqAPap7mzIEp3ciPHUt4cZ+2B94X//6LzrAP+dwwy8few10w0c23
Qa5mAPlPdg1r3PjO++QU60pd5xDy8HWW2D/rjTCERXU/l8cI8vftKZdEW6pg
Yrbua2cLPHLBn552zS0FEHsDc5x9gODO972Wyu8kQNOZkm63I6PIv508af3A
chSmPZllFaPshFtSf6ZgFR2m9KEMyxLAw0DXd75fq7uR09Cr58rVDsGantn7
Chf6kbPmjk8mpfVDW1Wl0L2GQ8i1u3T2N8v3wJVKOkGqA3jktUokLi8DOqH5
tSfGzQqjyO2mzOM2Rdsg+U9fcXrZMeTVkheETBdqoUpZl3/+NsGf134IGckr
geHRQywW7uPIRRQiI2MdkmHfzMdlp0aCpxYIr573G4dYmuter/RwsG3igkbF
UTpMRsDhG1dkxqD6gO2GQWAf8n79jJi6XDwUd8D5udkNIecQFu2lWBuC7uR2
Gb3P8cjZZjUbF3fel+SFiRST1GPIXUTLV82/4eA938PGv4zGkf+gZ7MJc2+H
HjP6BfinE8i96pLVV4oa4OW4P0LHNSaRM8wcPPP0aDkMjfXeYpwmuLnuk1p3
0U/QWvPMQt/5KeQpZq0vKC0nIVO1/vMIs53jcDb8ht9dWoydotK+87/H4eSH
AdKTfweQP3Ggfq+mOgbpbaFnKiceeZ2Qq270XTy8JqOl1ks/htz81olaEDkI
c65fSSapHkd+rvhR4SZ1L7wgeUizQmoSedyVe+vVwp3wCru8wHPrKeRf339i
/CvcDI+92g41uTWNPKdrPujRxwpomBQV67dNcHExBtaSqC/wFBWDwNrNGeRb
VSavvm9NQYZwmrzVo33QSKD9nEECDebQopneGYdJqGxZSGvEMIw8ai78V9rH
cTgjbca2pj6K3Ls8Uo8jehQmpLkp+5qOI6///iV9LHcYSim81Z+TmERu+7y7
NgS78zmvJzFXbppC/vlD4Z+Kd13QtsqGLPPgDPJxu6ybLQqtMGu4bR898yzy
rUj6Pz7rlXBDpt1Eporg4UX9lBlJX+GsiP36weNzyCVDzPPo5Wegi6FjNffL
fji1fVcrvpMa00qjTReeMAXvMl6687RgBLk5w2P1hNIJ+Hfws17XxTHkycvu
FaLhY/CCaNBm0aMJ5DHnC4wPi+LhjS9U9VR3ppDzTm2sxssMQN8sJ7J4uhnk
ukFqKQd3rivpDkosV+7NIj9Eb3ewebkNvjt/Rjbn8RzyqTsOw29JquGz/NXt
YOF55MYzD2u/XU6HDB8rsiNCCG6vR5qgLDq7c3lROd/yagCGh2yFzf3ahxHW
YgPJntNQLeWMQ9QzPPJD09XU0H8SqljqcqhsjSG3KBbkbFUbhweaWD9SyU8i
n7xqbTmBw0OMgKJ+iuI08n1+y/HJ1wahtXT/w/Y/M8g51rYF+MO7oQrDJuMB
iznkTKtOZHpvsbCeRvv5hss8cvlLm/gb9dVwZcFKhAosIL+YX3k9FZMB735U
r15IIvj/P7/6f+Y8LIdtAnf3lEr3f2Lf3VO6KHM1aXef5xCeMm93n0ciUil5
d58n/EKi+O7ei1Jgpvfu3svPl1ouu3svg8nWErv7Ic2LLuG7+yF6P8ZYdvdD
XtOci9vdozA2Svu+u0fRhlel292j+NJMtb67b/AOx2W0u2+w3/UX6e6+QXlO
yr3dXl5TiPfnZkEWkJ44LdK+k4v3m5b93/465d0q+5EPpdC6fKXh0+kMeLzJ
dPT/9tREciOx+VUqxQHfCpUVMDt5oZu5dRSUPJdyX2KjxZz+9YDi8dd5wJwf
yh8lMww+rLrPGjDSYeKwYzaUVlNg4nyES+iLXtDhkaEZOECH4Tq3eqZ5HA9s
OmMXzjl0AMFwWcz2M3qMOUZL2/lqN/hAItR4zKAemBsbXclb2MlRejLmmwdr
wMRFO4meffmAu+iypZowAwbTVOhN7hwGY5ean9g+84ZmaqU633ee/2PGp14l
Vo3Q59HsZeGREjj8Yu2MPQ/x3EhsfmXLG2DK/34RSH0Eiu5CeFB9hlUs1oAW
4yv9YRFEzYDt+9386eP9IKythsJdkg6zefAmTaLaOPixX+v6QxYcuM9QspY4
T4dRVvKtOuY/ANJqOcKFWVrBlGB4eKcdPUbiiIbg9nEscOO+Z2ofWwEazUmU
7zbSY4IP3c3MOZAFZu6Xc8mVJ4DHyZdMLq/TY6h+Le2Pf1cCe5Mm2D6CdHic
/Nqnb0v0GFEcTuBkcQd0xzDESj6phj4lvzceFxHPjcTmVy13y1S+vZoDRmQV
Pix/B0ERg/71Gi9aDP7l/ldDLyYB86XQ3/7OPeC0XnviO2U6zC8KgeLpM3gw
Hlct3J3ZDvgSD1x5Nka38/3IKzzWGAdCZvWF3fXrgLLI+TAPtZ28asR6CS9e
DZjFq46JHM4DR2/8lTMKosfEVt75lREZCjE/q6z43L2g6K2eLIU4eszmIcHu
e1oN0OI2n+fh9GLYjLn5YNODHvORf1WofKIHRs8Mi59ha4RHDIOa26WI50Zi
8yuuryLTQQ+mwZNhl1vfI/rAjci7KwPhtJgxu0mJ4yZj4E83af/KwS4wt8T/
2xqzk/fGrymrS/aDZPO6H3+Tm4Hvp+zLJ8t38oyV2zHe/FYQbtw18LGuHESx
iwU6M9NjXKVEYwzFMgGeLEzuZks86N8bFHJLmh4TkbjdOyJZDDEH3zdch1/h
8NuRx5fE6TGrt0WsG7vaobq2qZ778yqoNz6jcJ1i5zxgvL8MGz8Ij2Yfe5Ku
3AbNz9rlH/pIPDcSm19xR5O/7VSbAPrWi5K3znQDemGolBlAiwktrlWsejcM
rGfikiPzseDaev3PYxx0mO3fPvLy57rA52HZJB6mWjAUliXH40mHoRJw+7jO
UgWo6DMkGodzgF579bBjBR0mdrJqRe3JG4i5qpndqOgJPRXfahl20GEkfuZ+
Y1mpg/h+K+YIviJoJWV/LT6fDvP5rfrf/pBuiD2e+G7P53qYktd4OtyODmMh
Qzd2xh4P0x98PyH0sx1KDKZzHdlDPDcSm1/RvnIjCZUaBS0v/CkpOjvAQ1HD
iyb3aTHNnXtkc9/0guHLxT91+xtBLL7u25FxWox+zn46zYZmIEHl2VRlD0Hk
FfmQY2fpMPwcH3JyOTMAP8WmG+2HOEBO3xLvpE2HwVyKoeL3KIQ+2Qdo949+
gQemBU8VadBhNMWe+x7Nw8Il8SfKV+QrYRHdTYlp7p3PRaYuhXV3Pzxwxd72
s0YLdDLaF/m2aSevzn6x3XN9DK7ykrFfIOuCeoLUPGNXiOdGYvMrdUHMLxab
QRD/JLjw9P42UMN5W/aWyM7jZO4hPVrTATSWZKUOu1UDgzBtBeZgWowR6wgo
ghWgYEut64VZNrjofUWLu37n814mYfj0UAjEPOCyu1TlDkMyjJ729NBiGMOC
TojJ1sJqaz9yz2sF0KSI9sjfsp0cruoRnPGzC541UuJcm6uFgflvEmacaTEH
9r3logsZhlcNLp4yy8bC6DTT98YHaTGFlzHPvk3s5PLvmVqhJTjYyHmRPMeL
eG4kNr96SYUpexnbDZiOMv+aL6gHbrjFQK0hGgxJI/Z8rGsj4Gm/7Dy3XgKo
Ogz3Hjmx87pa4qYwWV8Bia6x8nHJWNAlMCzkcpkWE8GkFoV3zofqNSp9jZ8/
Qby7vBm7Ii1mvS7u7K2aVhg/wqx7v60cipqpuJOw0GJgcsVXtZ3v/zlYA+r9
fxthOsVSYlYdDWZ4fCTy+yAeKguv3y216oC0STHPz12n2TmPKX7cJzgJ9Ss4
7tQe64FqmwMnfuYRz43E5lfE9naI7rcQ2QMhti9BbK+AWP9OrKcm1ucSy43E
erSi3hLjoKE8kJueyPPjxDrKV0sZdkV9NdnAu0P36e5e8b98NWh5OzTPJgOk
llwx3d2//ZevqF5aqxk0fQKcnvZ0LcVTKF9p4Bf2Dt2JB6kp+0R29zn/5Ss1
nMvxAP8wcEAu4Nru3uO/fJUR0mWpz+sIaOGLpyOXclG+KlAVGh1gcYe2c/wK
H3DlKF8Rm18R69FGLEU0RMlzwaNAB+16z28oXzXbHnL83psJrt7quB39aBnl
KxmR+wGVc6ngULEdTstpFuWrh6ff9CnXJ4FjnRa3350dR/lKsPPexKHKKFB8
9Ha8i04fylcTr511o874gd5r7n6vvtehfIWfFc70yLsF8SfPeaRaRaB8dax7
EXuC+jXMuJdBsX9fK8pXxOZXxHq0MgEcg8bhbBA26x5g0rmC8tVR2QFNxdJ0
kK/7E8v+cx7lq49DQYfTp1MA8767J7jXJlG+yssfUHCsigOxD9n/FqiNoHyF
rf28rN0aCkLdQ1jVtTpQvkoJ72e/oP4EZOx/5kTln4PyVbkeTkvezg3mTNLd
2CtXjvIV1nOMSpQ/AloH3akwuoJD+YrY/IpYj0aiI1S5xb7z+Ww/HaAouITy
lff832GRoi8g5+PS8VmbGZSv5sx7Xw6NJYJLNv6XzC3GUL5iNv0ecubNe+DY
q/7E91ovylfMly4ynzngC4Qqh6dZf9eifGX0mjEkLkQPkhTf4LK6/xblK1oD
Q9M0rWCIf8ePzW9uRvnqp4v8chQ2Bt7bK6Phyz+A8hWx+RWxHs3CwJl+6nwa
eH2MZ5gxbg7lq2aHy8qGuR/BFB2zgtDkBMpXZUaOe7dnYoGZ4rW9Ma1DKF81
AptTD1vfgF+5X7F1Du0oXzF2nvzocuYxwLqnXdEIykb5yt/uZLx14EvIr91I
Kv2tDOUraS2ofPhvOFypnnf0VOxC+erc4Vzu00/i4SmPffJACY/yFbH5FbEe
7etUjSPz2c+g0lP5741D0yhfRVvPtL4oTgAdmNN/mc1GUb4SibH4JE0XCSaO
fzYa2Ogm5Csu4ciH8T4gY75z+GheDcpX+LT3Y72suhATwlYmxRmO8tWC6bw1
ZiQQhrIxc9uJNaF8lUwxHdfNHg1Haio+P03pI+SrUyFq+8WTYMVyiGxU1hjK
V8TmV8R6NBf8nW1RiWSQVmBaLB02jvLVSuFlCUzWB8BjWKh9d3kA5at7y4wN
ZMEhoNrY/AWXPhblKx1PP82MZgdAYvOwql8zC+UrUYsDs0IOrtCneuJbDkkp
yldGxtFBhgFhMI/eqCp1uwPlK+p3WCdV1Ti4N7or4dzLYZSvpPYJJih/+Ahd
E2yf57ZPoHxFbH5FrEfra41Twd2MBw/4mCwG+PEoXwnRrGrvFYsApB8ePU67
iUP5yvGFBNfiuBfQZMh6GSpUjfJVrHP7gZYb2pCk+uvfU0GhKF9R1Zm8+5Lu
DznNOIbkLzegfNUaoD3NS/ce4icflpj39KB81e9ggfXKS4AKpPX02YajKF9F
Bm4EyaR8gtLNXUq9UVMoXxGbXxHr0b4tzefZnY4GRYH4pT/5fShfPbu1dezX
odeg6xXWJvVYK8pXEqyrn8+fsAcZspTlbvgMlK8Yj75JWlFxgQUOCpeisotQ
vloKus8T1fMGSuyLvzvg1I7yle1F78DwoQ/Q4Nrp8+8ODqJ8JfXrXOkzmAS3
BYX6cYLjKF+ZlASouap8gcby3JS36GdQviI2vyLWoxHbHya2Z0tsH5XY3iax
/UZie4DE9uWI7ZURm1/RCOKfxuycf5Vy2uc2jwyB406aucVYGsydCHX6d/0z
QIikz9PRchQ5RbikAVvNFGi8RKPoxDCBXFaAsqrw7gTQOlGwV/DxFPLHzyvD
5htGQT7rAc7FsBnkSnzsvNPTQyBUl7KK0WIOeeRW4Wu/oR6Qr+kpiZuaR05K
Uv6l27sdWHSzU7rRLSJ32avK+3qjBpDrP2x26CX4YMzBcIxQJlC6kNgodHEJ
ecbNx6pgeBJQUtDLpU70gLtfT3VZh9FiuARU/r63mADmdtJu9hGDyPM26jRe
ZY2BsS5D/p+OeOQp3Sqm1hV4QHsvyD/Pbgx5ZFmkTt/5IWA07aEtITKB/Mqd
C7ffBfYCtzGy1q20SeSxyl8Z2yM6QQnvu4cqs1PI3+OUpjQam0EgNeuiwsA0
8g236sqjVJVARCj/0tzLGeTWop6ps+dSwdbrbT2RAYL/nRBnw+HwgHy7oGrV
rAOce9vIcnfnvLrHfIDX0GoE2KexeX0l60bu3VlEPdwwCKZzFQ/M6Pch99Pm
cHRs7gNdJ3VX6M0Hkaeyqf6Mce4GaaYkj/wPjiDfwJ9UrJruAF7Zh/faZ+KR
/0hWfmEt1ApuRUS8vJ8wivzcoQ/+TIK1wO35uZxsgzHkludFdRyrioEXJvn5
3yGCG1m0n7iUlwSebHLWF3KNE56POI+1t0gPIHWi/H1JqwFojcUt5STRYZRU
QuXVE3GA0dPtU7NGK3KuDssf1Z2dwFMkl7fxSTvyWTk7mdjqdvDi4gmbvUGd
yDvWbtdV/2oF/NUuah4WOORlB2xyn1I3ApM2gUHL7W7kQ4pRa5t11SCKyex1
nkov4e/2Sfqme5eB5ChlTtWrfciT+o/y0R3LBpF+UpaUfwhuXmIcJjwTDaKW
o55m3upH7n/rI/+t3nqQUkx+4xZvMaA3yL+0sEiHCb1052W/cB3gcpWUeilb
jhxjHzbHI1sDpBMvGtPkVyLfmtmoM9hXBTJe5+57sV6NXMQoV8Ypohzom3xI
bViuRe6xqHTn3lQxsBoxT/oUX488Nctjs0YhD0Q99vhWsbcR+Yr4kQ2cXgbQ
UbXh+cLahDyCu+S0l1Mi0D8kenFPL8FVPR7eN6MIBhOHO/ytZZqR490fsGXt
iwDlmFskQQq+oBvzm1uAeifHVsbZblmHA3ybNnOtdRByjIl09cOAUMDoxVtt
yBaCnNNZ/j7F/RBQPljldEYplPA4hp3v7OiDAafSd1eSM+HIY/V9JXS2fUF5
+8vzEPcWefmQ6Ls3vp6AU9FWQkb4HeFxcgOlIpJeANdowUJxyUjk+EmbAI4O
e2DkaLJpvkJwEoPyUHkSQ4BfuBPqo/EeOfOnIon7I5Ww2vk4Iy4mB7r58ekz
/aLDtFLbTm5EVcDY87cTCr4WINecP5I+wVUOMdptsZnWJciDR+86Vs6XwMsr
3KRTOIjcB+fDYnW+EPqnng9XsapAvvL0i9/zM7mQfUlyKS6vEnn988b7cbMZ
MGMmkHm8pgp5xgWxs1W9KbB+MzQmL7waOVWoD2/8sQ+QmcLtwqsTNcjLxa5f
XtfygdiLe7pD7Qn+qqP89gX5LpjB513gylULi+KkSvpLd3Lg1FrR4ZkO6DVd
Wpju3Ih8qepgqbxGO5SuvqhywbMVeXnSRcvnNG1wypJNRexQO/IMFkWxB4xN
0ONu7ov+7A7kdqpnlRbgzt+bbrxsf6gLeaXuZtNbvkpY8HiJJVYSh5zDH+c4
/qUIlnG+T2hg6SY8n8rf1CY5GbDrYqaAUA7Bk0Vij/lzRkJ+s3UZPGUPcjMm
36XGrmFICz+L9PdgoZjPIVOMFh2G8TWHAFntIFSdoe+z4e9C7q9AfcRUvx/i
vGuhkWQPcqPe6EKfDz3Q/1pb4jZJP/Kcat32IxFdUPd3Y73b40HkUreyohPU
2qHgtFe8QMgw8qju/Kt0cU2Q/gpDZtIJPPKrC1PkESrV8HWs4DOJdoLL2t/6
ftSqEJ6Sm47LUhpFrnimxCB4NAFqdeIwcZ4ET+Znct/jPgHfar+3V7Lshgz0
4J5vNS2mURzL68czDtlvhBww7ehHXqbRGZL4ahQW3LjQi50aRm4oViFaZj0C
42iLOXqujiKvPJFWsmE+ACPMsePJy2PIa32iND9R9cCBGwx2X7UmkDsnChYl
GHfA+t7Hss7Ok8j5G3/5Ce5pgv2/+F/aG00h5zzce6hpthxuYfmSq7YILs1p
9PWgw2fIXVzM76o+jZyqRrhxgH8UzL1uK0mBHeDD7YDGc3n0mFD1tK9fyPCA
ur2xR9a2G3nalZ/YHu0hkIhT/L7V14f8BWtCYLZKP+j1nXVi/zaIfIlC5uur
hW4gpFn6vDdtBLmHqpfZX8VOQBvBdLmWbBQ5SWb/kHRFK5Bcesps8YvgIdge
ceO3tcBC5suGUsEY8kXDcgazqyVAIn3cnubCOHJSI9dKvePJwPVlZuaRlwRn
O+CgzHVwJz856QuPpbWCsz553F8K6TGD9GnC18X6geCca7Chfgfya04U3BWk
vWAxhQl7kw+HPPvFBWUTDxz4+PGat1t6D/IOwf2czJUdwPmpsOL0Qh/y9aYq
BZZXbeC+02yaXe8A8mkKdt+ngg2A1ylkaL/VEHKVWGa9O06VQK1k/klL/DDy
xtia1Gm+fFCR1GTd5jiCnH78dVQ7WzwYOB100GGe4LI+Yh+yRnCAP9p7XW6q
DnBnzeZMJtBjjn2o9t607AKkY350upHNyA83TjTXZneA4yMOA4zPsMh7BjMn
fCuxQMr8T+Hyww7kjtFP7jRItICyeg+uoDNdyN3USLWlguvBNXhtyyoDh7z/
1xj2aGQVcJEpfYqd60aeJWhZfqG5BDwe7ibpG+xBfnBDZFltXxZQND9m6eve
i7x6f75/vVgU8J2WtegeJLjT+5pECt82oOMdUTIgVQGoL3dL8T+ix6TO8Jnz
2rSABWkGx8WYauTVNE2DWt8bgW+8uPZERR3ytA6lFjPBelAZHXe2710jcrfX
vxpO7K8B1crXctqPtiDXM7h7+U5uBeDZeyBj8GIbclYZEqdHCiWgi9T6fZgn
FvnDhhmVicoc0MFRTn1AtB25y1isecCez6D1Z24pSCX4aRYt0wdaocCJ79Pl
3d+Z/nOSAzlamRlVwKn5j4WTey4glba8RHmaHpPTqGWoaFQJrH7XTRvCQuQR
5wLHO3DlIKVaqvPHp1LkVFSlzx2EyoBIaeegw/ty5Iyj/ef+XisCjBQVD1xO
VSJnH39N58yeBzSrl+u4vaqQx9KV6J9MygRC/tgGnrhq5M0XKUouLXwCVF8k
Vsie1xAe5/cRMyPpWMBjf//W/v21yLdcyu/1mb0CjobjV+yNCW4xXf0mPjMD
MFJ3sd2RiQdXZsdsjNp2rpvbyRRt/OlghiJbVacrCXmszDWBH36p4Iejbqnh
iU/IXetq5EnCPgH8e5aUv/ypyBmpgl4rXEsGwUkipPkdach/fOnQOgrjweXf
5YWBARnI1TmfXwycjwYSOlTfXJwzka+4z9fd+xUGeo1yYs6fz0JuxK2SWOvt
B6hCGQKscwn+w4FFL8L7EZgxEGMO+0lwErcfHBqtIRD/ZzvmhL8HjMLXehvr
7eTM0wFLJAGvYaxd4F/9tz4ET1WTVmIOhhgAjJc1/ZG7cn2dC2z3hxj3p8/o
C4OQc97aGx167BU0enw8z03/NeFxPt6QvczhCY365+u+JoUgLy++t6Da7wpj
h1ZO/8x+g5zk4UYQptIRup556MPgEUpwFfYyGzIbaETxvdfwQBhyI10ezDRG
B2L6/JN5jQjOH7RZmutbAPmLJ/k9sZ/h1DWz1WfYne+hdZTQdDoPbg0fpy2l
yUD+41HLu4rfOVBTWM4k0CkLuc9TDveRliyI7RUY35Odg9x29Gl/jFkGdAw/
z8WWkoe8YHFhVCLoC8R8kqNKvVmAnEqwhOTMdhK00MrQssgrRM7+vVi6kC8W
ijjZGW/WFCEXSeH/8UI6FPqsHGvaci1GrjNY7pCU/AIW3J4/fGqS4IPnaF8G
XqmB9XePnFRVyIeJBe8YjM7RYrZ+lcfWDldB8k635zd6ipF7xHwIkhethCTx
xTSF+8qRs0tuzSmql0PNr1fCeDcqkNMqUwb42ZXA4NiB1pGPVchtDa0+wpl8
GLWX+bQfew3ykvdbDE+Ys2FojsLhINVa5Dqv39OO7U2FPtYXjHil6gh/l8ph
82ZgHEzty5cyHyS4rSv1R9tHftAqvcLo+rl65FHVefZzV1tgAX/RYPWecjgj
dn6axJMG00Xbp3BFpwmGiLz9KsFfhTzRatFBk64BykseTBVhqkXeO3lIVMm+
FrYWFCQ8zatHfiKy7K5WSBWkdjxlUkLbhHw9Saz8vf1OLvKPoo2ga0E+8eY1
DV6uCHr4gPuFWa3If3iU/olTzoZWxomjS5ttyF3fqsj94U+BVwuv3GO/j0Uu
WEtKMTwRAr8NLfPMJxO8Uu5mZZ37DDg3S2X78xDhd3yycVR6rV2TIKvvnQEl
ySrqByc/txdM48eAdQ97fYv5IuoH376qH2o2GQFX8Cv4ItsZ1A8Oqns8yYrt
BZJx99adT42jfjCcDER2B7eDZjk6bstbA6gfbO8IAtRvqkF28u/xxxRY1A/q
zNL319F9BBgvL+XStUzUD1rxMt3IlM6CZkOaA/R3S1A/ONe/Iaw7XgdHGFY/
0ER2oH5wMWopmK53Cpj8eVsz1UD4fR/5C5qTg4wTYEr4lZMwZhn1g+T6Efbq
1KPg3vc9k1Ov5lA/6NMmmLn8fgDA3qCrXS8mUT8os1p++UBzF6h/TFOLP4NH
/SA7MPvG+qgJaFKR3n1rjEP9oGPzM4fSzUIQMeH6tke8mrB/ZX90Iv6RL8RE
6nCmRIaifnCTzuZxv2I5bP3gt2ao1YD6wZKur8p/0lqhiw/XbO4EYa8++9mq
MRvzJIgvlj/xw5LwOz4ukY5JRakxMKZm/e6y6QLqB3/+CSmqujYM8Mux6iTv
plE/OGNz1029rgeopolwiHuMoX5QnC6Hr3EDC1hzaLDLlv2oH/Tt2P/AZqgK
9H8MiD6y0or6QazGyM0r8snAgv3O0KWbmagfnEhU28x/nwmrfXoP3LIqRv1g
416+YBfGOuhx+bt8/0Y76gfptkbf83l2QGsJn/OzPYOoH+w4WcDuITMO5N/e
s9z3fRH1g2w33NW6tfBA+zIdhcnDWdQP6se0+fE87Qe2NXJcptETqB8c89Mz
3rvaCfi4tVyj6EdQP9j99WDjzI1GkPcCm2/m2oX6QSq3iGClyQLgdB2uT5hU
EfavLMYer91+BTkPPypMrHiD+kGPCChAcg7CWDE1djeVetQP8p6UccHeb4F2
P/SD6Wa7UT+oyd8bHGOLg2mjyzdJswh79Y2qll4H9UZBftA+HBcl4Xd8YUoK
tnR9g2DqaFp39eMp1A/eKKq8NxDfDXL5pRWfxY6ifvDtzcyYM7xYULbEarGf
tw/1gxrs6knss5WA/FNHbZh9C+oHqQ6/W/QUSQIZmJKhI64ZqB8swFyNP1GV
ATH83DeY2IpQPxi3vEHasV4D4ZWAW+tH2lE/OFjBLGoY1w7vuOdQCDwaQP0g
azBX+BfGXnhlW+SRVilhr35dhmHYb2QYKN35nOxJTfgdX6DJebVq0j4g+DLl
x8DTcdQP7vNgSaHo6QB7jsrYWu58L/zXDyayRB95O1IP/Kuy3zy53Yn6QXb5
rgwxz3yAf7KKKVWvRP0giRzbpMAPb8jpfYJd7nMI6gcdvR1dGp+WQvJ1/erl
tVrUD4peHluATU0wAzeBZUjCoX4wXsBQ0kquC0aWnFds9xtB/eClmNwecoN+
OONonuXkPYH6wQVd0lL5fQPAtz6xPoqO8Ds+RaEA/6XzOLA55Wt1yBOP+kHf
xw9OAoVW0LyKe98W3IP6QXOOfJWDhRVg66ZX3KHIJtQPrkTV/+ofSACxrFJF
8YtpqB8sN/hwRuN+OvQxjnUyeFKA+sGrUfwCnW+q4Rq5l/YBSizqB0f8in8v
eGLhFP1dhoLiPtQPNot/e1YT0A0XetVH6INHUT9oQXm21kJ1EKqcvBncSk3Y
n0/g1v1lJdMDtE57S/ccIfyOz8VU8JnPy3aQH59G/kZhAPWDl5973y5lrwOy
upM6pn/bUT/Io7xtOtafCwrMuBkqyStQP4ipBJtZF70gvmHKeFn7NaEfNEpZ
8DAthilKf1Mmb9SgfrD/fum5xc4GKChLFV7M3oX6QZpIVRmljg7ITd7xilx9
CPWDyx+sLQVe9EI1P1BqdGAc9YMjjZ/+5gwPQfwG01lP+WnUDw7rbU5T+nSC
J2c+JjLfH0b9YFmTNV/QYBMQoC6rOJGFQ/2gw8s2RZbr5QBTcffYQ8sG1A/W
kycyOF+OB64HjNofPE1F/SBeQ+4Oz2IqlBju+WrplYf6wcC/M66Dq5WQqZDs
xPmXLagfFPEZz/tg1QrNz9kzUmX2oH5QiLTp5OeqLuiQ08OleRKP+kEzs6n4
Rwz9cOzmn9uHVSZQP9hRn/P1J9UIfL1/2qZekLA/71JESeY/2wYWI/KMU/p6
UT+41J5HN89YAwJe+zs63SXcv0i05OHXwPlsUF8pse8wJUT9YPkjMaO5qx6Q
hNHuTn50EOoHnS43XqrdLNh5367c8LStQv3gGBsDeZBvHaxqtwqY1iTcX0VC
aKkhm68dCkaoN3/J70f9YHyt/J3NG92Q6kVYNbUp4X4d2iar6kWSAzDUrj1K
7xzhvhbf5tPYXjWMwJial7+KymdQP5jwrvLtbr4qODimupuv/t2HU/aCsNhu
vmqjnMHs5qt/96t8rMIXtZuv6kkc3+/mq3/3dVRv3y7YzVeT8irVu/nq3/0P
86fe39vNV/yOBnO7+erffQLD+xI+7uYrOb1Z8d189e9+egyJfa9389WFdAae
JxSE+9pxkuaTN+7kK5/rtj/L1gj3hWMW0WtL38lXV4ddXtDu5Kt/9zFTtOaS
0tnJV3c5PPDUO/nq3/2+JO7OvHokNg3ybMg4Epi+gYUbto4Kp2kwe/F7T86Y
TABmHqlL3+EyYGWZGLW7RouppCuw+6M2Cq7uNzS4tzkHSE1ni+sv0u3kT4vc
TtpBULnfs7+kZxKwWKlsiO9cdzokhObZj+MAybbMyBEHPJqbJcubvjCabAIM
RkeCyAtxIGQio61EgAHjNFVxbsaoCHzO9Wpw960Gj+YmZM0f7OQrxfgLDaM7
+crDPnxkKRQyJpx11HNlwDBdHVM29C6HuZG1tT9KG2DJ1/UEAxOG/3XvW2Lf
C4jlZ2I5k1geI5ZbiF3fiV0HiV0viJ1XiR1HYt8XiOVqYvmTWE4jlmeIXfeJ
XR+JXUeInW+JHV9ic3Vi82dic1pi80xicz9i8zFicyRi8xZicwli39+JzduJ
zaWJzW+JzTmJzQOJzc2IzZeIzWGIzSuIfa8/dTjM1PIOHnCyu3Qxv5pF+xt1
xr4btxb7AeOTgNSx74T9CkMScc7Roi5QRlXe5MRD2DdgahjUnwpoBrEkNSpA
mtDLS3dGfq19WwZKzD4ZxXfUoX65PPGXneq7AFAe3qH2jJbQ82ZMbTkfl8mH
weshDlFHCH2oYk3Md8pv9ZCc7PFGh1Un6vVimhccDX53QFfLoYtkwUOo/9Kx
/CbfINcH7b7YVC19Hkc9EbHrQrSpjjfPlXaQgHlDr7bzuv8d3ynNVSHe5Uaw
yNyeq/ST0AtwPBPRX71XBRLNbCfqZVoJ72eRwZB22QIgkcxDrzRKmDPPtDEO
qAYmgoKe/W/q9hDmrnicdv0Bfm9oVFnyRJyKMJ+MCMioEZRIgxGMJyZP++Sj
45vzBBq5ihfB3qeVny7eqybMtfgnqFmoK2Fyl82I/JFmdHz/t73eVKZ70bt9
36spKUrwn74vUSCvYPc8pnazYeu/57FDrufydntAcc2Ug3/+0wMOxiQ77faA
3U8xAf/tAdnpvybv9oBiA06u/+0Bz2WF3N/tAbPzDyj9twdco/KllqloBRFD
WQz/7QHPL5l67vaAeHz78n97QGfmtqXdHjCwrNL6vz3gkpIMlcHOeSnc7mj6
f89LwWRnDDqj8KC8nsKGJbYLuZRo4UxR8TCQ8Brm+HK6F/mFgOTK2sEBsMrA
eXvcdgB5bYldbn9SL9DS0Nd21B9G3uiZAj7ptoMPR0w3Sb0Iz7+LweHWYF4j
KM/LiH1nRHj+5mvflOjNKwFO8XCx4B7C8+Tx3GzXyMsGJpodGsV3CE5sXkrs
/Pw/fY5KbN+P2H7g//Q9QGL7BsT2E/6n7yEQ23skdnyJ7Q0S268jtodGbF+L
2F4Tsf0fYnsyxI4XsX1IYu8HYvuExPbuiO2nEdvjIrbvRGwviNj+DLHje/nh
+gFSqiJgkdDqHSedBVQzHg+H7Nm5DlYc/RI2UQzqZTSiu5+mIQ/25Xgr25cP
utJfwyrSXOShffCWsGEuIHFRmwzyykfulCOqkByXBWaGFnUOlhciL5i6Yheq
kQ50XutuXUspRl6f8BN+304BRu9jjEQlS5HP9P5dt9GPByK+4fXdD8oIz+fV
3mcjf8OBhA1fjbk8RG4RQX3afeslCDZvlLyYQ/D/ba+XWO4llpP/p+dhYjmK
2Osl9jjE3g/E/p8n/t/zQ/nDvsGKsqL/z/5/ADZ7cbQ=
      "], {{{
         EdgeForm[], Lighting -> Automatic, 
         RGBColor[0.87, 0.94, 1], 
         Opacity[0.5], 
         GraphicsGroup[{
           Polygon[CompressedData["
1:eJxNlGtfjUEUxZ8acr8VIpVSTq7dRIhKUrrTiRSR8ppPoaRyy+UL+HDud6V3
1vrN//mdXqyzz8yeWXvPzFpP5dSj4Yf5SZK8F4KwVcgTPFfMeJuwnfw6xvmM
H+vnubBe/3eQ9/99a/btFbbAtUsoEDYI+8l7XyXrXPMQ0RwHhJ3sKyUWCoeF
EvK7hU3CZmFGvbwIkbNcKCJfxj6Pq+B2/QphD7Wd2whXIT16PCu+lyGurxOq
qV9PzAgNxBrhJGdwnSfa9yrE/yNCj9ALfxlnWlH+u3Be///x39yXhDNCs3Cc
Xn1PR9hbTr1Sxu3CafZkuBvnTrDPPc2JeynEHluFU0KT0Eb0/mPCQeoVUcf8
jdRz/cvCWeGcMC3cEsaEq8JFuLuEFuGCcCWJ52vhDlqpuaBe3gp9cAwIg8KU
MMrcJt4wfZ+iNe9TiCY66cU15sX3JsT7M18H/Wbpz/Vv8A59xDbub5C1ndTv
h6OPvDmHyPtM14hdvN2PEM9oH1iD1re1b31ZZ/fQgeuPw2UO+8l+sUfuCNeF
YWFRfO9CXP9U8XWIergv3KTHB9y9+fLgcM0C7sb+ucv5R1g7SG33Z12nfnVM
fV9M3xP0kk1yPrS2ZvFaCTU85/8BXvPN8H1wflXxZ4h68F1UJFFn1uPXEPWU
+r8SLvvLfnuCB6upkyFnDnvjGL1Xsd96tU5rqOH8UXhPsMfrTjKew6e1nLkS
7kX06bteVvwWos5K4c4kOf/X06u/EbVJ7htRx9stcUbva2TPL819DLG3XnTh
N1pAw9af/WzdtSfRn47W6zx6sL6tOWsw9ZmjNWp/22v2pGtb215/jrzHqf/T
dfZIN+Mexp3oJfWH4wC9pB7qYK4/yXljiP1ZNNTD+bLU7GWum7x1/0dn+hTi
G5nP34BRdPKFb5fH/kakPnCcFJ7hl0nq2G/W/m/NfQhRB1nmrOsV3tRvtIp/
GzjDKNzuyZ68zbnH6Okv/TTzjuZvopb7P81ZPof4ht5nr05z1gl4fUfja3in
OdsyvqhlfTd3+RdO6/Y/lv+LLQ==
            "]], 
           Polygon[CompressedData["
1:eJwtkckyg0EURrs0tmzZGTbEEETEsIltskoVW8oD8BRmYuYVY0xMwc756n6L
U//5q7vvOLKz19jtSSmNQi+85ZR+YAVfhiv8EIbxcXjBb+AElvi/5nsM0/iY
7x/l8CGf619vZ+ABb8IWvgGvjqcYOr90Pr0dgFvnquDrcIef5vA63ONnOXwT
LvCDHG/7nO88x9k2POIfUMIXfH8f+vFBaPl8Ap+Ed/wP5vB59/8NUyl6fsK/
oIwvuj7VU8Orrk/51WvN/ak+5dJMnx1Psyw73meO3AXvo+vZzEIb/4XVFDtq
+b5ylzwvzUe5FbPj+8UUPXTcz1qKGG3vW7GL3ofyafcV16f+VIt6/gcbsEOr

            "]]}]}, {
         EdgeForm[], Lighting -> Automatic, 
         RGBColor[0.87, 0.94, 1], 
         Opacity[0.5], 
         GraphicsGroup[{
           Polygon[CompressedData["
1:eJxFlNlWU0EQRS+5+RF/h0c/waU4AQHBAQ2DSphkDmgGkhAMxoFAEEGGKE6o
3+QAnmNvlw973brV3dVVdbr73IXU+e5EFEUXRVIciiNxIr6IS6JD9IiUmBCT
IisW8feKe+KuuCH6xKBIiynxSCyLJbEiSqIunolyrP84+P2fE3nxVKyJgiiK
dVHzv+bmRb/soSh8b4ph/m3fEiP4PDcnrjHX3+vM68fuZI19V8RVanAtXaJb
3BG3nb9iFePQh0XW2X+fPXtZl6YnKXrk3gzQuyl6kWXvLmI41hN64NqroiJW
xUvxQjznuyNeY3vsDb5XYkPsiV3xU7n+iENMxz6Lg9AltPit/9M49LjAeHsy
1On6fsVhTgFd6uTgvbfRdlo8RuOG2BT74i25NLD30LXGHPvL1PivthpabzGn
Si82qG2NGA18CeUaJ0OPHMe1Ouc8fXStbcmwj2u27TWV6L+vzN51dLDWD9B0
AO1GxUNs65rBd0p/5mUvEN97ZtHYvfScBc6Ox87obZb+OudZ2XPoYZ/teeqx
hjNRmFORvSoO6JHtqvgcBV9Mfcto0s5+f++dvi1934lv4itnxmfnPX7f/RZj
J+i8w1gLDa3lR/EBv9d+J+Yuczx2zDlwXs7vE2v3sR2jSj1+d/ze+D1wjVuc
gwT6LXG+ctx/30/fU9t+E/ze9KFJhl7Ootko9gyaWlufWZ/dMTGOdu735Si8
Ab7Dvssd+Gz3oH2adRkYY+0cejj/ZhRqKpKf36YJ9HONfuuaxBokB8cZIUfn
5ndiGw1aaON4K5wnrynwHo0Tp0R8967I+fQZKHHHm8Q8Ir9h9vRefps2OVcH
6OR75Lvve+l7N8SaSepxHXViOvYfWA3ufw==
            "]]}]}, {
         EdgeForm[], Lighting -> Automatic, 
         RGBColor[0.87, 0.94, 1], 
         Opacity[0.5], 
         GraphicsGroup[{
           Polygon[CompressedData["
1:eJwtlNlWU0EQRfsml4RMCkYGgQTirPgnvvkJLp/1nwAn+BIVBxRBHBAwIAhB
QSCoDJ6zej/ster27erqOlXVjbsP7tzPhBDuiVQ0khAuiltiVExkQxgXH/Rv
RbwTs2JRfMF+L76yZntOLLH2UXwSq6KJj/eti+8hnrsgvnH+In4b7LHvZ7HG
GT57XiwTY4EYTc7YFj/FH3EoOpVDQVTFOZFP4prtblERZ0S/6BNnRZcYEBfE
U+X+RBRlnxclURa9oieJsX6Jv8QssqeH/WVi9OEzoz1vycm5dXGPQWK+1tob
cnJuXh8SV8WVJOryQ+yJ3yH6DPLvchI1sTab7GuhibVoi0fK5aHYkn2Axhuc
tUtN1rF30HUVTbfRuIndIoZj7XOnLdbbnO9Y7qEV/j0XL+iTWXKdQQvn7Fxr
4ho5T2vtJTWfw3ca22e8ClGzBXqpLp9hcUNcT2IvLXFP5+leHstGP+9/xp18
F/d2h4YgFUey/4VYM9fOczFCTwyg9SXxmP5wj52SrzXeR5e8zsqlUV/rM4H+
m9TJsRxzB819lnuuSt9Oyp7KRj1q5OZ73CTHTvneTuM9HH+Eu46yZ4z5nUfH
ceqxjKb29Rn79Ia/C34IyK3I9wE1rcgup7HX3fOlNO5p02P+57VD5iGHnrv0
mHNzTjVm0rGt0R491M1sDtHXU+yvU89j7Tlhrj3Ltp13gbnuEDlm2LOcFSkz
7jksoJdzdU45zunGZxL9Pbt1ZrgH3ev0Qj9vZIN/vdRkmLtXqZVzKKJHRnaS
xFla483wTBXQz/+seYY7V3g7EtbKvCuBNdt+a9wvrlPg231rjfLo4B4+Ik9r
4/q4hilxcuh/gl9Kvx7jV6LeWe6Rp19OqcMKb0KL+f4PoBDKiQ==
            "]]}]}, {
         EdgeForm[], Lighting -> Automatic, 
         RGBColor[0.87, 0.94, 1], 
         Opacity[0.5], 
         GraphicsGroup[{
           Polygon[CompressedData["
1:eJwtlNlWU0EQRe+VPCsg6LMf4IxTAFEMxGiYEgUCYQphkEAIgkFADDI5ix+C
OPJ/nrPuftisom91d9U51blUWM6UzwRBUBQx0aw/TWI0DIK86BAPxBOREnfF
PfFIdIp2cV88FknRxprjbr51sNc5dzjDex+C47R4Ki6LK+Q4t0Xc4gyfdVIX
BD/qont8fly0ii6RoLY4sWv8q9w/oqz4tVgVa+Kt2BaLYkmsi6pYFhWxKTaI
V8QWa2VyNjivXlo1iILiKXEe/WYUF/k/LX6phhF6u0qd7rFEDb77Fd+u0YNz
3OuxuIG2SXTMiAF06xFDYjCM7vkp+sPovl7RJ3JiGM2S7O1nby/ffIb3WuNu
vqfw/pnIoltNfBQf0DjBGT7rbCzSxFpMinOxaM3xRBj1cZMZaSNuwdt2vL4d
RjNnz61to5gOI407ubOH3hN430cNWWodF2P0aa08y6P0NEA8gtbXmaE4Xr8U
b/B8DV9q9P5CLLC+Sr/u8Tn6N9D/EJr+Zv48Y+7BsWfS8+a1RubDedPUmqX2
PP7bjy5qnGTOStRxIA7Fd3Ek/in3VHxW/C6MzvYMFqnlmPfTSt/22zOTQ8ML
seg3IE8NTewfQbMT5qsP3dP4k2P2LsaiM8bwoB7/B5kv9+4aXds6/VuzYWZ0
Cp8X6XGCnhfQvoCOS+S4L781v0m/zSrn+vwd9pXw0J7NiXk8XkEv1+S3vEte
lb323Ot+63t8d+zfgX3WtpiVQ7zYZO2AnG3mxm/lfRi93RQeZzjDed/EV+7Y
I/4iZqnZtVaYH/tYpN8ZcipoYO3twTz7dtDC8/AJP+3vLHv3qfWIGmrsca7f
eDN+znHPKf7tUp9/s/0O7a/fwH/lNZih
            "]]}]}, {
         EdgeForm[], Lighting -> Automatic, 
         RGBColor[0.87, 0.94, 1], 
         Opacity[0.5], 
         GraphicsGroup[{
           Polygon[CompressedData["
1:eJwtlFlWk0EQhdMmi1AiIAvyxSV4fNa1KCoiYEIcIjIjgRCSIIpgBARjEAQE
hwDihNMCvPf09/Cdv/7q6urqrtvdcfHKhcunEonEJZES10Ii0Sn6RK94J7ZF
U3wWW/hsfyLujngo8mJIDItpMSW6xW1xT9wlZx+xD0QPPtv3PS+peeKm7Kw4
r6JaxQvZs/KP6zshyv4XYyH6bJfEKD7bM9QyIorUNEiNrq0gTit3i3guewGf
x56IOWzPm8c3Ta6n+FzLY1GhppJqnBGTsqshfgvk8n+H1jqXinOd4xE1FYj9
Io7FX/FHHOGz/dt5lLsqDmX/CtGuiN0Qff6+Z94hfTsQJ+InY3vkdMy++CC+
sq57vcOcJj12r3+I79hNctm3iSYc95E1j6jV9Z3VXtOpeDY+oy5xS+REv2ij
v1XOyGfTnopnPU9sN9rJ0a8zqdhb99i9s6+EBqwXr1mhF87lNeboX5p+z9Kv
Y/b+jzPPsdYwOunHN0SfsvgG6Z01Y+2siVch6raLGMcWk1HTWeKvixsiE+K9
8dxFsU6OZ+R0rlXGlsRrYtrRz7Lsl5yXNbUSos93pJyM8Z5XRh875LBerKET
elxEr9bwgLga4hvgO9lDjRnGfMcr6O8AnUyxvwz7su2cA8xzL9wT1+V6vJb7
4v54zU7OxOv4XWgl3jFV/t2/WohvQBv68N5r7M+5PVYPURvWwwJn2YL+Fjnr
DfE2RM1b+3n25Xs8jj1AbRPU6/24d5Now2/ZCBrxm+W3y2/OKHaeXGPU6Fob
4k2Id8R3xXfpG3123/2mbtHDFWrcYO4ydoMzqJHL+02j9yW0UmfM93wX2/P2
8DXItY9vDV1tU8M6OtnBV0FPdfKtUrNr3SSHc/ke+U35D5XaGhQ=
            "]]}]}, {
         EdgeForm[], Lighting -> Automatic, 
         RGBColor[0.87, 0.94, 1], 
         Opacity[0.5], 
         GraphicsGroup[{
           Polygon[CompressedData["
1:eJwtlNlWU0EQRe9kVJAoGkMIIiYhQIgBFTARNCAogpgog+izy2f9RRVx+CDn
8ZzV+2Gv1be7uqvqVNWtvHi99yqJouilyMTHNIo+iW1troo5MS9uiY64K9bF
I7Ej1tjz+iG218Qyd2y7IXrcuSna3PHdRbGEr664Le6IB2JTrLDn9X2xwJ0u
dtfFDWxWWC9w5r2cOCmGxTknKmIxKAY4OyXOY5OJE+KsyIt30uJtGvbznOV4
yzYzEq2RBZ1aYlrrvnivO1V9f42j6FscfNq39f2QBh2c/2niKIgLxO3cttCg
je7raOZ912Zf7GGzxXoXnfviuXgmrhJXG+1b1LTD3g51ORRPxWfq71oeUNMd
zg6w7fH2ITVxbZ6Ix87fmlAH18bazGahL+bohXvE2EMva9gi1i69sE1em+S4
i49Z0aRn3AcN9hapu301s9CD88TS4MwxWfvjNPiyHs7VNVkjV9cpEWfokX9x
qNsAtRrkrCguikn5qorLWk/wnjXcJwf3wpSoJKEnnKtzdm/UknBWF1eSYGPf
qRjCTx97x+u4C/i1L/v8qfh+xaEvM/S2D789iR7e87qeBG285/WUOEpDXPY9
Qjz2aV9N9LI+q2h+TP8uUYc3zIdj/hGHefGbI+RgbWpinHidqzVz/M7Zd31n
iLw9AwVsx8m1iK3veEY9q5fEWBJm0LNYFqPit2L4E4e5zlG3Qd4poOkQufpd
a+c7trWGvvs3Dv8Ev+F/2DI9u0G8ztm6W2/P9vc41M2947V1SKllnfo36dMv
cfgnRPTWGLlMUZ8yedWp3xH6+N9TItdhbGybR4NRzovkVkGzEmfuhyr96pgc
i+eiRv96TqbpUfssYV+lXrad4a0ysfnNDhpZG/+j/gPZeGyZ
            "]]}]}, {
         EdgeForm[], Lighting -> Automatic, 
         RGBColor[0.87, 0.94, 1], 
         Opacity[0.5], 
         GraphicsGroup[{
           Polygon[CompressedData["
1:eJwtlNtSU0EQRU+dnB/xXUVQv8Mq37wgipGbQkgEAQNBJAYQASEiGq9cRUED
ioroFyAoyg+xd816WJVOn5np7t3Tc6wxc74jjqIoLRIxrT8z4qV4IWbFU/FW
vMHnbytiGfuVeIfP9muxiq8snuDzt/1UFP0RD2TniVXmm896Jp6LRbFA7Dkx
Tw5/tfcgFdZMiVNK+lwS/FPk6HUfxHtxVVwTN0Wb2BSfxS/xU/SLATEiSuKK
aBCtooWzHHuNM+fJax1fAzHa2PNPefwXH6nB9qH4HgffHDUucFZBDIoxMSoq
aGxtl8ReKmhmrbJxqLVWPEaLS/q9LJrEDXExDj7bab7Vi2bWrNAX67BBDMeq
ik9xOHeaHCrk75qW+L7MGRvsqWLviB/it9bupoKO1u8COTmX6+hmHazHN9a7
xiw5HhBvkXtwiJ6OvY3m6+z9So/dly3xhRyr5LJNrav0fJO1a+zdol+OWUEL
9+KeeEhPTkjr4+KR7HHuSoFe+c6c0bfTSfjv9bbrkvDNvjr6VWJ9mt5kRAf9
9B32PHgubNeIYhx8OXGbuP1omGZvuzhJfvdlD3O2+9xJjCZ0zeKz1ta8hbP9
20qMHLbvchc+256dbny2b4k7+NrJpVf0YDtuH74Mce/i6ySXPL59+p+jPs9q
IzEcy7W5B0NxqNHauGbXWiTHLnoywHp/n4hDz2rQ12/DpDibhJ6N0eM+8iii
Xy++YeL14LM9RI+cs3Mtob9j+OwJzsrTuyI52O+591u2i/7uST05OTffdb8L
niXfU79NO9wn5+s3cgbNrb3v6CC1d2MXuG/e47We41ruo23Pts+yBn6Ty+Q2
SWznOE7OznWW/b6jrtv1elb87vgsz4zfMc+qZ9pzeQRBr9oR
            "]]}]}, {
         EdgeForm[], Lighting -> Automatic, 
         RGBColor[0.87, 0.94, 1], 
         Opacity[0.5], 
         GraphicsGroup[{
           Polygon[CompressedData["
1:eJw1lOdWU0EUhS/3UiMQI2CvLyESBV4hy0dw+Vuf0N4oIkIKkAIBRIgGKQqR
orj3ms8f38rJzJk5Zfa5dx49ffgkjqLosWgX1SSKauKayIiSKIqPcirF4XdG
LPDf9iexyNoCZ7xXF1NiWhRFQbwUr1jzXlm+S+KX7L/itXjDvY71gjP2nSSm
46xy/wKx11grEGtFLIsHKuq++Cb7u7gn+674KntTJIrdLvpFnxjV3phoae9Q
zIl5URUVMSs+O2+xJPLEdKwavnnsKr2ZxXcRbK+Tc5tixqJXnEtC7W85N0PN
U8TIc+6L+CG249C3A3FG/1zbsNiIQ42uPScWk9CDOn1yL/x/mH7syt6Jg5/f
sM5+jdq26NcyvW2w5v66Z4fk4XheOyC3EdlZ8VP2PrW9oyeu8YOYoG9z2JPU
Ok+vytS8zhtUqW0Du0ZuXsvx3o5tXfm9nYNj79Efr9neJZf3vKlz6tCZTnFe
pNFGB3Y/e13Mhn18r2v7I07jMDueoQviOr8D4gb/bQ+Km6ylueequEKMNPZl
9jLMo312yPtYHFHTPrFP4nD3kLhFDNsXxW3WnsdhpibpteevQk2uocT7rzBH
Bebf8+q5Lf7/JsRhzT0uc9Z98FlrqIFO7O87PNee5wr9yRAziz59j89nea8t
NJZDPw20+SwONUygnVH0Zj03mQHPQo98u5OgyxZzFiVBl16zfYav7VQSzthu
YxZT5FrhLr/lCPrZRP89+A3ytt3cM4B/hf44L++tMvdNZtC5/Y7DN8Bxj3nH
DrTnb9F4e/Bp8fZ7+Nh3jPlr8j0Y5/tl29+IPjRlLV2irl40YW2coJ1OdH6K
lrvQeoozQ9R4RFzn5m/nGnO5TU3/AK9f99k=
            "]]}]}, {
         EdgeForm[], Lighting -> Automatic, 
         RGBColor[0.87, 0.94, 1], 
         Opacity[0.5], 
         GraphicsGroup[{
           Polygon[CompressedData["
1:eJxNlNdak0EQhvO4uRGsWKkSRZB0iiAGDGDESAgoREnUiGDFdp1CAONt+H3u
e+DBC5P5p+3szPastxaaZxKJxIZIiin9mRQPQiJREnnJJXEieVnsi/fih/gu
3ok98U18FX9EF71/2+9U1PD3t9/iJTan/H6BzaE4ErdFn+iIYzEncmJHNKlj
jzg7yI73VuyKA/ElxLP4TI8lV0SR81l+JG6Qx/luhWg7LVZD9GkT07E+izfo
LH+ilhY92Ud+JT6ge42PbT9ytg5ncc7r1ODcKXzs+5Med+npAed6LraI6dib
4hk5nXsDXYvaquKp2MbP/xucqU3vfQfr9LJOjCZ9te8aPtvY1flm+yfk2KI2
92yVmlzHQ1EmTpWeV8hR/6+mNmdqMAu71LiCT53crvUEvX8vhThXNepcJGeV
2irUtEnOMj5r1PQrxJnzDFwQ/WJA3BGj3Nch+lG+DYoxbLLcpedzFv24mBRF
5LtiCt2ESIsZMU1MxymIfIizcYQ+z7cxYhXAsvdzPsTd6GB7n9gZcY8cGWqc
Ref77tJf99n7UEhGX8fzblg3T4605Ewy1up4fg9yyRjv305KziZjbOss22eG
fN5L74XfCr8RfkscY47ap9lPvzULIdZim2POk8N+mXvLkn+Ju0yTv8y9XxZX
xLAYEmfFuRD37JroQWf5KlgeETeRbZtCN0icce6hQH88kyv4jND3CWyHuXP7
9FLTELE8dzV636C28yG+Ac7rWfNMXgxx5orMTYn7yDMD8/QnQ7999kVsp+il
ffyu+J3JMQcpdFlmw3kuhTjX/ci91DrA2VLYpqnVe9JHzSXmx73wXv4FycKa
HQ==
            "]]}]}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}}, {{
         Directive[
          RGBColor[0, 0, 1]], 
         Line[{2, 1, 11, 21, 31, 41, 1001, 51, 61, 71, 81, 91, 92, 93, 94, 95,
           910, 96, 97, 98, 99, 100, 90, 80, 70, 60, 1009, 50, 40, 30, 20, 10,
           9, 8, 7, 6, 901, 5, 4, 3, 2}]}, {
         Directive[
          RGBColor[0, 0, 1]], 
         Line[{102, 101, 111, 121, 131, 141, 1019, 151, 161, 171, 181, 191, 
          192, 193, 194, 195, 929, 196, 197, 198, 199, 200, 190, 180, 170, 
          160, 1027, 150, 140, 130, 120, 110, 109, 108, 107, 106, 920, 105, 
          104, 103, 102}]}, {
         Directive[
          RGBColor[0, 0, 1]], 
         Line[{202, 201, 211, 221, 231, 241, 1029, 251, 261, 271, 281, 291, 
          292, 293, 294, 295, 939, 296, 297, 298, 299, 300, 290, 280, 270, 
          260, 1037, 250, 240, 230, 220, 210, 209, 208, 207, 206, 930, 205, 
          204, 203, 202}]}, {
         Directive[
          RGBColor[0, 0, 1]], 
         Line[{302, 301, 311, 321, 331, 341, 1039, 351, 361, 371, 381, 391, 
          392, 393, 394, 395, 949, 396, 397, 398, 399, 400, 390, 380, 370, 
          360, 1047, 350, 340, 330, 320, 310, 309, 308, 307, 306, 940, 305, 
          304, 303, 302}]}, {
         Directive[
          RGBColor[0, 0, 1]], 
         Line[{402, 401, 411, 421, 431, 441, 1049, 451, 461, 471, 481, 491, 
          492, 493, 494, 495, 959, 496, 497, 498, 499, 500, 490, 480, 470, 
          460, 1057, 450, 440, 430, 420, 410, 409, 408, 407, 406, 950, 405, 
          404, 403, 402}]}, {
         Directive[
          RGBColor[0, 0, 1]], 
         Line[{502, 501, 511, 521, 531, 541, 1059, 551, 561, 571, 581, 591, 
          592, 593, 594, 595, 969, 596, 597, 598, 599, 600, 590, 580, 570, 
          560, 1067, 550, 540, 530, 520, 510, 509, 508, 507, 506, 960, 505, 
          504, 503, 502}]}, {
         Directive[
          RGBColor[0, 0, 1]], 
         Line[{602, 601, 611, 621, 631, 641, 1069, 651, 661, 671, 681, 691, 
          692, 693, 694, 695, 979, 696, 697, 698, 699, 700, 690, 680, 670, 
          660, 1077, 650, 640, 630, 620, 610, 609, 608, 607, 606, 970, 605, 
          604, 603, 602}]}, {
         Directive[
          RGBColor[0, 0, 1]], 
         Line[{702, 701, 711, 721, 731, 741, 1079, 751, 761, 771, 781, 791, 
          792, 793, 794, 795, 989, 796, 797, 798, 799, 800, 790, 780, 770, 
          760, 1087, 750, 740, 730, 720, 710, 709, 708, 707, 706, 980, 705, 
          704, 703, 702}]}, {
         Directive[
          RGBColor[0, 0, 1]], 
         Line[{802, 801, 811, 821, 831, 841, 1089, 851, 861, 871, 881, 891, 
          892, 893, 894, 895, 999, 896, 897, 898, 899, 900, 890, 880, 870, 
          860, 1097, 850, 840, 830, 820, 810, 809, 808, 807, 806, 990, 805, 
          804, 803, 
          802}]}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, \
{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, \
{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, \
{}, {
         GrayLevel[0.2], 
         Line[{901, 911, 902, 912, 903, 913, 904, 914, 905, 915, 906, 916, 
          907, 917, 908, 918, 909, 919, 910}], 
         Line[{920, 921, 922, 923, 924, 1098, 925, 926, 927, 928, 929}], 
         Line[{930, 931, 932, 933, 934, 1099, 935, 936, 937, 938, 939}], 
         Line[{940, 941, 942, 943, 944, 1100, 945, 946, 947, 948, 949}], 
         Line[{950, 951, 952, 953, 954, 1101, 955, 956, 957, 958, 959}], 
         Line[{960, 961, 962, 963, 964, 1102, 965, 966, 967, 968, 969}], 
         Line[{970, 971, 972, 973, 974, 1103, 975, 976, 977, 978, 979}], 
         Line[{980, 981, 982, 983, 984, 1104, 985, 986, 987, 988, 989}], 
         Line[{990, 991, 992, 993, 994, 1105, 995, 996, 997, 998, 999}]}, {
         GrayLevel[0.2], 
         Line[{1001, 1010, 1000, 1011, 1002, 1012, 1003, 1013, 1004, 915, 
          1005, 1014, 1006, 1015, 1007, 1016, 1008, 1017, 1009}], 
         Line[{1019, 1018, 1020, 1021, 1022, 1098, 1023, 1024, 1025, 1026, 
          1027}], 
         Line[{1029, 1028, 1030, 1031, 1032, 1099, 1033, 1034, 1035, 1036, 
          1037}], 
         Line[{1039, 1038, 1040, 1041, 1042, 1100, 1043, 1044, 1045, 1046, 
          1047}], 
         Line[{1049, 1048, 1050, 1051, 1052, 1101, 1053, 1054, 1055, 1056, 
          1057}], 
         Line[{1059, 1058, 1060, 1061, 1062, 1102, 1063, 1064, 1065, 1066, 
          1067}], 
         Line[{1069, 1068, 1070, 1071, 1072, 1103, 1073, 1074, 1075, 1076, 
          1077}], 
         Line[{1079, 1078, 1080, 1081, 1082, 1104, 1083, 1084, 1085, 1086, 
          1087}], 
         Line[{1089, 1088, 1090, 1091, 1092, 1105, 1093, 1094, 1095, 1096, 
          1097}]}}}, VertexNormals -> CompressedData["
1:eJws23c4lu0bB/AyQr1GCimyklFWlJDnTEZRZGWUFGkp0iRCNBCKlEolMylb
VvHc9pa9995UJIn83Ofz++s9js97Hud1Xtd134+H45uQ9RXDs3SrVq0yYFi1
in7lv8kHjXyU/AageaL2svn1Jgh96621WD0Ip4I2fBSV6ocWeZVrV7vaYI+J
MHpCv5ntw8BeeHb65IvPs13Q6bxZk/S30X2lXXe6YXTE7etb416IJ/IOkS7+
pydM63o7xNHlZkrM98Gz2BjsE7AwHk+/oRkY2CXe1Z4aAP0Zfw3Sv2vdrxm6
WAf1txXGRL0GQfP+Zqx3FAhZq8daAeF3W2Q8bYeA02we652dQyLO/siFoStD
zzcxDYO0aC/O88h+PfOi+0d4o2Oas3B8GNYyRdP2lXhjwvZIHxBsTHJ7/9VD
+A8dZ2mtEZgc2bK/aEMvMBjHZLhPNgO94RP0p6n2MRd1umGs/3OgSmA73GoU
vUN6mLXrare1HeC4J5phqrML1jSLuJK+8xC/7NprLcC2Z4kzeKAH2pdeYR+9
m03cF/waYJvjwbPd2/rAf8kc+8y4lmQNnq0BbkHzz907+uEH7EIXvHF2gwpP
KfBIKceZzfUDdRNg/2uyr7/ukaLCrPTKaXgMwPleG+y/V9PN2OpVDOz6kuPv
VjkA3E85XUiXrFcajYvuBrZV3rd/a9fCj1k2xpwrY5A4LSQx690JwSfO69Al
N0AupRz9ppxA1l+2duBb9hfKamoGvRBxetJr1zoyPYcWuCYzqbQzuQ12h3Jg
fSFXle87kUZY/rbezFyxE4RHz2E9n+RXykRhLfwnfdX4kFU3/LfqKbomqBaq
WlRCGpexKOezHrg4HsBAeuCSXNKmxkKYW/QOFFfvBfaj57H/zv6ZKyc2fYGm
HWciqV97wUB4Bv3O8rSwxoUosN27HGG62AteAT7oHEd8GV/ptQGDB2+7GrUS
4gZ/SzZoTkBi1kX+rf4tYKgs0iMrXwuX90rvID3WY5+3gFsTTMoc/vGzph6K
bsuhNxql7eQXb4A7edevbZNtgtmvuehrBNM2OD6tBSWTGqtD2i3gJNKPfvxV
gF/coyqIrUqqp3K1gZNRGfqZ1925okElEJ3CytD2pB3sTzWia4R6OMoa5MES
x0H57ykd0PhFGD1x29wfvuQ0iKi4rWfr0glnp26ga/ofZMuaC4MQK5nT1UOd
cGPdy504v17q5VtRDfDf/nWTDf7FwKqU9yUmfxLi9rNfLFSvh+R2nT0i/8pB
3f8z+vqKyTB4Xwv5XaVOe3mrYe33CPRRul/76SOqIcqqT5D9Yi38SU1AFzrM
3x8TVAG+V9P++rDVw0yt1FfSpbNFPFSkS6DYalCgzbEBHkgWY31TgtPhd+75
kORcX/worBFEV7Nh/S2VvSyKUl/B4pMkzNxvgjMK37B+oqGuyooxGRItuU4e
EG6Gha5udHWZXcWq70JgvFTnutetZvjx5D72Kfu3u0XvWhVUunkE7eLOhRvZ
F08+6J0C4vtMlYxLBTisz3L5tL8A7F8fQtfb1zN4RKoMrl3JvHFCuhimyu+h
q89Yx008KYZv5gIO9E2lELRsgd5r/stF/1MBlB9oDFGTqYA0PwN075PT90K8
c+GAADX3064q8Ja9jb53Zuiq9ZkvEHVum99Q6zeYNjiNrst46w5Yp0LWw7iu
DvYamJe8i57glrW1/uAHaNKXMuK4WwOV+ifQJXbtkl1cfLry/nVa7ciugUCn
YPR/nz+8yqTkgXmwi6RfcwrUx5mZ94dOA/+a8t+cm3JBoFv6Y6NfOrznV0VX
eke9qeyaA7qKmrMTC1nwg8EU/dDd/fIKdl/gGdeVjCXuHBCVNEdPjBWPoLJk
gH8fvcOeZgIsqZbosqBm63cyFTwFXkxc580DEV1anzNmFeKXRxIgc9l+8uj6
fBC/aIH+nvCVkPz8HpTT7n0WrMkH7jW66EN77LivO7wBj/FLtb5GBfBkTAM9
vbpK84nqQzAOazT8+aoAEs8cQj/y5GZ8rmEkKNlwLLXwPYfXu5eqLs1PQ53H
tcyXzBHgOG7u4KPxCuZ1af6dtWCrx4MwEHWumr7G9xbspWjud8X9p/ubt5Bx
SDdU6UgY/Hi2iL7f/OHauzWvILlgw+aipnCIkqXVVzcFn11OeQ6zCwW7/4lH
Qu91mpcyeb1e1A+EtlNmfzMpURBQQ+tj+zCf97WDDzywskzx3hgNru60ekPf
3bONne7g/F4QymOiYQuV5iy3AsZSxy+A+c29SXFz0aA6TPO9hTG7zjxNAIET
4UFBqWFgfHOCg2FymqgzbWNe6x0HV605HRiXIuFc+yS6WPWefW9EPsLa316x
J5hiwJxrCj21l+1GzOUYUL9VWLRYGgvmWrT6Z8PvU3IuRUHYqlzm3Upx4GY2
jS7Hdzv7uGQ4nOuM9TPQT4C5cVq9pKy2u+/Z1+BKiF3uoE8CVoLWX2uUd7FX
4BmoX+RvWI5LgguWtPpDXJymJ3f7QLmtwu87Qsnw3ZVWf+W9xM7c9OtQtmP4
v4FTyWD6jVY/0CxwL5exEBwl++9c7kmD98mtQjV3pgnu8w/2vk/MB0F6f8UQ
6S8g7VaFrpE5W+23NQ/6AzsFtkpSYTG2Ef2v54BiVQIVDI845e7qz4X/0hrQ
f9u/MAonvoDO1up1jHfywaC0Fl2Cvb1k9Z10EK/8t8elrQBYq2l9LgwLSJz9
kQz3xNVzDywUgu79JvSvgTwcGhYf4WyZDVd1cxGEx9Hq6wQUxu55hoHKOcVM
G4diODpUgX5z4Mu/x3U+UNZ3fVgovxjy0+rRHxw+FlyVVgvmaSNDMeYFYG3l
Rc9lOQV3NAa1d3vVgK5Z3hs/8RJ4xDuEziZxVbTj0Dfo13A5a5RQDm6eb9Dp
r/yaSxSpgJ51pSqd3VVgv6kPfW9/g0HDhxLIoQpfeWReAw3enuiLcDNHs78A
mG8lu3o9qYW+uLPoot843rbmEFAuU8y64XEdaAs5oEu+FDz0XT8DnrEsu7AY
1sM6Diq6cP6Tm/UK8eCs95t9VWs9fHhVgB4lyJEj2BwMfS+sko8INUAfmzP6
hrUlo4RVCbD8lrjM7pcFQ1rmqwKPTEO9NRRBXREM+tQ9lAiiwlsVA/TLdvtu
tC0VwGuHV6mdb/Lg4M2b6PyFjo0mY3lQ+1GfzTezAII/O6H3hjSxP9xHwAU2
senZ50VQMW6Gvvq1WxCh9gXcN576qyhTAo/lT6DnfRJK55z9DFbWb8UPuZdC
hrEG+ptss7A7pQlQ3pZovMqzDP4FUdDf1bENRhyMBD6QSZNRKIeAUBv09gpB
gY9ij6E4Ui41MqgcqkZprv3wcKkIXzpYUtYvHhb/AD5thWdUWqahjbN02jLi
M6QV3rS0Ho2D+qUi9AfcrHY3hlPg2GtbnZcxSZAVQPPV5Y7ebd+TgN89U3Us
OgUePqS51WmZM94+8VCT/yvrxMXPsFxbgk7w+UptbvgAspoTLicm0sCppQB9
awbdx4M1kTDBF1ksIpkBrSeq0TdPlqR92/IWClQT1pVtzYQnJyvRm0Ys725X
eQpSD6TWHyzJhP49tPndc+9qfL13B5YOPK3xEcqCfx9y0d1+7t0k7/0QfrQo
nDoZ6gQHbWpkTyxPE92roocUmR4As2vtvqRbruB1lOZP5epftqjfg4vCMxf+
zd8F9f/Xs3K217xX9wCdfQyLrKvvgb0pzUMfBZ8c5HWD3lr7If6q+7DmGM3V
bOdvXNe9DZuqdXd7mD4EbX2az5tPWPLL3YC+0Q39x197wdQ5mgdVXpDg+mgH
xpdHH3565g0j/183y4r3r9ozaxi+qCdhtd8HdprQPPrii9KN7LrgureI/+Qb
H4j9/7rru7+ePpL2FW6HK3nt1EqArQp3+27kTROvAu7m9LRnQdF/j6bS+VOg
LMcXXcpg8+OapAyokl+nJNr9GRwi/dB9r4crzCqnwWmupwvxlhnAaR6IfpPy
1prDMwW66z3m7J6t/LxO80b/ayz4zpk/EWrTQmdtb3+FmgO0PkoiVu0vVu49
Vv5uZCxTDuS4vUR/FLzF9ahqBLi8UW/dr0wFn1MB6GsKQ/J7W4Ih5/Hc/Qus
BDxzc0W/MiYRzNDlAe8+S089cCNAPdcDPVjuudU6oXJwLZzKdvHLBruq8dHD
Aiuf82yFRSHWpWCQELb1AlceCL9qRR+lqy/YaVoMEa+8L0QkFMDS6d/oPhO/
+3evfM73RzA8zWQphpfUOvTxve/cWW7kQRhD1nllgVK4oNqJnna5Zei+aQ5E
6wt7B42UgdSmZnSe3Jf/GdlmgBh/KZ3g6Qr44/oDvfa0xDN5+WRIoZcUp3hU
gkVEE7pH2KSPn280JJ2e3HTyUBXM5HahH5OldJq2BoBHXYYCc2oV7N1XhR5j
QTfotLYeei+G3uiuKoTr/6z39WyeIooFvv8WKaqFc0EzUwxDpTAZVYNuOeOi
8/lgDXjy5Yk/TqwEcflA9NDInqPZUlWwLGvz98DK9729Bl/RvcP/qny/UgZf
2BLer31WC68FXqDfELW0a7AqAsWP008Ne+vg9btr6HZMFpZ7OPKg3uTIw38/
6+F6si964JlN76+uz4KdV3WvJxQ1gBrvKfSx0t9aEecSgeHDI7Eyk5Xfwz47
ohvVvvF1fvwSDOcbPO9HNYKYPYF+lHv/Yb3SFoBMVca6DRVwOa5o3YWMCYL3
zd2ebsFm2HAgtYxOoQa+P7v2H+nvd2vUXJJtBDkTDd3Zj3Vw9OsgOmPQb8/1
S3Xg1yTi82uoAfQGNNCPnRCKLfCvgZbd9Q7zPU2gWOSJfp9vWNemoQJYqF13
iKctcKk+C9c1PJHSfGZ3MTQc1ub+MdcKohtssP6kmYW4pn4unFVvShJnaYfO
I2/QI6NCuXWufIbHFuyeDMXtIMAigC7GvXSZa+4dsHJGHpDd0QEhfw6he3rd
ZR5U7IJH/w7fFvGsAYmz82maf8aIK6Zjq4LlO0A25tmXtrx6aMygyyD9g/TH
5X1NrZDKXbNFrqAJtgRPpZO+rSd0t4FYM8i+2fUh9k4rGETqo3/cKWZ/WKYB
3sJPtY+D7RDFZYL9N9jk6Pj+roGopZpvwXOdoBOyCfvvHwlYuLaxAkT1vN1H
P3SDfrg39onnejdzdKAAdmQTbpEuPaB/eBL9/L/5W9SxTJDV0KwrmeoB3YsU
9D+pauFn7kSC4Ko8mVs7e2HLaAqu+2D1v2aWqF4QWlRYA1r1UDBUnPNncITY
lcPKE36rB77aRlQarG+GAutSKunRRq7XXCS7QEz76IaAoDbwyDmArvJt7/db
zO1gPqfWPZjXCXlmE9mk3+BUb9gU2AyHNgazbNPugb1btmN9Qml4tWF1PTCJ
RYiMufbCk8o4XNdc49UXutBqyD1pfuaWSx80HhbEPsxtxzb4K5WA3dB9ygdK
P7TdW4v1iuybYr14cmB1reA1nqJ+qNn9C5335fwuxdz3wJDn7reFdQDUm1Rw
3R1r9/tFmHWBl3m9xZpnNSDU2vLKeHIMQuWvim4z7ICdAU9yDzTXQ1rm7Zek
r9bXM1D52QpNHQVxbe1N4K5w9znpvd93q53f3wzslmt7tV62Qlxo1QvSxZjk
GCQ1G+AnT/lsP30HvOcIwz6vhJNPTLLUwn6tjeGsPF2gL+CD9ZeOvGIrl6sA
tR8f+vdUdIPOrsBg0pvGU491/imAY773TNz9e+A/tj/PSDdQ+fEuhzELfh3x
sxVc3Qvdm1LR67ctpt8NjIT51w+C10MvrDaOwf5ss/YaO8ZbgE9O1n5UqQIe
FO7c/zp+Aqii0tsuqjVDNstO7m86NeCuWahGumf132xlvUao3sMlrVhUBx/r
BoD0i93c/SU89fB9sz7XSYZG+MYcgvVb4sdkWaNrYMFyS5PY6mZovG2H/mZ6
Y8SjvxWg0NDPY5DWAjeV1NANBoz7w42LoW+Tf7XjljY48pgJ+8tUee/Ze37l
9+Uu3m1yMu2gEW6F9aeWdFRZ/D6D8d7aCO7JdrjccRz9Q0DR3wfrw2A+UfVL
zcq9Pdx4DPc1P3FfhUmmHhwUbtvU/CqEt13T6SYbpuAq3bs0teFaCGvdqFLH
VQbRISXojoXO2b/O1cDrycKKsKFKUJdLQZfxuiaVd7YKfrYms22SroE3O25k
kN6rZWtUHFYGWR8PCJevfE8/JEDB+v2cfPa9j4qA8yr7cCBjPRyX3oP1NyKE
sudl80BtofjAYd4GUBe8i+4yf2H/U7UsqBmmi3sy2QC7GR+jX+T5ZTwekQhV
ra3zue6NMHxjHPvb2d9peZfzEqqJ/Y/Xf2sEyc9uWM/wQMEp8nI5PFeenNvQ
nQ1Vekfu82+ehvOTg5pisaXgw36lsUErD77bUdDPqOWPf3tRDMsanFlBwwWQ
qyyFPtbw/L0QFMK80M5YZ+1iuGQliH50oxuT9/s8CInp/0N/thRqe8bukZ5/
z/XKhbgcEBhdpeUtXw4VPdpYL7mHWWV9QgYIRV7VNkqsgPiYdKxXFtx0e84m
Gd441Nel1VdCVaMt1p86Efu4tywaxA9YdW98WwVfTGewvmjIO457bSC0/uzx
m2X+BgLXz2B9k4bE3+dHs2Eob2coMZUADQeZ5pKp0yC9jk+y69IX4My0Wv9i
5fuRY9MmdBWHGbmvCpnwWeWm60mjNFDv+v2LdIWi/eE7ktOg8Km1WXXPyvcv
ulH0CuHsowKjKdDBEuXTKfxl5bnjwD7Bf0Jas8sToSnr2cbznNmg2LqI9UpK
Q9ztlrFw+XXUqaOxOfBPlB7rjZadL3ZHREDKW9XswD7qyucLD/pgjeiuzWYv
4O0fC1GDLwSY3WVAZ4/R4Lmn5gmDdqJKP7lzgfHtAPYXnn0YttnGF5xfRxTd
nHQFdr1wh/+Wp4Ex4/C+zQE+QBlx6gwb8YBAe5qrn3/uZ3PJC9qMW1kWw+/D
9eM0tzQUH81ZvA+MfvWZuzZ7AZc7zV/Eiyo3HPYE8wCTBidtH7gZTXPfb9Vb
dfPd4H3UVNiIuC90OdE89sijlAZfJ7jVEnX6ZYofOIXSfHLh9/sOpmvQaBco
Ht/rD/73aP7l2dl7U8/PgZeoGIttymOQc6C5iOtHamysAbxNPrmoIvwEFExo
Xn3iV/QpjzSi0OJaxy73GOLRu1VlVm3ToNtvID29/jNhUDGQtFogjpiqXigl
fU3gzwXBMymE1Idd2fTyScQR3v+wfuaaOkXTOYlQ17fztxJJIcSUv2N9xuee
nOIvccRfPQNGl7ZUIm2CAesPnxo9orf9AzFC2dczaJhGaF6dxfrb07xZwZqR
BH3LuU9LXumEpewP9GzrJiFW/TdEqsdz9583MwijH3/QE79Ysfl8DSQKD1lz
NfJmEpNZ/9Br3g27CWe7EMO/mZbeXc0kXjxnx3WDy5fm47lKiJD0L39/7M0i
whtsi0QNpiFow6Cnmm8RIcU4VJAkQyU2hvmg32GM4a3JLiAecce/DjiTRxR7
30EfeyZHZU3KI7pKmSWtnQqI5v+M0VMrioyGmqgEB5TVfzlaRFjXu6DXz8Vz
Gk1kEZ9SG66U9xcTBVZG6Ld8sw2HEz8Tu4X7FZ7LlBJhrqXoz3M8RE1sEojo
spg0epkyIrryGvo1wTM+o8yRhCPVzHK6q4xg57yJ7j1s8eNxkT/hV3+5fqdq
OaGaaYc+Y2nJNxhQS9j+qHE2lisgfhW8jmI5OwVjPZIOBTY1xOXmTA/t+WLC
cJMZeobOFmGC5RuxTea/v5m3yoknssHoWmIpS4Lj5cSZlyYsr8OriPMfrqDv
XBRveuBYQghftstzVKwhrAQd0Nf+nXGpzyogop48LZuyrSVGf95H1xg8vS/V
hSC4Op51nz9fR6zbU4he1OHMaS6YQQjsDBkrkqgnlB/6olcNmv749TuOmJy9
rxKZWE9kW/uge/+ZdvKKCV75HsQrb/67nqi5tR79uMiDM66nmgkNujp44VNG
GP5X2De3aRL01IPrRWoaiWTnR3S7A74R6oMX+0l/+qqnZvxvPaGWqtYr8bqW
EEnzRNfYtvrJ65FaQs5rgScyfeX/j6min3+QpeKhVE3cW3WvuiuokShP7MX+
8x8jW3dDOaH3+qHpws5mYjQmCf1b49pj//0sJKKK1VeP3Wkh3jraob9aZ1fu
WUQlKKU7pH3cWwmzUBvsH9yb9b1IM5UQfCNtEiPXRuQmMaEf4vHUzt0WSrxx
fbCFM7CNaPH4TutTYqGwsl/Iji3kXtkv/Gu+M7XixMGKw4kr+4UsgZM5CgHf
YCEqD/2P/+yHlf2ChcJi4cp+QelQFLqgcrfDyn7hdCX7VER6PTwzWUAfHQ2d
vqtUDWJ0lE8r+4Wq9DR0DkIzfmW/EBmmJbayX2i/X4kumuohuLJf2MGoWT96
pwUKBx3R6f2zo1f2CyeEDP95u7eCpq0L+qzky9yV/cJF2RjRlf1Cnch19PK0
j5LEtlDYUag2uj6wDZomUtH3JmdK6Qx0gFjfZVGlwGpYX3ntD+PVceLF056w
6Lk24KuXSavprwNPfVF0J0XHnO7UFijjLlo8XdAIYRH66J/jJeq2iDTB/u5m
bhXLFhDbeQ49tavF5fqRehhe0+MvGtcGntSL6FNlF3507K2BCfeAgdboDmCq
+Yq+9KfEl3+gDBb8bnps0ewC7ewd6IwvNr/LZyiAv/5TOhX3u4GqVz5P+ra7
A3UZdRnAP+I+6yLbA10mG7E+NnRQ8Fx4BOTcuCFw81IPMMXS+rTdsAieXtML
FZIcp16G1EGHYJTx1WujhKqByPY1pd0Q8uOIQL96E1yQ23OM9Fv5+7Ma+Tvh
5EW2X73vWyG9hQ/rj9nJfM7gaYPEkwNe/Zkd8JrbAeuPsTju2ks0QYbKJs09
Dt3QvpsR6wd9354c3loPL7JSzov09YDMV1msN/Clmjjc/wabDk+HdTb1Qn/X
JfT62AHOTNtiYHt4TEwzqA/S5sRNSLc7ohd+bWM2fBD4KcLE1Q/zI2+x/5ap
g6yvf0XD3merB3yO98OVJC/sM/Y8cOD32X7wfFl2r1muEYLs+RfuiwwT8Mj8
g9fmPjgcTsTFnmyFnY8Y/pB+wlWbd/p5D2z5ZJd2VqkT9qabzJMup1sv/jOr
E7rFIoPtLXrg7gMB9MK46tgdY63wMqrH1nOkF7IGdmOf/Qyjt4NKG0E60bVl
3b7+ld+LX/0m/cLvvx8VrGthTuNfa5vhADQ+UMM+1Od9/w2vvG/0To6S7hKD
wM12FfucmtxV8WCJgIHoYBGjnEGQaZdFb/IV0XZmiIXcbR4M9/8bgrql07gv
jdPl1nlegyB3OGdyx8OV9+hk18dHvgPE/Rr/rn6eAfjUwsB5XGzl+zaTdyzp
KlHtrc8v9AFjl4FVnWIPBOgmx5Nu9V8ok4R7Dwgs7HIsEO8Dz9LyONJftowZ
6xZ0wOxPIY1TPf0rP78vfiJ99qLWsJNUC7CIJ35TMh+E9KI57CM8c6BI73A9
AHXQMP7xEFw/W4B9rO9ziNwwqwSGiOqCkTvD0F8ZgvWXLm40qMzNg3h+meW/
giNg5H4K6xurmCtPFn6CLQs5Cz1OI/BpqhJ9zenUyYCzw8BsE9KnHNAK2Tvc
Pqek9BI/gS2ounFljrVHecRedkEay+VU0oPddOr4Vw8A3XPeiJx3vZDXEphO
+oGvHNfVJ1d+jx2q4i742g+r9IWx/oSrsP47tW7wNrDtcX41CMGDlWmk130e
i2fWaoObpt55q+SH4TMrA647WLbzsNvvBqim7so/eG8Elo1+YB+/CvF8z6pv
wFjuLiX5cHTl92ZBrD9ssTm0VKcAhGb/ht3aOwbpPfQ4T8fwPFPwzgTgy8/a
vfBiDN7AT6z//Unmj8efEVinO7n9mXY7aHfwWsyqdxKMPtoj5XrDwG5zgVXa
uAcc9jVYky7xrkTe9fgg8OwIUmGo6YMG9mtWpM9qbnMsFO6H5x7WbDc5BmGr
bcl50ks0hATCE3rgVQ79Kxu2YZBc7YJ91ig8/hy1rgPOTZxr8y4YgW/12uak
8439MHaxaoLQGrNQV8kxiPpZjPUFUj8efXCogcbA8KhGyji42gyfJH1fiMX6
jheFIKQ8fk58aRyM2WVOk84pfbK/cHMS8LgJ0fOfmQCGeOpZ0nc2vJZ4RRkD
6RYF8eK4DniifdmOv7iJkByueCnjNgIvCrOyv4j3gjPL31ukp4qm+151GIK/
Gi8dBwv7Ic6T5jHZoa1tWwYg/65PXrTwEBgc17pBOpXpp2iJVy8Eb7Vnuqc8
ArwlS1dJj9biHfjs1An5lTdu9jKOQUeQpD3py+ePMlTfbwZ/3vWp3O7joFTG
jv1vQv9YjFYteOYZNLuET8Cck6Mj6coD3t1jkUUAeVcZ7lychE9br1whPZbn
sfq5iSR4K/Ij/13DJJzw/459FDSYNzbIjkODX/SbK2mdUJmccfnUqhqC3oih
DE6PgmVmtPvmkl6QzPS7QXqtiqC6PwzD9yOcYOEyABIN8vakv10PKjcaB2Cz
Ta39zNQQmHxa50D62oQ2qWcSffC2QPG18OZRyIswv0T60VypkEzvLmAsEO7l
mBuDzsixa6RvFwx+0HuyBZhngz5235kAXSujq6QLWWqPbRmqBUXto5w5sZPg
uJbPmXQNSdbv7y8Vg9LV6/IzTlNwjz78OunRmYPCr7yT4UB7Y/3M8BTM9ytc
Id2E99P87qpx+HRauPHI6S5w7mPT38uRTwRXb8mV+zkKV45c1zGX6oO7FQmX
SX9cu8H9QNEwWPl+thf/MQCH55WsST8Ln4O0dAfhR1rsltZrw3A6U9aYdKpg
tinL0z7I0ovT9/g4Cr+7TE+QnmaxesF4bzf0J3jUCgaOw7WoRVx3fCLShXGs
Bfp3yqtnCE4C/auRo6QXPJZby6pbBwfzw5hNjadAyeGyEemTr3LsX00Uw6SR
56CC+Mr3ezkhE9J92Zc9HSaTIUPWcjL11TTYfWU+RXpCy2vLkYQu6K48+J84
1wTxvo1erSjemMr7R8SB9PEOVybS39U1mfozq1BOeY+dIz1hgG4t6QcY7W60
zZtT7ZyULpHe9a6RmfTr2mnpEjIm1HbnPFfSW8uL6El3cFq7hpHXnHJZcvQq
6dGcdNi/wEJPT2fPBoqgosl10idOhK4h/eAgz+ONdhRqhsMo+ty2W+i2ebNb
2K4rUibd1l8j3Y3tN/qBOh7e8gI5imd7zE3SbyssMpLOrDu1d72zNMU8Pe4W
6bkzn9F/yifNvzITp/Cr7bxPOuN6RjrSkykPNdflmlDm7Vfbkd6zwRz3dbIu
jnLFQo+6z8jQmXT16TQG0p8xC7e4BBpR5Lm5cV0q4zach0HINTBEUIcamWmH
+00rF8H9hkcEnheNkaSuvWmK9a8kObGeX2TysDfLIWpLu8cN0ls8j6DzisXp
BDfuoPZXpuM5sC7cQ5+dyLjwdJssZdX8EazX+HUcfVSqPOflGmHK+io3R9K/
B7jgfm2c39xtt99JyctKxvsN32aK8yRqZuTsd6OjhnA/wP0m8u/G/U4qWGSK
qx6g7rqmiOeclzRBO//SRTGjVlFKbtk67H/pbwj2dwnJ0I7nUqPURx3Fc846
9Q19R1Q8l7K9JuXm+WO43x1faPt9Wl0wMactTC3+twfn//brNO0eFfNYM9bs
o6wWt8c5fV5dxDlfFjM7HC4Rp+yeMMPz3HhbAt3GvsPWg+UAxeqcMq5LkWrA
dYXfzpSEJPBSJYZfos9YfEG/03lP+N5LfUqhGOC+4p6P4brdx97vYpuSp3A2
0M7/933a+V+USljfYSZP3cBm70R68h5l7PP97kLJi1FhasOeDqx/3iCP9VyZ
6qVaXxmoj9atwvvyGkxDrzstbuIg8FeVkUMbPWYmHL3piRn7vQB16snITTgn
77lO7L9B69vj/WKbqE/zVHHOe79pc/aUbEtPqZeimnaH4flsJU7gOdQ0dti8
kD5AjR5gxTn/OBtjn6p3dPzl00M545Ml2N//fjw6EWT586m4HoWj/Tq6n0sh
usXLW9tbvPdRz7VM4Jwde27iujFz27uKGaQo61mTcB4KUYFe0J38cIRvO2VN
20a8Xz4L2vPvE2G/ZzJWjEpfoY/1xoWD6Ed2Tf5U3raPsq8oinZufw6gm1KN
7hdVslDDWS5gH7lIVtr5r+bvmq8WpEx/l8Y+zvTT6Krsf5fefpxWPfTroz3p
x1oG8Bw2Dl0OCg0QogaqirmQXlHsgu+p/8m83dtDDCkb8ySxj5r4d+xzrOlA
wbuL26i32rjwPNUzPbCPdXzRPpba/VRRyTNY/4HooT0nWUP62w8KUa5qD+H8
Khvl0KnvQ6RsJrdRnxSnoMN2QG8Xi+e5wSFDfWwjgeepGPIRPeLvU+t/k5zU
nK6ztH3R0fr3TEkraFpuoGalDeO9QFMU3gu7weKdQXUVylTgAvYf8tmB9V8U
e+WuB4hQ7K/XY5/WDfnoH4MyGIxNpalXrV7iuvLafuhXtetjyj//yPFJscY+
bYzH0I/O3DrBJbWZai2fge+XdyY7ngP91sOiF74AxVFPBeuNSk9h/a6lUmYn
wV2U/A56XLepZAE9fJXWxRLz3ZTVyen4HDqe2o7zlzBHmnDZ7KEmPJjB+t0G
WVi/PujE3HwmhVJW4Yp+8XYrulswb/u0ghxVfCgY1w0s0ELvfXA0oDdBieJ6
2Qp9zJI2v4IN0bO1XYh6xqcB55dMY8L5x/pMlMvKxagvs8/jPJ7fVXAeU5Wb
HcdKV+635Qreu6sI7XNGzW9xp0jSeur5cxvw801k+CXWH9g7+pNPW5lq/dQT
57y5owXXlVfu1Bll3kOtVvmI8zxxUUNPv+vUGDh7gDo9rnWFdEPKF+x/UntO
VmtwO6Us1Qj7J5t4Yf8bzFfUQsLFKdxSyjg/T7cc1id5Do5/juOj3JTegnMm
qt9Fv5R3j/KGR5mazdWE92tn74LrLvoWdrOwylPeuN/HObOjmtHVE+45TTkI
UNa+uI/v15smRvTiuJoEugkByo57HVi/J4RAnzx2mOPB6d0UaUY9rL9L3Yje
qLdBxMWFl6ohU4Pr0q9zRX88naz//NUuSuGpLOxjIF+GXtS49ViAgCwlS5i2
7pk52rp859cypwpJU7YfPYR9TMQj0L91yEbsvLWeosKjgee5wfokuveXmcIX
japUmUDa8zkSSns+tR/fHzzDLkrlTZrCPgzpN7D+7Za1lzTjxCkeG5WwD4MN
7efL5N1FzhS6XdTYnBM4D8cyB3plYHnX5xE+6j2zbLwXgzxbvBemWav99Wq7
qJIvefF9VBKhfT4z+Z3YmF1DR/XxvID77dXuxj63TJgmGs7vpDS9lXvtPD4N
GpF3dBu3pBBOh2q9V+UPq0be10J3StXSIF0nYtiPTW87ZeCtBHrqZnoD0rVC
U1LD7i7kDL/hQ/+Pm2pC+t2T0UclP0lTuYdF0Wd53huS3j1w5+7PTkaqYSvN
TSXi0G1ZRYO0f0tQn27dQZsn2Fyf9ECH60oHmASpWg9k0I9zXdQjPURGnKkn
ZDBnn7AkelJpKdbbXhFbrxhJTxFLl0ZnOVGI9eXe05xtu1mpNUVi6G3C9Ti/
hTNXuIEeE0VJjBe96LSGGemKKelD6Q2C1Ow1+9FfsoocJF1og4X1112SlEUt
2jw3vO9j//Qzd8Sa/UQoJWFK6Amts9qkBzwISRlkl6fsZpVClwt1P0p6ojWL
7BmNjdSfF2l9FARvYh9Dg6xwAeWNlEtBtHqD2xpY/+tj8yqgMlDnw2nnX6O3
GuePu71scJpxVtV0hHaexx5F43m27v+zQ4iem7KQq4wu03oT5zmjdGKIc4Gf
wq68C92iQhHvPSU0OljbdQPFSJFWf9IzBesdr2dn5YrJUp5do52zqlU0nrPh
rbdvg7fwUVqSaX1209FjH3Uzt+NDJQuq4avl0X1au4+QzrY7b1nBkY3Kr0eb
f1htO84/rjOzxXMTC+XEFtp9+Qcy436Lou/VJ57cSGE9R/PdMcN4PvJjG4ZL
EydVNW7tRD8llI/1oxbPdSTZVlO8dFTRY9+YHiK9b4tfccAeQcqiKu08hQVs
sT4tYLUk25Y+VcYU2rl9cc7Bc+uYctfNCi5V5Q2kPYfWW8Vwv/tNnwfEs/JT
zdVo6ypeb8U+kS3vlVmWBlWN9tLcxq4TvSxqyK42P011/xOaOwpkoD/yO+wU
KsNKHZGguX9JP7q94uUhzyhOqtY6mmcd/IGuf0f12dledqrdpm3oF/5cNSLd
zNDlkpPdNorsLyF0esEzxqRvv9E/ayo+rzp9g/Zc1RxywHPz61wnu82dlRK0
SZz2vH1MwPNPjbr8qcufmcKbSnsvjKza0ZklA3w/r3zvdZGj3XsLRz6eg3Rf
0J/Cr+upskO0ewyk9KN3/+oRlH/zN4c5ltbfYvUV7CPY+v6PSh0DlbqVVm8m
rIJeEuv6uWxgRPVhCu1+E2aLcE6i8wP9ecMFVbEQ2jlwG6TgOfT1Naz6bjqa
syuZNo9+mQ+uu+7MgN5Djv+okRTaPbIbfcd7fJP+mMU8mZPKJ097Dh+sK8fn
sHlvgVXayvNQ7Uhbd21DN66rf65fb+DZOsqmAdpzUjsuiOs2rg2Lb9Dnpsro
0eoddvzE+usTqo+52JiopUy0fZXt18R9PeWWfqOgzEQlPtGenw2nt+Ccu8pu
JvxrWFY9zkHb19a109j/L8OeZP0nLNRf+2i+y7od/SXLpb1RL4dVufbvQf82
cPUwznN49SqpP0IUeea96MTgBx28RxdRz0MneSmPGGmf59MEge9jdcmnradY
+CnnxmjzWx2Lx/nHuJe04g1ZKX9aaPfemVSO8zv+DN19jnkVdcaaVn+LcRTr
ldYp7BGUpaNqp9D2dd9tE+5rYhcf270NvarvKmTRWzVbcd24m83b+qnjOX4l
tOfhz2Mr7L/G2H+7Rdec6olJWp8Ht/7hfv8kfvihvmsmZ38L7X6tg5ywfyyz
R1T2PlaKtBbtXt56nsP6ygUfHmWP1dRcIZpvD7iFrnpr6qm3HxclQJ7Wf6OJ
PvZJtlIIcR9cT629R+t/pPQt+oua1a8eBKylHrxA+xzLsBPB+emLhucKLRtU
E4xp79FDdzc8h71fX8l37VlN3fIf7XxiddhxXV691BeLH9ZQrexp5yn88Q/u
16jw1mbV5U2Urfwi6M8zevD93XpBwS3LhoPifJI2vzjFAvvU8ih17jjOQt0k
Rnueh1IW8HlmNb5rcmH1cs6xhP8/b+1/cX6N2z7XCl6vp9zKpZ1zcoINrnt2
f2ebqQ0d9QwT7RyKqs5gvbVip2vE17WUcVba89bH9x3XVQ7ZmVLe3pezfTvN
a64MoltcGL4tvYWBsjSqgA7RzPgeVW/vu8syOaPKx0Wbp+U04LoDGpY/WzwH
VVk+0J4H+tkRPM/kO4ZZjCOs1PoZ2vlfu3wZ57nxSebu4yBGyj0O2vy2iinY
RzU+Rq1WbTP16yjNh90M0W8bzyyozrFSjmvQzk3B4Dztc7i9gqM+bx21uZN2
/hx+pVjf6Fi0Rqt3OUdnLa2+fOgu7f2Sd+JLoStR/WxL81u2Juj0BMNjGYm+
nMZl2vPAHtWD+7XVusG7w2wddZ8P7Xx8i7KwnqV5ps3r5H/UNdto792Efgru
t7/oupfDhuGcTx7bwsn8lfmrVsxfMS/OBpE5JdujS3Jk/kogyr+EzF/xW/Wj
865OO0rmr/7EmKWT+Su9/W3oxzP4NMn8VXOYyTiZv9rufw9zUDy7HP4j81cH
U38vkfkr+7eJ6Gw9apZk/uq8xpsYMn9VKKSHOa4H8R4cZP6qec/SKJm/kud5
hvUO+o3GZP5qec8DMzJ/dfhNCa47Myf4ksxfvftreonMX/27dxTr/aRMs8j8
1a5PAW/I/NU3ohjrR2eEVMj81U+27vNk/krse+ABMqfUPKYlRuav/t47w07m
r5JlNqNnLRsmkPkrB7t/MmT+yrNUF/3BaN4Amb+6F5bDQeav1l6QQE8yer2d
zF/16cti/uo0ZQT9B2/1BzJ/ZdSjv57MX21IOYDeZ9XYTeavtj81LSDzV1Sn
BcxTpUccVifzV+eLrwmT+avFLQboTKLfZMn81bJfZwiZvwrba69O+larH0Nk
/urnfb0kMn/FndCA/eluXGQj81dBwoxKZP6qqIIxl8wpnf864Uvmrwz/bmUg
81fTIa0E6Wx0UUFk/ur9pOZTMn+lF5qBvtn+bW/u2SooXfrzk0e6BgTdatD7
3bZxkPmrqU8p82VptUD5+AP9YZHVUTJ/xfOnPZvMX+U+OoD+e2uvP5m/uvVp
Bw+Zv7ol5YDzOMVYcJD5K94NZz3J/NW24stY3/Ake2AsIhFcP+SUkvmrGc8F
9GaGVQOhOS+h+7rsKTJ/5Z63F/tMf++5TeavhCwrfpP5KwmTXZhTevKX/hCZ
vyo33FZP5q/+TSuif4sOmyDzV+m2Zulk/iprHaCXTY5j/mo/x5doMn9VbaWB
/kBzbMnrfR6s+3RumcxfTX5axHzUzXVfLpH5q33EBR0yfxXfzof19JFPDpD5
K7ljehQyf2VynJb7yj9v7U3mr2bmZRrI/NV7BwX0rz1ViWT+yjT9fiuZv4q5
tBs92exVGJm/sn0+FkjmryyBB/33k0rN4KPZ8OWgaBeZv5pKos6SOaXxcxkm
ZP7qUImkBZm/GhxuRI/PEjtD5q9SO3nTyPxVrGYl+vft7QVk/mr7Gt7nZP6q
IL8evUXNwovMXyk/fFdM5q8+NOejq342WibzVwV/NCzI/NWIWyv6YKYhkPkr
Y5uOGDJ/xTDVgH6j5gJB5q88UlsYnvZRYZ18ObrAmuslZP6qeVLUjsxfOa6n
ef46FkYyf+XhOXmCzF/d2l2BPk1fkcVr4wuFYYcWyfzVUmsc5pSuKvKb8gb4
QPtSaBeZvxpopPnZ3805Zy55wTiPTuvf8PsQWkjzj7m/w8j8VcSLfavJ/NW7
HJq3nNuTV3/YE/IWN+uQ+av2CporhcjsO5LvBmqWpevI/NVS2f/Xnd/zmMxf
fb3CGfkixQ/OJND8Vai2Apm/UidOu8T1+sNsOM13PSSMxp+fg4mLYVcvpjyG
0SKarxFVsY2MNYD8OX4DMn/16x3NT47NLFt6pEHxtfdSu9xjoK1brcKqbZp4
obTrydT6z+BN59WxSiAOHE0PorOcKLMXPJMCX9+WH6CXT4LyVC30SSk6QsM5
CcabQhZOi6SA9G9N9LUilR1FX+KAjan4inNbKsgXaqAfyGvz193+AcRfeCkN
GqZBaLI6ule/9YZgzUiwyRhfXPRKBylBmrN0d9/5T/8NqFWlFv24mQEJygfQ
W6X7A72/BsKmTT3HG3gzoeUBrf+jo47JQtkuYGJYfPDd1UxwyaegOzAHqyZw
lQCVy0nn594sOCumkSdqME2821JZruZbBDL+X1mTZagw9/AY+su6RNOa7ALY
Mj3yLeBMHkCNOjqn6ugwa1IeRP1hsrd2KgDXj3vQj6v2vB9qokJrh8jmr0eL
4PxNmpdVsV8xmsgCwrg4vry/GI5s0kX/nE0fMpz4GcTiWu4/lymFTAFZdDsh
+i8mNgmw43vYEr1MGQxF0uZ88L00epQ5ElZzsyVNd5XB0/od6EzGlr5PivzB
U4JfSEq1HAw2G6EniVxmHwyoheGnzOeN5QrAzuVADMvZKaJBUNSqwKYGjNIp
V7Xni2GpTxT98YIuE8HyDVYtLA9m3ioHVfkz6K2iFj2C4+VQsan1Z0h4FYiL
26OnSXbmPnAsgbp331MdFWvgJ6GO3vRim2N9VgEEd/tkTNnWQu7hI+hPeGKk
U10I0L24v+b8+Tq4su0o+mRCKru5YAb85PdqKZKoh82H7dCjbQI//vodB2tj
23ZEJtbDA1kT9AR7/6CHMcHQx1QpYP67HmwKz6O735o7TOaR1szHyZN5JLNo
iRHMC7G15pN5pEZPsVEyj1QVrYhOaVHPIfNIudK3W8g80sFBBfTFnUN3yTzS
KcmtjJHpKz/fDF+jx+s/2OihVA3Hsq4QZB5pINsV/db1O4VkHslRVk6NzCPt
uWCMfjnZVYnMI1Gqro2QeaTwy/3oExfflZB5pNhuOW4f91b4J5KMbn/1bSeZ
R1oVwg9kHilQ/RJ6SrLVOTKPdOTjHwbOwDZYW6qPrhPJFvS+NBF6H79+xXky
HOacuvB7mMArCeWXsfHwZ+rdpp6SKBjXHkTX4xaT6N/5CULF39uu/xEDuUat
6I9twdLq+AfgDkto/PTtI/Ak9dD+DlLaLruoHA2SG5/xRprGA78grX+l6mmr
Jy3hUDBQd3DuXiLYCo2in58rVXju8gbuvX7oYvM+CQLf0ProAb/O1oln8Nek
9cI+k2SI6aD1+ShwxEtu9yMYexSSbVqdDPO6/eias5JratxugBAlXHgvRwoc
0mqmfb+fdJil44sgRngWzI3Lggj4t7xs+mcabNbwmb0cCiM23ww1kE94QZzi
XrXKbMWNpTj3Rf4OJfrhuYPeodcE3ROaqwbu1NbPfU089J2+oOUXSnw4sRr9
mPlbw1NyLwn7C/B9MCOM8Auk9Wfi1rL6z+QZ8YBye757Npy4GEXrk+i4Pvq+
cgChFN8SKP4ngujcQfOvo6ayd5u9iP1Tq6Z98iIJo8+0Pr/S25KsOl2JcP5j
530PRxG86rR1t83uX/Xj+HmiWHNI+/bzKCI7k9ancssGtZ4fuQTX7dnC5/dW
vp9byKsfiJgGtd0+IivjETH5lWrm4unEpTZJ9KJcn+t1f7KJtOdF2WJeWURZ
AAWdkS9y/OrvLEJ7Iknk5PNsYpWOMjprWOpXrsh0Yv7E7T4jbYLYOquAHpAn
J6vGlEoQWjNBalW5xAfnrehDTeE3ki0TCIlsCcGq3Dwiqk8NPUvsoMI/vffE
Ezr+sMN38gnpXFH0h8kObzJ+vCaWBDL47Zbyidfy4ugtXEt3rrY9IGK2Ove/
OlBA9DrT+rS2Kc6CWBXhpyh+1bqNIJ4eNDM9MjwFHT87rUV2VxCqj26rv2Qv
IIYonOisbd/YBSdLCeuqWvPaX0WEF8t5dKWuVWttTYqJmgimn5+DS4kKYUDf
KN77Vup2AUGV9AudGy0n9PmaTEinnIr9eNMyl5ALb6KDkUqi6r4u1jdMOE9t
FfxCqL7SpFf3/UbsETyOrsVo+bZJNpU46Zac1FxVTQhMqqJvUZM6OMXygWAu
Vziy2qSG2OFsia7pWqL7rPIpIWxlt+1eYA0hlncYPS+5kkXnbgPh6r/kpXeu
mKh3Xfu4sHwS3oSarqETrieaXb9QG7+VE/vbJtGnpzZMGHjVEkXqYRxZfd8I
UZsX6KeXUy2f2VYTR+JXefvp1BLP5EWekK5fH/fI+EwFcW68omXbZB1xtOop
1n/Oyxd+w1hCRB471fbXqIE41Xsb601s/gQpnMonJH+HP4lybiT0a3dg/Y9f
60ReLH0hFp6ezbtzsonYzJSA7hY1nuY/nEQETYvuzvzTRNQFFaMPrHM6+Mwh
hAC6mxEXdZuJ9LQM9Dv/7ZNU3tlG1E+FOaa+rCS0I15JSOmv/B50/tyltXYt
xHK/cXcGXy3xy6AE/ao1+43s003EbrbT1WwZ9YTdojl6WZq+ZQlrA9F2h5td
bEMTsf3FPknSzULCgz1caglNBTkjf4kWIvynNjrHoTWPN9hUEVz3dYQ+zrQS
pWZj2Gded8trsxslxAm2ap3mS+1Eam0Uuo/+94MBynlEokCP5FX/DqJemB29
yyTzUeGzNML8iIrloHEnEWpuh/0viT665t8TRgy++MFPJToJwvQR1svqKv/w
etANb5cZKOf31kJwh/hzuTtjxDvrTr1Z2044smD2Zul1A8xdYUVXu3WG9e5M
G2gkdxR6UJvhd+frYNJH5W28Zra3wOr9Fx6zB7eBweIm9NPZ2/23szVC2ayb
dhxvJ5xRPP+M9KEp5ofdKbWQ9mNdyAW1bpALfYE+1uvHdgUq4e9/fftU7vXA
3LUBXPf1xZa734lC+OFxivpOrhcYT0mga/5UXdu0lAWcmeV3vsb2gmf8dvQl
jZ5+D6MoeDMbW8s51gstC2ew/79nt/k+qPZBLEPtDPfUys/NnZ2yL4xHiMLZ
+Hf/GHuBzo174662ZjBsOILOy8zTskOuGwYfnD32z7kd8kdV0A0VXUW7Z9rh
UtO+AKaiLlhDXZIhvUFhVb+BZQuoFD2w+9XUAx3ff0qTzt7OEtTg0gA3K+nO
7OPug4vcO7F+6y2OfZ/MagD8vvtd4OuHZT3aupb/fLMq6Ethw/PiXZShfrgk
vBr91ua5rZ0bqfA0eHjT/qsDkM4wgv3dDp1tOPYwBnrDqboZXwcgMNQE64np
wwuzbgMwNqI+0GrTBJ6/HyXu6Bkk1lR9zCsQ7IetmTb5PFVtoPv2WgLpd3Yf
0ZN+2Atv2TJ1M3q6oOqoJNYb+9sX+Fzohkd+On2XtXuBbzOtzz9u939+Vu3w
RNZ9YXS8DzbctY4n3b+S5Rw/QzPsd9YNjyVzobu4kkiPD7PvpLOsg4Pn4gBu
D0LrGgX0b3WNLY1/y+FDvvkXZosheH9fHvswnW+UuzGUCwK3DRmU54fAIvo2
zsl8+4V6h/1HSJEc/zd4eBiqH0lhveIFrp++7kPQkz3bNvKsBTYURrAmbu8n
dDkckrctD4BJb7XYqUud8CCT5z/S59jqSq7t64dTlxUDb//sgXz74+tIZ90i
LMeo2gsjDmmjeX/64PareqzflLWod4ytC/62POMXKhoApUYN7H/KoV3nq3or
1BY6dU/oDYHzsRasv69+/b+TEg2QOJXbqv10GEoFS9FjPVv2JIRWAXsnG5+K
7wjonlJBDy2LZNrqnw/3dIlPCXtGwespBecpPFUzPUUfD8svuZW8no5CnKIz
egnjQiD5dx19U8F14jq9xOoStRsSN/dTQg9SAkifs6tnJZ2VNdJUaaMa5Yp0
Bf59qFM+Zw3pXVPqgvzmlpRiB3Xsszll33+k88HlN6WSxpSjviH+pB+pn15P
+vOTw+e8dZSpfvHbH5N+4xkXemnKxWwDz+PUb7t8nuC/I7w2yUZ6z+0gGRUD
OcrBL+/QN1uUo28osvh65rchha7HDfu82evHQbry415b48W5nC9tdejH7FnQ
x0//+LrTRppC7/ED+8itOox9ljf/4pCWVaJ4tp/Gf6eodk6TgfSjxnEpCVdt
KBBojH0e8n3DPo8ZUgPM6YSpfqrufqQ7rYvdQDrleGFP8d5D1J9a3tg/r3cK
+5s6lI86ch6gHJEg0N+df4V+5PCGmyWv1CiPyhewf4pKBTvpzbJWdTXPDlD8
jp7A+sRRBXSHxN+hp4WYKAqBN7E+JCME55mXrX+Z2LKJkpRejs7Cx41uXXlc
PIVPiFLea4RzGjydwDkf16es2fRVk9qa4Y73FVHbhfc+/TjwhP34AcqX7/N4
X/3VFngv1232CTaZ7aBaHzJF3/nMjJP07xzjfMsJhtR/uddx3cBHb3HdtTJr
uewLNSlndiigXw1chX18wvorjPuPUI+lW+O+cqwlcF81np+UbFVUKZJOa9Fb
a1LQtc+XfLu9R4IqJcOBbjoWi66bIuT2vWk/5d6ZQuyf/0sA1023U3q9JLWO
UunT4Et6+WDKRtIjdo5yCFIPUg9xVuPzzDr1A5/PfUu84vlKhpSNqlO4r3Bn
O5wzX2KVTe9GDmqxxyOsd0y3wOf/fkLrstILTcoBFXv0csIb/V/klaa6h3qU
zZIM6IbfpfB+12xReLj9hyq1VuIlzqnaZINzBqeal8rZL6kqfLLEfbUnyOC+
fOM2WIYelqQKjDRhffJWetr9Ro8erblwgML1/gD6a4Yx9KKidVe7yuUpxQmL
6I+0S7GP8y2zr7lLq6mDF0uw/w1/P5xHVyrmlvunvRQLsavovwY4sX51sLTD
Wg1dqtGT33gOmnOWeA69in5a40L61PpX+9Ethl3w3om3x58F9bNTB+2vom9u
k0HXO/1RiuOCGtVkF+05lBijPYfLcx3/FPcIUeTtjqH/LqpEF0teeGOorEq5
48WMHuWtgesmv3gv6KNNoTRtCsA5/6X24fw/42z5Kpi3UC+/z0SP8I1EV2gO
09M/DlS1Jtr9Nmd/x/v97PpJ6vioNsVTeDPWb7oUgvuVONt/eODpQcqfxLfo
gT5V2IdFbrUpV58Y9fKObHxfFMV08H3p0dQUus67m2pzfwbnrHnSRHs+l52d
0u9wUh75cGOfy4xh6Ek+wVI8a5Wox9N3o69zuoK+00hGnO8GHWVbxFl0nqZt
6Pa1fsnurXKUASER9MrzfugFFvU7+XklKO+CF3Ffb9Z+wn2d4eWMOTayl/LW
vQbrLc674fz37pz6UH5SgdLhXoj30uOSgOeZl7bHPjFZnWr/7xPOPzCjieff
/G2Q0dNzP1Ve7hS6Q0AOutzGmS+mE7soX75GopvVGqJL8OiY6cxwUCqHotCD
lvXRr1aNe1jEU6jJ/cPoxKcRnP/9Sw/fqO+KFBs5e5zT3oMH/ZPT3PFvFBVq
0NpL6L8v8qEXVTtcyd4iR7l9Yg7nz2w8jfN/fXbNx7hci6r7Zhr7n4juwHq9
u493bq/eTdlzfR268CJg/Ynt/FJTNyWoiuFHaV7ViHP2/FazbDslSf1Ml42+
iU4KXebO+Nj7rcrUjmeFOE/YXACe5+a7TTP5SsLURZNs9IDKt7SfO048jbJT
DFTtmfPoxspCOE/k9lV2Y2pcFDvNK9i/8kEU9k/QfasveFiBQgRy4OftwQlf
vMeHERH/6PLlqTzXv+B+s4SpOL/9/K+jX6oPUH8xncQ+66dysY/iw6cFTFv3
U/3pP+C6djdycJ7r2gdXnS6VpHQkGOBz8mN3Gn4ulb1wnuJL2kTdHEn7XF3w
p32uVrw8Nh2/fR3VhP43eu6lA9jnEnEraY8mDyVngQ99ZvE51j8+rLXd13E1
NfBlFLpIST7Wf/wvdEbMrUa1JakLffrIBfRH5su27OskqfzrL9Peu9rN2Od/
LZx5OFVdG8YzewvJkESpQ8YkU1LOeZQGUTI0mIUUkUpKFIqUlKG8GV6UDJki
SZNhb7NkykyIzAqHRBp9Z639/fu71vXsffZ13fvc+1nPur0H3051z6gyek2o
+nMc0ZjT9/4nI3BuG6H2+Bj+vcGGhfj3rmD2lgXmyjKeuYVjbu3hgXlrdesG
fT46MclzCNeR0NXFdT63NK3Z4chOOJ2SoZ5nXRp+L7F5KCad36HF+JzghJ/P
KcUo/Hy8830uM0/JEJIF1Pvhj18srhOsf89QxV+dEehP+ROlHsqfGH9t6H40
LcG4ZMiG63g4q+Hf+2aF649D69iImxPqlN+Ypf7v5uq5Q9+sl2JYZDTr+ep3
gYeKC7dc7iAwcp1CY1uHwStzheRiQDtY27+ezrz5CYziFst0DcZgObe0w8uL
LaBWu9t1jfFHkK8hk/jPf4Zvb+cNDyi/hz8+yVJ5ql3wKbJDdq/+BLyEWdn9
DjXQqgkdUNECssfqlzAqJiHxuI3q+dVloN+94UyBah08Yv9VazUwBX3N7Byd
6i/AYWFoICegBM6JJqduesiEK/YX5StKo0GX3ik57J8EfuoG1ecWmJDQVM8m
WpsE95fp/nq/KxvmnVIXhSaZ5FnV3Mh1D17DQENopq9SOXiE+mzR9GWSE+Gi
5v38neCQHvnSNnYAcs0ytoWvGoW26KKpUdk2uF/AftPdrB/4PdKEnzqPw7zr
+lMX/zRB6W9jI/v8bli2dHbMy/kLGN23IiW06iGyPsvz66l2qHkZeOf5ikkY
H+ixzROrgjf/kA90ut9Da51oJN1uCnS0tWcVTxTDu+zI817KVdDVZ5EpeIAJ
85eM+1uuPYFbaxpGxYl86A03yDXuZEKnN6nJN9sPbqm63QXSX2BPxdIb7i9v
wcqN9v2X7XJh58DvmLDZApA5wTYTV8ok922TbhitqoQ8qUFT9bn38I8jXXoP
67p+C78zE8ki6BjluLDgUAUW6424V7Ou694z9+nLpizIT4g1mVZ8ASd3qYZa
ojwrs8GuJ88vQ2V62PDBe0Gg6DKQh3KWLDhOcV/jfQYVYTu3id4sgLxhu9xU
1nW9dYTf0QVL4HCCceEZzhpwFnooOC/FJJ+tX7L9q0s1pMwGHQ3Y3gx2ixEj
dMkp0vV4uPMBnwYIc5XtyynsgJ+qHgoJbybIV79v/Qm53wzGdGeVMzM9wEW7
LWH1+zMpYCWwK/p5G2h2pXEojPSDSnHo3u7xMTJOwl43L6MZHq4YZjQL9sJh
YVvjM9OfYSwmdplGagP479u+Rqy3A4S2Fre3PZ0A/TS1mfGoasj32XHnBuv7
LNFz1vCNyBRIPVwx+5pRAq6KzMiW/TXwK1DWrRbtG8Z7vbM8+Qwu61XW1q8u
hPMHe4eGCCYc2TxZ49t+FW4UK5qraodA4qOOzfyLTHjmELyYdDaT/LCSEVAe
lU+6ZYvv8f/AhFrewkZRhyJyyUfazLrVVWRFzJRzlgmT9T+UTguIrSSz71hd
62p6T1oWyqzccmIKNr9qum5K1JGHx8Xgm307GaT1UfXb6km4/OuiPosDcBIi
LA5O+4voLE66fBExCaxoAi5B9bTBrG7gU1AUGDr/hdTZ/HJe/GsrHCOsGb4H
++HIBlJf+uI4mXSpPPgj67lkeNker74/AEZDfx5/khslfytf33LL7wPQGnwM
oh4NgYnNTVfjiCHynkSO9+vaHjixtE3J0W0ETG4+aQt99YnsbYq4zE98hKAp
qDk7PQqdA2MyEgd6SRvex/RkuX4wDPOr65MfhxulWct+1beTTw7ntSkW90Pm
8KtXJSs+g0D+iKkRN+t3l4rxFqz5BNOPLsTT0lk++u4DHm2xMjLVtA3Pl5ZM
OeI5UrotM9NlvwHDdXQHnl/dNpZMnQuoyJ1TddIlDF5l47nTe9nUuRWzh3lP
Z3foMTxpqZgnc73CfM59mY/LqlVEsYcRriNwORHX2eLqltcRocAwCtDBc7Pz
26m54tY/Yib+f+WJ+a98eL2qXR5ev8n+dANNRYFh+84Vz80uCTGl5t7rRHZa
/lUjDt3mxDy72B3zIOXMyEs5UowDA8LUnG3oKcyv1PVeoOdxExr91LzN43UZ
eN4mac8u9Sp3KeLvEWp+bHp7AZ4DeWuxuT72jwSjuIuaD+k/PYP5FG9A8w7h
meL2/89xGZ2l5rh0sh2dHV900tuXUXMv+WnNeO7lsVRdaVMSLyEjTs0RJd7+
B6+/edWugPlFkJhfQ83tKHZTc7CdfuOaepJsBN9Faj17XD+eR/reIfPzCedC
cXoItf7wnAJe3yZo2vSUc54u6Eld9/SbFGr+59yWcuaSyWLibP1upF/Ph1V7
kH4fuR44gPRr8SOcE+n3rN5ncaTfe8IjHUi//fq0EqTfYz2mDki/e/tKDyH9
mmofnkT6dbMeiEH67eVeifWrZPaaifQbyiMl0MDSrzRnxiDS7yGf+kNIvzVv
vd9uZumXv3MA63fM3lOFpV/Q2ik5VBaVD7t07PRZ+iV/jHjJrXQoApX5verr
V1fB4ZzK4yz9kpOBkaIs/UL4i3vnWPqFzJNjq1j6JU0sI08gna4aaldCOqUV
ntFCOvU5XpjReCsZtgnVCPWczgH/rkkihPVcxlOtr73RiyJpclV1TkcekVXr
LcHmBxM2VV6ysmrLJ0eicgUktEvI2PAvf0KSmLDE6leC4UQpGRbXXd05Xkv+
GFqTvXZsCoSemxbUataQM+mbk2IetZBtoeTLw3WTEOxR0y676j051GZSVCrY
RRr6qqT+ZzoBiSar1044tMCm066+hqofwepVPLev/2fST58Ue3+2HYzdOLgC
vD9B7LX6/anmY+SXDfMLtK1dcJJbUtI1eRB2ztFcqoaGydVX7sfx5XbDe4n1
Nb7hwzDoOF0aojRIusf3Uz7/eQD2G2KVK754hxgTUq+TsZ/JlanA/qS51/2p
VPsOhuq8GfYn7AKN2EexVTl1/3dZkshR08Z+RsC8G/uihY1Zsv6SKgxrTgtc
v7D/MPZRikunT+w7oU6Md3RgHmQVga+7/IZs4G6fdYTXxWu4/vDmYFz/dyyX
iaKCHOOtoS71nfLbFvurtbauahUJqsTWlquY9175ie9z6Uj0OdJxli7PWYnr
3Pu1FtcJVvnw9rvYasYupQBjlBdqZ2wziPJChdWTFkVsp0A8RjUG5Yv2qLtC
ukU52IQXYr7dpQrniB4VuJOFckTt6ATm0rUVXLnStaBX3OyBckStvmVhvqCs
cBzliHbtTQ5EOaImsd2YHxT/0Y1yRLXbxcJQjmjIjyrM5TjNR1GOaInD8TUo
RzSirR1zt8alpShHdI3Gg9soR5R40ID5dOMLl1aNbJDl+0JDOaLVvCWY/w67
a0PriAKe9uvlKEe0YqfiEpQjanicXefK1iYQbdS8aDJdAZ3WZfeQz+mSGl6h
M9YI69sjU8YT38KQ3QjmP73yH5T9rIObRl4pJEcdmNvWYG4nEWH+7lkNbOFu
eEYTaIRoGwJz7XJOrg9aZVDIW/pY5WcTPLbtw9yAt/LaD50iKEw0u3CsvxlK
NIWxv9qzvdZY7NlzUNO7b7gQ0sJ6Dh14vdbYKmmhhlQYPh96q+d7Cxyls+P1
VvMKEyjPVkYpoxPl2R4cWyaO8mDn9Bt0S7kqwP3e9AzKv716eynmBmF6nqFr
S8ErqOUryrk1+i6GecvDVrOGHAKi2J12qw+WgM5hfswnzwVtTyILwFFWPA3l
3PZKUvVDTb77s195CRzBXz+hnNtuU4pf6XTPQzm3naeNPfV+VsDuvWsxPzXo
/QDl3M4EyrahnNszawWo+/wexXE9IBE0ordfcTpbBR5RHJhridxeHtp8C5oX
+LNpZVUQObu4CnF+U4l4tB+tdMPEGO1H63OcwO/lHt5vc2j/OuvFw1q0f330
3THM/yZkeaJ9ar183UK0T50XaU3NI7KLPEf71JnX/BSfNGTClmJzzJ2u/TmL
9qn7lk+dQfvU3mLHMdfPf5OC9qml1C1y0D41TdcW8xJ2y3C0T+2YcPAp2qfm
t6auq3FulQfap06Xu5+A9qndCSfMa+u9+tA+9UmeXXNon/qjCbX+tq9CNNqn
7siPsUT71Pa0o5j3XTDLRf3w1cbflFA/PNb/Ne4nL9N7ifvnXHc6cP9cS5GJ
+9KyAt9rUZ9ce9nUTdQnVw50Xo64pPawBuqThze1DKM+uZbqPtwPl/F+twf1
yX9e4liH+uTWrZ0UL/h3H+qTn0iL+Ij65IZepgKIyxdECqI+uY27fCfqk0f0
puP1PLkfD6A++d3XtatQn1xpQgWvj9/Zpoz65M1fQ9NQn5y97Dzm2q+XpDE5
skGPtmsL6pNP2Slg/t3NGvuT9Ix47CuEBOmacYtChPTYSexn9ttvwH4mcnxo
YuKpKkHc/47PeT1JFcG+6KXZpQ6tDmOGsNFB7E9GTahzshVsfJyttuqEuwUb
Pr8T4fWLOh/X57X0qZQ0ER93C3NPaKfOb+6vc+XJk2YU3f2D62x8rYh5Sgpn
XrqhLFHz9hq+T4ML9zAv0lFuYNsqzhgWGcV8pIY6v9zwoUjGvnU5ofJtBPP2
ZooPya0UtAj8Ta+xoHzF9IFc7CtyGXvuaIX1FauayVJzDZ4XsY+aNhlOz19g
FnemUnO2PZY62OdwOjzgU7ZcyfAUpXxLzn82uM5YYg5z/i038W4p5aMO+VHn
IALmY1v2c4sQqqspzvbiM+Z56kfDxdzZCedOak5YaL0lnvvt8vpssJWbmwAn
6j61dmRR5wUOWlY8k56nn7Wn7ueEvyWus2Vjue22urHicFeqfmpXFeaP/k24
UfQtrjjvutsTpN9VlTFxSL9HOyi/6DK0SSaNpd8Jqb1MpN8uaynMxQyt7iD9
PhaVn0T6Xf6LRvm/uOk6pN/LF+8bI/0OcVJ1+Jc1xCL9vvf7Jx7p1+TEOswZ
+gkZSL/x++sHkX5TiqnnKxQf/Rzpd5NcTTXSb5AmVWeX2NICpN/k4P2vkH5z
RKnrBhIbl6ux9PtvxsRyc5Z+X9pRnCeaXtDI0q8Z27AH0m+kBcWb6q2KUF6T
5E3+RY2IBpIx/3cU5R3VJ+YcRflOz8AP5R2Ri9+cMY+sX1aFcpys8t2bUY6T
fHc85uXC1YEox0lXVZ0T5TjZFZmM4dynrI4fV7UbSa+sh7Uox2nIgQvzyOaM
TJTjRA810kc5TqfcbDEfuODoiHKcGgWFRsavdJI9X6/i+iuvBssFVhLkaKGu
CMpx+u9IMObcfLsqUY5Twm2tHSjHafcLNVxn34vTTqTMA/LRczselOP042MF
Xi+caIh9jmwqgX2Ocoj/tztSeozYTEvsQ6I+a2J/wj7Lpc5Zv5mYTtmNOU/6
EcztyzfejalYSTx+SPW3vwDV32a3dZtRXqtHJDr8xX1jA55B7JdiYt9HDosq
ECneanj9nAYH9kvvlmwp/rp5EzFjM415rVc3rq8mIL/pl/s6ovou5X/YpSj/
Exq3hNYerMJoEnmFud1TDczv5ou7XF+xlWGTYIHvczhUC9f5l0+HfkaBhzGX
JMzY/OA1dHdHf7ylVA6iBQ/DNHyZZP8GVd4elk92/dfIiMnyyYVC3KnIJ/f9
zH4nxPKZ1QZHHwSwfGZQf9PKQJbPvOGzG79/ArrH8PtB90moIp9hGd1XiNJR
pvgU1pFlxO44czo/YdjBJ4Xq3zov7o/qG9+tSUH1g55Wr7VnfXfvcNMtWWR9
d5urTojMsL67ae4x2E+uWzuLn4/agUB5t2Ql4n+cMVPH
       "]], {
     DisplayFunction -> Identity, Axes -> True, DisplayFunction :> Identity, 
      FaceGridsStyle -> Automatic, Method -> {}, 
      PlotRange -> {{-0.8067224167583664, 
       1.2443110347131079`}, {-0.9169525481046421, 1.4143330471708688`}, {
       3.138614994663922, 3.9360320548988814`}}, PlotRangePadding -> {
        Scaled[0.02], 
        Scaled[0.02], 
        Scaled[0.02]}, Ticks -> {Automatic, Automatic, Automatic}}], 
    Typeset`show$$ = True, Typeset`bookmarkList$$ = {}, 
    Typeset`bookmarkMode$$ = "Menu", Typeset`animator$$, Typeset`animvar$$ = 
    1, Typeset`name$$ = "\"untitled\"", Typeset`specs$$ = {{{
       Hold[$CellContext`dof$$], 3}, 1, 3, 1}, {{
       Hold[$CellContext`workspace3D$$], 
       Graphics3D[
        GraphicsComplex[CompressedData["
1:eJzt3FVYVG/YNnykRFrEIEQkJERBBQmBewhRRAERKUFSBEVCUFEBkZYWAREB
aVGR7rrpjiGGjqG7JCzEF97v857/zux8e8/xfFsex29jmHHNrHXOdV6zjpvY
aNwhJSEhuU5OQkK28y/PJP/TrJk1cFPgedTX8kmQU2176exrasysp12MT+g3
oKkyvfCeag55Ocd16VyKVXBYTM1BZ3sR+aCaN+UltyUwMnJAJbNxBfk9a/1E
bMQcYGd00zlr/A25J7PAaeNbUwArMbz2pXQN+bat7YdbPaMgd7h9on1gHbnK
icoYsvP9QFhpsYi9YAP5qmpLf+A9LHjctvdbl9om8tZzeONDXwsBrkLAiy2O
4E3N9rcbi1ZBiQmjLJvhOPgeupr9opIGY8oaOTLotwJMLQ5bMQlPI1+9ovXE
9PoS2KS14OnJnUMeYs8QqXlyHqSnNPNcmllEXiG374BL+jSQnlsW4729gtyZ
YjpuZGYceLzN8Hsauoq89b4fprF2BLjTJTdeffMN+Xbz+5KX2j1g9bT23Wfa
a8iPfc45Sy/TAvDxP/6GDxN8ukU09PVQHhjv2Pz7/cQ6cv3HMhbYT0ug4iRJ
rWE5HkgfzcCkttJi8nr1vk9FL4DbPK8fadFOIO+5F4dPEJ8DvwdC2fewTCPv
ku51j3SZBjnHOX+VDc4iT6aOfGv6fAI45Qv44VQXkHOO81K4iY+CMWcbFv+7
S8gpN30qXj0bAGXTYvRcjCvI/SK5i68Jd4Hz9zIZ6goJzmmaH5B2uQEYnlV6
FH16FXnDFZHEgZpsMHskXF/0AcG/7RtUk3aeBUGlhSUv+wYAR96qtGUWHcYq
9fOrooZpoPvB+bHqZzzyiMZBMo6mScAuqawie24c+dIeoeEs73HQMLfqFvRi
Erno8PqjGMpRwN6U/pHEZxq5Jsly6usPg2BDWzT4hNIscvKNcQ/vnm6gJO6V
4Jozhzy4v17FsA4LdLqVJXxb55E/yPh88Dl5DXhWHt1bH7yA3Npa8EyBTQY4
Pewq0/OD4BXUSU9Ok0wAtSskulr9OEAqYBw/fY8e0/PbQN/+8xignBawuM7X
j9xVQcy7gWUUXNPPLSw8Pox8tefbvELyMHAb6/3xYhiPfGyJSUqkoB/I+W15
4x+PIeebf2le/qgbuHrUntnoGkc+MXFa/O5CO2i9zZRqtT6B/Pqdk5+WNBvB
Te4K/eL2SeRKr2y4FpzLQYOarN+Je1PIj4x8nzZs+gRCpry3m4sJnnTfeGXD
exBcpbQP0hFqA0XqZ9bwG/SYjwqGc89i+8Hc3SJe+48dyK2kNqKWzHoBTlK5
qssZh3zfQ15/g0EckIxurLrF2Is8ndLPvI6+EzSLj1DMavUjt+7UqKagwAJx
ntxL164MIk+4ftfK6HUDYBkl7TacGEI+Fn3h5++6SvA8g257P+cIckvdP6xv
vfJBV4gWxYufBOdgDf05fCceLFHLS22dxyMvKfn2+lkuFvSaXfGNkKsE9zhO
W8ZcZ8B064t9+i3cBta/ZF407KtBbmUgivUNaQaV1z8YCgo1IDfrYBWeiGwA
qrqLac+Fm5FLfG0UMteoBes36b639bUiL/R5amVdVQnWGG463n6NRc64Scn3
8Fsp4Dgcs+j5sh059dlHcc/+5gKNUz3HLaQ6kFdaVWtyBH4BbScO6pcWEpx2
hTstyD8MrAqSvF37Q/CMn14pPwqywNVtXpNOh0SAjaMz3HZiwKzE5MkyP80E
BZGdTx8+T0Ee4dAkTUGaAUiUzhbUiKYin3mHDenJSAU+OQbSNGHpyGPP/5Rk
IPsEVpiy+FouZyIXpYgfVqVMApeflHm6BWchv4w5p3CqLhZY2CYxvo/ORi5S
WfecNzkCBIuFn66xykH+I5Ey+AI+EDgeJGt9+Ps/7uEkYMzrCFz3pD8evZSL
nP/i3lcHP5TCIgrOTymnM+CVa/NjT57sHBeVIw9+JhbDRDHx7VWDbOTlrcoR
YTcKYdFv3omD6nnIc0IfeFzIyoOOTgx6db8LCE49yCnZkA359U41vtUpRo7H
6UeWxWVAiZKtlP2GpchDKXwy3fw+Q6t0blIsFURu/SyXrfBFAow8rKepzF9O
eJwN4/Eyqwi4nel9h+8dwUmyz8gNsrjDZLp9A9E4gqu0s3ndNe+EN6VutV8/
WwMvSiSbs8vtvE8W9vQus3bAx9OHJy/cbkAudDPUTzUcC2PnhFnppVqQqxy4
JBdd1AKZ/Tk7m/WxyEndx4fK5xrgRki67+xMO3I2H8d1xoYaKI93D30m3Yn8
87uZiQiTCrjnbViv2o0u5P22gbbPfAvhVY1v2z8EcMh9g8kYwXY6pNE5q4It
JfhU2IoqHcU7KH4X/CCl60auIMXf+HmuD/B16ZAxXW8G2BaZBlZ2GoxYqpzS
gHAv4OvtD+tfxSLfdpcLSuPvBhO8ea6pnp3IDU/rtvoNdIKbcf1+AwM45Owc
Bxny1dpBoU4i697FHuSn77tEO7C2ALPpPw4pWX3IT2S+FYuQrAPt9rImfHwD
yD/xzrBbMFSAgC6qzvmLg8gfvjZgybudC8zE7ppY0Qwhb3M/JXYpLxYI0em4
Sj4iuE9xck8OWzvQeft0WzmzEpxi2tTQs6fBGCmKTjYutQF8ZR07rWEt8hRe
VRU9uRZggmfyF6ptQH4k8tZvP/FGsHXpzsrCQDPyE/Ymad/7a4H+j5i+oPA2
5MFnDOylzlcB0eGFiuJNLHJ+iTjdz5/KwIxSrWfsznXpn1+14nNh9MoDIfo1
I4ufOpBTkuK8s8+nggWyD9yqQp3IFcF3FzPGcJAseiOD9yHB2bkjRR6fKgcr
rp+u/U7NBOEOa0MPk2kwsT4ig6xny0BX4NdSN75c5AupLB5rn4oBj8vLwxjD
AuScjluFDo0FgF/o53vqK8XIBzmOxmVE5QJ80oJz6nAp8iOXLZ9nCGYB/Xzh
3y+oy5Fj+s3WPm3vPP+AaRfyNYJnDDyuu3s1CfgvFKRSJFcQnudnDqbWr5Eg
Jd/klurxSoJXS9hzDXmAwXfHVXVN/+PLUNHbKRiWK3i33iZzg1x/4OGqnJ2/
m9rFn14TCEnEeBKmAj2Rk2g+I+M/4w9jT/1YM+j2QR4rT+Yfo/AKlt8e17vX
5kd4HJGfUtcpvSBJmuDeT7aByPEFWobP3N0gZnBYeEonmPD43PUfDf2dIV71
2NGBi68Jfhm3/SLnEcREkQle2iJ4ucZfibMi9yEJDbvp3NMQ5K4Bed2sCjch
CfOloxKlBBcqkdQ7pFQN+c8psuafyIM5p162HY3Zeb9NT3dnzFdCZuc/N4vc
ipBb39SLDrxZATNs5cWp/cqQl8w0jpruh7C7Kq30EUsFcv3bPmVvDhZDK7PY
z5b5lQRfUYzGVOdBnqtKyxdZq5FXR/hepD2ZBcmHuAU/y9Qgv4r7O7KZ8Rl2
aZfrbx+tRV6W9WEkKD8WltNdVXQpILjOtAypJ68vTLGoOyhPU4d85UJg3lWO
TsijLqTwarwaamUleTSa0GDumbqTJvW0w1j617Lf/tYjvznQyJ2ig4Uefx/s
e9DSjNxBffgNx9UW+CBbmXHrGBa5j27vyQe+DTDwrFygTnI7chJGZv0jjjVw
86yZ9+/1DuQWQmb7fh+vgIwGZxyq93YhTzn6QMz/ZCGMKX5brTZA8CXdv1ua
3unQ/xjTmz0PcMgDL71vok2JgDm2BqN0xQQ3vTNXfmZoAKY5FL+osmuFx7b4
PT3IaDBO3JPW7Hv6YXB9S3CzeAfyWaOaPwsdPTAopiLu7J8u5HPXFCs8dXHQ
FMspqunagzzhiLZPXlQHPJljUClf2Ie8nC5y6tKVNuhr2Ub9K3YA+UMNCk2d
H/WQkWTu/OMzQ8hD6BKDQ7QrYab6eG/tnWHkPIc8HOt+5kE73jZ3DqkR5Af5
GUTh9zjYEbPZPPyF4KxZ43X0XGMw74fR4+W2TvjmmViM40tqzMMzh4alF/Aw
ol1Lni2vB/n+8Nu0v/aMQJF34ph71weQa10/JR3aMABVP+BuPA0fRk7Fn8Rh
L9kLM921e5+V4pEPFKp5dOt3wYTy1YLhX6PIH31fyFsGWBieVi7u8ncMeVZr
8MajkTr4LYfxOVf9OPLTpFhpvelSuDVtuk9DYwI5KX+zU6nJR8gd9+ydThTB
hUdG3iiyT8ET57912/D3QhJfW3O54X0Y0/ux4c7xE1C7oZE3YHYQebLAFefg
6TEoaCn2K+MjHvmRgpGctRU8PDRjMkyRPIY84BuvRM6rISia93XPM8sJ5O+P
XByT7eqFrs4Z1E4Lk8h/PTl48w62E/o/CjiiJDiN3Hos+vdPthYouZTVtcAx
gzyy5WWE3IVKqF+V0HG/juB+8rFCU+6psFA+61D68VnkgqnR3l+ws9BC08zu
s+ggLPRJPjXGtg9j3HPgXMThGWjL5q44gMcjt+lVv+jANAX5XIWuH344jlz+
mdLz2LpxmKP24/pSzSThcTLSw3VlR+HkaDHjXtw08k8Z81QKfwYh231l49GI
WeQGTHwCV7l6IFdp/Mc5mnnkD74HnFDZxsJ1d07bIIEF5GurBhZKyjVw/fKe
MeUlguN7/JPdkzKg3lblcKr2IvIaddKM3flVxe+9jrvzq18JKmd35zzOSmHt
u/OrMAPpid351T9vrrWw3J1faSZwe+7Or/75Y2pj9d35lYlb5e3d+dU/vzYj
0L87v0ren/pod371z18ttV7dnV8dvvjkSGrpGnIGDtfE3flVOm84fcfAOvJe
MtWHu/Mre33r6d351T83jJp9tDu/ErIo4sCpbSK3+eMxf+BrIcgnc8jYnV/9
cx9uy3bNgG/gJfgVw3NxAoSIRY0o7KHBQLdFKj31VbBySNPDWH4G+TJZ4tN7
hcugmUuwiHJtHvkfE++33x8sgNpP5bUPVJaRT/Z8NqusnwFnhV3vuHOuIqdd
1Ja9hpsA53g+LErf+YZcmi0iaH8sHkiTBYIxmzXkm1eihzKsdr5XJ6U5C8is
I5eTTEiTj2gFSkGLvr6NBJ+KiQg8tJgP1INI9106tIGcnYmuRYVjFXw9EKDD
kTMGqBx8S7P0dq4L2MfXY9eWgfa5FnIbkynkXmynN+suLgJx2z3XDzTOIle+
HDbrfmEObGmPYlTwC8h7ve/rPBieAkeoh+e03y8jp81fW+CSHAcVe95yXPu5
gtwRvxSomjYMqm/uZdNeXkUef/6Y6tNX3UD+TRJ7Reo35BjKEtwPqWbwSSmN
XUx4DTm1CO2L4gN5YL91x4XDjwiuIftyVhqzDEhwwbX+TKNgv+A9G7MoGkzy
1QgtZcFFEPb+WG669QTyezcpRjEFc8CoOPSQuu80ctX8QqbB1WnAnqq3FH1r
DnngpdDWoZEJoCmhNJ3dt4DcuZT643e/UcAYnmaZvbWEvPmOo5fWnkFwVWG7
cOn2CnIyEjZzjZAuMH3jw1onzSryj0kCVnnVDaB/Xs5t2o3gM8sZ0n0ncsCZ
hlG7zEqCp7pmtVMKL4C526YBQk3DYC9Vqt+rGhpMDk7q+vNrO587PxmLaw/G
kDMJVIRMHJkBGoKvhrqlJpFHNH05LhkxufP9Xey8bs00cgY6hjLDzjFw1d4k
rpV8DnnhfEGXUPIIwEwwy3N8m0dOHzIe/1WuDxhuOL89672I3MhSt8A2oAOI
fLuTRQuXkOs19d+Qk6kDD2OZf3eGLSM/yUG17C6UBe5c3hC0I19BTlrL8yCR
aRZsSx43or4zAGJD8gVjcTt5w4ZyCphPA6wVyd8oMzxyX3+XzF82k8Aicolp
bWwMeflG6TvK8+OAsaV5XwrvJHIKVkHW+xAPrG5fqIngnUZ+NHz+1jDfILD9
fWbNYXAGOW7qgscex24wlnX0T4fiHHJWwzgR0ZdY8CDFg0NObx55W9jB4rDU
ajAUc+HBDZYF5CMOrKK/BDNA/fBIctoLgpPn/WCfWZ8EUw1BstZ/eoA+H8PL
R800GKmgN0kVzhOgLOcajjlvEDnWKZZ2tmYMWJho5lH74pHjrydSNHXgAbuB
CFunxxjygo+5zRw3h4BNcGcQ98758J/Th3Zb06T0grGbiokONZPIM/VU7h77
2gl89vduUu2ZRu5smDnFN9MMNll4Tm6tEXzBy3mqmbUS6PWXnVN7P4Oc3N65
g+t6KvilSFG/uk7whbjHXikjY0Br4DTNkQ9dAAqlvzjxmQZThCtlE44eBTeY
SFuo0nqRi4dpsbBw40HvyOpvOptB5F8SGiw5FwZBKunp+9e6R5D/VRD/W3q+
DzQF2Z72sBpFfr6d2bT8DA7canuaP5w3htxVM+xW7ywWkHPOqXLXjiMfvpoy
faCvHgz9kX/xM3wCOQ+Jt+xeTgg+Z+svt5yYRN5ldVRHQTsFTDw55tFvT3BG
fX5KhcIRoPf2nvFnzXbAxJLmeMyGBhPp0JB44f0QSDWNvo4t7ULe2i1bV3Rs
AIxvfBx9OtmD3L1gbihctRfobc0yZ1b0IzenCyvMEcGBn5szXNbKQ8j1LTce
z7e1AzeKU7+yHUaQf+d/0d/4oBmUcHiUaMbgketFvCH7M1oNOBxuePVcG0Ue
suEg9JK/CGwHMkkW1BJc+JZu0rhDIkicahF5uncM+f+2eU65y0tjC/NOSPGd
z1TjbA108HlhvDvXetecl7jC2gHXuZNzpW83IPe6anFRLRwLlcMjeBikWpBz
RPh5xxS1QP2hiw9b9LHIOXXJVirmGmDOO1XM3Ew78oXMjOb9DTVwYqtQ/7l0
J3KWQ4Eu70wq4DOlE+nqN7qQT3MVHHvuWwgduPRGfgrgkEuS7DfYndeN0RaL
tJcSnP10QRkNxTuYuc08TEbXjby4ifWy2HwvrHSqvL0u2QSP1XjzcvbQY0yo
Sb0eyvXAD0YcsV1XsMgdVA5cVlDFQXipq0S2pgM5ObdZZNvhTqh+UcLzDjkO
+fg5vrkDSVgoOxfKfGZPD/JkCd2lsN9NMLJpz95bub3I1Wx/9X7WrIUlj/iU
3dj6kXd7SfXL3i2HMhWkzNLCA8g/KI3H7ffPgbffUo0cXyT4lLXWc//9sbBR
J559UGMQufCx8yqNzMPQlkn594Y1Fn69y7b1TJseMxFQxtjJNAiDmTNppz93
IjcM+HtJsqAPap7mzIEp3ciPHUt4cZ+2B94X//6LzrAP+dwwy8few10w0c23
Qa5mAPlPdg1r3PjO++QU60pd5xDy8HWW2D/rjTCERXU/l8cI8vftKZdEW6pg
Yrbua2cLPHLBn552zS0FEHsDc5x9gODO972Wyu8kQNOZkm63I6PIv508af3A
chSmPZllFaPshFtSf6ZgFR2m9KEMyxLAw0DXd75fq7uR09Cr58rVDsGantn7
Chf6kbPmjk8mpfVDW1Wl0L2GQ8i1u3T2N8v3wJVKOkGqA3jktUokLi8DOqH5
tSfGzQqjyO2mzOM2Rdsg+U9fcXrZMeTVkheETBdqoUpZl3/+NsGf134IGckr
geHRQywW7uPIRRQiI2MdkmHfzMdlp0aCpxYIr573G4dYmuter/RwsG3igkbF
UTpMRsDhG1dkxqD6gO2GQWAf8n79jJi6XDwUd8D5udkNIecQFu2lWBuC7uR2
Gb3P8cjZZjUbF3fel+SFiRST1GPIXUTLV82/4eA938PGv4zGkf+gZ7MJc2+H
HjP6BfinE8i96pLVV4oa4OW4P0LHNSaRM8wcPPP0aDkMjfXeYpwmuLnuk1p3
0U/QWvPMQt/5KeQpZq0vKC0nIVO1/vMIs53jcDb8ht9dWoydotK+87/H4eSH
AdKTfweQP3Ggfq+mOgbpbaFnKiceeZ2Qq270XTy8JqOl1ks/htz81olaEDkI
c65fSSapHkd+rvhR4SZ1L7wgeUizQmoSedyVe+vVwp3wCru8wHPrKeRf339i
/CvcDI+92g41uTWNPKdrPujRxwpomBQV67dNcHExBtaSqC/wFBWDwNrNGeRb
VSavvm9NQYZwmrzVo33QSKD9nEECDebQopneGYdJqGxZSGvEMIw8ai78V9rH
cTgjbca2pj6K3Ls8Uo8jehQmpLkp+5qOI6///iV9LHcYSim81Z+TmERu+7y7
NgS78zmvJzFXbppC/vlD4Z+Kd13QtsqGLPPgDPJxu6ybLQqtMGu4bR898yzy
rUj6Pz7rlXBDpt1Eporg4UX9lBlJX+GsiP36weNzyCVDzPPo5Wegi6FjNffL
fji1fVcrvpMa00qjTReeMAXvMl6687RgBLk5w2P1hNIJ+Hfws17XxTHkycvu
FaLhY/CCaNBm0aMJ5DHnC4wPi+LhjS9U9VR3ppDzTm2sxssMQN8sJ7J4uhnk
ukFqKQd3rivpDkosV+7NIj9Eb3ewebkNvjt/Rjbn8RzyqTsOw29JquGz/NXt
YOF55MYzD2u/XU6HDB8rsiNCCG6vR5qgLDq7c3lROd/yagCGh2yFzf3ahxHW
YgPJntNQLeWMQ9QzPPJD09XU0H8SqljqcqhsjSG3KBbkbFUbhweaWD9SyU8i
n7xqbTmBw0OMgKJ+iuI08n1+y/HJ1wahtXT/w/Y/M8g51rYF+MO7oQrDJuMB
iznkTKtOZHpvsbCeRvv5hss8cvlLm/gb9dVwZcFKhAosIL+YX3k9FZMB735U
r15IIvj/P7/6f+Y8LIdtAnf3lEr3f2Lf3VO6KHM1aXef5xCeMm93n0ciUil5
d58n/EKi+O7ei1Jgpvfu3svPl1ouu3svg8nWErv7Ic2LLuG7+yF6P8ZYdvdD
XtOci9vdozA2Svu+u0fRhlel292j+NJMtb67b/AOx2W0u2+w3/UX6e6+QXlO
yr3dXl5TiPfnZkEWkJ44LdK+k4v3m5b93/465d0q+5EPpdC6fKXh0+kMeLzJ
dPT/9tREciOx+VUqxQHfCpUVMDt5oZu5dRSUPJdyX2KjxZz+9YDi8dd5wJwf
yh8lMww+rLrPGjDSYeKwYzaUVlNg4nyES+iLXtDhkaEZOECH4Tq3eqZ5HA9s
OmMXzjl0AMFwWcz2M3qMOUZL2/lqN/hAItR4zKAemBsbXclb2MlRejLmmwdr
wMRFO4meffmAu+iypZowAwbTVOhN7hwGY5ean9g+84ZmaqU633ee/2PGp14l
Vo3Q59HsZeGREjj8Yu2MPQ/x3EhsfmXLG2DK/34RSH0Eiu5CeFB9hlUs1oAW
4yv9YRFEzYDt+9386eP9IKythsJdkg6zefAmTaLaOPixX+v6QxYcuM9QspY4
T4dRVvKtOuY/ANJqOcKFWVrBlGB4eKcdPUbiiIbg9nEscOO+Z2ofWwEazUmU
7zbSY4IP3c3MOZAFZu6Xc8mVJ4DHyZdMLq/TY6h+Le2Pf1cCe5Mm2D6CdHic
/Nqnb0v0GFEcTuBkcQd0xzDESj6phj4lvzceFxHPjcTmVy13y1S+vZoDRmQV
Pix/B0ERg/71Gi9aDP7l/ldDLyYB86XQ3/7OPeC0XnviO2U6zC8KgeLpM3gw
Hlct3J3ZDvgSD1x5Nka38/3IKzzWGAdCZvWF3fXrgLLI+TAPtZ28asR6CS9e
DZjFq46JHM4DR2/8lTMKosfEVt75lREZCjE/q6z43L2g6K2eLIU4eszmIcHu
e1oN0OI2n+fh9GLYjLn5YNODHvORf1WofKIHRs8Mi59ha4RHDIOa26WI50Zi
8yuuryLTQQ+mwZNhl1vfI/rAjci7KwPhtJgxu0mJ4yZj4E83af/KwS4wt8T/
2xqzk/fGrymrS/aDZPO6H3+Tm4Hvp+zLJ8t38oyV2zHe/FYQbtw18LGuHESx
iwU6M9NjXKVEYwzFMgGeLEzuZks86N8bFHJLmh4TkbjdOyJZDDEH3zdch1/h
8NuRx5fE6TGrt0WsG7vaobq2qZ778yqoNz6jcJ1i5zxgvL8MGz8Ij2Yfe5Ku
3AbNz9rlH/pIPDcSm19xR5O/7VSbAPrWi5K3znQDemGolBlAiwktrlWsejcM
rGfikiPzseDaev3PYxx0mO3fPvLy57rA52HZJB6mWjAUliXH40mHoRJw+7jO
UgWo6DMkGodzgF579bBjBR0mdrJqRe3JG4i5qpndqOgJPRXfahl20GEkfuZ+
Y1mpg/h+K+YIviJoJWV/LT6fDvP5rfrf/pBuiD2e+G7P53qYktd4OtyODmMh
Qzd2xh4P0x98PyH0sx1KDKZzHdlDPDcSm1/RvnIjCZUaBS0v/CkpOjvAQ1HD
iyb3aTHNnXtkc9/0guHLxT91+xtBLL7u25FxWox+zn46zYZmIEHl2VRlD0Hk
FfmQY2fpMPwcH3JyOTMAP8WmG+2HOEBO3xLvpE2HwVyKoeL3KIQ+2Qdo949+
gQemBU8VadBhNMWe+x7Nw8Il8SfKV+QrYRHdTYlp7p3PRaYuhXV3Pzxwxd72
s0YLdDLaF/m2aSevzn6x3XN9DK7ykrFfIOuCeoLUPGNXiOdGYvMrdUHMLxab
QRD/JLjw9P42UMN5W/aWyM7jZO4hPVrTATSWZKUOu1UDgzBtBeZgWowR6wgo
ghWgYEut64VZNrjofUWLu37n814mYfj0UAjEPOCyu1TlDkMyjJ729NBiGMOC
TojJ1sJqaz9yz2sF0KSI9sjfsp0cruoRnPGzC541UuJcm6uFgflvEmacaTEH
9r3logsZhlcNLp4yy8bC6DTT98YHaTGFlzHPvk3s5PLvmVqhJTjYyHmRPMeL
eG4kNr96SYUpexnbDZiOMv+aL6gHbrjFQK0hGgxJI/Z8rGsj4Gm/7Dy3XgKo
Ogz3Hjmx87pa4qYwWV8Bia6x8nHJWNAlMCzkcpkWE8GkFoV3zofqNSp9jZ8/
Qby7vBm7Ii1mvS7u7K2aVhg/wqx7v60cipqpuJOw0GJgcsVXtZ3v/zlYA+r9
fxthOsVSYlYdDWZ4fCTy+yAeKguv3y216oC0STHPz12n2TmPKX7cJzgJ9Ss4
7tQe64FqmwMnfuYRz43E5lfE9naI7rcQ2QMhti9BbK+AWP9OrKcm1ucSy43E
erSi3hLjoKE8kJueyPPjxDrKV0sZdkV9NdnAu0P36e5e8b98NWh5OzTPJgOk
llwx3d2//ZevqF5aqxk0fQKcnvZ0LcVTKF9p4Bf2Dt2JB6kp+0R29zn/5Ss1
nMvxAP8wcEAu4Nru3uO/fJUR0mWpz+sIaOGLpyOXclG+KlAVGh1gcYe2c/wK
H3DlKF8Rm18R69FGLEU0RMlzwaNAB+16z28oXzXbHnL83psJrt7quB39aBnl
KxmR+wGVc6ngULEdTstpFuWrh6ff9CnXJ4FjnRa3350dR/lKsPPexKHKKFB8
9Ha8i04fylcTr511o874gd5r7n6vvtehfIWfFc70yLsF8SfPeaRaRaB8dax7
EXuC+jXMuJdBsX9fK8pXxOZXxHq0MgEcg8bhbBA26x5g0rmC8tVR2QFNxdJ0
kK/7E8v+cx7lq49DQYfTp1MA8767J7jXJlG+yssfUHCsigOxD9n/FqiNoHyF
rf28rN0aCkLdQ1jVtTpQvkoJ72e/oP4EZOx/5kTln4PyVbkeTkvezg3mTNLd
2CtXjvIV1nOMSpQ/AloH3akwuoJD+YrY/IpYj0aiI1S5xb7z+Ww/HaAouITy
lff832GRoi8g5+PS8VmbGZSv5sx7Xw6NJYJLNv6XzC3GUL5iNv0ecubNe+DY
q/7E91ovylfMly4ynzngC4Qqh6dZf9eifGX0mjEkLkQPkhTf4LK6/xblK1oD
Q9M0rWCIf8ePzW9uRvnqp4v8chQ2Bt7bK6Phyz+A8hWx+RWxHs3CwJl+6nwa
eH2MZ5gxbg7lq2aHy8qGuR/BFB2zgtDkBMpXZUaOe7dnYoGZ4rW9Ma1DKF81
AptTD1vfgF+5X7F1Du0oXzF2nvzocuYxwLqnXdEIykb5yt/uZLx14EvIr91I
Kv2tDOUraS2ofPhvOFypnnf0VOxC+erc4Vzu00/i4SmPffJACY/yFbH5FbEe
7etUjSPz2c+g0lP5741D0yhfRVvPtL4oTgAdmNN/mc1GUb4SibH4JE0XCSaO
fzYa2Ogm5Csu4ciH8T4gY75z+GheDcpX+LT3Y72suhATwlYmxRmO8tWC6bw1
ZiQQhrIxc9uJNaF8lUwxHdfNHg1Haio+P03pI+SrUyFq+8WTYMVyiGxU1hjK
V8TmV8R6NBf8nW1RiWSQVmBaLB02jvLVSuFlCUzWB8BjWKh9d3kA5at7y4wN
ZMEhoNrY/AWXPhblKx1PP82MZgdAYvOwql8zC+UrUYsDs0IOrtCneuJbDkkp
yldGxtFBhgFhMI/eqCp1uwPlK+p3WCdV1Ti4N7or4dzLYZSvpPYJJih/+Ahd
E2yf57ZPoHxFbH5FrEfra41Twd2MBw/4mCwG+PEoXwnRrGrvFYsApB8ePU67
iUP5yvGFBNfiuBfQZMh6GSpUjfJVrHP7gZYb2pCk+uvfU0GhKF9R1Zm8+5Lu
DznNOIbkLzegfNUaoD3NS/ce4icflpj39KB81e9ggfXKS4AKpPX02YajKF9F
Bm4EyaR8gtLNXUq9UVMoXxGbXxHr0b4tzefZnY4GRYH4pT/5fShfPbu1dezX
odeg6xXWJvVYK8pXEqyrn8+fsAcZspTlbvgMlK8Yj75JWlFxgQUOCpeisotQ
vloKus8T1fMGSuyLvzvg1I7yle1F78DwoQ/Q4Nrp8+8ODqJ8JfXrXOkzmAS3
BYX6cYLjKF+ZlASouap8gcby3JS36GdQviI2vyLWoxHbHya2Z0tsH5XY3iax
/UZie4DE9uWI7ZURm1/RCOKfxuycf5Vy2uc2jwyB406aucVYGsydCHX6d/0z
QIikz9PRchQ5RbikAVvNFGi8RKPoxDCBXFaAsqrw7gTQOlGwV/DxFPLHzyvD
5htGQT7rAc7FsBnkSnzsvNPTQyBUl7KK0WIOeeRW4Wu/oR6Qr+kpiZuaR05K
Uv6l27sdWHSzU7rRLSJ32avK+3qjBpDrP2x26CX4YMzBcIxQJlC6kNgodHEJ
ecbNx6pgeBJQUtDLpU70gLtfT3VZh9FiuARU/r63mADmdtJu9hGDyPM26jRe
ZY2BsS5D/p+OeOQp3Sqm1hV4QHsvyD/Pbgx5ZFmkTt/5IWA07aEtITKB/Mqd
C7ffBfYCtzGy1q20SeSxyl8Z2yM6QQnvu4cqs1PI3+OUpjQam0EgNeuiwsA0
8g236sqjVJVARCj/0tzLGeTWop6ps+dSwdbrbT2RAYL/nRBnw+HwgHy7oGrV
rAOce9vIcnfnvLrHfIDX0GoE2KexeX0l60bu3VlEPdwwCKZzFQ/M6Pch99Pm
cHRs7gNdJ3VX6M0Hkaeyqf6Mce4GaaYkj/wPjiDfwJ9UrJruAF7Zh/faZ+KR
/0hWfmEt1ApuRUS8vJ8wivzcoQ/+TIK1wO35uZxsgzHkludFdRyrioEXJvn5
3yGCG1m0n7iUlwSebHLWF3KNE56POI+1t0gPIHWi/H1JqwFojcUt5STRYZRU
QuXVE3GA0dPtU7NGK3KuDssf1Z2dwFMkl7fxSTvyWTk7mdjqdvDi4gmbvUGd
yDvWbtdV/2oF/NUuah4WOORlB2xyn1I3ApM2gUHL7W7kQ4pRa5t11SCKyex1
nkov4e/2Sfqme5eB5ChlTtWrfciT+o/y0R3LBpF+UpaUfwhuXmIcJjwTDaKW
o55m3upH7n/rI/+t3nqQUkx+4xZvMaA3yL+0sEiHCb1052W/cB3gcpWUeilb
jhxjHzbHI1sDpBMvGtPkVyLfmtmoM9hXBTJe5+57sV6NXMQoV8Ypohzom3xI
bViuRe6xqHTn3lQxsBoxT/oUX488Nctjs0YhD0Q99vhWsbcR+Yr4kQ2cXgbQ
UbXh+cLahDyCu+S0l1Mi0D8kenFPL8FVPR7eN6MIBhOHO/ytZZqR490fsGXt
iwDlmFskQQq+oBvzm1uAeifHVsbZblmHA3ybNnOtdRByjIl09cOAUMDoxVtt
yBaCnNNZ/j7F/RBQPljldEYplPA4hp3v7OiDAafSd1eSM+HIY/V9JXS2fUF5
+8vzEPcWefmQ6Ls3vp6AU9FWQkb4HeFxcgOlIpJeANdowUJxyUjk+EmbAI4O
e2DkaLJpvkJwEoPyUHkSQ4BfuBPqo/EeOfOnIon7I5Ww2vk4Iy4mB7r58ekz
/aLDtFLbTm5EVcDY87cTCr4WINecP5I+wVUOMdptsZnWJciDR+86Vs6XwMsr
3KRTOIjcB+fDYnW+EPqnng9XsapAvvL0i9/zM7mQfUlyKS6vEnn988b7cbMZ
MGMmkHm8pgp5xgWxs1W9KbB+MzQmL7waOVWoD2/8sQ+QmcLtwqsTNcjLxa5f
XtfygdiLe7pD7Qn+qqP89gX5LpjB513gylULi+KkSvpLd3Lg1FrR4ZkO6DVd
Wpju3Ih8qepgqbxGO5SuvqhywbMVeXnSRcvnNG1wypJNRexQO/IMFkWxB4xN
0ONu7ov+7A7kdqpnlRbgzt+bbrxsf6gLeaXuZtNbvkpY8HiJJVYSh5zDH+c4
/qUIlnG+T2hg6SY8n8rf1CY5GbDrYqaAUA7Bk0Vij/lzRkJ+s3UZPGUPcjMm
36XGrmFICz+L9PdgoZjPIVOMFh2G8TWHAFntIFSdoe+z4e9C7q9AfcRUvx/i
vGuhkWQPcqPe6EKfDz3Q/1pb4jZJP/Kcat32IxFdUPd3Y73b40HkUreyohPU
2qHgtFe8QMgw8qju/Kt0cU2Q/gpDZtIJPPKrC1PkESrV8HWs4DOJdoLL2t/6
ftSqEJ6Sm47LUhpFrnimxCB4NAFqdeIwcZ4ET+Znct/jPgHfar+3V7Lshgz0
4J5vNS2mURzL68czDtlvhBww7ehHXqbRGZL4ahQW3LjQi50aRm4oViFaZj0C
42iLOXqujiKvPJFWsmE+ACPMsePJy2PIa32iND9R9cCBGwx2X7UmkDsnChYl
GHfA+t7Hss7Ok8j5G3/5Ce5pgv2/+F/aG00h5zzce6hpthxuYfmSq7YILs1p
9PWgw2fIXVzM76o+jZyqRrhxgH8UzL1uK0mBHeDD7YDGc3n0mFD1tK9fyPCA
ur2xR9a2G3nalZ/YHu0hkIhT/L7V14f8BWtCYLZKP+j1nXVi/zaIfIlC5uur
hW4gpFn6vDdtBLmHqpfZX8VOQBvBdLmWbBQ5SWb/kHRFK5Bcesps8YvgIdge
ceO3tcBC5suGUsEY8kXDcgazqyVAIn3cnubCOHJSI9dKvePJwPVlZuaRlwRn
O+CgzHVwJz856QuPpbWCsz553F8K6TGD9GnC18X6geCca7Chfgfya04U3BWk
vWAxhQl7kw+HPPvFBWUTDxz4+PGat1t6D/IOwf2czJUdwPmpsOL0Qh/y9aYq
BZZXbeC+02yaXe8A8mkKdt+ngg2A1ylkaL/VEHKVWGa9O06VQK1k/klL/DDy
xtia1Gm+fFCR1GTd5jiCnH78dVQ7WzwYOB100GGe4LI+Yh+yRnCAP9p7XW6q
DnBnzeZMJtBjjn2o9t607AKkY350upHNyA83TjTXZneA4yMOA4zPsMh7BjMn
fCuxQMr8T+Hyww7kjtFP7jRItICyeg+uoDNdyN3USLWlguvBNXhtyyoDh7z/
1xj2aGQVcJEpfYqd60aeJWhZfqG5BDwe7ibpG+xBfnBDZFltXxZQND9m6eve
i7x6f75/vVgU8J2WtegeJLjT+5pECt82oOMdUTIgVQGoL3dL8T+ix6TO8Jnz
2rSABWkGx8WYauTVNE2DWt8bgW+8uPZERR3ytA6lFjPBelAZHXe2710jcrfX
vxpO7K8B1crXctqPtiDXM7h7+U5uBeDZeyBj8GIbclYZEqdHCiWgi9T6fZgn
FvnDhhmVicoc0MFRTn1AtB25y1isecCez6D1Z24pSCX4aRYt0wdaocCJ79Pl
3d+Z/nOSAzlamRlVwKn5j4WTey4glba8RHmaHpPTqGWoaFQJrH7XTRvCQuQR
5wLHO3DlIKVaqvPHp1LkVFSlzx2EyoBIaeegw/ty5Iyj/ef+XisCjBQVD1xO
VSJnH39N58yeBzSrl+u4vaqQx9KV6J9MygRC/tgGnrhq5M0XKUouLXwCVF8k
Vsie1xAe5/cRMyPpWMBjf//W/v21yLdcyu/1mb0CjobjV+yNCW4xXf0mPjMD
MFJ3sd2RiQdXZsdsjNp2rpvbyRRt/OlghiJbVacrCXmszDWBH36p4Iejbqnh
iU/IXetq5EnCPgH8e5aUv/ypyBmpgl4rXEsGwUkipPkdach/fOnQOgrjweXf
5YWBARnI1TmfXwycjwYSOlTfXJwzka+4z9fd+xUGeo1yYs6fz0JuxK2SWOvt
B6hCGQKscwn+w4FFL8L7EZgxEGMO+0lwErcfHBqtIRD/ZzvmhL8HjMLXehvr
7eTM0wFLJAGvYaxd4F/9tz4ET1WTVmIOhhgAjJc1/ZG7cn2dC2z3hxj3p8/o
C4OQc97aGx167BU0enw8z03/NeFxPt6QvczhCY365+u+JoUgLy++t6Da7wpj
h1ZO/8x+g5zk4UYQptIRup556MPgEUpwFfYyGzIbaETxvdfwQBhyI10ezDRG
B2L6/JN5jQjOH7RZmutbAPmLJ/k9sZ/h1DWz1WfYne+hdZTQdDoPbg0fpy2l
yUD+41HLu4rfOVBTWM4k0CkLuc9TDveRliyI7RUY35Odg9x29Gl/jFkGdAw/
z8WWkoe8YHFhVCLoC8R8kqNKvVmAnEqwhOTMdhK00MrQssgrRM7+vVi6kC8W
ijjZGW/WFCEXSeH/8UI6FPqsHGvaci1GrjNY7pCU/AIW3J4/fGqS4IPnaF8G
XqmB9XePnFRVyIeJBe8YjM7RYrZ+lcfWDldB8k635zd6ipF7xHwIkhethCTx
xTSF+8qRs0tuzSmql0PNr1fCeDcqkNMqUwb42ZXA4NiB1pGPVchtDa0+wpl8
GLWX+bQfew3ykvdbDE+Ys2FojsLhINVa5Dqv39OO7U2FPtYXjHil6gh/l8ph
82ZgHEzty5cyHyS4rSv1R9tHftAqvcLo+rl65FHVefZzV1tgAX/RYPWecjgj
dn6axJMG00Xbp3BFpwmGiLz9KsFfhTzRatFBk64BykseTBVhqkXeO3lIVMm+
FrYWFCQ8zatHfiKy7K5WSBWkdjxlUkLbhHw9Saz8vf1OLvKPoo2ga0E+8eY1
DV6uCHr4gPuFWa3If3iU/olTzoZWxomjS5ttyF3fqsj94U+BVwuv3GO/j0Uu
WEtKMTwRAr8NLfPMJxO8Uu5mZZ37DDg3S2X78xDhd3yycVR6rV2TIKvvnQEl
ySrqByc/txdM48eAdQ97fYv5IuoH376qH2o2GQFX8Cv4ItsZ1A8Oqns8yYrt
BZJx99adT42jfjCcDER2B7eDZjk6bstbA6gfbO8IAtRvqkF28u/xxxRY1A/q
zNL319F9BBgvL+XStUzUD1rxMt3IlM6CZkOaA/R3S1A/ONe/Iaw7XgdHGFY/
0ER2oH5wMWopmK53Cpj8eVsz1UD4fR/5C5qTg4wTYEr4lZMwZhn1g+T6Efbq
1KPg3vc9k1Ov5lA/6NMmmLn8fgDA3qCrXS8mUT8os1p++UBzF6h/TFOLP4NH
/SA7MPvG+qgJaFKR3n1rjEP9oGPzM4fSzUIQMeH6tke8mrB/ZX90Iv6RL8RE
6nCmRIaifnCTzuZxv2I5bP3gt2ao1YD6wZKur8p/0lqhiw/XbO4EYa8++9mq
MRvzJIgvlj/xw5LwOz4ukY5JRakxMKZm/e6y6QLqB3/+CSmqujYM8Mux6iTv
plE/OGNz1029rgeopolwiHuMoX5QnC6Hr3EDC1hzaLDLlv2oH/Tt2P/AZqgK
9H8MiD6y0or6QazGyM0r8snAgv3O0KWbmagfnEhU28x/nwmrfXoP3LIqRv1g
416+YBfGOuhx+bt8/0Y76gfptkbf83l2QGsJn/OzPYOoH+w4WcDuITMO5N/e
s9z3fRH1g2w33NW6tfBA+zIdhcnDWdQP6se0+fE87Qe2NXJcptETqB8c89Mz
3rvaCfi4tVyj6EdQP9j99WDjzI1GkPcCm2/m2oX6QSq3iGClyQLgdB2uT5hU
EfavLMYer91+BTkPPypMrHiD+kGPCChAcg7CWDE1djeVetQP8p6UccHeb4F2
P/SD6Wa7UT+oyd8bHGOLg2mjyzdJswh79Y2qll4H9UZBftA+HBcl4Xd8YUoK
tnR9g2DqaFp39eMp1A/eKKq8NxDfDXL5pRWfxY6ifvDtzcyYM7xYULbEarGf
tw/1gxrs6knss5WA/FNHbZh9C+oHqQ6/W/QUSQIZmJKhI64ZqB8swFyNP1GV
ATH83DeY2IpQPxi3vEHasV4D4ZWAW+tH2lE/OFjBLGoY1w7vuOdQCDwaQP0g
azBX+BfGXnhlW+SRVilhr35dhmHYb2QYKN35nOxJTfgdX6DJebVq0j4g+DLl
x8DTcdQP7vNgSaHo6QB7jsrYWu58L/zXDyayRB95O1IP/Kuy3zy53Yn6QXb5
rgwxz3yAf7KKKVWvRP0giRzbpMAPb8jpfYJd7nMI6gcdvR1dGp+WQvJ1/erl
tVrUD4peHluATU0wAzeBZUjCoX4wXsBQ0kquC0aWnFds9xtB/eClmNwecoN+
OONonuXkPYH6wQVd0lL5fQPAtz6xPoqO8Ds+RaEA/6XzOLA55Wt1yBOP+kHf
xw9OAoVW0LyKe98W3IP6QXOOfJWDhRVg66ZX3KHIJtQPrkTV/+ofSACxrFJF
8YtpqB8sN/hwRuN+OvQxjnUyeFKA+sGrUfwCnW+q4Rq5l/YBSizqB0f8in8v
eGLhFP1dhoLiPtQPNot/e1YT0A0XetVH6INHUT9oQXm21kJ1EKqcvBncSk3Y
n0/g1v1lJdMDtE57S/ccIfyOz8VU8JnPy3aQH59G/kZhAPWDl5973y5lrwOy
upM6pn/bUT/Io7xtOtafCwrMuBkqyStQP4ipBJtZF70gvmHKeFn7NaEfNEpZ
8DAthilKf1Mmb9SgfrD/fum5xc4GKChLFV7M3oX6QZpIVRmljg7ITd7xilx9
CPWDyx+sLQVe9EI1P1BqdGAc9YMjjZ/+5gwPQfwG01lP+WnUDw7rbU5T+nSC
J2c+JjLfH0b9YFmTNV/QYBMQoC6rOJGFQ/2gw8s2RZbr5QBTcffYQ8sG1A/W
kycyOF+OB64HjNofPE1F/SBeQ+4Oz2IqlBju+WrplYf6wcC/M66Dq5WQqZDs
xPmXLagfFPEZz/tg1QrNz9kzUmX2oH5QiLTp5OeqLuiQ08OleRKP+kEzs6n4
Rwz9cOzmn9uHVSZQP9hRn/P1J9UIfL1/2qZekLA/71JESeY/2wYWI/KMU/p6
UT+41J5HN89YAwJe+zs63SXcv0i05OHXwPlsUF8pse8wJUT9YPkjMaO5qx6Q
hNHuTn50EOoHnS43XqrdLNh5367c8LStQv3gGBsDeZBvHaxqtwqY1iTcX0VC
aKkhm68dCkaoN3/J70f9YHyt/J3NG92Q6kVYNbUp4X4d2iar6kWSAzDUrj1K
7xzhvhbf5tPYXjWMwJial7+KymdQP5jwrvLtbr4qODimupuv/t2HU/aCsNhu
vmqjnMHs5qt/96t8rMIXtZuv6kkc3+/mq3/3dVRv3y7YzVeT8irVu/nq3/0P
86fe39vNV/yOBnO7+erffQLD+xI+7uYrOb1Z8d189e9+egyJfa9389WFdAae
JxSE+9pxkuaTN+7kK5/rtj/L1gj3hWMW0WtL38lXV4ddXtDu5Kt/9zFTtOaS
0tnJV3c5PPDUO/nq3/2+JO7OvHokNg3ybMg4Epi+gYUbto4Kp2kwe/F7T86Y
TABmHqlL3+EyYGWZGLW7RouppCuw+6M2Cq7uNzS4tzkHSE1ni+sv0u3kT4vc
TtpBULnfs7+kZxKwWKlsiO9cdzokhObZj+MAybbMyBEHPJqbJcubvjCabAIM
RkeCyAtxIGQio61EgAHjNFVxbsaoCHzO9Wpw960Gj+YmZM0f7OQrxfgLDaM7
+crDPnxkKRQyJpx11HNlwDBdHVM29C6HuZG1tT9KG2DJ1/UEAxOG/3XvW2Lf
C4jlZ2I5k1geI5ZbiF3fiV0HiV0viJ1XiR1HYt8XiOVqYvmTWE4jlmeIXfeJ
XR+JXUeInW+JHV9ic3Vi82dic1pi80xicz9i8zFicyRi8xZicwli39+JzduJ
zaWJzW+JzTmJzQOJzc2IzZeIzWGIzSuIfa8/dTjM1PIOHnCyu3Qxv5pF+xt1
xr4btxb7AeOTgNSx74T9CkMScc7Roi5QRlXe5MRD2DdgahjUnwpoBrEkNSpA
mtDLS3dGfq19WwZKzD4ZxXfUoX65PPGXneq7AFAe3qH2jJbQ82ZMbTkfl8mH
weshDlFHCH2oYk3Md8pv9ZCc7PFGh1Un6vVimhccDX53QFfLoYtkwUOo/9Kx
/CbfINcH7b7YVC19Hkc9EbHrQrSpjjfPlXaQgHlDr7bzuv8d3ynNVSHe5Uaw
yNyeq/ST0AtwPBPRX71XBRLNbCfqZVoJ72eRwZB22QIgkcxDrzRKmDPPtDEO
qAYmgoKe/W/q9hDmrnicdv0Bfm9oVFnyRJyKMJ+MCMioEZRIgxGMJyZP++Sj
45vzBBq5ihfB3qeVny7eqybMtfgnqFmoK2Fyl82I/JFmdHz/t73eVKZ70bt9
36spKUrwn74vUSCvYPc8pnazYeu/57FDrufydntAcc2Ug3/+0wMOxiQ77faA
3U8xAf/tAdnpvybv9oBiA06u/+0Bz2WF3N/tAbPzDyj9twdco/KllqloBRFD
WQz/7QHPL5l67vaAeHz78n97QGfmtqXdHjCwrNL6vz3gkpIMlcHOeSnc7mj6
f89LwWRnDDqj8KC8nsKGJbYLuZRo4UxR8TCQ8Brm+HK6F/mFgOTK2sEBsMrA
eXvcdgB5bYldbn9SL9DS0Nd21B9G3uiZAj7ptoMPR0w3Sb0Iz7+LweHWYF4j
KM/LiH1nRHj+5mvflOjNKwFO8XCx4B7C8+Tx3GzXyMsGJpodGsV3CE5sXkrs
/Pw/fY5KbN+P2H7g//Q9QGL7BsT2E/6n7yEQ23skdnyJ7Q0S268jtodGbF+L
2F4Tsf0fYnsyxI4XsX1IYu8HYvuExPbuiO2nEdvjIrbvRGwviNj+DLHje/nh
+gFSqiJgkdDqHSedBVQzHg+H7Nm5DlYc/RI2UQzqZTSiu5+mIQ/25Xgr25cP
utJfwyrSXOShffCWsGEuIHFRmwzyykfulCOqkByXBWaGFnUOlhciL5i6Yheq
kQ50XutuXUspRl6f8BN+304BRu9jjEQlS5HP9P5dt9GPByK+4fXdD8oIz+fV
3mcjf8OBhA1fjbk8RG4RQX3afeslCDZvlLyYQ/D/ba+XWO4llpP/p+dhYjmK
2Osl9jjE3g/E/p8n/t/zQ/nDvsGKsqL/z/5/ADZ7cbQ=
         "], {{{
            EdgeForm[], Lighting -> Automatic, 
            RGBColor[0.87, 0.94, 1], 
            Opacity[0.5], 
            GraphicsGroup[{
              Polygon[CompressedData["
1:eJxNlGtfjUEUxZ8acr8VIpVSTq7dRIhKUrrTiRSR8ppPoaRyy+UL+HDud6V3
1vrN//mdXqyzz8yeWXvPzFpP5dSj4Yf5SZK8F4KwVcgTPFfMeJuwnfw6xvmM
H+vnubBe/3eQ9/99a/btFbbAtUsoEDYI+8l7XyXrXPMQ0RwHhJ3sKyUWCoeF
EvK7hU3CZmFGvbwIkbNcKCJfxj6Pq+B2/QphD7Wd2whXIT16PCu+lyGurxOq
qV9PzAgNxBrhJGdwnSfa9yrE/yNCj9ALfxlnWlH+u3Be///x39yXhDNCs3Cc
Xn1PR9hbTr1Sxu3CafZkuBvnTrDPPc2JeynEHluFU0KT0Eb0/mPCQeoVUcf8
jdRz/cvCWeGcMC3cEsaEq8JFuLuEFuGCcCWJ52vhDlqpuaBe3gp9cAwIg8KU
MMrcJt4wfZ+iNe9TiCY66cU15sX3JsT7M18H/Wbpz/Vv8A59xDbub5C1ndTv
h6OPvDmHyPtM14hdvN2PEM9oH1iD1re1b31ZZ/fQgeuPw2UO+8l+sUfuCNeF
YWFRfO9CXP9U8XWIergv3KTHB9y9+fLgcM0C7sb+ucv5R1g7SG33Z12nfnVM
fV9M3xP0kk1yPrS2ZvFaCTU85/8BXvPN8H1wflXxZ4h68F1UJFFn1uPXEPWU
+r8SLvvLfnuCB6upkyFnDnvjGL1Xsd96tU5rqOH8UXhPsMfrTjKew6e1nLkS
7kX06bteVvwWos5K4c4kOf/X06u/EbVJ7htRx9stcUbva2TPL819DLG3XnTh
N1pAw9af/WzdtSfRn47W6zx6sL6tOWsw9ZmjNWp/22v2pGtb215/jrzHqf/T
dfZIN+Mexp3oJfWH4wC9pB7qYK4/yXljiP1ZNNTD+bLU7GWum7x1/0dn+hTi
G5nP34BRdPKFb5fH/kakPnCcFJ7hl0nq2G/W/m/NfQhRB1nmrOsV3tRvtIp/
GzjDKNzuyZ68zbnH6Okv/TTzjuZvopb7P81ZPof4ht5nr05z1gl4fUfja3in
OdsyvqhlfTd3+RdO6/Y/lv+LLQ==
               "]], 
              Polygon[CompressedData["
1:eJwtkckyg0EURrs0tmzZGTbEEETEsIltskoVW8oD8BRmYuYVY0xMwc756n6L
U//5q7vvOLKz19jtSSmNQi+85ZR+YAVfhiv8EIbxcXjBb+AElvi/5nsM0/iY
7x/l8CGf619vZ+ABb8IWvgGvjqcYOr90Pr0dgFvnquDrcIef5vA63ONnOXwT
LvCDHG/7nO88x9k2POIfUMIXfH8f+vFBaPl8Ap+Ed/wP5vB59/8NUyl6fsK/
oIwvuj7VU8Orrk/51WvN/ak+5dJMnx1Psyw73meO3AXvo+vZzEIb/4XVFDtq
+b5ylzwvzUe5FbPj+8UUPXTcz1qKGG3vW7GL3ofyafcV16f+VIt6/gcbsEOr

               "]]}]}, {
            EdgeForm[], Lighting -> Automatic, 
            RGBColor[0.87, 0.94, 1], 
            Opacity[0.5], 
            GraphicsGroup[{
              Polygon[CompressedData["
1:eJxFlNlWU0EQRS+5+RF/h0c/waU4AQHBAQ2DSphkDmgGkhAMxoFAEEGGKE6o
3+QAnmNvlw973brV3dVVdbr73IXU+e5EFEUXRVIciiNxIr6IS6JD9IiUmBCT
IisW8feKe+KuuCH6xKBIiynxSCyLJbEiSqIunolyrP84+P2fE3nxVKyJgiiK
dVHzv+bmRb/soSh8b4ph/m3fEiP4PDcnrjHX3+vM68fuZI19V8RVanAtXaJb
3BG3nb9iFePQh0XW2X+fPXtZl6YnKXrk3gzQuyl6kWXvLmI41hN64NqroiJW
xUvxQjznuyNeY3vsDb5XYkPsiV3xU7n+iENMxz6Lg9AltPit/9M49LjAeHsy
1On6fsVhTgFd6uTgvbfRdlo8RuOG2BT74i25NLD30LXGHPvL1PivthpabzGn
Si82qG2NGA18CeUaJ0OPHMe1Ouc8fXStbcmwj2u27TWV6L+vzN51dLDWD9B0
AO1GxUNs65rBd0p/5mUvEN97ZtHYvfScBc6Ox87obZb+OudZ2XPoYZ/teeqx
hjNRmFORvSoO6JHtqvgcBV9Mfcto0s5+f++dvi1934lv4itnxmfnPX7f/RZj
J+i8w1gLDa3lR/EBv9d+J+Yuczx2zDlwXs7vE2v3sR2jSj1+d/ze+D1wjVuc
gwT6LXG+ctx/30/fU9t+E/ze9KFJhl7Ootko9gyaWlufWZ/dMTGOdu735Si8
Ab7Dvssd+Gz3oH2adRkYY+0cejj/ZhRqKpKf36YJ9HONfuuaxBokB8cZIUfn
5ndiGw1aaON4K5wnrynwHo0Tp0R8967I+fQZKHHHm8Q8Ir9h9vRefps2OVcH
6OR75Lvve+l7N8SaSepxHXViOvYfWA3ufw==
               "]]}]}, {
            EdgeForm[], Lighting -> Automatic, 
            RGBColor[0.87, 0.94, 1], 
            Opacity[0.5], 
            GraphicsGroup[{
              Polygon[CompressedData["
1:eJwtlNlWU0EQRfsml4RMCkYGgQTirPgnvvkJLp/1nwAn+BIVBxRBHBAwIAhB
QSCoDJ6zej/ster27erqOlXVjbsP7tzPhBDuiVQ0khAuiltiVExkQxgXH/Rv
RbwTs2JRfMF+L76yZntOLLH2UXwSq6KJj/eti+8hnrsgvnH+In4b7LHvZ7HG
GT57XiwTY4EYTc7YFj/FH3EoOpVDQVTFOZFP4prtblERZ0S/6BNnRZcYEBfE
U+X+RBRlnxclURa9oieJsX6Jv8QssqeH/WVi9OEzoz1vycm5dXGPQWK+1tob
cnJuXh8SV8WVJOryQ+yJ3yH6DPLvchI1sTab7GuhibVoi0fK5aHYkn2Axhuc
tUtN1rF30HUVTbfRuIndIoZj7XOnLdbbnO9Y7qEV/j0XL+iTWXKdQQvn7Fxr
4ho5T2vtJTWfw3ca22e8ClGzBXqpLp9hcUNcT2IvLXFP5+leHstGP+9/xp18
F/d2h4YgFUey/4VYM9fOczFCTwyg9SXxmP5wj52SrzXeR5e8zsqlUV/rM4H+
m9TJsRxzB819lnuuSt9Oyp7KRj1q5OZ73CTHTvneTuM9HH+Eu46yZ4z5nUfH
ceqxjKb29Rn79Ia/C34IyK3I9wE1rcgup7HX3fOlNO5p02P+57VD5iGHnrv0
mHNzTjVm0rGt0R491M1sDtHXU+yvU89j7Tlhrj3Ltp13gbnuEDlm2LOcFSkz
7jksoJdzdU45zunGZxL9Pbt1ZrgH3ev0Qj9vZIN/vdRkmLtXqZVzKKJHRnaS
xFla483wTBXQz/+seYY7V3g7EtbKvCuBNdt+a9wvrlPg231rjfLo4B4+Ik9r
4/q4hilxcuh/gl9Kvx7jV6LeWe6Rp19OqcMKb0KL+f4PoBDKiQ==
               "]]}]}, {
            EdgeForm[], Lighting -> Automatic, 
            RGBColor[0.87, 0.94, 1], 
            Opacity[0.5], 
            GraphicsGroup[{
              Polygon[CompressedData["
1:eJwtlNlWU0EQRe+VPCsg6LMf4IxTAFEMxGiYEgUCYQphkEAIgkFADDI5ix+C
OPJ/nrPuftisom91d9U51blUWM6UzwRBUBQx0aw/TWI0DIK86BAPxBOREnfF
PfFIdIp2cV88FknRxprjbr51sNc5dzjDex+C47R4Ki6LK+Q4t0Xc4gyfdVIX
BD/qont8fly0ii6RoLY4sWv8q9w/oqz4tVgVa+Kt2BaLYkmsi6pYFhWxKTaI
V8QWa2VyNjivXlo1iILiKXEe/WYUF/k/LX6phhF6u0qd7rFEDb77Fd+u0YNz
3OuxuIG2SXTMiAF06xFDYjCM7vkp+sPovl7RJ3JiGM2S7O1nby/ffIb3WuNu
vqfw/pnIoltNfBQf0DjBGT7rbCzSxFpMinOxaM3xRBj1cZMZaSNuwdt2vL4d
RjNnz61to5gOI407ubOH3hN430cNWWodF2P0aa08y6P0NEA8gtbXmaE4Xr8U
b/B8DV9q9P5CLLC+Sr/u8Tn6N9D/EJr+Zv48Y+7BsWfS8+a1RubDedPUmqX2
PP7bjy5qnGTOStRxIA7Fd3Ek/in3VHxW/C6MzvYMFqnlmPfTSt/22zOTQ8ML
seg3IE8NTewfQbMT5qsP3dP4k2P2LsaiM8bwoB7/B5kv9+4aXds6/VuzYWZ0
Cp8X6XGCnhfQvoCOS+S4L781v0m/zSrn+vwd9pXw0J7NiXk8XkEv1+S3vEte
lb323Ot+63t8d+zfgX3WtpiVQ7zYZO2AnG3mxm/lfRi93RQeZzjDed/EV+7Y
I/4iZqnZtVaYH/tYpN8ZcipoYO3twTz7dtDC8/AJP+3vLHv3qfWIGmrsca7f
eDN+znHPKf7tUp9/s/0O7a/fwH/lNZih
               "]]}]}, {
            EdgeForm[], Lighting -> Automatic, 
            RGBColor[0.87, 0.94, 1], 
            Opacity[0.5], 
            GraphicsGroup[{
              Polygon[CompressedData["
1:eJwtlFlWk0EQhdMmi1AiIAvyxSV4fNa1KCoiYEIcIjIjgRCSIIpgBARjEAQE
hwDihNMCvPf09/Cdv/7q6urqrtvdcfHKhcunEonEJZES10Ii0Sn6RK94J7ZF
U3wWW/hsfyLujngo8mJIDItpMSW6xW1xT9wlZx+xD0QPPtv3PS+peeKm7Kw4
r6JaxQvZs/KP6zshyv4XYyH6bJfEKD7bM9QyIorUNEiNrq0gTit3i3guewGf
x56IOWzPm8c3Ta6n+FzLY1GhppJqnBGTsqshfgvk8n+H1jqXinOd4xE1FYj9
Io7FX/FHHOGz/dt5lLsqDmX/CtGuiN0Qff6+Z94hfTsQJ+InY3vkdMy++CC+
sq57vcOcJj12r3+I79hNctm3iSYc95E1j6jV9Z3VXtOpeDY+oy5xS+REv2ij
v1XOyGfTnopnPU9sN9rJ0a8zqdhb99i9s6+EBqwXr1mhF87lNeboX5p+z9Kv
Y/b+jzPPsdYwOunHN0SfsvgG6Z01Y+2siVch6raLGMcWk1HTWeKvixsiE+K9
8dxFsU6OZ+R0rlXGlsRrYtrRz7Lsl5yXNbUSos93pJyM8Z5XRh875LBerKET
elxEr9bwgLga4hvgO9lDjRnGfMcr6O8AnUyxvwz7su2cA8xzL9wT1+V6vJb7
4v54zU7OxOv4XWgl3jFV/t2/WohvQBv68N5r7M+5PVYPURvWwwJn2YL+Fjnr
DfE2RM1b+3n25Xs8jj1AbRPU6/24d5Now2/ZCBrxm+W3y2/OKHaeXGPU6Fob
4k2Id8R3xXfpG3123/2mbtHDFWrcYO4ydoMzqJHL+02j9yW0UmfM93wX2/P2
8DXItY9vDV1tU8M6OtnBV0FPdfKtUrNr3SSHc/ke+U35D5XaGhQ=
               "]]}]}, {
            EdgeForm[], Lighting -> Automatic, 
            RGBColor[0.87, 0.94, 1], 
            Opacity[0.5], 
            GraphicsGroup[{
              Polygon[CompressedData["
1:eJwtlNlWU0EQRe9kVJAoGkMIIiYhQIgBFTARNCAogpgog+izy2f9RRVx+CDn
8ZzV+2Gv1be7uqvqVNWtvHi99yqJouilyMTHNIo+iW1troo5MS9uiY64K9bF
I7Ej1tjz+iG218Qyd2y7IXrcuSna3PHdRbGEr664Le6IB2JTrLDn9X2xwJ0u
dtfFDWxWWC9w5r2cOCmGxTknKmIxKAY4OyXOY5OJE+KsyIt30uJtGvbznOV4
yzYzEq2RBZ1aYlrrvnivO1V9f42j6FscfNq39f2QBh2c/2niKIgLxO3cttCg
je7raOZ912Zf7GGzxXoXnfviuXgmrhJXG+1b1LTD3g51ORRPxWfq71oeUNMd
zg6w7fH2ITVxbZ6Ix87fmlAH18bazGahL+bohXvE2EMva9gi1i69sE1em+S4
i49Z0aRn3AcN9hapu301s9CD88TS4MwxWfvjNPiyHs7VNVkjV9cpEWfokX9x
qNsAtRrkrCguikn5qorLWk/wnjXcJwf3wpSoJKEnnKtzdm/UknBWF1eSYGPf
qRjCTx97x+u4C/i1L/v8qfh+xaEvM/S2D789iR7e87qeBG285/WUOEpDXPY9
Qjz2aV9N9LI+q2h+TP8uUYc3zIdj/hGHefGbI+RgbWpinHidqzVz/M7Zd31n
iLw9AwVsx8m1iK3veEY9q5fEWBJm0LNYFqPit2L4E4e5zlG3Qd4poOkQufpd
a+c7trWGvvs3Dv8Ev+F/2DI9u0G8ztm6W2/P9vc41M2947V1SKllnfo36dMv
cfgnRPTWGLlMUZ8yedWp3xH6+N9TItdhbGybR4NRzovkVkGzEmfuhyr96pgc
i+eiRv96TqbpUfssYV+lXrad4a0ysfnNDhpZG/+j/gPZeGyZ
               "]]}]}, {
            EdgeForm[], Lighting -> Automatic, 
            RGBColor[0.87, 0.94, 1], 
            Opacity[0.5], 
            GraphicsGroup[{
              Polygon[CompressedData["
1:eJwtlNtSU0EQRU+dnB/xXUVQv8Mq37wgipGbQkgEAQNBJAYQASEiGq9cRUED
ioroFyAoyg+xd816WJVOn5np7t3Tc6wxc74jjqIoLRIxrT8z4qV4IWbFU/FW
vMHnbytiGfuVeIfP9muxiq8snuDzt/1UFP0RD2TniVXmm896Jp6LRbFA7Dkx
Tw5/tfcgFdZMiVNK+lwS/FPk6HUfxHtxVVwTN0Wb2BSfxS/xU/SLATEiSuKK
aBCtooWzHHuNM+fJax1fAzHa2PNPefwXH6nB9qH4HgffHDUucFZBDIoxMSoq
aGxtl8ReKmhmrbJxqLVWPEaLS/q9LJrEDXExDj7bab7Vi2bWrNAX67BBDMeq
ik9xOHeaHCrk75qW+L7MGRvsqWLviB/it9bupoKO1u8COTmX6+hmHazHN9a7
xiw5HhBvkXtwiJ6OvY3m6+z9So/dly3xhRyr5LJNrav0fJO1a+zdol+OWUEL
9+KeeEhPTkjr4+KR7HHuSoFe+c6c0bfTSfjv9bbrkvDNvjr6VWJ9mt5kRAf9
9B32PHgubNeIYhx8OXGbuP1omGZvuzhJfvdlD3O2+9xJjCZ0zeKz1ta8hbP9
20qMHLbvchc+256dbny2b4k7+NrJpVf0YDtuH74Mce/i6ySXPL59+p+jPs9q
IzEcy7W5B0NxqNHauGbXWiTHLnoywHp/n4hDz2rQ12/DpDibhJ6N0eM+8iii
Xy++YeL14LM9RI+cs3Mtob9j+OwJzsrTuyI52O+591u2i/7uST05OTffdb8L
niXfU79NO9wn5+s3cgbNrb3v6CC1d2MXuG/e47We41ruo23Pts+yBn6Ty+Q2
SWznOE7OznWW/b6jrtv1elb87vgsz4zfMc+qZ9pzeQRBr9oR
               "]]}]}, {
            EdgeForm[], Lighting -> Automatic, 
            RGBColor[0.87, 0.94, 1], 
            Opacity[0.5], 
            GraphicsGroup[{
              Polygon[CompressedData["
1:eJw1lOdWU0EUhS/3UiMQI2CvLyESBV4hy0dw+Vuf0N4oIkIKkAIBRIgGKQqR
orj3ms8f38rJzJk5Zfa5dx49ffgkjqLosWgX1SSKauKayIiSKIqPcirF4XdG
LPDf9iexyNoCZ7xXF1NiWhRFQbwUr1jzXlm+S+KX7L/itXjDvY71gjP2nSSm
46xy/wKx11grEGtFLIsHKuq++Cb7u7gn+674KntTJIrdLvpFnxjV3phoae9Q
zIl5URUVMSs+O2+xJPLEdKwavnnsKr2ZxXcRbK+Tc5tixqJXnEtC7W85N0PN
U8TIc+6L+CG249C3A3FG/1zbsNiIQ42uPScWk9CDOn1yL/x/mH7syt6Jg5/f
sM5+jdq26NcyvW2w5v66Z4fk4XheOyC3EdlZ8VP2PrW9oyeu8YOYoG9z2JPU
Ok+vytS8zhtUqW0Du0ZuXsvx3o5tXfm9nYNj79Efr9neJZf3vKlz6tCZTnFe
pNFGB3Y/e13Mhn18r2v7I07jMDueoQviOr8D4gb/bQ+Km6ylueequEKMNPZl
9jLMo312yPtYHFHTPrFP4nD3kLhFDNsXxW3WnsdhpibpteevQk2uocT7rzBH
Bebf8+q5Lf7/JsRhzT0uc9Z98FlrqIFO7O87PNee5wr9yRAziz59j89nea8t
NJZDPw20+SwONUygnVH0Zj03mQHPQo98u5OgyxZzFiVBl16zfYav7VQSzthu
YxZT5FrhLr/lCPrZRP89+A3ytt3cM4B/hf44L++tMvdNZtC5/Y7DN8Bxj3nH
DrTnb9F4e/Bp8fZ7+Nh3jPlr8j0Y5/tl29+IPjRlLV2irl40YW2coJ1OdH6K
lrvQeoozQ9R4RFzn5m/nGnO5TU3/AK9f99k=
               "]]}]}, {
            EdgeForm[], Lighting -> Automatic, 
            RGBColor[0.87, 0.94, 1], 
            Opacity[0.5], 
            GraphicsGroup[{
              Polygon[CompressedData["
1:eJxNlNdak0EQhvO4uRGsWKkSRZB0iiAGDGDESAgoREnUiGDFdp1CAONt+H3u
e+DBC5P5p+3szPastxaaZxKJxIZIiin9mRQPQiJREnnJJXEieVnsi/fih/gu
3ok98U18FX9EF71/2+9U1PD3t9/iJTan/H6BzaE4ErdFn+iIYzEncmJHNKlj
jzg7yI73VuyKA/ElxLP4TI8lV0SR81l+JG6Qx/luhWg7LVZD9GkT07E+izfo
LH+ilhY92Ud+JT6ge42PbT9ytg5ncc7r1ODcKXzs+5Med+npAed6LraI6dib
4hk5nXsDXYvaquKp2MbP/xucqU3vfQfr9LJOjCZ9te8aPtvY1flm+yfk2KI2
92yVmlzHQ1EmTpWeV8hR/6+mNmdqMAu71LiCT53crvUEvX8vhThXNepcJGeV
2irUtEnOMj5r1PQrxJnzDFwQ/WJA3BGj3Nch+lG+DYoxbLLcpedzFv24mBRF
5LtiCt2ESIsZMU1MxymIfIizcYQ+z7cxYhXAsvdzPsTd6GB7n9gZcY8cGWqc
Ref77tJf99n7UEhGX8fzblg3T4605Ewy1up4fg9yyRjv305KziZjbOss22eG
fN5L74XfCr8RfkscY47ap9lPvzULIdZim2POk8N+mXvLkn+Ju0yTv8y9XxZX
xLAYEmfFuRD37JroQWf5KlgeETeRbZtCN0icce6hQH88kyv4jND3CWyHuXP7
9FLTELE8dzV636C28yG+Ac7rWfNMXgxx5orMTYn7yDMD8/QnQ7999kVsp+il
ffyu+J3JMQcpdFlmw3kuhTjX/ci91DrA2VLYpqnVe9JHzSXmx73wXv4FycKa
HQ==
               "]]}]}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}}, {{
            Directive[
             RGBColor[0, 0, 1]], 
            
            Line[{2, 1, 11, 21, 31, 41, 1001, 51, 61, 71, 81, 91, 92, 93, 94, 
             95, 910, 96, 97, 98, 99, 100, 90, 80, 70, 60, 1009, 50, 40, 30, 
             20, 10, 9, 8, 7, 6, 901, 5, 4, 3, 2}]}, {
            Directive[
             RGBColor[0, 0, 1]], 
            
            Line[{102, 101, 111, 121, 131, 141, 1019, 151, 161, 171, 181, 191,
              192, 193, 194, 195, 929, 196, 197, 198, 199, 200, 190, 180, 170,
              160, 1027, 150, 140, 130, 120, 110, 109, 108, 107, 106, 920, 
             105, 104, 103, 102}]}, {
            Directive[
             RGBColor[0, 0, 1]], 
            
            Line[{202, 201, 211, 221, 231, 241, 1029, 251, 261, 271, 281, 291,
              292, 293, 294, 295, 939, 296, 297, 298, 299, 300, 290, 280, 270,
              260, 1037, 250, 240, 230, 220, 210, 209, 208, 207, 206, 930, 
             205, 204, 203, 202}]}, {
            Directive[
             RGBColor[0, 0, 1]], 
            
            Line[{302, 301, 311, 321, 331, 341, 1039, 351, 361, 371, 381, 391,
              392, 393, 394, 395, 949, 396, 397, 398, 399, 400, 390, 380, 370,
              360, 1047, 350, 340, 330, 320, 310, 309, 308, 307, 306, 940, 
             305, 304, 303, 302}]}, {
            Directive[
             RGBColor[0, 0, 1]], 
            
            Line[{402, 401, 411, 421, 431, 441, 1049, 451, 461, 471, 481, 491,
              492, 493, 494, 495, 959, 496, 497, 498, 499, 500, 490, 480, 470,
              460, 1057, 450, 440, 430, 420, 410, 409, 408, 407, 406, 950, 
             405, 404, 403, 402}]}, {
            Directive[
             RGBColor[0, 0, 1]], 
            
            Line[{502, 501, 511, 521, 531, 541, 1059, 551, 561, 571, 581, 591,
              592, 593, 594, 595, 969, 596, 597, 598, 599, 600, 590, 580, 570,
              560, 1067, 550, 540, 530, 520, 510, 509, 508, 507, 506, 960, 
             505, 504, 503, 502}]}, {
            Directive[
             RGBColor[0, 0, 1]], 
            
            Line[{602, 601, 611, 621, 631, 641, 1069, 651, 661, 671, 681, 691,
              692, 693, 694, 695, 979, 696, 697, 698, 699, 700, 690, 680, 670,
              660, 1077, 650, 640, 630, 620, 610, 609, 608, 607, 606, 970, 
             605, 604, 603, 602}]}, {
            Directive[
             RGBColor[0, 0, 1]], 
            
            Line[{702, 701, 711, 721, 731, 741, 1079, 751, 761, 771, 781, 791,
              792, 793, 794, 795, 989, 796, 797, 798, 799, 800, 790, 780, 770,
              760, 1087, 750, 740, 730, 720, 710, 709, 708, 707, 706, 980, 
             705, 704, 703, 702}]}, {
            Directive[
             RGBColor[0, 0, 1]], 
            
            Line[{802, 801, 811, 821, 831, 841, 1089, 851, 861, 871, 881, 891,
              892, 893, 894, 895, 999, 896, 897, 898, 899, 900, 890, 880, 870,
              860, 1097, 850, 840, 830, 820, 810, 809, 808, 807, 806, 990, 
             805, 804, 803, 
             802}]}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, \
{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, \
{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, \
{}, {}, {
            GrayLevel[0.2], 
            
            Line[{901, 911, 902, 912, 903, 913, 904, 914, 905, 915, 906, 916, 
             907, 917, 908, 918, 909, 919, 910}], 
            Line[{920, 921, 922, 923, 924, 1098, 925, 926, 927, 928, 929}], 
            Line[{930, 931, 932, 933, 934, 1099, 935, 936, 937, 938, 939}], 
            Line[{940, 941, 942, 943, 944, 1100, 945, 946, 947, 948, 949}], 
            Line[{950, 951, 952, 953, 954, 1101, 955, 956, 957, 958, 959}], 
            Line[{960, 961, 962, 963, 964, 1102, 965, 966, 967, 968, 969}], 
            Line[{970, 971, 972, 973, 974, 1103, 975, 976, 977, 978, 979}], 
            Line[{980, 981, 982, 983, 984, 1104, 985, 986, 987, 988, 989}], 
            Line[{990, 991, 992, 993, 994, 1105, 995, 996, 997, 998, 999}]}, {
           
            GrayLevel[0.2], 
            
            Line[{1001, 1010, 1000, 1011, 1002, 1012, 1003, 1013, 1004, 915, 
             1005, 1014, 1006, 1015, 1007, 1016, 1008, 1017, 1009}], 
            
            Line[{1019, 1018, 1020, 1021, 1022, 1098, 1023, 1024, 1025, 1026, 
             1027}], 
            
            Line[{1029, 1028, 1030, 1031, 1032, 1099, 1033, 1034, 1035, 1036, 
             1037}], 
            
            Line[{1039, 1038, 1040, 1041, 1042, 1100, 1043, 1044, 1045, 1046, 
             1047}], 
            
            Line[{1049, 1048, 1050, 1051, 1052, 1101, 1053, 1054, 1055, 1056, 
             1057}], 
            
            Line[{1059, 1058, 1060, 1061, 1062, 1102, 1063, 1064, 1065, 1066, 
             1067}], 
            
            Line[{1069, 1068, 1070, 1071, 1072, 1103, 1073, 1074, 1075, 1076, 
             1077}], 
            
            Line[{1079, 1078, 1080, 1081, 1082, 1104, 1083, 1084, 1085, 1086, 
             1087}], 
            
            Line[{1089, 1088, 1090, 1091, 1092, 1105, 1093, 1094, 1095, 1096, 
             1097}]}}}, VertexNormals -> CompressedData["
1:eJws23c4lu0bB/AyQr1GCimyklFWlJDnTEZRZGWUFGkp0iRCNBCKlEolMylb
VvHc9pa9995UJIn83Ofz++s9js97Hud1Xtd134+H45uQ9RXDs3SrVq0yYFi1
in7lv8kHjXyU/AageaL2svn1Jgh96621WD0Ip4I2fBSV6ocWeZVrV7vaYI+J
MHpCv5ntw8BeeHb65IvPs13Q6bxZk/S30X2lXXe6YXTE7etb416IJ/IOkS7+
pydM63o7xNHlZkrM98Gz2BjsE7AwHk+/oRkY2CXe1Z4aAP0Zfw3Sv2vdrxm6
WAf1txXGRL0GQfP+Zqx3FAhZq8daAeF3W2Q8bYeA02we652dQyLO/siFoStD
zzcxDYO0aC/O88h+PfOi+0d4o2Oas3B8GNYyRdP2lXhjwvZIHxBsTHJ7/9VD
+A8dZ2mtEZgc2bK/aEMvMBjHZLhPNgO94RP0p6n2MRd1umGs/3OgSmA73GoU
vUN6mLXrare1HeC4J5phqrML1jSLuJK+8xC/7NprLcC2Z4kzeKAH2pdeYR+9
m03cF/waYJvjwbPd2/rAf8kc+8y4lmQNnq0BbkHzz907+uEH7EIXvHF2gwpP
KfBIKceZzfUDdRNg/2uyr7/ukaLCrPTKaXgMwPleG+y/V9PN2OpVDOz6kuPv
VjkA3E85XUiXrFcajYvuBrZV3rd/a9fCj1k2xpwrY5A4LSQx690JwSfO69Al
N0AupRz9ppxA1l+2duBb9hfKamoGvRBxetJr1zoyPYcWuCYzqbQzuQ12h3Jg
fSFXle87kUZY/rbezFyxE4RHz2E9n+RXykRhLfwnfdX4kFU3/LfqKbomqBaq
WlRCGpexKOezHrg4HsBAeuCSXNKmxkKYW/QOFFfvBfaj57H/zv6ZKyc2fYGm
HWciqV97wUB4Bv3O8rSwxoUosN27HGG62AteAT7oHEd8GV/ptQGDB2+7GrUS
4gZ/SzZoTkBi1kX+rf4tYKgs0iMrXwuX90rvID3WY5+3gFsTTMoc/vGzph6K
bsuhNxql7eQXb4A7edevbZNtgtmvuehrBNM2OD6tBSWTGqtD2i3gJNKPfvxV
gF/coyqIrUqqp3K1gZNRGfqZ1925okElEJ3CytD2pB3sTzWia4R6OMoa5MES
x0H57ykd0PhFGD1x29wfvuQ0iKi4rWfr0glnp26ga/ofZMuaC4MQK5nT1UOd
cGPdy504v17q5VtRDfDf/nWTDf7FwKqU9yUmfxLi9rNfLFSvh+R2nT0i/8pB
3f8z+vqKyTB4Xwv5XaVOe3mrYe33CPRRul/76SOqIcqqT5D9Yi38SU1AFzrM
3x8TVAG+V9P++rDVw0yt1FfSpbNFPFSkS6DYalCgzbEBHkgWY31TgtPhd+75
kORcX/worBFEV7Nh/S2VvSyKUl/B4pMkzNxvgjMK37B+oqGuyooxGRItuU4e
EG6Gha5udHWZXcWq70JgvFTnutetZvjx5D72Kfu3u0XvWhVUunkE7eLOhRvZ
F08+6J0C4vtMlYxLBTisz3L5tL8A7F8fQtfb1zN4RKoMrl3JvHFCuhimyu+h
q89Yx008KYZv5gIO9E2lELRsgd5r/stF/1MBlB9oDFGTqYA0PwN075PT90K8
c+GAADX3064q8Ja9jb53Zuiq9ZkvEHVum99Q6zeYNjiNrst46w5Yp0LWw7iu
DvYamJe8i57glrW1/uAHaNKXMuK4WwOV+ifQJXbtkl1cfLry/nVa7ciugUCn
YPR/nz+8yqTkgXmwi6RfcwrUx5mZ94dOA/+a8t+cm3JBoFv6Y6NfOrznV0VX
eke9qeyaA7qKmrMTC1nwg8EU/dDd/fIKdl/gGdeVjCXuHBCVNEdPjBWPoLJk
gH8fvcOeZgIsqZbosqBm63cyFTwFXkxc580DEV1anzNmFeKXRxIgc9l+8uj6
fBC/aIH+nvCVkPz8HpTT7n0WrMkH7jW66EN77LivO7wBj/FLtb5GBfBkTAM9
vbpK84nqQzAOazT8+aoAEs8cQj/y5GZ8rmEkKNlwLLXwPYfXu5eqLs1PQ53H
tcyXzBHgOG7u4KPxCuZ1af6dtWCrx4MwEHWumr7G9xbspWjud8X9p/ubt5Bx
SDdU6UgY/Hi2iL7f/OHauzWvILlgw+aipnCIkqXVVzcFn11OeQ6zCwW7/4lH
Qu91mpcyeb1e1A+EtlNmfzMpURBQQ+tj+zCf97WDDzywskzx3hgNru60ekPf
3bONne7g/F4QymOiYQuV5iy3AsZSxy+A+c29SXFz0aA6TPO9hTG7zjxNAIET
4UFBqWFgfHOCg2FymqgzbWNe6x0HV605HRiXIuFc+yS6WPWefW9EPsLa316x
J5hiwJxrCj21l+1GzOUYUL9VWLRYGgvmWrT6Z8PvU3IuRUHYqlzm3Upx4GY2
jS7Hdzv7uGQ4nOuM9TPQT4C5cVq9pKy2u+/Z1+BKiF3uoE8CVoLWX2uUd7FX
4BmoX+RvWI5LgguWtPpDXJymJ3f7QLmtwu87Qsnw3ZVWf+W9xM7c9OtQtmP4
v4FTyWD6jVY/0CxwL5exEBwl++9c7kmD98mtQjV3pgnu8w/2vk/MB0F6f8UQ
6S8g7VaFrpE5W+23NQ/6AzsFtkpSYTG2Ef2v54BiVQIVDI845e7qz4X/0hrQ
f9u/MAonvoDO1up1jHfywaC0Fl2Cvb1k9Z10EK/8t8elrQBYq2l9LgwLSJz9
kQz3xNVzDywUgu79JvSvgTwcGhYf4WyZDVd1cxGEx9Hq6wQUxu55hoHKOcVM
G4diODpUgX5z4Mu/x3U+UNZ3fVgovxjy0+rRHxw+FlyVVgvmaSNDMeYFYG3l
Rc9lOQV3NAa1d3vVgK5Z3hs/8RJ4xDuEziZxVbTj0Dfo13A5a5RQDm6eb9Dp
r/yaSxSpgJ51pSqd3VVgv6kPfW9/g0HDhxLIoQpfeWReAw3enuiLcDNHs78A
mG8lu3o9qYW+uLPoot843rbmEFAuU8y64XEdaAs5oEu+FDz0XT8DnrEsu7AY
1sM6Diq6cP6Tm/UK8eCs95t9VWs9fHhVgB4lyJEj2BwMfS+sko8INUAfmzP6
hrUlo4RVCbD8lrjM7pcFQ1rmqwKPTEO9NRRBXREM+tQ9lAiiwlsVA/TLdvtu
tC0VwGuHV6mdb/Lg4M2b6PyFjo0mY3lQ+1GfzTezAII/O6H3hjSxP9xHwAU2
senZ50VQMW6Gvvq1WxCh9gXcN576qyhTAo/lT6DnfRJK55z9DFbWb8UPuZdC
hrEG+ptss7A7pQlQ3pZovMqzDP4FUdDf1bENRhyMBD6QSZNRKIeAUBv09gpB
gY9ij6E4Ui41MqgcqkZprv3wcKkIXzpYUtYvHhb/AD5thWdUWqahjbN02jLi
M6QV3rS0Ho2D+qUi9AfcrHY3hlPg2GtbnZcxSZAVQPPV5Y7ebd+TgN89U3Us
OgUePqS51WmZM94+8VCT/yvrxMXPsFxbgk7w+UptbvgAspoTLicm0sCppQB9
awbdx4M1kTDBF1ksIpkBrSeq0TdPlqR92/IWClQT1pVtzYQnJyvRm0Ys725X
eQpSD6TWHyzJhP49tPndc+9qfL13B5YOPK3xEcqCfx9y0d1+7t0k7/0QfrQo
nDoZ6gQHbWpkTyxPE92roocUmR4As2vtvqRbruB1lOZP5epftqjfg4vCMxf+
zd8F9f/Xs3K217xX9wCdfQyLrKvvgb0pzUMfBZ8c5HWD3lr7If6q+7DmGM3V
bOdvXNe9DZuqdXd7mD4EbX2az5tPWPLL3YC+0Q39x197wdQ5mgdVXpDg+mgH
xpdHH3565g0j/183y4r3r9ozaxi+qCdhtd8HdprQPPrii9KN7LrgureI/+Qb
H4j9/7rru7+ePpL2FW6HK3nt1EqArQp3+27kTROvAu7m9LRnQdF/j6bS+VOg
LMcXXcpg8+OapAyokl+nJNr9GRwi/dB9r4crzCqnwWmupwvxlhnAaR6IfpPy
1prDMwW66z3m7J6t/LxO80b/ayz4zpk/EWrTQmdtb3+FmgO0PkoiVu0vVu49
Vv5uZCxTDuS4vUR/FLzF9ahqBLi8UW/dr0wFn1MB6GsKQ/J7W4Ih5/Hc/Qus
BDxzc0W/MiYRzNDlAe8+S089cCNAPdcDPVjuudU6oXJwLZzKdvHLBruq8dHD
Aiuf82yFRSHWpWCQELb1AlceCL9qRR+lqy/YaVoMEa+8L0QkFMDS6d/oPhO/
+3evfM73RzA8zWQphpfUOvTxve/cWW7kQRhD1nllgVK4oNqJnna5Zei+aQ5E
6wt7B42UgdSmZnSe3Jf/GdlmgBh/KZ3g6Qr44/oDvfa0xDN5+WRIoZcUp3hU
gkVEE7pH2KSPn280JJ2e3HTyUBXM5HahH5OldJq2BoBHXYYCc2oV7N1XhR5j
QTfotLYeei+G3uiuKoTr/6z39WyeIooFvv8WKaqFc0EzUwxDpTAZVYNuOeOi
8/lgDXjy5Yk/TqwEcflA9NDInqPZUlWwLGvz98DK9729Bl/RvcP/qny/UgZf
2BLer31WC68FXqDfELW0a7AqAsWP008Ne+vg9btr6HZMFpZ7OPKg3uTIw38/
6+F6si964JlN76+uz4KdV3WvJxQ1gBrvKfSx0t9aEecSgeHDI7Eyk5Xfwz47
ohvVvvF1fvwSDOcbPO9HNYKYPYF+lHv/Yb3SFoBMVca6DRVwOa5o3YWMCYL3
zd2ebsFm2HAgtYxOoQa+P7v2H+nvd2vUXJJtBDkTDd3Zj3Vw9OsgOmPQb8/1
S3Xg1yTi82uoAfQGNNCPnRCKLfCvgZbd9Q7zPU2gWOSJfp9vWNemoQJYqF13
iKctcKk+C9c1PJHSfGZ3MTQc1ub+MdcKohtssP6kmYW4pn4unFVvShJnaYfO
I2/QI6NCuXWufIbHFuyeDMXtIMAigC7GvXSZa+4dsHJGHpDd0QEhfw6he3rd
ZR5U7IJH/w7fFvGsAYmz82maf8aIK6Zjq4LlO0A25tmXtrx6aMygyyD9g/TH
5X1NrZDKXbNFrqAJtgRPpZO+rSd0t4FYM8i+2fUh9k4rGETqo3/cKWZ/WKYB
3sJPtY+D7RDFZYL9N9jk6Pj+roGopZpvwXOdoBOyCfvvHwlYuLaxAkT1vN1H
P3SDfrg39onnejdzdKAAdmQTbpEuPaB/eBL9/L/5W9SxTJDV0KwrmeoB3YsU
9D+pauFn7kSC4Ko8mVs7e2HLaAqu+2D1v2aWqF4QWlRYA1r1UDBUnPNncITY
lcPKE36rB77aRlQarG+GAutSKunRRq7XXCS7QEz76IaAoDbwyDmArvJt7/db
zO1gPqfWPZjXCXlmE9mk3+BUb9gU2AyHNgazbNPugb1btmN9Qml4tWF1PTCJ
RYiMufbCk8o4XNdc49UXutBqyD1pfuaWSx80HhbEPsxtxzb4K5WA3dB9ygdK
P7TdW4v1iuybYr14cmB1reA1nqJ+qNn9C5335fwuxdz3wJDn7reFdQDUm1Rw
3R1r9/tFmHWBl3m9xZpnNSDU2vLKeHIMQuWvim4z7ICdAU9yDzTXQ1rm7Zek
r9bXM1D52QpNHQVxbe1N4K5w9znpvd93q53f3wzslmt7tV62Qlxo1QvSxZjk
GCQ1G+AnT/lsP30HvOcIwz6vhJNPTLLUwn6tjeGsPF2gL+CD9ZeOvGIrl6sA
tR8f+vdUdIPOrsBg0pvGU491/imAY773TNz9e+A/tj/PSDdQ+fEuhzELfh3x
sxVc3Qvdm1LR67ctpt8NjIT51w+C10MvrDaOwf5ss/YaO8ZbgE9O1n5UqQIe
FO7c/zp+Aqii0tsuqjVDNstO7m86NeCuWahGumf132xlvUao3sMlrVhUBx/r
BoD0i93c/SU89fB9sz7XSYZG+MYcgvVb4sdkWaNrYMFyS5PY6mZovG2H/mZ6
Y8SjvxWg0NDPY5DWAjeV1NANBoz7w42LoW+Tf7XjljY48pgJ+8tUee/Ze37l
9+Uu3m1yMu2gEW6F9aeWdFRZ/D6D8d7aCO7JdrjccRz9Q0DR3wfrw2A+UfVL
zcq9Pdx4DPc1P3FfhUmmHhwUbtvU/CqEt13T6SYbpuAq3bs0teFaCGvdqFLH
VQbRISXojoXO2b/O1cDrycKKsKFKUJdLQZfxuiaVd7YKfrYms22SroE3O25k
kN6rZWtUHFYGWR8PCJevfE8/JEDB+v2cfPa9j4qA8yr7cCBjPRyX3oP1NyKE
sudl80BtofjAYd4GUBe8i+4yf2H/U7UsqBmmi3sy2QC7GR+jX+T5ZTwekQhV
ra3zue6NMHxjHPvb2d9peZfzEqqJ/Y/Xf2sEyc9uWM/wQMEp8nI5PFeenNvQ
nQ1Vekfu82+ehvOTg5pisaXgw36lsUErD77bUdDPqOWPf3tRDMsanFlBwwWQ
qyyFPtbw/L0QFMK80M5YZ+1iuGQliH50oxuT9/s8CInp/0N/thRqe8bukZ5/
z/XKhbgcEBhdpeUtXw4VPdpYL7mHWWV9QgYIRV7VNkqsgPiYdKxXFtx0e84m
Gd441Nel1VdCVaMt1p86Efu4tywaxA9YdW98WwVfTGewvmjIO457bSC0/uzx
m2X+BgLXz2B9k4bE3+dHs2Eob2coMZUADQeZ5pKp0yC9jk+y69IX4My0Wv9i
5fuRY9MmdBWHGbmvCpnwWeWm60mjNFDv+v2LdIWi/eE7ktOg8Km1WXXPyvcv
ulH0CuHsowKjKdDBEuXTKfxl5bnjwD7Bf0Jas8sToSnr2cbznNmg2LqI9UpK
Q9ztlrFw+XXUqaOxOfBPlB7rjZadL3ZHREDKW9XswD7qyucLD/pgjeiuzWYv
4O0fC1GDLwSY3WVAZ4/R4Lmn5gmDdqJKP7lzgfHtAPYXnn0YttnGF5xfRxTd
nHQFdr1wh/+Wp4Ex4/C+zQE+QBlx6gwb8YBAe5qrn3/uZ3PJC9qMW1kWw+/D
9eM0tzQUH81ZvA+MfvWZuzZ7AZc7zV/Eiyo3HPYE8wCTBidtH7gZTXPfb9Vb
dfPd4H3UVNiIuC90OdE89sijlAZfJ7jVEnX6ZYofOIXSfHLh9/sOpmvQaBco
Ht/rD/73aP7l2dl7U8/PgZeoGIttymOQc6C5iOtHamysAbxNPrmoIvwEFExo
Xn3iV/QpjzSi0OJaxy73GOLRu1VlVm3ToNtvID29/jNhUDGQtFogjpiqXigl
fU3gzwXBMymE1Idd2fTyScQR3v+wfuaaOkXTOYlQ17fztxJJIcSUv2N9xuee
nOIvccRfPQNGl7ZUIm2CAesPnxo9orf9AzFC2dczaJhGaF6dxfrb07xZwZqR
BH3LuU9LXumEpewP9GzrJiFW/TdEqsdz9583MwijH3/QE79Ysfl8DSQKD1lz
NfJmEpNZ/9Br3g27CWe7EMO/mZbeXc0kXjxnx3WDy5fm47lKiJD0L39/7M0i
whtsi0QNpiFow6Cnmm8RIcU4VJAkQyU2hvmg32GM4a3JLiAecce/DjiTRxR7
30EfeyZHZU3KI7pKmSWtnQqI5v+M0VMrioyGmqgEB5TVfzlaRFjXu6DXz8Vz
Gk1kEZ9SG66U9xcTBVZG6Ld8sw2HEz8Tu4X7FZ7LlBJhrqXoz3M8RE1sEojo
spg0epkyIrryGvo1wTM+o8yRhCPVzHK6q4xg57yJ7j1s8eNxkT/hV3+5fqdq
OaGaaYc+Y2nJNxhQS9j+qHE2lisgfhW8jmI5OwVjPZIOBTY1xOXmTA/t+WLC
cJMZeobOFmGC5RuxTea/v5m3yoknssHoWmIpS4Lj5cSZlyYsr8OriPMfrqDv
XBRveuBYQghftstzVKwhrAQd0Nf+nXGpzyogop48LZuyrSVGf95H1xg8vS/V
hSC4Op51nz9fR6zbU4he1OHMaS6YQQjsDBkrkqgnlB/6olcNmv749TuOmJy9
rxKZWE9kW/uge/+ZdvKKCV75HsQrb/67nqi5tR79uMiDM66nmgkNujp44VNG
GP5X2De3aRL01IPrRWoaiWTnR3S7A74R6oMX+0l/+qqnZvxvPaGWqtYr8bqW
EEnzRNfYtvrJ65FaQs5rgScyfeX/j6min3+QpeKhVE3cW3WvuiuokShP7MX+
8x8jW3dDOaH3+qHpws5mYjQmCf1b49pj//0sJKKK1VeP3Wkh3jraob9aZ1fu
WUQlKKU7pH3cWwmzUBvsH9yb9b1IM5UQfCNtEiPXRuQmMaEf4vHUzt0WSrxx
fbCFM7CNaPH4TutTYqGwsl/Iji3kXtkv/Gu+M7XixMGKw4kr+4UsgZM5CgHf
YCEqD/2P/+yHlf2ChcJi4cp+QelQFLqgcrfDyn7hdCX7VER6PTwzWUAfHQ2d
vqtUDWJ0lE8r+4Wq9DR0DkIzfmW/EBmmJbayX2i/X4kumuohuLJf2MGoWT96
pwUKBx3R6f2zo1f2CyeEDP95u7eCpq0L+qzky9yV/cJF2RjRlf1Cnch19PK0
j5LEtlDYUag2uj6wDZomUtH3JmdK6Qx0gFjfZVGlwGpYX3ntD+PVceLF056w
6Lk24KuXSavprwNPfVF0J0XHnO7UFijjLlo8XdAIYRH66J/jJeq2iDTB/u5m
bhXLFhDbeQ49tavF5fqRehhe0+MvGtcGntSL6FNlF3507K2BCfeAgdboDmCq
+Yq+9KfEl3+gDBb8bnps0ewC7ewd6IwvNr/LZyiAv/5TOhX3u4GqVz5P+ra7
A3UZdRnAP+I+6yLbA10mG7E+NnRQ8Fx4BOTcuCFw81IPMMXS+rTdsAieXtML
FZIcp16G1EGHYJTx1WujhKqByPY1pd0Q8uOIQL96E1yQ23OM9Fv5+7Ma+Tvh
5EW2X73vWyG9hQ/rj9nJfM7gaYPEkwNe/Zkd8JrbAeuPsTju2ks0QYbKJs09
Dt3QvpsR6wd9354c3loPL7JSzov09YDMV1msN/Clmjjc/wabDk+HdTb1Qn/X
JfT62AHOTNtiYHt4TEwzqA/S5sRNSLc7ohd+bWM2fBD4KcLE1Q/zI2+x/5ap
g6yvf0XD3merB3yO98OVJC/sM/Y8cOD32X7wfFl2r1muEYLs+RfuiwwT8Mj8
g9fmPjgcTsTFnmyFnY8Y/pB+wlWbd/p5D2z5ZJd2VqkT9qabzJMup1sv/jOr
E7rFIoPtLXrg7gMB9MK46tgdY63wMqrH1nOkF7IGdmOf/Qyjt4NKG0E60bVl
3b7+ld+LX/0m/cLvvx8VrGthTuNfa5vhADQ+UMM+1Od9/w2vvG/0To6S7hKD
wM12FfucmtxV8WCJgIHoYBGjnEGQaZdFb/IV0XZmiIXcbR4M9/8bgrql07gv
jdPl1nlegyB3OGdyx8OV9+hk18dHvgPE/Rr/rn6eAfjUwsB5XGzl+zaTdyzp
KlHtrc8v9AFjl4FVnWIPBOgmx5Nu9V8ok4R7Dwgs7HIsEO8Dz9LyONJftowZ
6xZ0wOxPIY1TPf0rP78vfiJ99qLWsJNUC7CIJ35TMh+E9KI57CM8c6BI73A9
AHXQMP7xEFw/W4B9rO9ziNwwqwSGiOqCkTvD0F8ZgvWXLm40qMzNg3h+meW/
giNg5H4K6xurmCtPFn6CLQs5Cz1OI/BpqhJ9zenUyYCzw8BsE9KnHNAK2Tvc
Pqek9BI/gS2ounFljrVHecRedkEay+VU0oPddOr4Vw8A3XPeiJx3vZDXEphO
+oGvHNfVJ1d+jx2q4i742g+r9IWx/oSrsP47tW7wNrDtcX41CMGDlWmk130e
i2fWaoObpt55q+SH4TMrA647WLbzsNvvBqim7so/eG8Elo1+YB+/CvF8z6pv
wFjuLiX5cHTl92ZBrD9ssTm0VKcAhGb/ht3aOwbpPfQ4T8fwPFPwzgTgy8/a
vfBiDN7AT6z//Unmj8efEVinO7n9mXY7aHfwWsyqdxKMPtoj5XrDwG5zgVXa
uAcc9jVYky7xrkTe9fgg8OwIUmGo6YMG9mtWpM9qbnMsFO6H5x7WbDc5BmGr
bcl50ks0hATCE3rgVQ79Kxu2YZBc7YJ91ig8/hy1rgPOTZxr8y4YgW/12uak
8439MHaxaoLQGrNQV8kxiPpZjPUFUj8efXCogcbA8KhGyji42gyfJH1fiMX6
jheFIKQ8fk58aRyM2WVOk84pfbK/cHMS8LgJ0fOfmQCGeOpZ0nc2vJZ4RRkD
6RYF8eK4DniifdmOv7iJkByueCnjNgIvCrOyv4j3gjPL31ukp4qm+151GIK/
Gi8dBwv7Ic6T5jHZoa1tWwYg/65PXrTwEBgc17pBOpXpp2iJVy8Eb7Vnuqc8
ArwlS1dJj9biHfjs1An5lTdu9jKOQUeQpD3py+ePMlTfbwZ/3vWp3O7joFTG
jv1vQv9YjFYteOYZNLuET8Cck6Mj6coD3t1jkUUAeVcZ7lychE9br1whPZbn
sfq5iSR4K/Ij/13DJJzw/459FDSYNzbIjkODX/SbK2mdUJmccfnUqhqC3oih
DE6PgmVmtPvmkl6QzPS7QXqtiqC6PwzD9yOcYOEyABIN8vakv10PKjcaB2Cz
Ta39zNQQmHxa50D62oQ2qWcSffC2QPG18OZRyIswv0T60VypkEzvLmAsEO7l
mBuDzsixa6RvFwx+0HuyBZhngz5235kAXSujq6QLWWqPbRmqBUXto5w5sZPg
uJbPmXQNSdbv7y8Vg9LV6/IzTlNwjz78OunRmYPCr7yT4UB7Y/3M8BTM9ytc
Id2E99P87qpx+HRauPHI6S5w7mPT38uRTwRXb8mV+zkKV45c1zGX6oO7FQmX
SX9cu8H9QNEwWPl+thf/MQCH55WsST8Ln4O0dAfhR1rsltZrw3A6U9aYdKpg
tinL0z7I0ovT9/g4Cr+7TE+QnmaxesF4bzf0J3jUCgaOw7WoRVx3fCLShXGs
Bfp3yqtnCE4C/auRo6QXPJZby6pbBwfzw5hNjadAyeGyEemTr3LsX00Uw6SR
56CC+Mr3ezkhE9J92Zc9HSaTIUPWcjL11TTYfWU+RXpCy2vLkYQu6K48+J84
1wTxvo1erSjemMr7R8SB9PEOVybS39U1mfozq1BOeY+dIz1hgG4t6QcY7W60
zZtT7ZyULpHe9a6RmfTr2mnpEjIm1HbnPFfSW8uL6El3cFq7hpHXnHJZcvQq
6dGcdNi/wEJPT2fPBoqgosl10idOhK4h/eAgz+ONdhRqhsMo+ty2W+i2ebNb
2K4rUibd1l8j3Y3tN/qBOh7e8gI5imd7zE3SbyssMpLOrDu1d72zNMU8Pe4W
6bkzn9F/yifNvzITp/Cr7bxPOuN6RjrSkykPNdflmlDm7Vfbkd6zwRz3dbIu
jnLFQo+6z8jQmXT16TQG0p8xC7e4BBpR5Lm5cV0q4zach0HINTBEUIcamWmH
+00rF8H9hkcEnheNkaSuvWmK9a8kObGeX2TysDfLIWpLu8cN0ls8j6DzisXp
BDfuoPZXpuM5sC7cQ5+dyLjwdJssZdX8EazX+HUcfVSqPOflGmHK+io3R9K/
B7jgfm2c39xtt99JyctKxvsN32aK8yRqZuTsd6OjhnA/wP0m8u/G/U4qWGSK
qx6g7rqmiOeclzRBO//SRTGjVlFKbtk67H/pbwj2dwnJ0I7nUqPURx3Fc846
9Q19R1Q8l7K9JuXm+WO43x1faPt9Wl0wMactTC3+twfn//brNO0eFfNYM9bs
o6wWt8c5fV5dxDlfFjM7HC4Rp+yeMMPz3HhbAt3GvsPWg+UAxeqcMq5LkWrA
dYXfzpSEJPBSJYZfos9YfEG/03lP+N5LfUqhGOC+4p6P4brdx97vYpuSp3A2
0M7/933a+V+USljfYSZP3cBm70R68h5l7PP97kLJi1FhasOeDqx/3iCP9VyZ
6qVaXxmoj9atwvvyGkxDrzstbuIg8FeVkUMbPWYmHL3piRn7vQB16snITTgn
77lO7L9B69vj/WKbqE/zVHHOe79pc/aUbEtPqZeimnaH4flsJU7gOdQ0dti8
kD5AjR5gxTn/OBtjn6p3dPzl00M545Ml2N//fjw6EWT586m4HoWj/Tq6n0sh
usXLW9tbvPdRz7VM4Jwde27iujFz27uKGaQo61mTcB4KUYFe0J38cIRvO2VN
20a8Xz4L2vPvE2G/ZzJWjEpfoY/1xoWD6Ed2Tf5U3raPsq8oinZufw6gm1KN
7hdVslDDWS5gH7lIVtr5r+bvmq8WpEx/l8Y+zvTT6Krsf5fefpxWPfTroz3p
x1oG8Bw2Dl0OCg0QogaqirmQXlHsgu+p/8m83dtDDCkb8ySxj5r4d+xzrOlA
wbuL26i32rjwPNUzPbCPdXzRPpba/VRRyTNY/4HooT0nWUP62w8KUa5qD+H8
Khvl0KnvQ6RsJrdRnxSnoMN2QG8Xi+e5wSFDfWwjgeepGPIRPeLvU+t/k5zU
nK6ztH3R0fr3TEkraFpuoGalDeO9QFMU3gu7weKdQXUVylTgAvYf8tmB9V8U
e+WuB4hQ7K/XY5/WDfnoH4MyGIxNpalXrV7iuvLafuhXtetjyj//yPFJscY+
bYzH0I/O3DrBJbWZai2fge+XdyY7ngP91sOiF74AxVFPBeuNSk9h/a6lUmYn
wV2U/A56XLepZAE9fJXWxRLz3ZTVyen4HDqe2o7zlzBHmnDZ7KEmPJjB+t0G
WVi/PujE3HwmhVJW4Yp+8XYrulswb/u0ghxVfCgY1w0s0ELvfXA0oDdBieJ6
2Qp9zJI2v4IN0bO1XYh6xqcB55dMY8L5x/pMlMvKxagvs8/jPJ7fVXAeU5Wb
HcdKV+635Qreu6sI7XNGzW9xp0jSeur5cxvw801k+CXWH9g7+pNPW5lq/dQT
57y5owXXlVfu1Bll3kOtVvmI8zxxUUNPv+vUGDh7gDo9rnWFdEPKF+x/UntO
VmtwO6Us1Qj7J5t4Yf8bzFfUQsLFKdxSyjg/T7cc1id5Do5/juOj3JTegnMm
qt9Fv5R3j/KGR5mazdWE92tn74LrLvoWdrOwylPeuN/HObOjmtHVE+45TTkI
UNa+uI/v15smRvTiuJoEugkByo57HVi/J4RAnzx2mOPB6d0UaUY9rL9L3Yje
qLdBxMWFl6ohU4Pr0q9zRX88naz//NUuSuGpLOxjIF+GXtS49ViAgCwlS5i2
7pk52rp859cypwpJU7YfPYR9TMQj0L91yEbsvLWeosKjgee5wfokuveXmcIX
japUmUDa8zkSSns+tR/fHzzDLkrlTZrCPgzpN7D+7Za1lzTjxCkeG5WwD4MN
7efL5N1FzhS6XdTYnBM4D8cyB3plYHnX5xE+6j2zbLwXgzxbvBemWav99Wq7
qJIvefF9VBKhfT4z+Z3YmF1DR/XxvID77dXuxj63TJgmGs7vpDS9lXvtPD4N
GpF3dBu3pBBOh2q9V+UPq0be10J3StXSIF0nYtiPTW87ZeCtBHrqZnoD0rVC
U1LD7i7kDL/hQ/+Pm2pC+t2T0UclP0lTuYdF0Wd53huS3j1w5+7PTkaqYSvN
TSXi0G1ZRYO0f0tQn27dQZsn2Fyf9ECH60oHmASpWg9k0I9zXdQjPURGnKkn
ZDBnn7AkelJpKdbbXhFbrxhJTxFLl0ZnOVGI9eXe05xtu1mpNUVi6G3C9Ti/
hTNXuIEeE0VJjBe96LSGGemKKelD6Q2C1Ow1+9FfsoocJF1og4X1112SlEUt
2jw3vO9j//Qzd8Sa/UQoJWFK6Amts9qkBzwISRlkl6fsZpVClwt1P0p6ojWL
7BmNjdSfF2l9FARvYh9Dg6xwAeWNlEtBtHqD2xpY/+tj8yqgMlDnw2nnX6O3
GuePu71scJpxVtV0hHaexx5F43m27v+zQ4iem7KQq4wu03oT5zmjdGKIc4Gf
wq68C92iQhHvPSU0OljbdQPFSJFWf9IzBesdr2dn5YrJUp5do52zqlU0nrPh
rbdvg7fwUVqSaX1209FjH3Uzt+NDJQuq4avl0X1au4+QzrY7b1nBkY3Kr0eb
f1htO84/rjOzxXMTC+XEFtp9+Qcy436Lou/VJ57cSGE9R/PdMcN4PvJjG4ZL
EydVNW7tRD8llI/1oxbPdSTZVlO8dFTRY9+YHiK9b4tfccAeQcqiKu08hQVs
sT4tYLUk25Y+VcYU2rl9cc7Bc+uYctfNCi5V5Q2kPYfWW8Vwv/tNnwfEs/JT
zdVo6ypeb8U+kS3vlVmWBlWN9tLcxq4TvSxqyK42P011/xOaOwpkoD/yO+wU
KsNKHZGguX9JP7q94uUhzyhOqtY6mmcd/IGuf0f12dledqrdpm3oF/5cNSLd
zNDlkpPdNorsLyF0esEzxqRvv9E/ayo+rzp9g/Zc1RxywHPz61wnu82dlRK0
SZz2vH1MwPNPjbr8qcufmcKbSnsvjKza0ZklA3w/r3zvdZGj3XsLRz6eg3Rf
0J/Cr+upskO0ewyk9KN3/+oRlH/zN4c5ltbfYvUV7CPY+v6PSh0DlbqVVm8m
rIJeEuv6uWxgRPVhCu1+E2aLcE6i8wP9ecMFVbEQ2jlwG6TgOfT1Naz6bjqa
syuZNo9+mQ+uu+7MgN5Djv+okRTaPbIbfcd7fJP+mMU8mZPKJ097Dh+sK8fn
sHlvgVXayvNQ7Uhbd21DN66rf65fb+DZOsqmAdpzUjsuiOs2rg2Lb9Dnpsro
0eoddvzE+usTqo+52JiopUy0fZXt18R9PeWWfqOgzEQlPtGenw2nt+Ccu8pu
JvxrWFY9zkHb19a109j/L8OeZP0nLNRf+2i+y7od/SXLpb1RL4dVufbvQf82
cPUwznN49SqpP0IUeea96MTgBx28RxdRz0MneSmPGGmf59MEge9jdcmnradY
+CnnxmjzWx2Lx/nHuJe04g1ZKX9aaPfemVSO8zv+DN19jnkVdcaaVn+LcRTr
ldYp7BGUpaNqp9D2dd9tE+5rYhcf270NvarvKmTRWzVbcd24m83b+qnjOX4l
tOfhz2Mr7L/G2H+7Rdec6olJWp8Ht/7hfv8kfvihvmsmZ38L7X6tg5ywfyyz
R1T2PlaKtBbtXt56nsP6ygUfHmWP1dRcIZpvD7iFrnpr6qm3HxclQJ7Wf6OJ
PvZJtlIIcR9cT629R+t/pPQt+oua1a8eBKylHrxA+xzLsBPB+emLhucKLRtU
E4xp79FDdzc8h71fX8l37VlN3fIf7XxiddhxXV691BeLH9ZQrexp5yn88Q/u
16jw1mbV5U2Urfwi6M8zevD93XpBwS3LhoPifJI2vzjFAvvU8ih17jjOQt0k
Rnueh1IW8HlmNb5rcmH1cs6xhP8/b+1/cX6N2z7XCl6vp9zKpZ1zcoINrnt2
f2ebqQ0d9QwT7RyKqs5gvbVip2vE17WUcVba89bH9x3XVQ7ZmVLe3pezfTvN
a64MoltcGL4tvYWBsjSqgA7RzPgeVW/vu8syOaPKx0Wbp+U04LoDGpY/WzwH
VVk+0J4H+tkRPM/kO4ZZjCOs1PoZ2vlfu3wZ57nxSebu4yBGyj0O2vy2iinY
RzU+Rq1WbTP16yjNh90M0W8bzyyozrFSjmvQzk3B4Dztc7i9gqM+bx21uZN2
/hx+pVjf6Fi0Rqt3OUdnLa2+fOgu7f2Sd+JLoStR/WxL81u2Juj0BMNjGYm+
nMZl2vPAHtWD+7XVusG7w2wddZ8P7Xx8i7KwnqV5ps3r5H/UNdto792Efgru
t7/oupfDhuGcTx7bwsn8lfmrVsxfMS/OBpE5JdujS3Jk/kogyr+EzF/xW/Wj
865OO0rmr/7EmKWT+Su9/W3oxzP4NMn8VXOYyTiZv9rufw9zUDy7HP4j81cH
U38vkfkr+7eJ6Gw9apZk/uq8xpsYMn9VKKSHOa4H8R4cZP6qec/SKJm/kud5
hvUO+o3GZP5qec8DMzJ/dfhNCa47Myf4ksxfvftreonMX/27dxTr/aRMs8j8
1a5PAW/I/NU3ohjrR2eEVMj81U+27vNk/krse+ABMqfUPKYlRuav/t47w07m
r5JlNqNnLRsmkPkrB7t/MmT+yrNUF/3BaN4Amb+6F5bDQeav1l6QQE8yer2d
zF/16cti/uo0ZQT9B2/1BzJ/ZdSjv57MX21IOYDeZ9XYTeavtj81LSDzV1Sn
BcxTpUccVifzV+eLrwmT+avFLQboTKLfZMn81bJfZwiZvwrba69O+larH0Nk
/urnfb0kMn/FndCA/eluXGQj81dBwoxKZP6qqIIxl8wpnf864Uvmrwz/bmUg
81fTIa0E6Wx0UUFk/ur9pOZTMn+lF5qBvtn+bW/u2SooXfrzk0e6BgTdatD7
3bZxkPmrqU8p82VptUD5+AP9YZHVUTJ/xfOnPZvMX+U+OoD+e2uvP5m/uvVp
Bw+Zv7ol5YDzOMVYcJD5K94NZz3J/NW24stY3/Ake2AsIhFcP+SUkvmrGc8F
9GaGVQOhOS+h+7rsKTJ/5Z63F/tMf++5TeavhCwrfpP5KwmTXZhTevKX/hCZ
vyo33FZP5q/+TSuif4sOmyDzV+m2Zulk/iprHaCXTY5j/mo/x5doMn9VbaWB
/kBzbMnrfR6s+3RumcxfTX5axHzUzXVfLpH5q33EBR0yfxXfzof19JFPDpD5
K7ljehQyf2VynJb7yj9v7U3mr2bmZRrI/NV7BwX0rz1ViWT+yjT9fiuZv4q5
tBs92exVGJm/sn0+FkjmryyBB/33k0rN4KPZ8OWgaBeZv5pKos6SOaXxcxkm
ZP7qUImkBZm/GhxuRI/PEjtD5q9SO3nTyPxVrGYl+vft7QVk/mr7Gt7nZP6q
IL8evUXNwovMXyk/fFdM5q8+NOejq342WibzVwV/NCzI/NWIWyv6YKYhkPkr
Y5uOGDJ/xTDVgH6j5gJB5q88UlsYnvZRYZ18ObrAmuslZP6qeVLUjsxfOa6n
ef46FkYyf+XhOXmCzF/d2l2BPk1fkcVr4wuFYYcWyfzVUmsc5pSuKvKb8gb4
QPtSaBeZvxpopPnZ3805Zy55wTiPTuvf8PsQWkjzj7m/w8j8VcSLfavJ/NW7
HJq3nNuTV3/YE/IWN+uQ+av2CporhcjsO5LvBmqWpevI/NVS2f/Xnd/zmMxf
fb3CGfkixQ/OJND8Vai2Apm/UidOu8T1+sNsOM13PSSMxp+fg4mLYVcvpjyG
0SKarxFVsY2MNYD8OX4DMn/16x3NT47NLFt6pEHxtfdSu9xjoK1brcKqbZp4
obTrydT6z+BN59WxSiAOHE0PorOcKLMXPJMCX9+WH6CXT4LyVC30SSk6QsM5
CcabQhZOi6SA9G9N9LUilR1FX+KAjan4inNbKsgXaqAfyGvz193+AcRfeCkN
GqZBaLI6ule/9YZgzUiwyRhfXPRKBylBmrN0d9/5T/8NqFWlFv24mQEJygfQ
W6X7A72/BsKmTT3HG3gzoeUBrf+jo47JQtkuYGJYfPDd1UxwyaegOzAHqyZw
lQCVy0nn594sOCumkSdqME2821JZruZbBDL+X1mTZagw9/AY+su6RNOa7ALY
Mj3yLeBMHkCNOjqn6ugwa1IeRP1hsrd2KgDXj3vQj6v2vB9qokJrh8jmr0eL
4PxNmpdVsV8xmsgCwrg4vry/GI5s0kX/nE0fMpz4GcTiWu4/lymFTAFZdDsh
+i8mNgmw43vYEr1MGQxF0uZ88L00epQ5ElZzsyVNd5XB0/od6EzGlr5PivzB
U4JfSEq1HAw2G6EniVxmHwyoheGnzOeN5QrAzuVADMvZKaJBUNSqwKYGjNIp
V7Xni2GpTxT98YIuE8HyDVYtLA9m3ioHVfkz6K2iFj2C4+VQsan1Z0h4FYiL
26OnSXbmPnAsgbp331MdFWvgJ6GO3vRim2N9VgEEd/tkTNnWQu7hI+hPeGKk
U10I0L24v+b8+Tq4su0o+mRCKru5YAb85PdqKZKoh82H7dCjbQI//vodB2tj
23ZEJtbDA1kT9AR7/6CHMcHQx1QpYP67HmwKz6O735o7TOaR1szHyZN5JLNo
iRHMC7G15pN5pEZPsVEyj1QVrYhOaVHPIfNIudK3W8g80sFBBfTFnUN3yTzS
KcmtjJHpKz/fDF+jx+s/2OihVA3Hsq4QZB5pINsV/db1O4VkHslRVk6NzCPt
uWCMfjnZVYnMI1Gqro2QeaTwy/3oExfflZB5pNhuOW4f91b4J5KMbn/1bSeZ
R1oVwg9kHilQ/RJ6SrLVOTKPdOTjHwbOwDZYW6qPrhPJFvS+NBF6H79+xXky
HOacuvB7mMArCeWXsfHwZ+rdpp6SKBjXHkTX4xaT6N/5CULF39uu/xEDuUat
6I9twdLq+AfgDkto/PTtI/Ak9dD+DlLaLruoHA2SG5/xRprGA78grX+l6mmr
Jy3hUDBQd3DuXiLYCo2in58rVXju8gbuvX7oYvM+CQLf0ProAb/O1oln8Nek
9cI+k2SI6aD1+ShwxEtu9yMYexSSbVqdDPO6/eias5JratxugBAlXHgvRwoc
0mqmfb+fdJil44sgRngWzI3Lggj4t7xs+mcabNbwmb0cCiM23ww1kE94QZzi
XrXKbMWNpTj3Rf4OJfrhuYPeodcE3ROaqwbu1NbPfU089J2+oOUXSnw4sRr9
mPlbw1NyLwn7C/B9MCOM8Auk9Wfi1rL6z+QZ8YBye757Npy4GEXrk+i4Pvq+
cgChFN8SKP4ngujcQfOvo6ayd5u9iP1Tq6Z98iIJo8+0Pr/S25KsOl2JcP5j
530PRxG86rR1t83uX/Xj+HmiWHNI+/bzKCI7k9ancssGtZ4fuQTX7dnC5/dW
vp9byKsfiJgGtd0+IivjETH5lWrm4unEpTZJ9KJcn+t1f7KJtOdF2WJeWURZ
AAWdkS9y/OrvLEJ7Iknk5PNsYpWOMjprWOpXrsh0Yv7E7T4jbYLYOquAHpAn
J6vGlEoQWjNBalW5xAfnrehDTeE3ki0TCIlsCcGq3Dwiqk8NPUvsoMI/vffE
Ezr+sMN38gnpXFH0h8kObzJ+vCaWBDL47Zbyidfy4ugtXEt3rrY9IGK2Ove/
OlBA9DrT+rS2Kc6CWBXhpyh+1bqNIJ4eNDM9MjwFHT87rUV2VxCqj26rv2Qv
IIYonOisbd/YBSdLCeuqWvPaX0WEF8t5dKWuVWttTYqJmgimn5+DS4kKYUDf
KN77Vup2AUGV9AudGy0n9PmaTEinnIr9eNMyl5ALb6KDkUqi6r4u1jdMOE9t
FfxCqL7SpFf3/UbsETyOrsVo+bZJNpU46Zac1FxVTQhMqqJvUZM6OMXygWAu
Vziy2qSG2OFsia7pWqL7rPIpIWxlt+1eYA0hlncYPS+5kkXnbgPh6r/kpXeu
mKh3Xfu4sHwS3oSarqETrieaXb9QG7+VE/vbJtGnpzZMGHjVEkXqYRxZfd8I
UZsX6KeXUy2f2VYTR+JXefvp1BLP5EWekK5fH/fI+EwFcW68omXbZB1xtOop
1n/Oyxd+w1hCRB471fbXqIE41Xsb601s/gQpnMonJH+HP4lybiT0a3dg/Y9f
60ReLH0hFp6ezbtzsonYzJSA7hY1nuY/nEQETYvuzvzTRNQFFaMPrHM6+Mwh
hAC6mxEXdZuJ9LQM9Dv/7ZNU3tlG1E+FOaa+rCS0I15JSOmv/B50/tyltXYt
xHK/cXcGXy3xy6AE/ao1+43s003EbrbT1WwZ9YTdojl6WZq+ZQlrA9F2h5td
bEMTsf3FPknSzULCgz1caglNBTkjf4kWIvynNjrHoTWPN9hUEVz3dYQ+zrQS
pWZj2Gded8trsxslxAm2ap3mS+1Eam0Uuo/+94MBynlEokCP5FX/DqJemB29
yyTzUeGzNML8iIrloHEnEWpuh/0viT665t8TRgy++MFPJToJwvQR1svqKv/w
etANb5cZKOf31kJwh/hzuTtjxDvrTr1Z2044smD2Zul1A8xdYUVXu3WG9e5M
G2gkdxR6UJvhd+frYNJH5W28Zra3wOr9Fx6zB7eBweIm9NPZ2/23szVC2ayb
dhxvJ5xRPP+M9KEp5ofdKbWQ9mNdyAW1bpALfYE+1uvHdgUq4e9/fftU7vXA
3LUBXPf1xZa734lC+OFxivpOrhcYT0mga/5UXdu0lAWcmeV3vsb2gmf8dvQl
jZ5+D6MoeDMbW8s51gstC2ew/79nt/k+qPZBLEPtDPfUys/NnZ2yL4xHiMLZ
+Hf/GHuBzo174662ZjBsOILOy8zTskOuGwYfnD32z7kd8kdV0A0VXUW7Z9rh
UtO+AKaiLlhDXZIhvUFhVb+BZQuoFD2w+9XUAx3ff0qTzt7OEtTg0gA3K+nO
7OPug4vcO7F+6y2OfZ/MagD8vvtd4OuHZT3aupb/fLMq6Ethw/PiXZShfrgk
vBr91ua5rZ0bqfA0eHjT/qsDkM4wgv3dDp1tOPYwBnrDqboZXwcgMNQE64np
wwuzbgMwNqI+0GrTBJ6/HyXu6Bkk1lR9zCsQ7IetmTb5PFVtoPv2WgLpd3Yf
0ZN+2Atv2TJ1M3q6oOqoJNYb+9sX+Fzohkd+On2XtXuBbzOtzz9u939+Vu3w
RNZ9YXS8DzbctY4n3b+S5Rw/QzPsd9YNjyVzobu4kkiPD7PvpLOsg4Pn4gBu
D0LrGgX0b3WNLY1/y+FDvvkXZosheH9fHvswnW+UuzGUCwK3DRmU54fAIvo2
zsl8+4V6h/1HSJEc/zd4eBiqH0lhveIFrp++7kPQkz3bNvKsBTYURrAmbu8n
dDkckrctD4BJb7XYqUud8CCT5z/S59jqSq7t64dTlxUDb//sgXz74+tIZ90i
LMeo2gsjDmmjeX/64PareqzflLWod4ytC/62POMXKhoApUYN7H/KoV3nq3or
1BY6dU/oDYHzsRasv69+/b+TEg2QOJXbqv10GEoFS9FjPVv2JIRWAXsnG5+K
7wjonlJBDy2LZNrqnw/3dIlPCXtGwespBecpPFUzPUUfD8svuZW8no5CnKIz
egnjQiD5dx19U8F14jq9xOoStRsSN/dTQg9SAkifs6tnJZ2VNdJUaaMa5Yp0
Bf59qFM+Zw3pXVPqgvzmlpRiB3Xsszll33+k88HlN6WSxpSjviH+pB+pn15P
+vOTw+e8dZSpfvHbH5N+4xkXemnKxWwDz+PUb7t8nuC/I7w2yUZ6z+0gGRUD
OcrBL+/QN1uUo28osvh65rchha7HDfu82evHQbry415b48W5nC9tdejH7FnQ
x0//+LrTRppC7/ED+8itOox9ljf/4pCWVaJ4tp/Gf6eodk6TgfSjxnEpCVdt
KBBojH0e8n3DPo8ZUgPM6YSpfqrufqQ7rYvdQDrleGFP8d5D1J9a3tg/r3cK
+5s6lI86ch6gHJEg0N+df4V+5PCGmyWv1CiPyhewf4pKBTvpzbJWdTXPDlD8
jp7A+sRRBXSHxN+hp4WYKAqBN7E+JCME55mXrX+Z2LKJkpRejs7Cx41uXXlc
PIVPiFLea4RzGjydwDkf16es2fRVk9qa4Y73FVHbhfc+/TjwhP34AcqX7/N4
X/3VFngv1232CTaZ7aBaHzJF3/nMjJP07xzjfMsJhtR/uddx3cBHb3HdtTJr
uewLNSlndiigXw1chX18wvorjPuPUI+lW+O+cqwlcF81np+UbFVUKZJOa9Fb
a1LQtc+XfLu9R4IqJcOBbjoWi66bIuT2vWk/5d6ZQuyf/0sA1023U3q9JLWO
UunT4Et6+WDKRtIjdo5yCFIPUg9xVuPzzDr1A5/PfUu84vlKhpSNqlO4r3Bn
O5wzX2KVTe9GDmqxxyOsd0y3wOf/fkLrstILTcoBFXv0csIb/V/klaa6h3qU
zZIM6IbfpfB+12xReLj9hyq1VuIlzqnaZINzBqeal8rZL6kqfLLEfbUnyOC+
fOM2WIYelqQKjDRhffJWetr9Ro8erblwgML1/gD6a4Yx9KKidVe7yuUpxQmL
6I+0S7GP8y2zr7lLq6mDF0uw/w1/P5xHVyrmlvunvRQLsavovwY4sX51sLTD
Wg1dqtGT33gOmnOWeA69in5a40L61PpX+9Ethl3w3om3x58F9bNTB+2vom9u
k0HXO/1RiuOCGtVkF+05lBijPYfLcx3/FPcIUeTtjqH/LqpEF0teeGOorEq5
48WMHuWtgesmv3gv6KNNoTRtCsA5/6X24fw/42z5Kpi3UC+/z0SP8I1EV2gO
09M/DlS1Jtr9Nmd/x/v97PpJ6vioNsVTeDPWb7oUgvuVONt/eODpQcqfxLfo
gT5V2IdFbrUpV58Y9fKObHxfFMV08H3p0dQUus67m2pzfwbnrHnSRHs+l52d
0u9wUh75cGOfy4xh6Ek+wVI8a5Wox9N3o69zuoK+00hGnO8GHWVbxFl0nqZt
6Pa1fsnurXKUASER9MrzfugFFvU7+XklKO+CF3Ffb9Z+wn2d4eWMOTayl/LW
vQbrLc674fz37pz6UH5SgdLhXoj30uOSgOeZl7bHPjFZnWr/7xPOPzCjieff
/G2Q0dNzP1Ve7hS6Q0AOutzGmS+mE7soX75GopvVGqJL8OiY6cxwUCqHotCD
lvXRr1aNe1jEU6jJ/cPoxKcRnP/9Sw/fqO+KFBs5e5zT3oMH/ZPT3PFvFBVq
0NpL6L8v8qEXVTtcyd4iR7l9Yg7nz2w8jfN/fXbNx7hci6r7Zhr7n4juwHq9
u493bq/eTdlzfR268CJg/Ynt/FJTNyWoiuFHaV7ViHP2/FazbDslSf1Ml42+
iU4KXebO+Nj7rcrUjmeFOE/YXACe5+a7TTP5SsLURZNs9IDKt7SfO048jbJT
DFTtmfPoxspCOE/k9lV2Y2pcFDvNK9i/8kEU9k/QfasveFiBQgRy4OftwQlf
vMeHERH/6PLlqTzXv+B+s4SpOL/9/K+jX6oPUH8xncQ+66dysY/iw6cFTFv3
U/3pP+C6djdycJ7r2gdXnS6VpHQkGOBz8mN3Gn4ulb1wnuJL2kTdHEn7XF3w
p32uVrw8Nh2/fR3VhP43eu6lA9jnEnEraY8mDyVngQ99ZvE51j8+rLXd13E1
NfBlFLpIST7Wf/wvdEbMrUa1JakLffrIBfRH5su27OskqfzrL9Peu9rN2Od/
LZx5OFVdG8YzewvJkESpQ8YkU1LOeZQGUTI0mIUUkUpKFIqUlKG8GV6UDJki
SZNhb7NkykyIzAqHRBp9Z639/fu71vXsffZ13fvc+1nPur0H3051z6gyek2o
+nMc0ZjT9/4nI3BuG6H2+Bj+vcGGhfj3rmD2lgXmyjKeuYVjbu3hgXlrdesG
fT46MclzCNeR0NXFdT63NK3Z4chOOJ2SoZ5nXRp+L7F5KCad36HF+JzghJ/P
KcUo/Hy8830uM0/JEJIF1Pvhj18srhOsf89QxV+dEehP+ROlHsqfGH9t6H40
LcG4ZMiG63g4q+Hf+2aF649D69iImxPqlN+Ypf7v5uq5Q9+sl2JYZDTr+ep3
gYeKC7dc7iAwcp1CY1uHwStzheRiQDtY27+ezrz5CYziFst0DcZgObe0w8uL
LaBWu9t1jfFHkK8hk/jPf4Zvb+cNDyi/hz8+yVJ5ql3wKbJDdq/+BLyEWdn9
DjXQqgkdUNECssfqlzAqJiHxuI3q+dVloN+94UyBah08Yv9VazUwBX3N7Byd
6i/AYWFoICegBM6JJqduesiEK/YX5StKo0GX3ik57J8EfuoG1ecWmJDQVM8m
WpsE95fp/nq/KxvmnVIXhSaZ5FnV3Mh1D17DQENopq9SOXiE+mzR9GWSE+Gi
5v38neCQHvnSNnYAcs0ytoWvGoW26KKpUdk2uF/AftPdrB/4PdKEnzqPw7zr
+lMX/zRB6W9jI/v8bli2dHbMy/kLGN23IiW06iGyPsvz66l2qHkZeOf5ikkY
H+ixzROrgjf/kA90ut9Da51oJN1uCnS0tWcVTxTDu+zI817KVdDVZ5EpeIAJ
85eM+1uuPYFbaxpGxYl86A03yDXuZEKnN6nJN9sPbqm63QXSX2BPxdIb7i9v
wcqN9v2X7XJh58DvmLDZApA5wTYTV8ok922TbhitqoQ8qUFT9bn38I8jXXoP
67p+C78zE8ki6BjluLDgUAUW6424V7Ou694z9+nLpizIT4g1mVZ8ASd3qYZa
ojwrs8GuJ88vQ2V62PDBe0Gg6DKQh3KWLDhOcV/jfQYVYTu3id4sgLxhu9xU
1nW9dYTf0QVL4HCCceEZzhpwFnooOC/FJJ+tX7L9q0s1pMwGHQ3Y3gx2ixEj
dMkp0vV4uPMBnwYIc5XtyynsgJ+qHgoJbybIV79v/Qm53wzGdGeVMzM9wEW7
LWH1+zMpYCWwK/p5G2h2pXEojPSDSnHo3u7xMTJOwl43L6MZHq4YZjQL9sJh
YVvjM9OfYSwmdplGagP479u+Rqy3A4S2Fre3PZ0A/TS1mfGoasj32XHnBuv7
LNFz1vCNyBRIPVwx+5pRAq6KzMiW/TXwK1DWrRbtG8Z7vbM8+Qwu61XW1q8u
hPMHe4eGCCYc2TxZ49t+FW4UK5qraodA4qOOzfyLTHjmELyYdDaT/LCSEVAe
lU+6ZYvv8f/AhFrewkZRhyJyyUfazLrVVWRFzJRzlgmT9T+UTguIrSSz71hd
62p6T1oWyqzccmIKNr9qum5K1JGHx8Xgm307GaT1UfXb6km4/OuiPosDcBIi
LA5O+4voLE66fBExCaxoAi5B9bTBrG7gU1AUGDr/hdTZ/HJe/GsrHCOsGb4H
++HIBlJf+uI4mXSpPPgj67lkeNker74/AEZDfx5/khslfytf33LL7wPQGnwM
oh4NgYnNTVfjiCHynkSO9+vaHjixtE3J0W0ETG4+aQt99YnsbYq4zE98hKAp
qDk7PQqdA2MyEgd6SRvex/RkuX4wDPOr65MfhxulWct+1beTTw7ntSkW90Pm
8KtXJSs+g0D+iKkRN+t3l4rxFqz5BNOPLsTT0lk++u4DHm2xMjLVtA3Pl5ZM
OeI5UrotM9NlvwHDdXQHnl/dNpZMnQuoyJ1TddIlDF5l47nTe9nUuRWzh3lP
Z3foMTxpqZgnc73CfM59mY/LqlVEsYcRriNwORHX2eLqltcRocAwCtDBc7Pz
26m54tY/Yib+f+WJ+a98eL2qXR5ev8n+dANNRYFh+84Vz80uCTGl5t7rRHZa
/lUjDt3mxDy72B3zIOXMyEs5UowDA8LUnG3oKcyv1PVeoOdxExr91LzN43UZ
eN4mac8u9Sp3KeLvEWp+bHp7AZ4DeWuxuT72jwSjuIuaD+k/PYP5FG9A8w7h
meL2/89xGZ2l5rh0sh2dHV900tuXUXMv+WnNeO7lsVRdaVMSLyEjTs0RJd7+
B6+/edWugPlFkJhfQ83tKHZTc7CdfuOaepJsBN9Faj17XD+eR/reIfPzCedC
cXoItf7wnAJe3yZo2vSUc54u6Eld9/SbFGr+59yWcuaSyWLibP1upF/Ph1V7
kH4fuR44gPRr8SOcE+n3rN5ncaTfe8IjHUi//fq0EqTfYz2mDki/e/tKDyH9
mmofnkT6dbMeiEH67eVeifWrZPaaifQbyiMl0MDSrzRnxiDS7yGf+kNIvzVv
vd9uZumXv3MA63fM3lOFpV/Q2ik5VBaVD7t07PRZ+iV/jHjJrXQoApX5verr
V1fB4ZzK4yz9kpOBkaIs/UL4i3vnWPqFzJNjq1j6JU0sI08gna4aaldCOqUV
ntFCOvU5XpjReCsZtgnVCPWczgH/rkkihPVcxlOtr73RiyJpclV1TkcekVXr
LcHmBxM2VV6ysmrLJ0eicgUktEvI2PAvf0KSmLDE6leC4UQpGRbXXd05Xkv+
GFqTvXZsCoSemxbUataQM+mbk2IetZBtoeTLw3WTEOxR0y676j051GZSVCrY
RRr6qqT+ZzoBiSar1044tMCm066+hqofwepVPLev/2fST58Ue3+2HYzdOLgC
vD9B7LX6/anmY+SXDfMLtK1dcJJbUtI1eRB2ztFcqoaGydVX7sfx5XbDe4n1
Nb7hwzDoOF0aojRIusf3Uz7/eQD2G2KVK754hxgTUq+TsZ/JlanA/qS51/2p
VPsOhuq8GfYn7AKN2EexVTl1/3dZkshR08Z+RsC8G/uihY1Zsv6SKgxrTgtc
v7D/MPZRikunT+w7oU6Md3RgHmQVga+7/IZs4G6fdYTXxWu4/vDmYFz/dyyX
iaKCHOOtoS71nfLbFvurtbauahUJqsTWlquY9175ie9z6Uj0OdJxli7PWYnr
3Pu1FtcJVvnw9rvYasYupQBjlBdqZ2wziPJChdWTFkVsp0A8RjUG5Yv2qLtC
ukU52IQXYr7dpQrniB4VuJOFckTt6ATm0rUVXLnStaBX3OyBckStvmVhvqCs
cBzliHbtTQ5EOaImsd2YHxT/0Y1yRLXbxcJQjmjIjyrM5TjNR1GOaInD8TUo
RzSirR1zt8alpShHdI3Gg9soR5R40ID5dOMLl1aNbJDl+0JDOaLVvCWY/w67
a0PriAKe9uvlKEe0YqfiEpQjanicXefK1iYQbdS8aDJdAZ3WZfeQz+mSGl6h
M9YI69sjU8YT38KQ3QjmP73yH5T9rIObRl4pJEcdmNvWYG4nEWH+7lkNbOFu
eEYTaIRoGwJz7XJOrg9aZVDIW/pY5WcTPLbtw9yAt/LaD50iKEw0u3CsvxlK
NIWxv9qzvdZY7NlzUNO7b7gQ0sJ6Dh14vdbYKmmhhlQYPh96q+d7Cxyls+P1
VvMKEyjPVkYpoxPl2R4cWyaO8mDn9Bt0S7kqwP3e9AzKv716eynmBmF6nqFr
S8ErqOUryrk1+i6GecvDVrOGHAKi2J12qw+WgM5hfswnzwVtTyILwFFWPA3l
3PZKUvVDTb77s195CRzBXz+hnNtuU4pf6XTPQzm3naeNPfV+VsDuvWsxPzXo
/QDl3M4EyrahnNszawWo+/wexXE9IBE0ordfcTpbBR5RHJhridxeHtp8C5oX
+LNpZVUQObu4CnF+U4l4tB+tdMPEGO1H63OcwO/lHt5vc2j/OuvFw1q0f330
3THM/yZkeaJ9ar183UK0T50XaU3NI7KLPEf71JnX/BSfNGTClmJzzJ2u/TmL
9qn7lk+dQfvU3mLHMdfPf5OC9qml1C1y0D41TdcW8xJ2y3C0T+2YcPAp2qfm
t6auq3FulQfap06Xu5+A9qndCSfMa+u9+tA+9UmeXXNon/qjCbX+tq9CNNqn
7siPsUT71Pa0o5j3XTDLRf3w1cbflFA/PNb/Ne4nL9N7ifvnXHc6cP9cS5GJ
+9KyAt9rUZ9ce9nUTdQnVw50Xo64pPawBuqThze1DKM+uZbqPtwPl/F+twf1
yX9e4liH+uTWrZ0UL/h3H+qTn0iL+Ij65IZepgKIyxdECqI+uY27fCfqk0f0
puP1PLkfD6A++d3XtatQn1xpQgWvj9/Zpoz65M1fQ9NQn5y97Dzm2q+XpDE5
skGPtmsL6pNP2Slg/t3NGvuT9Ix47CuEBOmacYtChPTYSexn9ttvwH4mcnxo
YuKpKkHc/47PeT1JFcG+6KXZpQ6tDmOGsNFB7E9GTahzshVsfJyttuqEuwUb
Pr8T4fWLOh/X57X0qZQ0ER93C3NPaKfOb+6vc+XJk2YU3f2D62x8rYh5Sgpn
XrqhLFHz9hq+T4ML9zAv0lFuYNsqzhgWGcV8pIY6v9zwoUjGvnU5ofJtBPP2
ZooPya0UtAj8Ta+xoHzF9IFc7CtyGXvuaIX1FauayVJzDZ4XsY+aNhlOz19g
FnemUnO2PZY62OdwOjzgU7ZcyfAUpXxLzn82uM5YYg5z/i038W4p5aMO+VHn
IALmY1v2c4sQqqspzvbiM+Z56kfDxdzZCedOak5YaL0lnvvt8vpssJWbmwAn
6j61dmRR5wUOWlY8k56nn7Wn7ueEvyWus2Vjue22urHicFeqfmpXFeaP/k24
UfQtrjjvutsTpN9VlTFxSL9HOyi/6DK0SSaNpd8Jqb1MpN8uaynMxQyt7iD9
PhaVn0T6Xf6LRvm/uOk6pN/LF+8bI/0OcVJ1+Jc1xCL9vvf7Jx7p1+TEOswZ
+gkZSL/x++sHkX5TiqnnKxQf/Rzpd5NcTTXSb5AmVWeX2NICpN/k4P2vkH5z
RKnrBhIbl6ux9PtvxsRyc5Z+X9pRnCeaXtDI0q8Z27AH0m+kBcWb6q2KUF6T
5E3+RY2IBpIx/3cU5R3VJ+YcRflOz8AP5R2Ri9+cMY+sX1aFcpys8t2bUY6T
fHc85uXC1YEox0lXVZ0T5TjZFZmM4dynrI4fV7UbSa+sh7Uox2nIgQvzyOaM
TJTjRA810kc5TqfcbDEfuODoiHKcGgWFRsavdJI9X6/i+iuvBssFVhLkaKGu
CMpx+u9IMObcfLsqUY5Twm2tHSjHafcLNVxn34vTTqTMA/LRczselOP042MF
Xi+caIh9jmwqgX2Ocoj/tztSeozYTEvsQ6I+a2J/wj7Lpc5Zv5mYTtmNOU/6
EcztyzfejalYSTx+SPW3vwDV32a3dZtRXqtHJDr8xX1jA55B7JdiYt9HDosq
ECneanj9nAYH9kvvlmwp/rp5EzFjM415rVc3rq8mIL/pl/s6ovou5X/YpSj/
Exq3hNYerMJoEnmFud1TDczv5ou7XF+xlWGTYIHvczhUC9f5l0+HfkaBhzGX
JMzY/OA1dHdHf7ylVA6iBQ/DNHyZZP8GVd4elk92/dfIiMnyyYVC3KnIJ/f9
zH4nxPKZ1QZHHwSwfGZQf9PKQJbPvOGzG79/ArrH8PtB90moIp9hGd1XiNJR
pvgU1pFlxO44czo/YdjBJ4Xq3zov7o/qG9+tSUH1g55Wr7VnfXfvcNMtWWR9
d5urTojMsL67ae4x2E+uWzuLn4/agUB5t2Ql4n+cMVPH
          "]], {
        DisplayFunction -> Identity, Axes -> True, DisplayFunction :> 
         Identity, FaceGridsStyle -> Automatic, Method -> {}, 
         PlotRange -> {{-0.8067224167583664, 
          1.2443110347131079`}, {-0.9169525481046421, 1.4143330471708688`}, {
          3.138614994663922, 3.9360320548988814`}}, PlotRangePadding -> {
           Scaled[0.02], 
           Scaled[0.02], 
           Scaled[0.02]}, Ticks -> {Automatic, Automatic, Automatic}}]}, 
      Graphics3D[
       Point[{0, 0, 0}]]}, {{
       Hold[$CellContext`TypeOld$$], {{"r", "r", "r"}, {0, 1, 1}, {
        Rational[1, 2] Pi, 0, 0}, {2, 0, 0}, {
         TextCell[
          RawBoxes[
           Cell[
            BoxData[
             FormBox[
              SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
          "InlineMath", $CellContext`ExpressionUUID -> 
          "ac4494d5-7022-4b04-9d21-ee2100588f50"], 
         TextCell[
          RawBoxes[
           Cell[
            BoxData[
             FormBox[
              SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
          "InlineMath", $CellContext`ExpressionUUID -> 
          "61f66885-76ae-4040-8977-edc2197c65ae"], 
         TextCell[
          RawBoxes[
           Cell[
            BoxData[
             FormBox[
              SubscriptBox["q", "3"], TraditionalForm]], "InlineMath"]], 
          "InlineMath", $CellContext`ExpressionUUID -> 
          "17945a6d-caf3-4555-ade9-6fdfb1395e36"]}}}, 0}, {{
       Hold[$CellContext`params$$], {0.5, 0.5, 0.5}}}, {
      Hold[
       Dynamic[
        Grid[
         Table[
          With[{$CellContext`i$ = $CellContext`i}, 
           If["p" == Part[$CellContext`Type$$, 1, $CellContext`i$], {
             Subscript["d", $CellContext`i$], 
             Slider[
             Part[$CellContext`params$$, $CellContext`i$] = 0.5; Dynamic[
                Part[$CellContext`params$$, $CellContext`i$]], {0, 1, 0.01}, 
              ImageSize -> 160]}, {
             Subscript["\[Theta]", $CellContext`i$], 
             Slider[
             Part[$CellContext`params$$, $CellContext`i$] = 0.5; Dynamic[
                Part[$CellContext`params$$, $CellContext`i$]], {0, 1, 0.01}, 
              ImageSize -> 160]}]], {$CellContext`i, $CellContext`dof$$}]]]], 
      Manipulate`Dump`ThisIsNotAControl}, {{
       Hold[$CellContext`r1$$], {0.5, 1.5}, 
       "\!\(\*SubscriptBox[\(r\), \(1\)]\)"}, -3.1, 3.1, 0.1}, {{
       Hold[$CellContext`r2$$], {0.5, 1.5}, 
       "\!\(\*SubscriptBox[\(r\), \(2\)]\)"}, -3.1, 3.1, 0.1}, {{
       Hold[$CellContext`r3$$], {0.5, 1.5}, 
       "\!\(\*SubscriptBox[\(r\), \(3\)]\)"}, -3.1, 3.1, 0.1}, {{
       Hold[$CellContext`r1old$$], {0.5, 1.5}}}, {{
       Hold[$CellContext`r2old$$], {0.5, 1.5}}}, {{
       Hold[$CellContext`r3old$$], {0.5, 1.5}}}, {{
       Hold[$CellContext`Type$$], {{"r", "r", "r"}, {0, 1, 1}, {
        Rational[1, 2] Pi, 0, 0}, {2, 0, 0}, {
         TextCell[
          RawBoxes[
           Cell[
            BoxData[
             FormBox[
              SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
          "InlineMath", $CellContext`ExpressionUUID -> 
          "ac4494d5-7022-4b04-9d21-ee2100588f50"], 
         TextCell[
          RawBoxes[
           Cell[
            BoxData[
             FormBox[
              SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
          "InlineMath", $CellContext`ExpressionUUID -> 
          "61f66885-76ae-4040-8977-edc2197c65ae"], 
         TextCell[
          RawBoxes[
           Cell[
            BoxData[
             FormBox[
              SubscriptBox["q", "3"], TraditionalForm]], "InlineMath"]], 
          "InlineMath", $CellContext`ExpressionUUID -> 
          "17945a6d-caf3-4555-ade9-6fdfb1395e36"]}}, 
       ""}, {{{"r", "r", "r"}, {1, 1, 1}, {0, 0, 0}, {0, 0, 0}, {
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "75a08159-001e-417c-b04c-4d8833c6c8c1"], 
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "85feb9af-fac7-4a31-96bc-02c9fa353adb"], 
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "ac226dba-08cd-4a3a-be9c-d0195bccdfd6"]}} -> 
       "planar robot arm", {{"r", "r", "r"}, {0, 1, 1}, {
         Rational[1, 2] Pi, 0, 0}, {2, 0, 0}, {
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "783e4ade-c5c0-4011-b7a6-c4f92ff43638"], 
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "f6cbe890-1676-4bde-9a07-e77f4fb4380e"], 
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "3"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "a1f6ecb4-a5e4-493f-963e-c45e92e4e9c3"]}} -> 
       "elbow robot arm", {{"r", "r", "r"}, {0, 1, 1}, {
         Rational[1, 2] Pi, 0, 0}, {2, 
          Rational[1, 3], 
          Rational[-1, 3]}, {
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "d18c7dfd-4993-4fea-8f30-5601ed07adc1"], 
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "65d49791-8247-491f-af42-235bb864d46d"], 
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "3"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "8f8a5e1f-905f-4876-b1e3-8755dcb0d157"]}} -> 
       "PUMA robot arm", {{"r", "r", "r"}, {0, 1, 1}, {
         Rational[1, 2] Pi, 0, 0}, {2, 
          Rational[1, 3], 
          Rational[1, 3]}, {
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "97c6b72b-7c13-43d3-9939-826b35321e0b"], 
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "6ba5e64c-8837-4658-85b8-7c421888a628"], 
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "3"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "b302060c-ab1c-49bf-a77a-70fab5ffd8c5"]}} -> 
       "offset PUMA arm", {{"r", "r", "p"}, {0, 0, 0}, {
         Rational[-1, 2] Pi, Rational[1, 2] Pi, 0}, {1, 1, 
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "3"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "30d84090-bbf0-4c36-92cd-6fa49f441749"]}, {
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "404e5879-03b2-45b0-8ae1-82793a684e31"], 
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "0bfa1c86-ec86-42ec-a81a-affde2f63536"], 0}} -> 
       "Stanford robot arm", {{"r", "p", "p"}, {0, 0, 0}, {
         0, Rational[-1, 2] Pi, 0}, {1, 
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "c2d2263d-5dc7-4f9e-8004-1ec48d64d401"], 
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "3"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "15691925-19b5-4369-b2a0-6ad121af4092"]}, {
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "b3ee7ad4-cb2a-4fe3-9b8a-1ef786aa800f"], 0, 0}} -> 
       "cylindrical robot arm", {{"r", "r", "p"}, {0, 0, 0}, {
         Rational[-1, 2] Pi, Rational[1, 2] Pi, 0}, {1, 0, 
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "3"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "43429225-23d6-452a-9db4-d5b9f2eafa87"]}, {
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "eb0be0cd-61f3-4f2a-bc08-320a4fa03a9a"], 
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "ca3ad2cd-03cc-48f6-820b-8f0717450f39"], 0}} -> 
       "spherical robot arm", {{"r", "r", "p"}, {0, 1, 0}, {
         Rational[-1, 2] Pi, 0, 0}, {1, 0, 
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "3"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "d27b1fce-af6b-4633-83c2-d25f6bfd6aa8"]}, {
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "3a04ad8d-fd68-4dc7-ba17-9c0555256829"], 
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "3346b394-321e-4396-b7da-8e1eae823eed"], 0}} -> 
       "offset spherical arm", {{"r", "r", "p"}, {0, 1, 0}, {
         Rational[-1, 2] Pi, Rational[1, 2] Pi, 0}, {1, 0, 
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "3"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "f545a84e-d885-4723-a911-5cf6565754c1"]}, {
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "279ea711-2f69-4fb7-9346-4c1bb2d1cc6d"], 
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "89f673fc-286e-44de-a783-b92f9162dd49"], 0}} -> 
       "twisted spherical", {{"r", "r", "p"}, {1.2, 0.6, 0}, {0, Pi, 0}, {
         1.5, 0, 
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "3"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "472057d8-b7f8-4b2a-b8c9-0a8987f32d9b"]}, {
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "3305ad2b-ea05-44ba-bc2f-2ed114fe44e4"], 
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "71a45250-720c-48f2-8c74-61297be636e3"], 0}} -> 
       "SCARA robot arm", {{"r", "p", "r"}, {1, 1, 1}, {
         Rational[1, 2] Pi, Rational[1, 2] Pi, 0}, {0, 
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "18797ff3-4302-4e50-ae39-512d28599863"], 0}, {
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "fac033f6-dd0a-4191-b8fe-642c1a88dce5"], 0, 
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "d25f4409-df56-4da0-ae08-fa6d5f2d5544"]}} -> 
       "planar RPR arm", {{"p", "p", "p"}, {0, 0, 0}, {
         Rational[-1, 2] Pi, Rational[1, 2] Pi, 0}, {
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "f25b47c6-4d41-455b-ac7f-e1c2cad6903d"], 
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "18868fbf-5381-47b4-a008-26da7ea70dd8"], 
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "3"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "efaddb6f-2b58-46a6-9708-307ecc29f113"]}, {
         Rational[-1, 2] Pi, Rational[-1, 2] Pi, 0}} -> 
       "CNC robot arm", {{"p", "p", "p"}, {0, 0, 0}, {
         Rational[-1, 2] Pi, Rational[-1, 2] Pi, 0}, {
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "c3056f93-3fad-42f7-8ec5-84e1318a3b3b"], 
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "caf662aa-42f7-48dc-9407-ded68dedd5cd"], 
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "3"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "a1ce9786-c98f-467a-af45-20c625ffd093"]}, {0, 0, 0}} -> 
       "Cartesian robot arm", {{"p", "p", "p"}, {0, 0, 0}, {0, 0, 0}, {
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "7a6ea635-61aa-4be6-94eb-9323296d28b0"], 
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "22733fc5-48a1-40a7-8c48-2d06f3710442"], 
          TextCell[
           RawBoxes[
            Cell[
             BoxData[
              FormBox[
               SubscriptBox["q", "3"], TraditionalForm]], "InlineMath"]], 
           "InlineMath", $CellContext`ExpressionUUID -> 
           "ad9e1ee6-b2d1-4fc8-a5f0-adcc3053b08e"]}, {0, 0, 0}} -> 
       "linear robot arm"}}}, Typeset`size$$ = {425., {210., 215.}}, 
    Typeset`update$$ = 0, Typeset`initDone$$, Typeset`skipInitDone$$ = 
    False, $CellContext`dof$1075$$ = 0, $CellContext`r1$1076$$ = 
    0, $CellContext`r2$1077$$ = 0, $CellContext`r3$1078$$ = 
    0, $CellContext`Type$1079$$ = False}, 
    DynamicBox[Manipulate`ManipulateBoxes[
     3, StandardForm, 
      "Variables" :> {$CellContext`dof$$ = 
        3, $CellContext`params$$ = {0.5, 0.5, 0.5}, $CellContext`r1$$ = {0.5, 
         1.5}, $CellContext`r1old$$ = {0.5, 1.5}, $CellContext`r2$$ = {0.5, 
         1.5}, $CellContext`r2old$$ = {0.5, 1.5}, $CellContext`r3$$ = {0.5, 
         1.5}, $CellContext`r3old$$ = {0.5, 
         1.5}, $CellContext`Type$$ = {{"r", "r", "r"}, {0, 1, 1}, {
          Rational[1, 2] Pi, 0, 0}, {2, 0, 0}, {
           TextCell[
            RawBoxes[
             Cell[
              BoxData[
               FormBox[
                SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
            "InlineMath", $CellContext`ExpressionUUID -> 
            "ac4494d5-7022-4b04-9d21-ee2100588f50"], 
           TextCell[
            RawBoxes[
             Cell[
              BoxData[
               FormBox[
                SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
            "InlineMath", $CellContext`ExpressionUUID -> 
            "61f66885-76ae-4040-8977-edc2197c65ae"], 
           TextCell[
            RawBoxes[
             Cell[
              BoxData[
               FormBox[
                SubscriptBox["q", "3"], TraditionalForm]], "InlineMath"]], 
            "InlineMath", $CellContext`ExpressionUUID -> 
            "17945a6d-caf3-4555-ade9-6fdfb1395e36"]}}, $CellContext`TypeOld$$ = \
{{"r", "r", "r"}, {0, 1, 1}, {Rational[1, 2] Pi, 0, 0}, {2, 0, 0}, {
           TextCell[
            RawBoxes[
             Cell[
              BoxData[
               FormBox[
                SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
            "InlineMath", $CellContext`ExpressionUUID -> 
            "ac4494d5-7022-4b04-9d21-ee2100588f50"], 
           TextCell[
            RawBoxes[
             Cell[
              BoxData[
               FormBox[
                SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
            "InlineMath", $CellContext`ExpressionUUID -> 
            "61f66885-76ae-4040-8977-edc2197c65ae"], 
           TextCell[
            RawBoxes[
             Cell[
              BoxData[
               FormBox[
                SubscriptBox["q", "3"], TraditionalForm]], "InlineMath"]], 
            "InlineMath", $CellContext`ExpressionUUID -> 
            "17945a6d-caf3-4555-ade9-6fdfb1395e36"]}}, \
$CellContext`workspace3D$$ = Graphics3D[
          GraphicsComplex[CompressedData["
1:eJzt3FVYVG/YNnykRFrEIEQkJERBBQmBewhRRAERKUFSBEVCUFEBkZYWAREB
aVGR7rrpjiGGjqG7JCzEF97v857/zux8e8/xfFsex29jmHHNrHXOdV6zjpvY
aNwhJSEhuU5OQkK28y/PJP/TrJk1cFPgedTX8kmQU2176exrasysp12MT+g3
oKkyvfCeag55Ocd16VyKVXBYTM1BZ3sR+aCaN+UltyUwMnJAJbNxBfk9a/1E
bMQcYGd00zlr/A25J7PAaeNbUwArMbz2pXQN+bat7YdbPaMgd7h9on1gHbnK
icoYsvP9QFhpsYi9YAP5qmpLf+A9LHjctvdbl9om8tZzeONDXwsBrkLAiy2O
4E3N9rcbi1ZBiQmjLJvhOPgeupr9opIGY8oaOTLotwJMLQ5bMQlPI1+9ovXE
9PoS2KS14OnJnUMeYs8QqXlyHqSnNPNcmllEXiG374BL+jSQnlsW4729gtyZ
YjpuZGYceLzN8Hsauoq89b4fprF2BLjTJTdeffMN+Xbz+5KX2j1g9bT23Wfa
a8iPfc45Sy/TAvDxP/6GDxN8ukU09PVQHhjv2Pz7/cQ6cv3HMhbYT0ug4iRJ
rWE5HkgfzcCkttJi8nr1vk9FL4DbPK8fadFOIO+5F4dPEJ8DvwdC2fewTCPv
ku51j3SZBjnHOX+VDc4iT6aOfGv6fAI45Qv44VQXkHOO81K4iY+CMWcbFv+7
S8gpN30qXj0bAGXTYvRcjCvI/SK5i68Jd4Hz9zIZ6goJzmmaH5B2uQEYnlV6
FH16FXnDFZHEgZpsMHskXF/0AcG/7RtUk3aeBUGlhSUv+wYAR96qtGUWHcYq
9fOrooZpoPvB+bHqZzzyiMZBMo6mScAuqawie24c+dIeoeEs73HQMLfqFvRi
Erno8PqjGMpRwN6U/pHEZxq5Jsly6usPg2BDWzT4hNIscvKNcQ/vnm6gJO6V
4Jozhzy4v17FsA4LdLqVJXxb55E/yPh88Dl5DXhWHt1bH7yA3Npa8EyBTQY4
Pewq0/OD4BXUSU9Ok0wAtSskulr9OEAqYBw/fY8e0/PbQN/+8xignBawuM7X
j9xVQcy7gWUUXNPPLSw8Pox8tefbvELyMHAb6/3xYhiPfGyJSUqkoB/I+W15
4x+PIeebf2le/qgbuHrUntnoGkc+MXFa/O5CO2i9zZRqtT6B/Pqdk5+WNBvB
Te4K/eL2SeRKr2y4FpzLQYOarN+Je1PIj4x8nzZs+gRCpry3m4sJnnTfeGXD
exBcpbQP0hFqA0XqZ9bwG/SYjwqGc89i+8Hc3SJe+48dyK2kNqKWzHoBTlK5
qssZh3zfQ15/g0EckIxurLrF2Is8ndLPvI6+EzSLj1DMavUjt+7UqKagwAJx
ntxL164MIk+4ftfK6HUDYBkl7TacGEI+Fn3h5++6SvA8g257P+cIckvdP6xv
vfJBV4gWxYufBOdgDf05fCceLFHLS22dxyMvKfn2+lkuFvSaXfGNkKsE9zhO
W8ZcZ8B064t9+i3cBta/ZF407KtBbmUgivUNaQaV1z8YCgo1IDfrYBWeiGwA
qrqLac+Fm5FLfG0UMteoBes36b639bUiL/R5amVdVQnWGG463n6NRc64Scn3
8Fsp4Dgcs+j5sh059dlHcc/+5gKNUz3HLaQ6kFdaVWtyBH4BbScO6pcWEpx2
hTstyD8MrAqSvF37Q/CMn14pPwqywNVtXpNOh0SAjaMz3HZiwKzE5MkyP80E
BZGdTx8+T0Ee4dAkTUGaAUiUzhbUiKYin3mHDenJSAU+OQbSNGHpyGPP/5Rk
IPsEVpiy+FouZyIXpYgfVqVMApeflHm6BWchv4w5p3CqLhZY2CYxvo/ORi5S
WfecNzkCBIuFn66xykH+I5Ey+AI+EDgeJGt9+Ps/7uEkYMzrCFz3pD8evZSL
nP/i3lcHP5TCIgrOTymnM+CVa/NjT57sHBeVIw9+JhbDRDHx7VWDbOTlrcoR
YTcKYdFv3omD6nnIc0IfeFzIyoOOTgx6db8LCE49yCnZkA359U41vtUpRo7H
6UeWxWVAiZKtlP2GpchDKXwy3fw+Q6t0blIsFURu/SyXrfBFAow8rKepzF9O
eJwN4/Eyqwi4nel9h+8dwUmyz8gNsrjDZLp9A9E4gqu0s3ndNe+EN6VutV8/
WwMvSiSbs8vtvE8W9vQus3bAx9OHJy/cbkAudDPUTzUcC2PnhFnppVqQqxy4
JBdd1AKZ/Tk7m/WxyEndx4fK5xrgRki67+xMO3I2H8d1xoYaKI93D30m3Yn8
87uZiQiTCrjnbViv2o0u5P22gbbPfAvhVY1v2z8EcMh9g8kYwXY6pNE5q4It
JfhU2IoqHcU7KH4X/CCl60auIMXf+HmuD/B16ZAxXW8G2BaZBlZ2GoxYqpzS
gHAv4OvtD+tfxSLfdpcLSuPvBhO8ea6pnp3IDU/rtvoNdIKbcf1+AwM45Owc
Bxny1dpBoU4i697FHuSn77tEO7C2ALPpPw4pWX3IT2S+FYuQrAPt9rImfHwD
yD/xzrBbMFSAgC6qzvmLg8gfvjZgybudC8zE7ppY0Qwhb3M/JXYpLxYI0em4
Sj4iuE9xck8OWzvQeft0WzmzEpxi2tTQs6fBGCmKTjYutQF8ZR07rWEt8hRe
VRU9uRZggmfyF6ptQH4k8tZvP/FGsHXpzsrCQDPyE/Ymad/7a4H+j5i+oPA2
5MFnDOylzlcB0eGFiuJNLHJ+iTjdz5/KwIxSrWfsznXpn1+14nNh9MoDIfo1
I4ufOpBTkuK8s8+nggWyD9yqQp3IFcF3FzPGcJAseiOD9yHB2bkjRR6fKgcr
rp+u/U7NBOEOa0MPk2kwsT4ig6xny0BX4NdSN75c5AupLB5rn4oBj8vLwxjD
AuScjluFDo0FgF/o53vqK8XIBzmOxmVE5QJ80oJz6nAp8iOXLZ9nCGYB/Xzh
3y+oy5Fj+s3WPm3vPP+AaRfyNYJnDDyuu3s1CfgvFKRSJFcQnudnDqbWr5Eg
Jd/klurxSoJXS9hzDXmAwXfHVXVN/+PLUNHbKRiWK3i33iZzg1x/4OGqnJ2/
m9rFn14TCEnEeBKmAj2Rk2g+I+M/4w9jT/1YM+j2QR4rT+Yfo/AKlt8e17vX
5kd4HJGfUtcpvSBJmuDeT7aByPEFWobP3N0gZnBYeEonmPD43PUfDf2dIV71
2NGBi68Jfhm3/SLnEcREkQle2iJ4ucZfibMi9yEJDbvp3NMQ5K4Bed2sCjch
CfOloxKlBBcqkdQ7pFQN+c8psuafyIM5p162HY3Zeb9NT3dnzFdCZuc/N4vc
ipBb39SLDrxZATNs5cWp/cqQl8w0jpruh7C7Kq30EUsFcv3bPmVvDhZDK7PY
z5b5lQRfUYzGVOdBnqtKyxdZq5FXR/hepD2ZBcmHuAU/y9Qgv4r7O7KZ8Rl2
aZfrbx+tRV6W9WEkKD8WltNdVXQpILjOtAypJ68vTLGoOyhPU4d85UJg3lWO
TsijLqTwarwaamUleTSa0GDumbqTJvW0w1j617Lf/tYjvznQyJ2ig4Uefx/s
e9DSjNxBffgNx9UW+CBbmXHrGBa5j27vyQe+DTDwrFygTnI7chJGZv0jjjVw
86yZ9+/1DuQWQmb7fh+vgIwGZxyq93YhTzn6QMz/ZCGMKX5brTZA8CXdv1ua
3unQ/xjTmz0PcMgDL71vok2JgDm2BqN0xQQ3vTNXfmZoAKY5FL+osmuFx7b4
PT3IaDBO3JPW7Hv6YXB9S3CzeAfyWaOaPwsdPTAopiLu7J8u5HPXFCs8dXHQ
FMspqunagzzhiLZPXlQHPJljUClf2Ie8nC5y6tKVNuhr2Ub9K3YA+UMNCk2d
H/WQkWTu/OMzQ8hD6BKDQ7QrYab6eG/tnWHkPIc8HOt+5kE73jZ3DqkR5Af5
GUTh9zjYEbPZPPyF4KxZ43X0XGMw74fR4+W2TvjmmViM40tqzMMzh4alF/Aw
ol1Lni2vB/n+8Nu0v/aMQJF34ph71weQa10/JR3aMABVP+BuPA0fRk7Fn8Rh
L9kLM921e5+V4pEPFKp5dOt3wYTy1YLhX6PIH31fyFsGWBieVi7u8ncMeVZr
8MajkTr4LYfxOVf9OPLTpFhpvelSuDVtuk9DYwI5KX+zU6nJR8gd9+ydThTB
hUdG3iiyT8ET57912/D3QhJfW3O54X0Y0/ux4c7xE1C7oZE3YHYQebLAFefg
6TEoaCn2K+MjHvmRgpGctRU8PDRjMkyRPIY84BuvRM6rISia93XPM8sJ5O+P
XByT7eqFrs4Z1E4Lk8h/PTl48w62E/o/CjiiJDiN3Hos+vdPthYouZTVtcAx
gzyy5WWE3IVKqF+V0HG/juB+8rFCU+6psFA+61D68VnkgqnR3l+ws9BC08zu
s+ggLPRJPjXGtg9j3HPgXMThGWjL5q44gMcjt+lVv+jANAX5XIWuH344jlz+
mdLz2LpxmKP24/pSzSThcTLSw3VlR+HkaDHjXtw08k8Z81QKfwYh231l49GI
WeQGTHwCV7l6IFdp/Mc5mnnkD74HnFDZxsJ1d07bIIEF5GurBhZKyjVw/fKe
MeUlguN7/JPdkzKg3lblcKr2IvIaddKM3flVxe+9jrvzq18JKmd35zzOSmHt
u/OrMAPpid351T9vrrWw3J1faSZwe+7Or/75Y2pj9d35lYlb5e3d+dU/vzYj
0L87v0ren/pod371z18ttV7dnV8dvvjkSGrpGnIGDtfE3flVOm84fcfAOvJe
MtWHu/Mre33r6d351T83jJp9tDu/ErIo4sCpbSK3+eMxf+BrIcgnc8jYnV/9
cx9uy3bNgG/gJfgVw3NxAoSIRY0o7KHBQLdFKj31VbBySNPDWH4G+TJZ4tN7
hcugmUuwiHJtHvkfE++33x8sgNpP5bUPVJaRT/Z8NqusnwFnhV3vuHOuIqdd
1Ja9hpsA53g+LErf+YZcmi0iaH8sHkiTBYIxmzXkm1eihzKsdr5XJ6U5C8is
I5eTTEiTj2gFSkGLvr6NBJ+KiQg8tJgP1INI9106tIGcnYmuRYVjFXw9EKDD
kTMGqBx8S7P0dq4L2MfXY9eWgfa5FnIbkynkXmynN+suLgJx2z3XDzTOIle+
HDbrfmEObGmPYlTwC8h7ve/rPBieAkeoh+e03y8jp81fW+CSHAcVe95yXPu5
gtwRvxSomjYMqm/uZdNeXkUef/6Y6tNX3UD+TRJ7Reo35BjKEtwPqWbwSSmN
XUx4DTm1CO2L4gN5YL91x4XDjwiuIftyVhqzDEhwwbX+TKNgv+A9G7MoGkzy
1QgtZcFFEPb+WG669QTyezcpRjEFc8CoOPSQuu80ctX8QqbB1WnAnqq3FH1r
DnngpdDWoZEJoCmhNJ3dt4DcuZT643e/UcAYnmaZvbWEvPmOo5fWnkFwVWG7
cOn2CnIyEjZzjZAuMH3jw1onzSryj0kCVnnVDaB/Xs5t2o3gM8sZ0n0ncsCZ
hlG7zEqCp7pmtVMKL4C526YBQk3DYC9Vqt+rGhpMDk7q+vNrO587PxmLaw/G
kDMJVIRMHJkBGoKvhrqlJpFHNH05LhkxufP9Xey8bs00cgY6hjLDzjFw1d4k
rpV8DnnhfEGXUPIIwEwwy3N8m0dOHzIe/1WuDxhuOL89672I3MhSt8A2oAOI
fLuTRQuXkOs19d+Qk6kDD2OZf3eGLSM/yUG17C6UBe5c3hC0I19BTlrL8yCR
aRZsSx43or4zAGJD8gVjcTt5w4ZyCphPA6wVyd8oMzxyX3+XzF82k8Aicolp
bWwMeflG6TvK8+OAsaV5XwrvJHIKVkHW+xAPrG5fqIngnUZ+NHz+1jDfILD9
fWbNYXAGOW7qgscex24wlnX0T4fiHHJWwzgR0ZdY8CDFg0NObx55W9jB4rDU
ajAUc+HBDZYF5CMOrKK/BDNA/fBIctoLgpPn/WCfWZ8EUw1BstZ/eoA+H8PL
R800GKmgN0kVzhOgLOcajjlvEDnWKZZ2tmYMWJho5lH74pHjrydSNHXgAbuB
CFunxxjygo+5zRw3h4BNcGcQ98758J/Th3Zb06T0grGbiokONZPIM/VU7h77
2gl89vduUu2ZRu5smDnFN9MMNll4Tm6tEXzBy3mqmbUS6PWXnVN7P4Oc3N65
g+t6KvilSFG/uk7whbjHXikjY0Br4DTNkQ9dAAqlvzjxmQZThCtlE44eBTeY
SFuo0nqRi4dpsbBw40HvyOpvOptB5F8SGiw5FwZBKunp+9e6R5D/VRD/W3q+
DzQF2Z72sBpFfr6d2bT8DA7canuaP5w3htxVM+xW7ywWkHPOqXLXjiMfvpoy
faCvHgz9kX/xM3wCOQ+Jt+xeTgg+Z+svt5yYRN5ldVRHQTsFTDw55tFvT3BG
fX5KhcIRoPf2nvFnzXbAxJLmeMyGBhPp0JB44f0QSDWNvo4t7ULe2i1bV3Rs
AIxvfBx9OtmD3L1gbihctRfobc0yZ1b0IzenCyvMEcGBn5szXNbKQ8j1LTce
z7e1AzeKU7+yHUaQf+d/0d/4oBmUcHiUaMbgketFvCH7M1oNOBxuePVcG0Ue
suEg9JK/CGwHMkkW1BJc+JZu0rhDIkicahF5uncM+f+2eU65y0tjC/NOSPGd
z1TjbA108HlhvDvXetecl7jC2gHXuZNzpW83IPe6anFRLRwLlcMjeBikWpBz
RPh5xxS1QP2hiw9b9LHIOXXJVirmGmDOO1XM3Ew78oXMjOb9DTVwYqtQ/7l0
J3KWQ4Eu70wq4DOlE+nqN7qQT3MVHHvuWwgduPRGfgrgkEuS7DfYndeN0RaL
tJcSnP10QRkNxTuYuc08TEbXjby4ifWy2HwvrHSqvL0u2QSP1XjzcvbQY0yo
Sb0eyvXAD0YcsV1XsMgdVA5cVlDFQXipq0S2pgM5ObdZZNvhTqh+UcLzDjkO
+fg5vrkDSVgoOxfKfGZPD/JkCd2lsN9NMLJpz95bub3I1Wx/9X7WrIUlj/iU
3dj6kXd7SfXL3i2HMhWkzNLCA8g/KI3H7ffPgbffUo0cXyT4lLXWc//9sbBR
J559UGMQufCx8yqNzMPQlkn594Y1Fn69y7b1TJseMxFQxtjJNAiDmTNppz93
IjcM+HtJsqAPap7mzIEp3ciPHUt4cZ+2B94X//6LzrAP+dwwy8few10w0c23
Qa5mAPlPdg1r3PjO++QU60pd5xDy8HWW2D/rjTCERXU/l8cI8vftKZdEW6pg
Yrbua2cLPHLBn552zS0FEHsDc5x9gODO972Wyu8kQNOZkm63I6PIv508af3A
chSmPZllFaPshFtSf6ZgFR2m9KEMyxLAw0DXd75fq7uR09Cr58rVDsGantn7
Chf6kbPmjk8mpfVDW1Wl0L2GQ8i1u3T2N8v3wJVKOkGqA3jktUokLi8DOqH5
tSfGzQqjyO2mzOM2Rdsg+U9fcXrZMeTVkheETBdqoUpZl3/+NsGf134IGckr
geHRQywW7uPIRRQiI2MdkmHfzMdlp0aCpxYIr573G4dYmuter/RwsG3igkbF
UTpMRsDhG1dkxqD6gO2GQWAf8n79jJi6XDwUd8D5udkNIecQFu2lWBuC7uR2
Gb3P8cjZZjUbF3fel+SFiRST1GPIXUTLV82/4eA938PGv4zGkf+gZ7MJc2+H
HjP6BfinE8i96pLVV4oa4OW4P0LHNSaRM8wcPPP0aDkMjfXeYpwmuLnuk1p3
0U/QWvPMQt/5KeQpZq0vKC0nIVO1/vMIs53jcDb8ht9dWoydotK+87/H4eSH
AdKTfweQP3Ggfq+mOgbpbaFnKiceeZ2Qq270XTy8JqOl1ks/htz81olaEDkI
c65fSSapHkd+rvhR4SZ1L7wgeUizQmoSedyVe+vVwp3wCru8wHPrKeRf339i
/CvcDI+92g41uTWNPKdrPujRxwpomBQV67dNcHExBtaSqC/wFBWDwNrNGeRb
VSavvm9NQYZwmrzVo33QSKD9nEECDebQopneGYdJqGxZSGvEMIw8ai78V9rH
cTgjbca2pj6K3Ls8Uo8jehQmpLkp+5qOI6///iV9LHcYSim81Z+TmERu+7y7
NgS78zmvJzFXbppC/vlD4Z+Kd13QtsqGLPPgDPJxu6ybLQqtMGu4bR898yzy
rUj6Pz7rlXBDpt1Eporg4UX9lBlJX+GsiP36weNzyCVDzPPo5Wegi6FjNffL
fji1fVcrvpMa00qjTReeMAXvMl6687RgBLk5w2P1hNIJ+Hfws17XxTHkycvu
FaLhY/CCaNBm0aMJ5DHnC4wPi+LhjS9U9VR3ppDzTm2sxssMQN8sJ7J4uhnk
ukFqKQd3rivpDkosV+7NIj9Eb3ewebkNvjt/Rjbn8RzyqTsOw29JquGz/NXt
YOF55MYzD2u/XU6HDB8rsiNCCG6vR5qgLDq7c3lROd/yagCGh2yFzf3ahxHW
YgPJntNQLeWMQ9QzPPJD09XU0H8SqljqcqhsjSG3KBbkbFUbhweaWD9SyU8i
n7xqbTmBw0OMgKJ+iuI08n1+y/HJ1wahtXT/w/Y/M8g51rYF+MO7oQrDJuMB
iznkTKtOZHpvsbCeRvv5hss8cvlLm/gb9dVwZcFKhAosIL+YX3k9FZMB735U
r15IIvj/P7/6f+Y8LIdtAnf3lEr3f2Lf3VO6KHM1aXef5xCeMm93n0ciUil5
d58n/EKi+O7ei1Jgpvfu3svPl1ouu3svg8nWErv7Ic2LLuG7+yF6P8ZYdvdD
XtOci9vdozA2Svu+u0fRhlel292j+NJMtb67b/AOx2W0u2+w3/UX6e6+QXlO
yr3dXl5TiPfnZkEWkJ44LdK+k4v3m5b93/465d0q+5EPpdC6fKXh0+kMeLzJ
dPT/9tREciOx+VUqxQHfCpUVMDt5oZu5dRSUPJdyX2KjxZz+9YDi8dd5wJwf
yh8lMww+rLrPGjDSYeKwYzaUVlNg4nyES+iLXtDhkaEZOECH4Tq3eqZ5HA9s
OmMXzjl0AMFwWcz2M3qMOUZL2/lqN/hAItR4zKAemBsbXclb2MlRejLmmwdr
wMRFO4meffmAu+iypZowAwbTVOhN7hwGY5ean9g+84ZmaqU633ee/2PGp14l
Vo3Q59HsZeGREjj8Yu2MPQ/x3EhsfmXLG2DK/34RSH0Eiu5CeFB9hlUs1oAW
4yv9YRFEzYDt+9386eP9IKythsJdkg6zefAmTaLaOPixX+v6QxYcuM9QspY4
T4dRVvKtOuY/ANJqOcKFWVrBlGB4eKcdPUbiiIbg9nEscOO+Z2ofWwEazUmU
7zbSY4IP3c3MOZAFZu6Xc8mVJ4DHyZdMLq/TY6h+Le2Pf1cCe5Mm2D6CdHic
/Nqnb0v0GFEcTuBkcQd0xzDESj6phj4lvzceFxHPjcTmVy13y1S+vZoDRmQV
Pix/B0ERg/71Gi9aDP7l/ldDLyYB86XQ3/7OPeC0XnviO2U6zC8KgeLpM3gw
Hlct3J3ZDvgSD1x5Nka38/3IKzzWGAdCZvWF3fXrgLLI+TAPtZ28asR6CS9e
DZjFq46JHM4DR2/8lTMKosfEVt75lREZCjE/q6z43L2g6K2eLIU4eszmIcHu
e1oN0OI2n+fh9GLYjLn5YNODHvORf1WofKIHRs8Mi59ha4RHDIOa26WI50Zi
8yuuryLTQQ+mwZNhl1vfI/rAjci7KwPhtJgxu0mJ4yZj4E83af/KwS4wt8T/
2xqzk/fGrymrS/aDZPO6H3+Tm4Hvp+zLJ8t38oyV2zHe/FYQbtw18LGuHESx
iwU6M9NjXKVEYwzFMgGeLEzuZks86N8bFHJLmh4TkbjdOyJZDDEH3zdch1/h
8NuRx5fE6TGrt0WsG7vaobq2qZ778yqoNz6jcJ1i5zxgvL8MGz8Ij2Yfe5Ku
3AbNz9rlH/pIPDcSm19xR5O/7VSbAPrWi5K3znQDemGolBlAiwktrlWsejcM
rGfikiPzseDaev3PYxx0mO3fPvLy57rA52HZJB6mWjAUliXH40mHoRJw+7jO
UgWo6DMkGodzgF579bBjBR0mdrJqRe3JG4i5qpndqOgJPRXfahl20GEkfuZ+
Y1mpg/h+K+YIviJoJWV/LT6fDvP5rfrf/pBuiD2e+G7P53qYktd4OtyODmMh
Qzd2xh4P0x98PyH0sx1KDKZzHdlDPDcSm1/RvnIjCZUaBS0v/CkpOjvAQ1HD
iyb3aTHNnXtkc9/0guHLxT91+xtBLL7u25FxWox+zn46zYZmIEHl2VRlD0Hk
FfmQY2fpMPwcH3JyOTMAP8WmG+2HOEBO3xLvpE2HwVyKoeL3KIQ+2Qdo949+
gQemBU8VadBhNMWe+x7Nw8Il8SfKV+QrYRHdTYlp7p3PRaYuhXV3Pzxwxd72
s0YLdDLaF/m2aSevzn6x3XN9DK7ykrFfIOuCeoLUPGNXiOdGYvMrdUHMLxab
QRD/JLjw9P42UMN5W/aWyM7jZO4hPVrTATSWZKUOu1UDgzBtBeZgWowR6wgo
ghWgYEut64VZNrjofUWLu37n814mYfj0UAjEPOCyu1TlDkMyjJ729NBiGMOC
TojJ1sJqaz9yz2sF0KSI9sjfsp0cruoRnPGzC541UuJcm6uFgflvEmacaTEH
9r3logsZhlcNLp4yy8bC6DTT98YHaTGFlzHPvk3s5PLvmVqhJTjYyHmRPMeL
eG4kNr96SYUpexnbDZiOMv+aL6gHbrjFQK0hGgxJI/Z8rGsj4Gm/7Dy3XgKo
Ogz3Hjmx87pa4qYwWV8Bia6x8nHJWNAlMCzkcpkWE8GkFoV3zofqNSp9jZ8/
Qby7vBm7Ii1mvS7u7K2aVhg/wqx7v60cipqpuJOw0GJgcsVXtZ3v/zlYA+r9
fxthOsVSYlYdDWZ4fCTy+yAeKguv3y216oC0STHPz12n2TmPKX7cJzgJ9Ss4
7tQe64FqmwMnfuYRz43E5lfE9naI7rcQ2QMhti9BbK+AWP9OrKcm1ucSy43E
erSi3hLjoKE8kJueyPPjxDrKV0sZdkV9NdnAu0P36e5e8b98NWh5OzTPJgOk
llwx3d2//ZevqF5aqxk0fQKcnvZ0LcVTKF9p4Bf2Dt2JB6kp+0R29zn/5Ss1
nMvxAP8wcEAu4Nru3uO/fJUR0mWpz+sIaOGLpyOXclG+KlAVGh1gcYe2c/wK
H3DlKF8Rm18R69FGLEU0RMlzwaNAB+16z28oXzXbHnL83psJrt7quB39aBnl
KxmR+wGVc6ngULEdTstpFuWrh6ff9CnXJ4FjnRa3350dR/lKsPPexKHKKFB8
9Ha8i04fylcTr511o874gd5r7n6vvtehfIWfFc70yLsF8SfPeaRaRaB8dax7
EXuC+jXMuJdBsX9fK8pXxOZXxHq0MgEcg8bhbBA26x5g0rmC8tVR2QFNxdJ0
kK/7E8v+cx7lq49DQYfTp1MA8767J7jXJlG+yssfUHCsigOxD9n/FqiNoHyF
rf28rN0aCkLdQ1jVtTpQvkoJ72e/oP4EZOx/5kTln4PyVbkeTkvezg3mTNLd
2CtXjvIV1nOMSpQ/AloH3akwuoJD+YrY/IpYj0aiI1S5xb7z+Ww/HaAouITy
lff832GRoi8g5+PS8VmbGZSv5sx7Xw6NJYJLNv6XzC3GUL5iNv0ecubNe+DY
q/7E91ovylfMly4ynzngC4Qqh6dZf9eifGX0mjEkLkQPkhTf4LK6/xblK1oD
Q9M0rWCIf8ePzW9uRvnqp4v8chQ2Bt7bK6Phyz+A8hWx+RWxHs3CwJl+6nwa
eH2MZ5gxbg7lq2aHy8qGuR/BFB2zgtDkBMpXZUaOe7dnYoGZ4rW9Ma1DKF81
AptTD1vfgF+5X7F1Du0oXzF2nvzocuYxwLqnXdEIykb5yt/uZLx14EvIr91I
Kv2tDOUraS2ofPhvOFypnnf0VOxC+erc4Vzu00/i4SmPffJACY/yFbH5FbEe
7etUjSPz2c+g0lP5741D0yhfRVvPtL4oTgAdmNN/mc1GUb4SibH4JE0XCSaO
fzYa2Ogm5Csu4ciH8T4gY75z+GheDcpX+LT3Y72suhATwlYmxRmO8tWC6bw1
ZiQQhrIxc9uJNaF8lUwxHdfNHg1Haio+P03pI+SrUyFq+8WTYMVyiGxU1hjK
V8TmV8R6NBf8nW1RiWSQVmBaLB02jvLVSuFlCUzWB8BjWKh9d3kA5at7y4wN
ZMEhoNrY/AWXPhblKx1PP82MZgdAYvOwql8zC+UrUYsDs0IOrtCneuJbDkkp
yldGxtFBhgFhMI/eqCp1uwPlK+p3WCdV1Ti4N7or4dzLYZSvpPYJJih/+Ahd
E2yf57ZPoHxFbH5FrEfra41Twd2MBw/4mCwG+PEoXwnRrGrvFYsApB8ePU67
iUP5yvGFBNfiuBfQZMh6GSpUjfJVrHP7gZYb2pCk+uvfU0GhKF9R1Zm8+5Lu
DznNOIbkLzegfNUaoD3NS/ce4icflpj39KB81e9ggfXKS4AKpPX02YajKF9F
Bm4EyaR8gtLNXUq9UVMoXxGbXxHr0b4tzefZnY4GRYH4pT/5fShfPbu1dezX
odeg6xXWJvVYK8pXEqyrn8+fsAcZspTlbvgMlK8Yj75JWlFxgQUOCpeisotQ
vloKus8T1fMGSuyLvzvg1I7yle1F78DwoQ/Q4Nrp8+8ODqJ8JfXrXOkzmAS3
BYX6cYLjKF+ZlASouap8gcby3JS36GdQviI2vyLWoxHbHya2Z0tsH5XY3iax
/UZie4DE9uWI7ZURm1/RCOKfxuycf5Vy2uc2jwyB406aucVYGsydCHX6d/0z
QIikz9PRchQ5RbikAVvNFGi8RKPoxDCBXFaAsqrw7gTQOlGwV/DxFPLHzyvD
5htGQT7rAc7FsBnkSnzsvNPTQyBUl7KK0WIOeeRW4Wu/oR6Qr+kpiZuaR05K
Uv6l27sdWHSzU7rRLSJ32avK+3qjBpDrP2x26CX4YMzBcIxQJlC6kNgodHEJ
ecbNx6pgeBJQUtDLpU70gLtfT3VZh9FiuARU/r63mADmdtJu9hGDyPM26jRe
ZY2BsS5D/p+OeOQp3Sqm1hV4QHsvyD/Pbgx5ZFmkTt/5IWA07aEtITKB/Mqd
C7ffBfYCtzGy1q20SeSxyl8Z2yM6QQnvu4cqs1PI3+OUpjQam0EgNeuiwsA0
8g236sqjVJVARCj/0tzLGeTWop6ps+dSwdbrbT2RAYL/nRBnw+HwgHy7oGrV
rAOce9vIcnfnvLrHfIDX0GoE2KexeX0l60bu3VlEPdwwCKZzFQ/M6Pch99Pm
cHRs7gNdJ3VX6M0Hkaeyqf6Mce4GaaYkj/wPjiDfwJ9UrJruAF7Zh/faZ+KR
/0hWfmEt1ApuRUS8vJ8wivzcoQ/+TIK1wO35uZxsgzHkludFdRyrioEXJvn5
3yGCG1m0n7iUlwSebHLWF3KNE56POI+1t0gPIHWi/H1JqwFojcUt5STRYZRU
QuXVE3GA0dPtU7NGK3KuDssf1Z2dwFMkl7fxSTvyWTk7mdjqdvDi4gmbvUGd
yDvWbtdV/2oF/NUuah4WOORlB2xyn1I3ApM2gUHL7W7kQ4pRa5t11SCKyex1
nkov4e/2Sfqme5eB5ChlTtWrfciT+o/y0R3LBpF+UpaUfwhuXmIcJjwTDaKW
o55m3upH7n/rI/+t3nqQUkx+4xZvMaA3yL+0sEiHCb1052W/cB3gcpWUeilb
jhxjHzbHI1sDpBMvGtPkVyLfmtmoM9hXBTJe5+57sV6NXMQoV8Ypohzom3xI
bViuRe6xqHTn3lQxsBoxT/oUX488Nctjs0YhD0Q99vhWsbcR+Yr4kQ2cXgbQ
UbXh+cLahDyCu+S0l1Mi0D8kenFPL8FVPR7eN6MIBhOHO/ytZZqR490fsGXt
iwDlmFskQQq+oBvzm1uAeifHVsbZblmHA3ybNnOtdRByjIl09cOAUMDoxVtt
yBaCnNNZ/j7F/RBQPljldEYplPA4hp3v7OiDAafSd1eSM+HIY/V9JXS2fUF5
+8vzEPcWefmQ6Ls3vp6AU9FWQkb4HeFxcgOlIpJeANdowUJxyUjk+EmbAI4O
e2DkaLJpvkJwEoPyUHkSQ4BfuBPqo/EeOfOnIon7I5Ww2vk4Iy4mB7r58ekz
/aLDtFLbTm5EVcDY87cTCr4WINecP5I+wVUOMdptsZnWJciDR+86Vs6XwMsr
3KRTOIjcB+fDYnW+EPqnng9XsapAvvL0i9/zM7mQfUlyKS6vEnn988b7cbMZ
MGMmkHm8pgp5xgWxs1W9KbB+MzQmL7waOVWoD2/8sQ+QmcLtwqsTNcjLxa5f
XtfygdiLe7pD7Qn+qqP89gX5LpjB513gylULi+KkSvpLd3Lg1FrR4ZkO6DVd
Wpju3Ih8qepgqbxGO5SuvqhywbMVeXnSRcvnNG1wypJNRexQO/IMFkWxB4xN
0ONu7ov+7A7kdqpnlRbgzt+bbrxsf6gLeaXuZtNbvkpY8HiJJVYSh5zDH+c4
/qUIlnG+T2hg6SY8n8rf1CY5GbDrYqaAUA7Bk0Vij/lzRkJ+s3UZPGUPcjMm
36XGrmFICz+L9PdgoZjPIVOMFh2G8TWHAFntIFSdoe+z4e9C7q9AfcRUvx/i
vGuhkWQPcqPe6EKfDz3Q/1pb4jZJP/Kcat32IxFdUPd3Y73b40HkUreyohPU
2qHgtFe8QMgw8qju/Kt0cU2Q/gpDZtIJPPKrC1PkESrV8HWs4DOJdoLL2t/6
ftSqEJ6Sm47LUhpFrnimxCB4NAFqdeIwcZ4ET+Znct/jPgHfar+3V7Lshgz0
4J5vNS2mURzL68czDtlvhBww7ehHXqbRGZL4ahQW3LjQi50aRm4oViFaZj0C
42iLOXqujiKvPJFWsmE+ACPMsePJy2PIa32iND9R9cCBGwx2X7UmkDsnChYl
GHfA+t7Hss7Ok8j5G3/5Ce5pgv2/+F/aG00h5zzce6hpthxuYfmSq7YILs1p
9PWgw2fIXVzM76o+jZyqRrhxgH8UzL1uK0mBHeDD7YDGc3n0mFD1tK9fyPCA
ur2xR9a2G3nalZ/YHu0hkIhT/L7V14f8BWtCYLZKP+j1nXVi/zaIfIlC5uur
hW4gpFn6vDdtBLmHqpfZX8VOQBvBdLmWbBQ5SWb/kHRFK5Bcesps8YvgIdge
ceO3tcBC5suGUsEY8kXDcgazqyVAIn3cnubCOHJSI9dKvePJwPVlZuaRlwRn
O+CgzHVwJz856QuPpbWCsz553F8K6TGD9GnC18X6geCca7Chfgfya04U3BWk
vWAxhQl7kw+HPPvFBWUTDxz4+PGat1t6D/IOwf2czJUdwPmpsOL0Qh/y9aYq
BZZXbeC+02yaXe8A8mkKdt+ngg2A1ylkaL/VEHKVWGa9O06VQK1k/klL/DDy
xtia1Gm+fFCR1GTd5jiCnH78dVQ7WzwYOB100GGe4LI+Yh+yRnCAP9p7XW6q
DnBnzeZMJtBjjn2o9t607AKkY350upHNyA83TjTXZneA4yMOA4zPsMh7BjMn
fCuxQMr8T+Hyww7kjtFP7jRItICyeg+uoDNdyN3USLWlguvBNXhtyyoDh7z/
1xj2aGQVcJEpfYqd60aeJWhZfqG5BDwe7ibpG+xBfnBDZFltXxZQND9m6eve
i7x6f75/vVgU8J2WtegeJLjT+5pECt82oOMdUTIgVQGoL3dL8T+ix6TO8Jnz
2rSABWkGx8WYauTVNE2DWt8bgW+8uPZERR3ytA6lFjPBelAZHXe2710jcrfX
vxpO7K8B1crXctqPtiDXM7h7+U5uBeDZeyBj8GIbclYZEqdHCiWgi9T6fZgn
FvnDhhmVicoc0MFRTn1AtB25y1isecCez6D1Z24pSCX4aRYt0wdaocCJ79Pl
3d+Z/nOSAzlamRlVwKn5j4WTey4glba8RHmaHpPTqGWoaFQJrH7XTRvCQuQR
5wLHO3DlIKVaqvPHp1LkVFSlzx2EyoBIaeegw/ty5Iyj/ef+XisCjBQVD1xO
VSJnH39N58yeBzSrl+u4vaqQx9KV6J9MygRC/tgGnrhq5M0XKUouLXwCVF8k
Vsie1xAe5/cRMyPpWMBjf//W/v21yLdcyu/1mb0CjobjV+yNCW4xXf0mPjMD
MFJ3sd2RiQdXZsdsjNp2rpvbyRRt/OlghiJbVacrCXmszDWBH36p4Iejbqnh
iU/IXetq5EnCPgH8e5aUv/ypyBmpgl4rXEsGwUkipPkdach/fOnQOgrjweXf
5YWBARnI1TmfXwycjwYSOlTfXJwzka+4z9fd+xUGeo1yYs6fz0JuxK2SWOvt
B6hCGQKscwn+w4FFL8L7EZgxEGMO+0lwErcfHBqtIRD/ZzvmhL8HjMLXehvr
7eTM0wFLJAGvYaxd4F/9tz4ET1WTVmIOhhgAjJc1/ZG7cn2dC2z3hxj3p8/o
C4OQc97aGx167BU0enw8z03/NeFxPt6QvczhCY365+u+JoUgLy++t6Da7wpj
h1ZO/8x+g5zk4UYQptIRup556MPgEUpwFfYyGzIbaETxvdfwQBhyI10ezDRG
B2L6/JN5jQjOH7RZmutbAPmLJ/k9sZ/h1DWz1WfYne+hdZTQdDoPbg0fpy2l
yUD+41HLu4rfOVBTWM4k0CkLuc9TDveRliyI7RUY35Odg9x29Gl/jFkGdAw/
z8WWkoe8YHFhVCLoC8R8kqNKvVmAnEqwhOTMdhK00MrQssgrRM7+vVi6kC8W
ijjZGW/WFCEXSeH/8UI6FPqsHGvaci1GrjNY7pCU/AIW3J4/fGqS4IPnaF8G
XqmB9XePnFRVyIeJBe8YjM7RYrZ+lcfWDldB8k635zd6ipF7xHwIkhethCTx
xTSF+8qRs0tuzSmql0PNr1fCeDcqkNMqUwb42ZXA4NiB1pGPVchtDa0+wpl8
GLWX+bQfew3ykvdbDE+Ys2FojsLhINVa5Dqv39OO7U2FPtYXjHil6gh/l8ph
82ZgHEzty5cyHyS4rSv1R9tHftAqvcLo+rl65FHVefZzV1tgAX/RYPWecjgj
dn6axJMG00Xbp3BFpwmGiLz9KsFfhTzRatFBk64BykseTBVhqkXeO3lIVMm+
FrYWFCQ8zatHfiKy7K5WSBWkdjxlUkLbhHw9Saz8vf1OLvKPoo2ga0E+8eY1
DV6uCHr4gPuFWa3If3iU/olTzoZWxomjS5ttyF3fqsj94U+BVwuv3GO/j0Uu
WEtKMTwRAr8NLfPMJxO8Uu5mZZ37DDg3S2X78xDhd3yycVR6rV2TIKvvnQEl
ySrqByc/txdM48eAdQ97fYv5IuoH376qH2o2GQFX8Cv4ItsZ1A8Oqns8yYrt
BZJx99adT42jfjCcDER2B7eDZjk6bstbA6gfbO8IAtRvqkF28u/xxxRY1A/q
zNL319F9BBgvL+XStUzUD1rxMt3IlM6CZkOaA/R3S1A/ONe/Iaw7XgdHGFY/
0ER2oH5wMWopmK53Cpj8eVsz1UD4fR/5C5qTg4wTYEr4lZMwZhn1g+T6Efbq
1KPg3vc9k1Ov5lA/6NMmmLn8fgDA3qCrXS8mUT8os1p++UBzF6h/TFOLP4NH
/SA7MPvG+qgJaFKR3n1rjEP9oGPzM4fSzUIQMeH6tke8mrB/ZX90Iv6RL8RE
6nCmRIaifnCTzuZxv2I5bP3gt2ao1YD6wZKur8p/0lqhiw/XbO4EYa8++9mq
MRvzJIgvlj/xw5LwOz4ukY5JRakxMKZm/e6y6QLqB3/+CSmqujYM8Mux6iTv
plE/OGNz1029rgeopolwiHuMoX5QnC6Hr3EDC1hzaLDLlv2oH/Tt2P/AZqgK
9H8MiD6y0or6QazGyM0r8snAgv3O0KWbmagfnEhU28x/nwmrfXoP3LIqRv1g
416+YBfGOuhx+bt8/0Y76gfptkbf83l2QGsJn/OzPYOoH+w4WcDuITMO5N/e
s9z3fRH1g2w33NW6tfBA+zIdhcnDWdQP6se0+fE87Qe2NXJcptETqB8c89Mz
3rvaCfi4tVyj6EdQP9j99WDjzI1GkPcCm2/m2oX6QSq3iGClyQLgdB2uT5hU
EfavLMYer91+BTkPPypMrHiD+kGPCChAcg7CWDE1djeVetQP8p6UccHeb4F2
P/SD6Wa7UT+oyd8bHGOLg2mjyzdJswh79Y2qll4H9UZBftA+HBcl4Xd8YUoK
tnR9g2DqaFp39eMp1A/eKKq8NxDfDXL5pRWfxY6ifvDtzcyYM7xYULbEarGf
tw/1gxrs6knss5WA/FNHbZh9C+oHqQ6/W/QUSQIZmJKhI64ZqB8swFyNP1GV
ATH83DeY2IpQPxi3vEHasV4D4ZWAW+tH2lE/OFjBLGoY1w7vuOdQCDwaQP0g
azBX+BfGXnhlW+SRVilhr35dhmHYb2QYKN35nOxJTfgdX6DJebVq0j4g+DLl
x8DTcdQP7vNgSaHo6QB7jsrYWu58L/zXDyayRB95O1IP/Kuy3zy53Yn6QXb5
rgwxz3yAf7KKKVWvRP0giRzbpMAPb8jpfYJd7nMI6gcdvR1dGp+WQvJ1/erl
tVrUD4peHluATU0wAzeBZUjCoX4wXsBQ0kquC0aWnFds9xtB/eClmNwecoN+
OONonuXkPYH6wQVd0lL5fQPAtz6xPoqO8Ds+RaEA/6XzOLA55Wt1yBOP+kHf
xw9OAoVW0LyKe98W3IP6QXOOfJWDhRVg66ZX3KHIJtQPrkTV/+ofSACxrFJF
8YtpqB8sN/hwRuN+OvQxjnUyeFKA+sGrUfwCnW+q4Rq5l/YBSizqB0f8in8v
eGLhFP1dhoLiPtQPNot/e1YT0A0XetVH6INHUT9oQXm21kJ1EKqcvBncSk3Y
n0/g1v1lJdMDtE57S/ccIfyOz8VU8JnPy3aQH59G/kZhAPWDl5973y5lrwOy
upM6pn/bUT/Io7xtOtafCwrMuBkqyStQP4ipBJtZF70gvmHKeFn7NaEfNEpZ
8DAthilKf1Mmb9SgfrD/fum5xc4GKChLFV7M3oX6QZpIVRmljg7ITd7xilx9
CPWDyx+sLQVe9EI1P1BqdGAc9YMjjZ/+5gwPQfwG01lP+WnUDw7rbU5T+nSC
J2c+JjLfH0b9YFmTNV/QYBMQoC6rOJGFQ/2gw8s2RZbr5QBTcffYQ8sG1A/W
kycyOF+OB64HjNofPE1F/SBeQ+4Oz2IqlBju+WrplYf6wcC/M66Dq5WQqZDs
xPmXLagfFPEZz/tg1QrNz9kzUmX2oH5QiLTp5OeqLuiQ08OleRKP+kEzs6n4
Rwz9cOzmn9uHVSZQP9hRn/P1J9UIfL1/2qZekLA/71JESeY/2wYWI/KMU/p6
UT+41J5HN89YAwJe+zs63SXcv0i05OHXwPlsUF8pse8wJUT9YPkjMaO5qx6Q
hNHuTn50EOoHnS43XqrdLNh5367c8LStQv3gGBsDeZBvHaxqtwqY1iTcX0VC
aKkhm68dCkaoN3/J70f9YHyt/J3NG92Q6kVYNbUp4X4d2iar6kWSAzDUrj1K
7xzhvhbf5tPYXjWMwJial7+KymdQP5jwrvLtbr4qODimupuv/t2HU/aCsNhu
vmqjnMHs5qt/96t8rMIXtZuv6kkc3+/mq3/3dVRv3y7YzVeT8irVu/nq3/0P
86fe39vNV/yOBnO7+erffQLD+xI+7uYrOb1Z8d189e9+egyJfa9389WFdAae
JxSE+9pxkuaTN+7kK5/rtj/L1gj3hWMW0WtL38lXV4ddXtDu5Kt/9zFTtOaS
0tnJV3c5PPDUO/nq3/2+JO7OvHokNg3ybMg4Epi+gYUbto4Kp2kwe/F7T86Y
TABmHqlL3+EyYGWZGLW7RouppCuw+6M2Cq7uNzS4tzkHSE1ni+sv0u3kT4vc
TtpBULnfs7+kZxKwWKlsiO9cdzokhObZj+MAybbMyBEHPJqbJcubvjCabAIM
RkeCyAtxIGQio61EgAHjNFVxbsaoCHzO9Wpw960Gj+YmZM0f7OQrxfgLDaM7
+crDPnxkKRQyJpx11HNlwDBdHVM29C6HuZG1tT9KG2DJ1/UEAxOG/3XvW2Lf
C4jlZ2I5k1geI5ZbiF3fiV0HiV0viJ1XiR1HYt8XiOVqYvmTWE4jlmeIXfeJ
XR+JXUeInW+JHV9ic3Vi82dic1pi80xicz9i8zFicyRi8xZicwli39+JzduJ
zaWJzW+JzTmJzQOJzc2IzZeIzWGIzSuIfa8/dTjM1PIOHnCyu3Qxv5pF+xt1
xr4btxb7AeOTgNSx74T9CkMScc7Roi5QRlXe5MRD2DdgahjUnwpoBrEkNSpA
mtDLS3dGfq19WwZKzD4ZxXfUoX65PPGXneq7AFAe3qH2jJbQ82ZMbTkfl8mH
weshDlFHCH2oYk3Md8pv9ZCc7PFGh1Un6vVimhccDX53QFfLoYtkwUOo/9Kx
/CbfINcH7b7YVC19Hkc9EbHrQrSpjjfPlXaQgHlDr7bzuv8d3ynNVSHe5Uaw
yNyeq/ST0AtwPBPRX71XBRLNbCfqZVoJ72eRwZB22QIgkcxDrzRKmDPPtDEO
qAYmgoKe/W/q9hDmrnicdv0Bfm9oVFnyRJyKMJ+MCMioEZRIgxGMJyZP++Sj
45vzBBq5ihfB3qeVny7eqybMtfgnqFmoK2Fyl82I/JFmdHz/t73eVKZ70bt9
36spKUrwn74vUSCvYPc8pnazYeu/57FDrufydntAcc2Ug3/+0wMOxiQ77faA
3U8xAf/tAdnpvybv9oBiA06u/+0Bz2WF3N/tAbPzDyj9twdco/KllqloBRFD
WQz/7QHPL5l67vaAeHz78n97QGfmtqXdHjCwrNL6vz3gkpIMlcHOeSnc7mj6
f89LwWRnDDqj8KC8nsKGJbYLuZRo4UxR8TCQ8Brm+HK6F/mFgOTK2sEBsMrA
eXvcdgB5bYldbn9SL9DS0Nd21B9G3uiZAj7ptoMPR0w3Sb0Iz7+LweHWYF4j
KM/LiH1nRHj+5mvflOjNKwFO8XCx4B7C8+Tx3GzXyMsGJpodGsV3CE5sXkrs
/Pw/fY5KbN+P2H7g//Q9QGL7BsT2E/6n7yEQ23skdnyJ7Q0S268jtodGbF+L
2F4Tsf0fYnsyxI4XsX1IYu8HYvuExPbuiO2nEdvjIrbvRGwviNj+DLHje/nh
+gFSqiJgkdDqHSedBVQzHg+H7Nm5DlYc/RI2UQzqZTSiu5+mIQ/25Xgr25cP
utJfwyrSXOShffCWsGEuIHFRmwzyykfulCOqkByXBWaGFnUOlhciL5i6Yheq
kQ50XutuXUspRl6f8BN+304BRu9jjEQlS5HP9P5dt9GPByK+4fXdD8oIz+fV
3mcjf8OBhA1fjbk8RG4RQX3afeslCDZvlLyYQ/D/ba+XWO4llpP/p+dhYjmK
2Osl9jjE3g/E/p8n/t/zQ/nDvsGKsqL/z/5/ADZ7cbQ=
           "], {{{
              EdgeForm[], Lighting -> Automatic, 
              RGBColor[0.87, 0.94, 1], 
              Opacity[0.5], 
              GraphicsGroup[{
                Polygon[CompressedData["
1:eJxNlGtfjUEUxZ8acr8VIpVSTq7dRIhKUrrTiRSR8ppPoaRyy+UL+HDud6V3
1vrN//mdXqyzz8yeWXvPzFpP5dSj4Yf5SZK8F4KwVcgTPFfMeJuwnfw6xvmM
H+vnubBe/3eQ9/99a/btFbbAtUsoEDYI+8l7XyXrXPMQ0RwHhJ3sKyUWCoeF
EvK7hU3CZmFGvbwIkbNcKCJfxj6Pq+B2/QphD7Wd2whXIT16PCu+lyGurxOq
qV9PzAgNxBrhJGdwnSfa9yrE/yNCj9ALfxlnWlH+u3Be///x39yXhDNCs3Cc
Xn1PR9hbTr1Sxu3CafZkuBvnTrDPPc2JeynEHluFU0KT0Eb0/mPCQeoVUcf8
jdRz/cvCWeGcMC3cEsaEq8JFuLuEFuGCcCWJ52vhDlqpuaBe3gp9cAwIg8KU
MMrcJt4wfZ+iNe9TiCY66cU15sX3JsT7M18H/Wbpz/Vv8A59xDbub5C1ndTv
h6OPvDmHyPtM14hdvN2PEM9oH1iD1re1b31ZZ/fQgeuPw2UO+8l+sUfuCNeF
YWFRfO9CXP9U8XWIergv3KTHB9y9+fLgcM0C7sb+ucv5R1g7SG33Z12nfnVM
fV9M3xP0kk1yPrS2ZvFaCTU85/8BXvPN8H1wflXxZ4h68F1UJFFn1uPXEPWU
+r8SLvvLfnuCB6upkyFnDnvjGL1Xsd96tU5rqOH8UXhPsMfrTjKew6e1nLkS
7kX06bteVvwWos5K4c4kOf/X06u/EbVJ7htRx9stcUbva2TPL819DLG3XnTh
N1pAw9af/WzdtSfRn47W6zx6sL6tOWsw9ZmjNWp/22v2pGtb215/jrzHqf/T
dfZIN+Mexp3oJfWH4wC9pB7qYK4/yXljiP1ZNNTD+bLU7GWum7x1/0dn+hTi
G5nP34BRdPKFb5fH/kakPnCcFJ7hl0nq2G/W/m/NfQhRB1nmrOsV3tRvtIp/
GzjDKNzuyZ68zbnH6Okv/TTzjuZvopb7P81ZPof4ht5nr05z1gl4fUfja3in
OdsyvqhlfTd3+RdO6/Y/lv+LLQ==
                 "]], 
                Polygon[CompressedData["
1:eJwtkckyg0EURrs0tmzZGTbEEETEsIltskoVW8oD8BRmYuYVY0xMwc756n6L
U//5q7vvOLKz19jtSSmNQi+85ZR+YAVfhiv8EIbxcXjBb+AElvi/5nsM0/iY
7x/l8CGf619vZ+ABb8IWvgGvjqcYOr90Pr0dgFvnquDrcIef5vA63ONnOXwT
LvCDHG/7nO88x9k2POIfUMIXfH8f+vFBaPl8Ap+Ed/wP5vB59/8NUyl6fsK/
oIwvuj7VU8Orrk/51WvN/ak+5dJMnx1Psyw73meO3AXvo+vZzEIb/4XVFDtq
+b5ylzwvzUe5FbPj+8UUPXTcz1qKGG3vW7GL3ofyafcV16f+VIt6/gcbsEOr

                 "]]}]}, {
              EdgeForm[], Lighting -> Automatic, 
              RGBColor[0.87, 0.94, 1], 
              Opacity[0.5], 
              GraphicsGroup[{
                Polygon[CompressedData["
1:eJxFlNlWU0EQRS+5+RF/h0c/waU4AQHBAQ2DSphkDmgGkhAMxoFAEEGGKE6o
3+QAnmNvlw973brV3dVVdbr73IXU+e5EFEUXRVIciiNxIr6IS6JD9IiUmBCT
IisW8feKe+KuuCH6xKBIiynxSCyLJbEiSqIunolyrP84+P2fE3nxVKyJgiiK
dVHzv+bmRb/soSh8b4ph/m3fEiP4PDcnrjHX3+vM68fuZI19V8RVanAtXaJb
3BG3nb9iFePQh0XW2X+fPXtZl6YnKXrk3gzQuyl6kWXvLmI41hN64NqroiJW
xUvxQjznuyNeY3vsDb5XYkPsiV3xU7n+iENMxz6Lg9AltPit/9M49LjAeHsy
1On6fsVhTgFd6uTgvbfRdlo8RuOG2BT74i25NLD30LXGHPvL1PivthpabzGn
Si82qG2NGA18CeUaJ0OPHMe1Ouc8fXStbcmwj2u27TWV6L+vzN51dLDWD9B0
AO1GxUNs65rBd0p/5mUvEN97ZtHYvfScBc6Ox87obZb+OudZ2XPoYZ/teeqx
hjNRmFORvSoO6JHtqvgcBV9Mfcto0s5+f++dvi1934lv4itnxmfnPX7f/RZj
J+i8w1gLDa3lR/EBv9d+J+Yuczx2zDlwXs7vE2v3sR2jSj1+d/ze+D1wjVuc
gwT6LXG+ctx/30/fU9t+E/ze9KFJhl7Ootko9gyaWlufWZ/dMTGOdu735Si8
Ab7Dvssd+Gz3oH2adRkYY+0cejj/ZhRqKpKf36YJ9HONfuuaxBokB8cZIUfn
5ndiGw1aaON4K5wnrynwHo0Tp0R8967I+fQZKHHHm8Q8Ir9h9vRefps2OVcH
6OR75Lvve+l7N8SaSepxHXViOvYfWA3ufw==
                 "]]}]}, {
              EdgeForm[], Lighting -> Automatic, 
              RGBColor[0.87, 0.94, 1], 
              Opacity[0.5], 
              GraphicsGroup[{
                Polygon[CompressedData["
1:eJwtlNlWU0EQRfsml4RMCkYGgQTirPgnvvkJLp/1nwAn+BIVBxRBHBAwIAhB
QSCoDJ6zej/ster27erqOlXVjbsP7tzPhBDuiVQ0khAuiltiVExkQxgXH/Rv
RbwTs2JRfMF+L76yZntOLLH2UXwSq6KJj/eti+8hnrsgvnH+In4b7LHvZ7HG
GT57XiwTY4EYTc7YFj/FH3EoOpVDQVTFOZFP4prtblERZ0S/6BNnRZcYEBfE
U+X+RBRlnxclURa9oieJsX6Jv8QssqeH/WVi9OEzoz1vycm5dXGPQWK+1tob
cnJuXh8SV8WVJOryQ+yJ3yH6DPLvchI1sTab7GuhibVoi0fK5aHYkn2Axhuc
tUtN1rF30HUVTbfRuIndIoZj7XOnLdbbnO9Y7qEV/j0XL+iTWXKdQQvn7Fxr
4ho5T2vtJTWfw3ca22e8ClGzBXqpLp9hcUNcT2IvLXFP5+leHstGP+9/xp18
F/d2h4YgFUey/4VYM9fOczFCTwyg9SXxmP5wj52SrzXeR5e8zsqlUV/rM4H+
m9TJsRxzB819lnuuSt9Oyp7KRj1q5OZ73CTHTvneTuM9HH+Eu46yZ4z5nUfH
ceqxjKb29Rn79Ia/C34IyK3I9wE1rcgup7HX3fOlNO5p02P+57VD5iGHnrv0
mHNzTjVm0rGt0R491M1sDtHXU+yvU89j7Tlhrj3Ltp13gbnuEDlm2LOcFSkz
7jksoJdzdU45zunGZxL9Pbt1ZrgH3ev0Qj9vZIN/vdRkmLtXqZVzKKJHRnaS
xFla483wTBXQz/+seYY7V3g7EtbKvCuBNdt+a9wvrlPg231rjfLo4B4+Ik9r
4/q4hilxcuh/gl9Kvx7jV6LeWe6Rp19OqcMKb0KL+f4PoBDKiQ==
                 "]]}]}, {
              EdgeForm[], Lighting -> Automatic, 
              RGBColor[0.87, 0.94, 1], 
              Opacity[0.5], 
              GraphicsGroup[{
                Polygon[CompressedData["
1:eJwtlNlWU0EQRe+VPCsg6LMf4IxTAFEMxGiYEgUCYQphkEAIgkFADDI5ix+C
OPJ/nrPuftisom91d9U51blUWM6UzwRBUBQx0aw/TWI0DIK86BAPxBOREnfF
PfFIdIp2cV88FknRxprjbr51sNc5dzjDex+C47R4Ki6LK+Q4t0Xc4gyfdVIX
BD/qont8fly0ii6RoLY4sWv8q9w/oqz4tVgVa+Kt2BaLYkmsi6pYFhWxKTaI
V8QWa2VyNjivXlo1iILiKXEe/WYUF/k/LX6phhF6u0qd7rFEDb77Fd+u0YNz
3OuxuIG2SXTMiAF06xFDYjCM7vkp+sPovl7RJ3JiGM2S7O1nby/ffIb3WuNu
vqfw/pnIoltNfBQf0DjBGT7rbCzSxFpMinOxaM3xRBj1cZMZaSNuwdt2vL4d
RjNnz61to5gOI407ubOH3hN430cNWWodF2P0aa08y6P0NEA8gtbXmaE4Xr8U
b/B8DV9q9P5CLLC+Sr/u8Tn6N9D/EJr+Zv48Y+7BsWfS8+a1RubDedPUmqX2
PP7bjy5qnGTOStRxIA7Fd3Ek/in3VHxW/C6MzvYMFqnlmPfTSt/22zOTQ8ML
seg3IE8NTewfQbMT5qsP3dP4k2P2LsaiM8bwoB7/B5kv9+4aXds6/VuzYWZ0
Cp8X6XGCnhfQvoCOS+S4L781v0m/zSrn+vwd9pXw0J7NiXk8XkEv1+S3vEte
lb323Ot+63t8d+zfgX3WtpiVQ7zYZO2AnG3mxm/lfRi93RQeZzjDed/EV+7Y
I/4iZqnZtVaYH/tYpN8ZcipoYO3twTz7dtDC8/AJP+3vLHv3qfWIGmrsca7f
eDN+znHPKf7tUp9/s/0O7a/fwH/lNZih
                 "]]}]}, {
              EdgeForm[], Lighting -> Automatic, 
              RGBColor[0.87, 0.94, 1], 
              Opacity[0.5], 
              GraphicsGroup[{
                Polygon[CompressedData["
1:eJwtlFlWk0EQhdMmi1AiIAvyxSV4fNa1KCoiYEIcIjIjgRCSIIpgBARjEAQE
hwDihNMCvPf09/Cdv/7q6urqrtvdcfHKhcunEonEJZES10Ii0Sn6RK94J7ZF
U3wWW/hsfyLujngo8mJIDItpMSW6xW1xT9wlZx+xD0QPPtv3PS+peeKm7Kw4
r6JaxQvZs/KP6zshyv4XYyH6bJfEKD7bM9QyIorUNEiNrq0gTit3i3guewGf
x56IOWzPm8c3Ta6n+FzLY1GhppJqnBGTsqshfgvk8n+H1jqXinOd4xE1FYj9
Io7FX/FHHOGz/dt5lLsqDmX/CtGuiN0Qff6+Z94hfTsQJ+InY3vkdMy++CC+
sq57vcOcJj12r3+I79hNctm3iSYc95E1j6jV9Z3VXtOpeDY+oy5xS+REv2ij
v1XOyGfTnopnPU9sN9rJ0a8zqdhb99i9s6+EBqwXr1mhF87lNeboX5p+z9Kv
Y/b+jzPPsdYwOunHN0SfsvgG6Z01Y+2siVch6raLGMcWk1HTWeKvixsiE+K9
8dxFsU6OZ+R0rlXGlsRrYtrRz7Lsl5yXNbUSos93pJyM8Z5XRh875LBerKET
elxEr9bwgLga4hvgO9lDjRnGfMcr6O8AnUyxvwz7su2cA8xzL9wT1+V6vJb7
4v54zU7OxOv4XWgl3jFV/t2/WohvQBv68N5r7M+5PVYPURvWwwJn2YL+Fjnr
DfE2RM1b+3n25Xs8jj1AbRPU6/24d5Now2/ZCBrxm+W3y2/OKHaeXGPU6Fob
4k2Id8R3xXfpG3123/2mbtHDFWrcYO4ydoMzqJHL+02j9yW0UmfM93wX2/P2
8DXItY9vDV1tU8M6OtnBV0FPdfKtUrNr3SSHc/ke+U35D5XaGhQ=
                 "]]}]}, {
              EdgeForm[], Lighting -> Automatic, 
              RGBColor[0.87, 0.94, 1], 
              Opacity[0.5], 
              GraphicsGroup[{
                Polygon[CompressedData["
1:eJwtlNlWU0EQRe9kVJAoGkMIIiYhQIgBFTARNCAogpgog+izy2f9RRVx+CDn
8ZzV+2Gv1be7uqvqVNWtvHi99yqJouilyMTHNIo+iW1troo5MS9uiY64K9bF
I7Ej1tjz+iG218Qyd2y7IXrcuSna3PHdRbGEr664Le6IB2JTrLDn9X2xwJ0u
dtfFDWxWWC9w5r2cOCmGxTknKmIxKAY4OyXOY5OJE+KsyIt30uJtGvbznOV4
yzYzEq2RBZ1aYlrrvnivO1V9f42j6FscfNq39f2QBh2c/2niKIgLxO3cttCg
je7raOZ912Zf7GGzxXoXnfviuXgmrhJXG+1b1LTD3g51ORRPxWfq71oeUNMd
zg6w7fH2ITVxbZ6Ix87fmlAH18bazGahL+bohXvE2EMva9gi1i69sE1em+S4
i49Z0aRn3AcN9hapu301s9CD88TS4MwxWfvjNPiyHs7VNVkjV9cpEWfokX9x
qNsAtRrkrCguikn5qorLWk/wnjXcJwf3wpSoJKEnnKtzdm/UknBWF1eSYGPf
qRjCTx97x+u4C/i1L/v8qfh+xaEvM/S2D789iR7e87qeBG285/WUOEpDXPY9
Qjz2aV9N9LI+q2h+TP8uUYc3zIdj/hGHefGbI+RgbWpinHidqzVz/M7Zd31n
iLw9AwVsx8m1iK3veEY9q5fEWBJm0LNYFqPit2L4E4e5zlG3Qd4poOkQufpd
a+c7trWGvvs3Dv8Ev+F/2DI9u0G8ztm6W2/P9vc41M2947V1SKllnfo36dMv
cfgnRPTWGLlMUZ8yedWp3xH6+N9TItdhbGybR4NRzovkVkGzEmfuhyr96pgc
i+eiRv96TqbpUfssYV+lXrad4a0ysfnNDhpZG/+j/gPZeGyZ
                 "]]}]}, {
              EdgeForm[], Lighting -> Automatic, 
              RGBColor[0.87, 0.94, 1], 
              Opacity[0.5], 
              GraphicsGroup[{
                Polygon[CompressedData["
1:eJwtlNtSU0EQRU+dnB/xXUVQv8Mq37wgipGbQkgEAQNBJAYQASEiGq9cRUED
ioroFyAoyg+xd816WJVOn5np7t3Tc6wxc74jjqIoLRIxrT8z4qV4IWbFU/FW
vMHnbytiGfuVeIfP9muxiq8snuDzt/1UFP0RD2TniVXmm896Jp6LRbFA7Dkx
Tw5/tfcgFdZMiVNK+lwS/FPk6HUfxHtxVVwTN0Wb2BSfxS/xU/SLATEiSuKK
aBCtooWzHHuNM+fJax1fAzHa2PNPefwXH6nB9qH4HgffHDUucFZBDIoxMSoq
aGxtl8ReKmhmrbJxqLVWPEaLS/q9LJrEDXExDj7bab7Vi2bWrNAX67BBDMeq
ik9xOHeaHCrk75qW+L7MGRvsqWLviB/it9bupoKO1u8COTmX6+hmHazHN9a7
xiw5HhBvkXtwiJ6OvY3m6+z9So/dly3xhRyr5LJNrav0fJO1a+zdol+OWUEL
9+KeeEhPTkjr4+KR7HHuSoFe+c6c0bfTSfjv9bbrkvDNvjr6VWJ9mt5kRAf9
9B32PHgubNeIYhx8OXGbuP1omGZvuzhJfvdlD3O2+9xJjCZ0zeKz1ta8hbP9
20qMHLbvchc+256dbny2b4k7+NrJpVf0YDtuH74Mce/i6ySXPL59+p+jPs9q
IzEcy7W5B0NxqNHauGbXWiTHLnoywHp/n4hDz2rQ12/DpDibhJ6N0eM+8iii
Xy++YeL14LM9RI+cs3Mtob9j+OwJzsrTuyI52O+591u2i/7uST05OTffdb8L
niXfU79NO9wn5+s3cgbNrb3v6CC1d2MXuG/e47We41ruo23Pts+yBn6Ty+Q2
SWznOE7OznWW/b6jrtv1elb87vgsz4zfMc+qZ9pzeQRBr9oR
                 "]]}]}, {
              EdgeForm[], Lighting -> Automatic, 
              RGBColor[0.87, 0.94, 1], 
              Opacity[0.5], 
              GraphicsGroup[{
                Polygon[CompressedData["
1:eJw1lOdWU0EUhS/3UiMQI2CvLyESBV4hy0dw+Vuf0N4oIkIKkAIBRIgGKQqR
orj3ms8f38rJzJk5Zfa5dx49ffgkjqLosWgX1SSKauKayIiSKIqPcirF4XdG
LPDf9iexyNoCZ7xXF1NiWhRFQbwUr1jzXlm+S+KX7L/itXjDvY71gjP2nSSm
46xy/wKx11grEGtFLIsHKuq++Cb7u7gn+674KntTJIrdLvpFnxjV3phoae9Q
zIl5URUVMSs+O2+xJPLEdKwavnnsKr2ZxXcRbK+Tc5tixqJXnEtC7W85N0PN
U8TIc+6L+CG249C3A3FG/1zbsNiIQ42uPScWk9CDOn1yL/x/mH7syt6Jg5/f
sM5+jdq26NcyvW2w5v66Z4fk4XheOyC3EdlZ8VP2PrW9oyeu8YOYoG9z2JPU
Ok+vytS8zhtUqW0Du0ZuXsvx3o5tXfm9nYNj79Efr9neJZf3vKlz6tCZTnFe
pNFGB3Y/e13Mhn18r2v7I07jMDueoQviOr8D4gb/bQ+Km6ylueequEKMNPZl
9jLMo312yPtYHFHTPrFP4nD3kLhFDNsXxW3WnsdhpibpteevQk2uocT7rzBH
Bebf8+q5Lf7/JsRhzT0uc9Z98FlrqIFO7O87PNee5wr9yRAziz59j89nea8t
NJZDPw20+SwONUygnVH0Zj03mQHPQo98u5OgyxZzFiVBl16zfYav7VQSzthu
YxZT5FrhLr/lCPrZRP89+A3ytt3cM4B/hf44L++tMvdNZtC5/Y7DN8Bxj3nH
DrTnb9F4e/Bp8fZ7+Nh3jPlr8j0Y5/tl29+IPjRlLV2irl40YW2coJ1OdH6K
lrvQeoozQ9R4RFzn5m/nGnO5TU3/AK9f99k=
                 "]]}]}, {
              EdgeForm[], Lighting -> Automatic, 
              RGBColor[0.87, 0.94, 1], 
              Opacity[0.5], 
              GraphicsGroup[{
                Polygon[CompressedData["
1:eJxNlNdak0EQhvO4uRGsWKkSRZB0iiAGDGDESAgoREnUiGDFdp1CAONt+H3u
e+DBC5P5p+3szPastxaaZxKJxIZIiin9mRQPQiJREnnJJXEieVnsi/fih/gu
3ok98U18FX9EF71/2+9U1PD3t9/iJTan/H6BzaE4ErdFn+iIYzEncmJHNKlj
jzg7yI73VuyKA/ElxLP4TI8lV0SR81l+JG6Qx/luhWg7LVZD9GkT07E+izfo
LH+ilhY92Ud+JT6ge42PbT9ytg5ncc7r1ODcKXzs+5Med+npAed6LraI6dib
4hk5nXsDXYvaquKp2MbP/xucqU3vfQfr9LJOjCZ9te8aPtvY1flm+yfk2KI2
92yVmlzHQ1EmTpWeV8hR/6+mNmdqMAu71LiCT53crvUEvX8vhThXNepcJGeV
2irUtEnOMj5r1PQrxJnzDFwQ/WJA3BGj3Nch+lG+DYoxbLLcpedzFv24mBRF
5LtiCt2ESIsZMU1MxymIfIizcYQ+z7cxYhXAsvdzPsTd6GB7n9gZcY8cGWqc
Ref77tJf99n7UEhGX8fzblg3T4605Ewy1up4fg9yyRjv305KziZjbOss22eG
fN5L74XfCr8RfkscY47ap9lPvzULIdZim2POk8N+mXvLkn+Ju0yTv8y9XxZX
xLAYEmfFuRD37JroQWf5KlgeETeRbZtCN0icce6hQH88kyv4jND3CWyHuXP7
9FLTELE8dzV636C28yG+Ac7rWfNMXgxx5orMTYn7yDMD8/QnQ7999kVsp+il
ffyu+J3JMQcpdFlmw3kuhTjX/ci91DrA2VLYpqnVe9JHzSXmx73wXv4FycKa
HQ==
                 
                 "]]}]}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}}, {{
              Directive[
               RGBColor[0, 0, 1]], 
              
              Line[{2, 1, 11, 21, 31, 41, 1001, 51, 61, 71, 81, 91, 92, 93, 
               94, 95, 910, 96, 97, 98, 99, 100, 90, 80, 70, 60, 1009, 50, 40,
                30, 20, 10, 9, 8, 7, 6, 901, 5, 4, 3, 2}]}, {
              Directive[
               RGBColor[0, 0, 1]], 
              
              Line[{102, 101, 111, 121, 131, 141, 1019, 151, 161, 171, 181, 
               191, 192, 193, 194, 195, 929, 196, 197, 198, 199, 200, 190, 
               180, 170, 160, 1027, 150, 140, 130, 120, 110, 109, 108, 107, 
               106, 920, 105, 104, 103, 102}]}, {
              Directive[
               RGBColor[0, 0, 1]], 
              
              Line[{202, 201, 211, 221, 231, 241, 1029, 251, 261, 271, 281, 
               291, 292, 293, 294, 295, 939, 296, 297, 298, 299, 300, 290, 
               280, 270, 260, 1037, 250, 240, 230, 220, 210, 209, 208, 207, 
               206, 930, 205, 204, 203, 202}]}, {
              Directive[
               RGBColor[0, 0, 1]], 
              
              Line[{302, 301, 311, 321, 331, 341, 1039, 351, 361, 371, 381, 
               391, 392, 393, 394, 395, 949, 396, 397, 398, 399, 400, 390, 
               380, 370, 360, 1047, 350, 340, 330, 320, 310, 309, 308, 307, 
               306, 940, 305, 304, 303, 302}]}, {
              Directive[
               RGBColor[0, 0, 1]], 
              
              Line[{402, 401, 411, 421, 431, 441, 1049, 451, 461, 471, 481, 
               491, 492, 493, 494, 495, 959, 496, 497, 498, 499, 500, 490, 
               480, 470, 460, 1057, 450, 440, 430, 420, 410, 409, 408, 407, 
               406, 950, 405, 404, 403, 402}]}, {
              Directive[
               RGBColor[0, 0, 1]], 
              
              Line[{502, 501, 511, 521, 531, 541, 1059, 551, 561, 571, 581, 
               591, 592, 593, 594, 595, 969, 596, 597, 598, 599, 600, 590, 
               580, 570, 560, 1067, 550, 540, 530, 520, 510, 509, 508, 507, 
               506, 960, 505, 504, 503, 502}]}, {
              Directive[
               RGBColor[0, 0, 1]], 
              
              Line[{602, 601, 611, 621, 631, 641, 1069, 651, 661, 671, 681, 
               691, 692, 693, 694, 695, 979, 696, 697, 698, 699, 700, 690, 
               680, 670, 660, 1077, 650, 640, 630, 620, 610, 609, 608, 607, 
               606, 970, 605, 604, 603, 602}]}, {
              Directive[
               RGBColor[0, 0, 1]], 
              
              Line[{702, 701, 711, 721, 731, 741, 1079, 751, 761, 771, 781, 
               791, 792, 793, 794, 795, 989, 796, 797, 798, 799, 800, 790, 
               780, 770, 760, 1087, 750, 740, 730, 720, 710, 709, 708, 707, 
               706, 980, 705, 704, 703, 702}]}, {
              Directive[
               RGBColor[0, 0, 1]], 
              
              Line[{802, 801, 811, 821, 831, 841, 1089, 851, 861, 871, 881, 
               891, 892, 893, 894, 895, 999, 896, 897, 898, 899, 900, 890, 
               880, 870, 860, 1097, 850, 840, 830, 820, 810, 809, 808, 807, 
               806, 990, 805, 804, 803, 
               802}]}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, \
{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, \
{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, \
{}, {}, {}, {
              GrayLevel[0.2], 
              
              Line[{901, 911, 902, 912, 903, 913, 904, 914, 905, 915, 906, 
               916, 907, 917, 908, 918, 909, 919, 910}], 
              Line[{920, 921, 922, 923, 924, 1098, 925, 926, 927, 928, 929}], 
              
              Line[{930, 931, 932, 933, 934, 1099, 935, 936, 937, 938, 939}], 
              
              Line[{940, 941, 942, 943, 944, 1100, 945, 946, 947, 948, 949}], 
              
              Line[{950, 951, 952, 953, 954, 1101, 955, 956, 957, 958, 959}], 
              
              Line[{960, 961, 962, 963, 964, 1102, 965, 966, 967, 968, 969}], 
              
              Line[{970, 971, 972, 973, 974, 1103, 975, 976, 977, 978, 979}], 
              
              Line[{980, 981, 982, 983, 984, 1104, 985, 986, 987, 988, 989}], 
              
              
              Line[{990, 991, 992, 993, 994, 1105, 995, 996, 997, 998, 
               999}]}, {
              GrayLevel[0.2], 
              
              Line[{1001, 1010, 1000, 1011, 1002, 1012, 1003, 1013, 1004, 915,
                1005, 1014, 1006, 1015, 1007, 1016, 1008, 1017, 1009}], 
              Line[{1019, 1018, 1020, 1021, 1022, 1098, 1023, 1024, 1025, 
               1026, 1027}], 
              
              Line[{1029, 1028, 1030, 1031, 1032, 1099, 1033, 1034, 1035, 
               1036, 1037}], 
              
              Line[{1039, 1038, 1040, 1041, 1042, 1100, 1043, 1044, 1045, 
               1046, 1047}], 
              
              Line[{1049, 1048, 1050, 1051, 1052, 1101, 1053, 1054, 1055, 
               1056, 1057}], 
              
              Line[{1059, 1058, 1060, 1061, 1062, 1102, 1063, 1064, 1065, 
               1066, 1067}], 
              
              Line[{1069, 1068, 1070, 1071, 1072, 1103, 1073, 1074, 1075, 
               1076, 1077}], 
              
              Line[{1079, 1078, 1080, 1081, 1082, 1104, 1083, 1084, 1085, 
               1086, 1087}], 
              
              Line[{1089, 1088, 1090, 1091, 1092, 1105, 1093, 1094, 1095, 
               1096, 1097}]}}}, VertexNormals -> CompressedData["
1:eJws23c4lu0bB/AyQr1GCimyklFWlJDnTEZRZGWUFGkp0iRCNBCKlEolMylb
VvHc9pa9995UJIn83Ofz++s9js97Hud1Xtd134+H45uQ9RXDs3SrVq0yYFi1
in7lv8kHjXyU/AageaL2svn1Jgh96621WD0Ip4I2fBSV6ocWeZVrV7vaYI+J
MHpCv5ntw8BeeHb65IvPs13Q6bxZk/S30X2lXXe6YXTE7etb416IJ/IOkS7+
pydM63o7xNHlZkrM98Gz2BjsE7AwHk+/oRkY2CXe1Z4aAP0Zfw3Sv2vdrxm6
WAf1txXGRL0GQfP+Zqx3FAhZq8daAeF3W2Q8bYeA02we652dQyLO/siFoStD
zzcxDYO0aC/O88h+PfOi+0d4o2Oas3B8GNYyRdP2lXhjwvZIHxBsTHJ7/9VD
+A8dZ2mtEZgc2bK/aEMvMBjHZLhPNgO94RP0p6n2MRd1umGs/3OgSmA73GoU
vUN6mLXrare1HeC4J5phqrML1jSLuJK+8xC/7NprLcC2Z4kzeKAH2pdeYR+9
m03cF/waYJvjwbPd2/rAf8kc+8y4lmQNnq0BbkHzz907+uEH7EIXvHF2gwpP
KfBIKceZzfUDdRNg/2uyr7/ukaLCrPTKaXgMwPleG+y/V9PN2OpVDOz6kuPv
VjkA3E85XUiXrFcajYvuBrZV3rd/a9fCj1k2xpwrY5A4LSQx690JwSfO69Al
N0AupRz9ppxA1l+2duBb9hfKamoGvRBxetJr1zoyPYcWuCYzqbQzuQ12h3Jg
fSFXle87kUZY/rbezFyxE4RHz2E9n+RXykRhLfwnfdX4kFU3/LfqKbomqBaq
WlRCGpexKOezHrg4HsBAeuCSXNKmxkKYW/QOFFfvBfaj57H/zv6ZKyc2fYGm
HWciqV97wUB4Bv3O8rSwxoUosN27HGG62AteAT7oHEd8GV/ptQGDB2+7GrUS
4gZ/SzZoTkBi1kX+rf4tYKgs0iMrXwuX90rvID3WY5+3gFsTTMoc/vGzph6K
bsuhNxql7eQXb4A7edevbZNtgtmvuehrBNM2OD6tBSWTGqtD2i3gJNKPfvxV
gF/coyqIrUqqp3K1gZNRGfqZ1925okElEJ3CytD2pB3sTzWia4R6OMoa5MES
x0H57ykd0PhFGD1x29wfvuQ0iKi4rWfr0glnp26ga/ofZMuaC4MQK5nT1UOd
cGPdy504v17q5VtRDfDf/nWTDf7FwKqU9yUmfxLi9rNfLFSvh+R2nT0i/8pB
3f8z+vqKyTB4Xwv5XaVOe3mrYe33CPRRul/76SOqIcqqT5D9Yi38SU1AFzrM
3x8TVAG+V9P++rDVw0yt1FfSpbNFPFSkS6DYalCgzbEBHkgWY31TgtPhd+75
kORcX/worBFEV7Nh/S2VvSyKUl/B4pMkzNxvgjMK37B+oqGuyooxGRItuU4e
EG6Gha5udHWZXcWq70JgvFTnutetZvjx5D72Kfu3u0XvWhVUunkE7eLOhRvZ
F08+6J0C4vtMlYxLBTisz3L5tL8A7F8fQtfb1zN4RKoMrl3JvHFCuhimyu+h
q89Yx008KYZv5gIO9E2lELRsgd5r/stF/1MBlB9oDFGTqYA0PwN075PT90K8
c+GAADX3064q8Ja9jb53Zuiq9ZkvEHVum99Q6zeYNjiNrst46w5Yp0LWw7iu
DvYamJe8i57glrW1/uAHaNKXMuK4WwOV+ifQJXbtkl1cfLry/nVa7ciugUCn
YPR/nz+8yqTkgXmwi6RfcwrUx5mZ94dOA/+a8t+cm3JBoFv6Y6NfOrznV0VX
eke9qeyaA7qKmrMTC1nwg8EU/dDd/fIKdl/gGdeVjCXuHBCVNEdPjBWPoLJk
gH8fvcOeZgIsqZbosqBm63cyFTwFXkxc580DEV1anzNmFeKXRxIgc9l+8uj6
fBC/aIH+nvCVkPz8HpTT7n0WrMkH7jW66EN77LivO7wBj/FLtb5GBfBkTAM9
vbpK84nqQzAOazT8+aoAEs8cQj/y5GZ8rmEkKNlwLLXwPYfXu5eqLs1PQ53H
tcyXzBHgOG7u4KPxCuZ1af6dtWCrx4MwEHWumr7G9xbspWjud8X9p/ubt5Bx
SDdU6UgY/Hi2iL7f/OHauzWvILlgw+aipnCIkqXVVzcFn11OeQ6zCwW7/4lH
Qu91mpcyeb1e1A+EtlNmfzMpURBQQ+tj+zCf97WDDzywskzx3hgNru60ekPf
3bONne7g/F4QymOiYQuV5iy3AsZSxy+A+c29SXFz0aA6TPO9hTG7zjxNAIET
4UFBqWFgfHOCg2FymqgzbWNe6x0HV605HRiXIuFc+yS6WPWefW9EPsLa316x
J5hiwJxrCj21l+1GzOUYUL9VWLRYGgvmWrT6Z8PvU3IuRUHYqlzm3Upx4GY2
jS7Hdzv7uGQ4nOuM9TPQT4C5cVq9pKy2u+/Z1+BKiF3uoE8CVoLWX2uUd7FX
4BmoX+RvWI5LgguWtPpDXJymJ3f7QLmtwu87Qsnw3ZVWf+W9xM7c9OtQtmP4
v4FTyWD6jVY/0CxwL5exEBwl++9c7kmD98mtQjV3pgnu8w/2vk/MB0F6f8UQ
6S8g7VaFrpE5W+23NQ/6AzsFtkpSYTG2Ef2v54BiVQIVDI845e7qz4X/0hrQ
f9u/MAonvoDO1up1jHfywaC0Fl2Cvb1k9Z10EK/8t8elrQBYq2l9LgwLSJz9
kQz3xNVzDywUgu79JvSvgTwcGhYf4WyZDVd1cxGEx9Hq6wQUxu55hoHKOcVM
G4diODpUgX5z4Mu/x3U+UNZ3fVgovxjy0+rRHxw+FlyVVgvmaSNDMeYFYG3l
Rc9lOQV3NAa1d3vVgK5Z3hs/8RJ4xDuEziZxVbTj0Dfo13A5a5RQDm6eb9Dp
r/yaSxSpgJ51pSqd3VVgv6kPfW9/g0HDhxLIoQpfeWReAw3enuiLcDNHs78A
mG8lu3o9qYW+uLPoot843rbmEFAuU8y64XEdaAs5oEu+FDz0XT8DnrEsu7AY
1sM6Diq6cP6Tm/UK8eCs95t9VWs9fHhVgB4lyJEj2BwMfS+sko8INUAfmzP6
hrUlo4RVCbD8lrjM7pcFQ1rmqwKPTEO9NRRBXREM+tQ9lAiiwlsVA/TLdvtu
tC0VwGuHV6mdb/Lg4M2b6PyFjo0mY3lQ+1GfzTezAII/O6H3hjSxP9xHwAU2
senZ50VQMW6Gvvq1WxCh9gXcN576qyhTAo/lT6DnfRJK55z9DFbWb8UPuZdC
hrEG+ptss7A7pQlQ3pZovMqzDP4FUdDf1bENRhyMBD6QSZNRKIeAUBv09gpB
gY9ij6E4Ui41MqgcqkZprv3wcKkIXzpYUtYvHhb/AD5thWdUWqahjbN02jLi
M6QV3rS0Ho2D+qUi9AfcrHY3hlPg2GtbnZcxSZAVQPPV5Y7ebd+TgN89U3Us
OgUePqS51WmZM94+8VCT/yvrxMXPsFxbgk7w+UptbvgAspoTLicm0sCppQB9
awbdx4M1kTDBF1ksIpkBrSeq0TdPlqR92/IWClQT1pVtzYQnJyvRm0Ys725X
eQpSD6TWHyzJhP49tPndc+9qfL13B5YOPK3xEcqCfx9y0d1+7t0k7/0QfrQo
nDoZ6gQHbWpkTyxPE92roocUmR4As2vtvqRbruB1lOZP5epftqjfg4vCMxf+
zd8F9f/Xs3K217xX9wCdfQyLrKvvgb0pzUMfBZ8c5HWD3lr7If6q+7DmGM3V
bOdvXNe9DZuqdXd7mD4EbX2az5tPWPLL3YC+0Q39x197wdQ5mgdVXpDg+mgH
xpdHH3565g0j/183y4r3r9ozaxi+qCdhtd8HdprQPPrii9KN7LrgureI/+Qb
H4j9/7rru7+ePpL2FW6HK3nt1EqArQp3+27kTROvAu7m9LRnQdF/j6bS+VOg
LMcXXcpg8+OapAyokl+nJNr9GRwi/dB9r4crzCqnwWmupwvxlhnAaR6IfpPy
1prDMwW66z3m7J6t/LxO80b/ayz4zpk/EWrTQmdtb3+FmgO0PkoiVu0vVu49
Vv5uZCxTDuS4vUR/FLzF9ahqBLi8UW/dr0wFn1MB6GsKQ/J7W4Ih5/Hc/Qus
BDxzc0W/MiYRzNDlAe8+S089cCNAPdcDPVjuudU6oXJwLZzKdvHLBruq8dHD
Aiuf82yFRSHWpWCQELb1AlceCL9qRR+lqy/YaVoMEa+8L0QkFMDS6d/oPhO/
+3evfM73RzA8zWQphpfUOvTxve/cWW7kQRhD1nllgVK4oNqJnna5Zei+aQ5E
6wt7B42UgdSmZnSe3Jf/GdlmgBh/KZ3g6Qr44/oDvfa0xDN5+WRIoZcUp3hU
gkVEE7pH2KSPn280JJ2e3HTyUBXM5HahH5OldJq2BoBHXYYCc2oV7N1XhR5j
QTfotLYeei+G3uiuKoTr/6z39WyeIooFvv8WKaqFc0EzUwxDpTAZVYNuOeOi
8/lgDXjy5Yk/TqwEcflA9NDInqPZUlWwLGvz98DK9729Bl/RvcP/qny/UgZf
2BLer31WC68FXqDfELW0a7AqAsWP008Ne+vg9btr6HZMFpZ7OPKg3uTIw38/
6+F6si964JlN76+uz4KdV3WvJxQ1gBrvKfSx0t9aEecSgeHDI7Eyk5Xfwz47
ohvVvvF1fvwSDOcbPO9HNYKYPYF+lHv/Yb3SFoBMVca6DRVwOa5o3YWMCYL3
zd2ebsFm2HAgtYxOoQa+P7v2H+nvd2vUXJJtBDkTDd3Zj3Vw9OsgOmPQb8/1
S3Xg1yTi82uoAfQGNNCPnRCKLfCvgZbd9Q7zPU2gWOSJfp9vWNemoQJYqF13
iKctcKk+C9c1PJHSfGZ3MTQc1ub+MdcKohtssP6kmYW4pn4unFVvShJnaYfO
I2/QI6NCuXWufIbHFuyeDMXtIMAigC7GvXSZa+4dsHJGHpDd0QEhfw6he3rd
ZR5U7IJH/w7fFvGsAYmz82maf8aIK6Zjq4LlO0A25tmXtrx6aMygyyD9g/TH
5X1NrZDKXbNFrqAJtgRPpZO+rSd0t4FYM8i+2fUh9k4rGETqo3/cKWZ/WKYB
3sJPtY+D7RDFZYL9N9jk6Pj+roGopZpvwXOdoBOyCfvvHwlYuLaxAkT1vN1H
P3SDfrg39onnejdzdKAAdmQTbpEuPaB/eBL9/L/5W9SxTJDV0KwrmeoB3YsU
9D+pauFn7kSC4Ko8mVs7e2HLaAqu+2D1v2aWqF4QWlRYA1r1UDBUnPNncITY
lcPKE36rB77aRlQarG+GAutSKunRRq7XXCS7QEz76IaAoDbwyDmArvJt7/db
zO1gPqfWPZjXCXlmE9mk3+BUb9gU2AyHNgazbNPugb1btmN9Qml4tWF1PTCJ
RYiMufbCk8o4XNdc49UXutBqyD1pfuaWSx80HhbEPsxtxzb4K5WA3dB9ygdK
P7TdW4v1iuybYr14cmB1reA1nqJ+qNn9C5335fwuxdz3wJDn7reFdQDUm1Rw
3R1r9/tFmHWBl3m9xZpnNSDU2vLKeHIMQuWvim4z7ICdAU9yDzTXQ1rm7Zek
r9bXM1D52QpNHQVxbe1N4K5w9znpvd93q53f3wzslmt7tV62Qlxo1QvSxZjk
GCQ1G+AnT/lsP30HvOcIwz6vhJNPTLLUwn6tjeGsPF2gL+CD9ZeOvGIrl6sA
tR8f+vdUdIPOrsBg0pvGU491/imAY773TNz9e+A/tj/PSDdQ+fEuhzELfh3x
sxVc3Qvdm1LR67ctpt8NjIT51w+C10MvrDaOwf5ss/YaO8ZbgE9O1n5UqQIe
FO7c/zp+Aqii0tsuqjVDNstO7m86NeCuWahGumf132xlvUao3sMlrVhUBx/r
BoD0i93c/SU89fB9sz7XSYZG+MYcgvVb4sdkWaNrYMFyS5PY6mZovG2H/mZ6
Y8SjvxWg0NDPY5DWAjeV1NANBoz7w42LoW+Tf7XjljY48pgJ+8tUee/Ze37l
9+Uu3m1yMu2gEW6F9aeWdFRZ/D6D8d7aCO7JdrjccRz9Q0DR3wfrw2A+UfVL
zcq9Pdx4DPc1P3FfhUmmHhwUbtvU/CqEt13T6SYbpuAq3bs0teFaCGvdqFLH
VQbRISXojoXO2b/O1cDrycKKsKFKUJdLQZfxuiaVd7YKfrYms22SroE3O25k
kN6rZWtUHFYGWR8PCJevfE8/JEDB+v2cfPa9j4qA8yr7cCBjPRyX3oP1NyKE
sudl80BtofjAYd4GUBe8i+4yf2H/U7UsqBmmi3sy2QC7GR+jX+T5ZTwekQhV
ra3zue6NMHxjHPvb2d9peZfzEqqJ/Y/Xf2sEyc9uWM/wQMEp8nI5PFeenNvQ
nQ1Vekfu82+ehvOTg5pisaXgw36lsUErD77bUdDPqOWPf3tRDMsanFlBwwWQ
qyyFPtbw/L0QFMK80M5YZ+1iuGQliH50oxuT9/s8CInp/0N/thRqe8bukZ5/
z/XKhbgcEBhdpeUtXw4VPdpYL7mHWWV9QgYIRV7VNkqsgPiYdKxXFtx0e84m
Gd441Nel1VdCVaMt1p86Efu4tywaxA9YdW98WwVfTGewvmjIO457bSC0/uzx
m2X+BgLXz2B9k4bE3+dHs2Eob2coMZUADQeZ5pKp0yC9jk+y69IX4My0Wv9i
5fuRY9MmdBWHGbmvCpnwWeWm60mjNFDv+v2LdIWi/eE7ktOg8Km1WXXPyvcv
ulH0CuHsowKjKdDBEuXTKfxl5bnjwD7Bf0Jas8sToSnr2cbznNmg2LqI9UpK
Q9ztlrFw+XXUqaOxOfBPlB7rjZadL3ZHREDKW9XswD7qyucLD/pgjeiuzWYv
4O0fC1GDLwSY3WVAZ4/R4Lmn5gmDdqJKP7lzgfHtAPYXnn0YttnGF5xfRxTd
nHQFdr1wh/+Wp4Ex4/C+zQE+QBlx6gwb8YBAe5qrn3/uZ3PJC9qMW1kWw+/D
9eM0tzQUH81ZvA+MfvWZuzZ7AZc7zV/Eiyo3HPYE8wCTBidtH7gZTXPfb9Vb
dfPd4H3UVNiIuC90OdE89sijlAZfJ7jVEnX6ZYofOIXSfHLh9/sOpmvQaBco
Ht/rD/73aP7l2dl7U8/PgZeoGIttymOQc6C5iOtHamysAbxNPrmoIvwEFExo
Xn3iV/QpjzSi0OJaxy73GOLRu1VlVm3ToNtvID29/jNhUDGQtFogjpiqXigl
fU3gzwXBMymE1Idd2fTyScQR3v+wfuaaOkXTOYlQ17fztxJJIcSUv2N9xuee
nOIvccRfPQNGl7ZUIm2CAesPnxo9orf9AzFC2dczaJhGaF6dxfrb07xZwZqR
BH3LuU9LXumEpewP9GzrJiFW/TdEqsdz9583MwijH3/QE79Ysfl8DSQKD1lz
NfJmEpNZ/9Br3g27CWe7EMO/mZbeXc0kXjxnx3WDy5fm47lKiJD0L39/7M0i
whtsi0QNpiFow6Cnmm8RIcU4VJAkQyU2hvmg32GM4a3JLiAecce/DjiTRxR7
30EfeyZHZU3KI7pKmSWtnQqI5v+M0VMrioyGmqgEB5TVfzlaRFjXu6DXz8Vz
Gk1kEZ9SG66U9xcTBVZG6Ld8sw2HEz8Tu4X7FZ7LlBJhrqXoz3M8RE1sEojo
spg0epkyIrryGvo1wTM+o8yRhCPVzHK6q4xg57yJ7j1s8eNxkT/hV3+5fqdq
OaGaaYc+Y2nJNxhQS9j+qHE2lisgfhW8jmI5OwVjPZIOBTY1xOXmTA/t+WLC
cJMZeobOFmGC5RuxTea/v5m3yoknssHoWmIpS4Lj5cSZlyYsr8OriPMfrqDv
XBRveuBYQghftstzVKwhrAQd0Nf+nXGpzyogop48LZuyrSVGf95H1xg8vS/V
hSC4Op51nz9fR6zbU4he1OHMaS6YQQjsDBkrkqgnlB/6olcNmv749TuOmJy9
rxKZWE9kW/uge/+ZdvKKCV75HsQrb/67nqi5tR79uMiDM66nmgkNujp44VNG
GP5X2De3aRL01IPrRWoaiWTnR3S7A74R6oMX+0l/+qqnZvxvPaGWqtYr8bqW
EEnzRNfYtvrJ65FaQs5rgScyfeX/j6min3+QpeKhVE3cW3WvuiuokShP7MX+
8x8jW3dDOaH3+qHpws5mYjQmCf1b49pj//0sJKKK1VeP3Wkh3jraob9aZ1fu
WUQlKKU7pH3cWwmzUBvsH9yb9b1IM5UQfCNtEiPXRuQmMaEf4vHUzt0WSrxx
fbCFM7CNaPH4TutTYqGwsl/Iji3kXtkv/Gu+M7XixMGKw4kr+4UsgZM5CgHf
YCEqD/2P/+yHlf2ChcJi4cp+QelQFLqgcrfDyn7hdCX7VER6PTwzWUAfHQ2d
vqtUDWJ0lE8r+4Wq9DR0DkIzfmW/EBmmJbayX2i/X4kumuohuLJf2MGoWT96
pwUKBx3R6f2zo1f2CyeEDP95u7eCpq0L+qzky9yV/cJF2RjRlf1Cnch19PK0
j5LEtlDYUag2uj6wDZomUtH3JmdK6Qx0gFjfZVGlwGpYX3ntD+PVceLF056w
6Lk24KuXSavprwNPfVF0J0XHnO7UFijjLlo8XdAIYRH66J/jJeq2iDTB/u5m
bhXLFhDbeQ49tavF5fqRehhe0+MvGtcGntSL6FNlF3507K2BCfeAgdboDmCq
+Yq+9KfEl3+gDBb8bnps0ewC7ewd6IwvNr/LZyiAv/5TOhX3u4GqVz5P+ra7
A3UZdRnAP+I+6yLbA10mG7E+NnRQ8Fx4BOTcuCFw81IPMMXS+rTdsAieXtML
FZIcp16G1EGHYJTx1WujhKqByPY1pd0Q8uOIQL96E1yQ23OM9Fv5+7Ma+Tvh
5EW2X73vWyG9hQ/rj9nJfM7gaYPEkwNe/Zkd8JrbAeuPsTju2ks0QYbKJs09
Dt3QvpsR6wd9354c3loPL7JSzov09YDMV1msN/Clmjjc/wabDk+HdTb1Qn/X
JfT62AHOTNtiYHt4TEwzqA/S5sRNSLc7ohd+bWM2fBD4KcLE1Q/zI2+x/5ap
g6yvf0XD3merB3yO98OVJC/sM/Y8cOD32X7wfFl2r1muEYLs+RfuiwwT8Mj8
g9fmPjgcTsTFnmyFnY8Y/pB+wlWbd/p5D2z5ZJd2VqkT9qabzJMup1sv/jOr
E7rFIoPtLXrg7gMB9MK46tgdY63wMqrH1nOkF7IGdmOf/Qyjt4NKG0E60bVl
3b7+ld+LX/0m/cLvvx8VrGthTuNfa5vhADQ+UMM+1Od9/w2vvG/0To6S7hKD
wM12FfucmtxV8WCJgIHoYBGjnEGQaZdFb/IV0XZmiIXcbR4M9/8bgrql07gv
jdPl1nlegyB3OGdyx8OV9+hk18dHvgPE/Rr/rn6eAfjUwsB5XGzl+zaTdyzp
KlHtrc8v9AFjl4FVnWIPBOgmx5Nu9V8ok4R7Dwgs7HIsEO8Dz9LyONJftowZ
6xZ0wOxPIY1TPf0rP78vfiJ99qLWsJNUC7CIJ35TMh+E9KI57CM8c6BI73A9
AHXQMP7xEFw/W4B9rO9ziNwwqwSGiOqCkTvD0F8ZgvWXLm40qMzNg3h+meW/
giNg5H4K6xurmCtPFn6CLQs5Cz1OI/BpqhJ9zenUyYCzw8BsE9KnHNAK2Tvc
Pqek9BI/gS2ounFljrVHecRedkEay+VU0oPddOr4Vw8A3XPeiJx3vZDXEphO
+oGvHNfVJ1d+jx2q4i742g+r9IWx/oSrsP47tW7wNrDtcX41CMGDlWmk130e
i2fWaoObpt55q+SH4TMrA647WLbzsNvvBqim7so/eG8Elo1+YB+/CvF8z6pv
wFjuLiX5cHTl92ZBrD9ssTm0VKcAhGb/ht3aOwbpPfQ4T8fwPFPwzgTgy8/a
vfBiDN7AT6z//Unmj8efEVinO7n9mXY7aHfwWsyqdxKMPtoj5XrDwG5zgVXa
uAcc9jVYky7xrkTe9fgg8OwIUmGo6YMG9mtWpM9qbnMsFO6H5x7WbDc5BmGr
bcl50ks0hATCE3rgVQ79Kxu2YZBc7YJ91ig8/hy1rgPOTZxr8y4YgW/12uak
8439MHaxaoLQGrNQV8kxiPpZjPUFUj8efXCogcbA8KhGyji42gyfJH1fiMX6
jheFIKQ8fk58aRyM2WVOk84pfbK/cHMS8LgJ0fOfmQCGeOpZ0nc2vJZ4RRkD
6RYF8eK4DniifdmOv7iJkByueCnjNgIvCrOyv4j3gjPL31ukp4qm+151GIK/
Gi8dBwv7Ic6T5jHZoa1tWwYg/65PXrTwEBgc17pBOpXpp2iJVy8Eb7Vnuqc8
ArwlS1dJj9biHfjs1An5lTdu9jKOQUeQpD3py+ePMlTfbwZ/3vWp3O7joFTG
jv1vQv9YjFYteOYZNLuET8Cck6Mj6coD3t1jkUUAeVcZ7lychE9br1whPZbn
sfq5iSR4K/Ij/13DJJzw/459FDSYNzbIjkODX/SbK2mdUJmccfnUqhqC3oih
DE6PgmVmtPvmkl6QzPS7QXqtiqC6PwzD9yOcYOEyABIN8vakv10PKjcaB2Cz
Ta39zNQQmHxa50D62oQ2qWcSffC2QPG18OZRyIswv0T60VypkEzvLmAsEO7l
mBuDzsixa6RvFwx+0HuyBZhngz5235kAXSujq6QLWWqPbRmqBUXto5w5sZPg
uJbPmXQNSdbv7y8Vg9LV6/IzTlNwjz78OunRmYPCr7yT4UB7Y/3M8BTM9ytc
Id2E99P87qpx+HRauPHI6S5w7mPT38uRTwRXb8mV+zkKV45c1zGX6oO7FQmX
SX9cu8H9QNEwWPl+thf/MQCH55WsST8Ln4O0dAfhR1rsltZrw3A6U9aYdKpg
tinL0z7I0ovT9/g4Cr+7TE+QnmaxesF4bzf0J3jUCgaOw7WoRVx3fCLShXGs
Bfp3yqtnCE4C/auRo6QXPJZby6pbBwfzw5hNjadAyeGyEemTr3LsX00Uw6SR
56CC+Mr3ezkhE9J92Zc9HSaTIUPWcjL11TTYfWU+RXpCy2vLkYQu6K48+J84
1wTxvo1erSjemMr7R8SB9PEOVybS39U1mfozq1BOeY+dIz1hgG4t6QcY7W60
zZtT7ZyULpHe9a6RmfTr2mnpEjIm1HbnPFfSW8uL6El3cFq7hpHXnHJZcvQq
6dGcdNi/wEJPT2fPBoqgosl10idOhK4h/eAgz+ONdhRqhsMo+ty2W+i2ebNb
2K4rUibd1l8j3Y3tN/qBOh7e8gI5imd7zE3SbyssMpLOrDu1d72zNMU8Pe4W
6bkzn9F/yifNvzITp/Cr7bxPOuN6RjrSkykPNdflmlDm7Vfbkd6zwRz3dbIu
jnLFQo+6z8jQmXT16TQG0p8xC7e4BBpR5Lm5cV0q4zach0HINTBEUIcamWmH
+00rF8H9hkcEnheNkaSuvWmK9a8kObGeX2TysDfLIWpLu8cN0ls8j6DzisXp
BDfuoPZXpuM5sC7cQ5+dyLjwdJssZdX8EazX+HUcfVSqPOflGmHK+io3R9K/
B7jgfm2c39xtt99JyctKxvsN32aK8yRqZuTsd6OjhnA/wP0m8u/G/U4qWGSK
qx6g7rqmiOeclzRBO//SRTGjVlFKbtk67H/pbwj2dwnJ0I7nUqPURx3Fc846
9Q19R1Q8l7K9JuXm+WO43x1faPt9Wl0wMactTC3+twfn//brNO0eFfNYM9bs
o6wWt8c5fV5dxDlfFjM7HC4Rp+yeMMPz3HhbAt3GvsPWg+UAxeqcMq5LkWrA
dYXfzpSEJPBSJYZfos9YfEG/03lP+N5LfUqhGOC+4p6P4brdx97vYpuSp3A2
0M7/933a+V+USljfYSZP3cBm70R68h5l7PP97kLJi1FhasOeDqx/3iCP9VyZ
6qVaXxmoj9atwvvyGkxDrzstbuIg8FeVkUMbPWYmHL3piRn7vQB16snITTgn
77lO7L9B69vj/WKbqE/zVHHOe79pc/aUbEtPqZeimnaH4flsJU7gOdQ0dti8
kD5AjR5gxTn/OBtjn6p3dPzl00M545Ml2N//fjw6EWT586m4HoWj/Tq6n0sh
usXLW9tbvPdRz7VM4Jwde27iujFz27uKGaQo61mTcB4KUYFe0J38cIRvO2VN
20a8Xz4L2vPvE2G/ZzJWjEpfoY/1xoWD6Ed2Tf5U3raPsq8oinZufw6gm1KN
7hdVslDDWS5gH7lIVtr5r+bvmq8WpEx/l8Y+zvTT6Krsf5fefpxWPfTroz3p
x1oG8Bw2Dl0OCg0QogaqirmQXlHsgu+p/8m83dtDDCkb8ySxj5r4d+xzrOlA
wbuL26i32rjwPNUzPbCPdXzRPpba/VRRyTNY/4HooT0nWUP62w8KUa5qD+H8
Khvl0KnvQ6RsJrdRnxSnoMN2QG8Xi+e5wSFDfWwjgeepGPIRPeLvU+t/k5zU
nK6ztH3R0fr3TEkraFpuoGalDeO9QFMU3gu7weKdQXUVylTgAvYf8tmB9V8U
e+WuB4hQ7K/XY5/WDfnoH4MyGIxNpalXrV7iuvLafuhXtetjyj//yPFJscY+
bYzH0I/O3DrBJbWZai2fge+XdyY7ngP91sOiF74AxVFPBeuNSk9h/a6lUmYn
wV2U/A56XLepZAE9fJXWxRLz3ZTVyen4HDqe2o7zlzBHmnDZ7KEmPJjB+t0G
WVi/PujE3HwmhVJW4Yp+8XYrulswb/u0ghxVfCgY1w0s0ELvfXA0oDdBieJ6
2Qp9zJI2v4IN0bO1XYh6xqcB55dMY8L5x/pMlMvKxagvs8/jPJ7fVXAeU5Wb
HcdKV+635Qreu6sI7XNGzW9xp0jSeur5cxvw801k+CXWH9g7+pNPW5lq/dQT
57y5owXXlVfu1Bll3kOtVvmI8zxxUUNPv+vUGDh7gDo9rnWFdEPKF+x/UntO
VmtwO6Us1Qj7J5t4Yf8bzFfUQsLFKdxSyjg/T7cc1id5Do5/juOj3JTegnMm
qt9Fv5R3j/KGR5mazdWE92tn74LrLvoWdrOwylPeuN/HObOjmtHVE+45TTkI
UNa+uI/v15smRvTiuJoEugkByo57HVi/J4RAnzx2mOPB6d0UaUY9rL9L3Yje
qLdBxMWFl6ohU4Pr0q9zRX88naz//NUuSuGpLOxjIF+GXtS49ViAgCwlS5i2
7pk52rp859cypwpJU7YfPYR9TMQj0L91yEbsvLWeosKjgee5wfokuveXmcIX
japUmUDa8zkSSns+tR/fHzzDLkrlTZrCPgzpN7D+7Za1lzTjxCkeG5WwD4MN
7efL5N1FzhS6XdTYnBM4D8cyB3plYHnX5xE+6j2zbLwXgzxbvBemWav99Wq7
qJIvefF9VBKhfT4z+Z3YmF1DR/XxvID77dXuxj63TJgmGs7vpDS9lXvtPD4N
GpF3dBu3pBBOh2q9V+UPq0be10J3StXSIF0nYtiPTW87ZeCtBHrqZnoD0rVC
U1LD7i7kDL/hQ/+Pm2pC+t2T0UclP0lTuYdF0Wd53huS3j1w5+7PTkaqYSvN
TSXi0G1ZRYO0f0tQn27dQZsn2Fyf9ECH60oHmASpWg9k0I9zXdQjPURGnKkn
ZDBnn7AkelJpKdbbXhFbrxhJTxFLl0ZnOVGI9eXe05xtu1mpNUVi6G3C9Ti/
hTNXuIEeE0VJjBe96LSGGemKKelD6Q2C1Ow1+9FfsoocJF1og4X1112SlEUt
2jw3vO9j//Qzd8Sa/UQoJWFK6Amts9qkBzwISRlkl6fsZpVClwt1P0p6ojWL
7BmNjdSfF2l9FARvYh9Dg6xwAeWNlEtBtHqD2xpY/+tj8yqgMlDnw2nnX6O3
GuePu71scJpxVtV0hHaexx5F43m27v+zQ4iem7KQq4wu03oT5zmjdGKIc4Gf
wq68C92iQhHvPSU0OljbdQPFSJFWf9IzBesdr2dn5YrJUp5do52zqlU0nrPh
rbdvg7fwUVqSaX1209FjH3Uzt+NDJQuq4avl0X1au4+QzrY7b1nBkY3Kr0eb
f1htO84/rjOzxXMTC+XEFtp9+Qcy436Lou/VJ57cSGE9R/PdMcN4PvJjG4ZL
EydVNW7tRD8llI/1oxbPdSTZVlO8dFTRY9+YHiK9b4tfccAeQcqiKu08hQVs
sT4tYLUk25Y+VcYU2rl9cc7Bc+uYctfNCi5V5Q2kPYfWW8Vwv/tNnwfEs/JT
zdVo6ypeb8U+kS3vlVmWBlWN9tLcxq4TvSxqyK42P011/xOaOwpkoD/yO+wU
KsNKHZGguX9JP7q94uUhzyhOqtY6mmcd/IGuf0f12dledqrdpm3oF/5cNSLd
zNDlkpPdNorsLyF0esEzxqRvv9E/ayo+rzp9g/Zc1RxywHPz61wnu82dlRK0
SZz2vH1MwPNPjbr8qcufmcKbSnsvjKza0ZklA3w/r3zvdZGj3XsLRz6eg3Rf
0J/Cr+upskO0ewyk9KN3/+oRlH/zN4c5ltbfYvUV7CPY+v6PSh0DlbqVVm8m
rIJeEuv6uWxgRPVhCu1+E2aLcE6i8wP9ecMFVbEQ2jlwG6TgOfT1Naz6bjqa
syuZNo9+mQ+uu+7MgN5Djv+okRTaPbIbfcd7fJP+mMU8mZPKJ097Dh+sK8fn
sHlvgVXayvNQ7Uhbd21DN66rf65fb+DZOsqmAdpzUjsuiOs2rg2Lb9Dnpsro
0eoddvzE+usTqo+52JiopUy0fZXt18R9PeWWfqOgzEQlPtGenw2nt+Ccu8pu
JvxrWFY9zkHb19a109j/L8OeZP0nLNRf+2i+y7od/SXLpb1RL4dVufbvQf82
cPUwznN49SqpP0IUeea96MTgBx28RxdRz0MneSmPGGmf59MEge9jdcmnradY
+CnnxmjzWx2Lx/nHuJe04g1ZKX9aaPfemVSO8zv+DN19jnkVdcaaVn+LcRTr
ldYp7BGUpaNqp9D2dd9tE+5rYhcf270NvarvKmTRWzVbcd24m83b+qnjOX4l
tOfhz2Mr7L/G2H+7Rdec6olJWp8Ht/7hfv8kfvihvmsmZ38L7X6tg5ywfyyz
R1T2PlaKtBbtXt56nsP6ygUfHmWP1dRcIZpvD7iFrnpr6qm3HxclQJ7Wf6OJ
PvZJtlIIcR9cT629R+t/pPQt+oua1a8eBKylHrxA+xzLsBPB+emLhucKLRtU
E4xp79FDdzc8h71fX8l37VlN3fIf7XxiddhxXV691BeLH9ZQrexp5yn88Q/u
16jw1mbV5U2Urfwi6M8zevD93XpBwS3LhoPifJI2vzjFAvvU8ih17jjOQt0k
Rnueh1IW8HlmNb5rcmH1cs6xhP8/b+1/cX6N2z7XCl6vp9zKpZ1zcoINrnt2
f2ebqQ0d9QwT7RyKqs5gvbVip2vE17WUcVba89bH9x3XVQ7ZmVLe3pezfTvN
a64MoltcGL4tvYWBsjSqgA7RzPgeVW/vu8syOaPKx0Wbp+U04LoDGpY/WzwH
VVk+0J4H+tkRPM/kO4ZZjCOs1PoZ2vlfu3wZ57nxSebu4yBGyj0O2vy2iinY
RzU+Rq1WbTP16yjNh90M0W8bzyyozrFSjmvQzk3B4Dztc7i9gqM+bx21uZN2
/hx+pVjf6Fi0Rqt3OUdnLa2+fOgu7f2Sd+JLoStR/WxL81u2Juj0BMNjGYm+
nMZl2vPAHtWD+7XVusG7w2wddZ8P7Xx8i7KwnqV5ps3r5H/UNdto792Efgru
t7/oupfDhuGcTx7bwsn8lfmrVsxfMS/OBpE5JdujS3Jk/kogyr+EzF/xW/Wj
865OO0rmr/7EmKWT+Su9/W3oxzP4NMn8VXOYyTiZv9rufw9zUDy7HP4j81cH
U38vkfkr+7eJ6Gw9apZk/uq8xpsYMn9VKKSHOa4H8R4cZP6qec/SKJm/kud5
hvUO+o3GZP5qec8DMzJ/dfhNCa47Myf4ksxfvftreonMX/27dxTr/aRMs8j8
1a5PAW/I/NU3ohjrR2eEVMj81U+27vNk/krse+ABMqfUPKYlRuav/t47w07m
r5JlNqNnLRsmkPkrB7t/MmT+yrNUF/3BaN4Amb+6F5bDQeav1l6QQE8yer2d
zF/16cti/uo0ZQT9B2/1BzJ/ZdSjv57MX21IOYDeZ9XYTeavtj81LSDzV1Sn
BcxTpUccVifzV+eLrwmT+avFLQboTKLfZMn81bJfZwiZvwrba69O+larH0Nk
/urnfb0kMn/FndCA/eluXGQj81dBwoxKZP6qqIIxl8wpnf864Uvmrwz/bmUg
81fTIa0E6Wx0UUFk/ur9pOZTMn+lF5qBvtn+bW/u2SooXfrzk0e6BgTdatD7
3bZxkPmrqU8p82VptUD5+AP9YZHVUTJ/xfOnPZvMX+U+OoD+e2uvP5m/uvVp
Bw+Zv7ol5YDzOMVYcJD5K94NZz3J/NW24stY3/Ake2AsIhFcP+SUkvmrGc8F
9GaGVQOhOS+h+7rsKTJ/5Z63F/tMf++5TeavhCwrfpP5KwmTXZhTevKX/hCZ
vyo33FZP5q/+TSuif4sOmyDzV+m2Zulk/iprHaCXTY5j/mo/x5doMn9VbaWB
/kBzbMnrfR6s+3RumcxfTX5axHzUzXVfLpH5q33EBR0yfxXfzof19JFPDpD5
K7ljehQyf2VynJb7yj9v7U3mr2bmZRrI/NV7BwX0rz1ViWT+yjT9fiuZv4q5
tBs92exVGJm/sn0+FkjmryyBB/33k0rN4KPZ8OWgaBeZv5pKos6SOaXxcxkm
ZP7qUImkBZm/GhxuRI/PEjtD5q9SO3nTyPxVrGYl+vft7QVk/mr7Gt7nZP6q
IL8evUXNwovMXyk/fFdM5q8+NOejq342WibzVwV/NCzI/NWIWyv6YKYhkPkr
Y5uOGDJ/xTDVgH6j5gJB5q88UlsYnvZRYZ18ObrAmuslZP6qeVLUjsxfOa6n
ef46FkYyf+XhOXmCzF/d2l2BPk1fkcVr4wuFYYcWyfzVUmsc5pSuKvKb8gb4
QPtSaBeZvxpopPnZ3805Zy55wTiPTuvf8PsQWkjzj7m/w8j8VcSLfavJ/NW7
HJq3nNuTV3/YE/IWN+uQ+av2CporhcjsO5LvBmqWpevI/NVS2f/Xnd/zmMxf
fb3CGfkixQ/OJND8Vai2Apm/UidOu8T1+sNsOM13PSSMxp+fg4mLYVcvpjyG
0SKarxFVsY2MNYD8OX4DMn/16x3NT47NLFt6pEHxtfdSu9xjoK1brcKqbZp4
obTrydT6z+BN59WxSiAOHE0PorOcKLMXPJMCX9+WH6CXT4LyVC30SSk6QsM5
CcabQhZOi6SA9G9N9LUilR1FX+KAjan4inNbKsgXaqAfyGvz193+AcRfeCkN
GqZBaLI6ule/9YZgzUiwyRhfXPRKBylBmrN0d9/5T/8NqFWlFv24mQEJygfQ
W6X7A72/BsKmTT3HG3gzoeUBrf+jo47JQtkuYGJYfPDd1UxwyaegOzAHqyZw
lQCVy0nn594sOCumkSdqME2821JZruZbBDL+X1mTZagw9/AY+su6RNOa7ALY
Mj3yLeBMHkCNOjqn6ugwa1IeRP1hsrd2KgDXj3vQj6v2vB9qokJrh8jmr0eL
4PxNmpdVsV8xmsgCwrg4vry/GI5s0kX/nE0fMpz4GcTiWu4/lymFTAFZdDsh
+i8mNgmw43vYEr1MGQxF0uZ88L00epQ5ElZzsyVNd5XB0/od6EzGlr5PivzB
U4JfSEq1HAw2G6EniVxmHwyoheGnzOeN5QrAzuVADMvZKaJBUNSqwKYGjNIp
V7Xni2GpTxT98YIuE8HyDVYtLA9m3ioHVfkz6K2iFj2C4+VQsan1Z0h4FYiL
26OnSXbmPnAsgbp331MdFWvgJ6GO3vRim2N9VgEEd/tkTNnWQu7hI+hPeGKk
U10I0L24v+b8+Tq4su0o+mRCKru5YAb85PdqKZKoh82H7dCjbQI//vodB2tj
23ZEJtbDA1kT9AR7/6CHMcHQx1QpYP67HmwKz6O735o7TOaR1szHyZN5JLNo
iRHMC7G15pN5pEZPsVEyj1QVrYhOaVHPIfNIudK3W8g80sFBBfTFnUN3yTzS
KcmtjJHpKz/fDF+jx+s/2OihVA3Hsq4QZB5pINsV/db1O4VkHslRVk6NzCPt
uWCMfjnZVYnMI1Gqro2QeaTwy/3oExfflZB5pNhuOW4f91b4J5KMbn/1bSeZ
R1oVwg9kHilQ/RJ6SrLVOTKPdOTjHwbOwDZYW6qPrhPJFvS+NBF6H79+xXky
HOacuvB7mMArCeWXsfHwZ+rdpp6SKBjXHkTX4xaT6N/5CULF39uu/xEDuUat
6I9twdLq+AfgDkto/PTtI/Ak9dD+DlLaLruoHA2SG5/xRprGA78grX+l6mmr
Jy3hUDBQd3DuXiLYCo2in58rVXju8gbuvX7oYvM+CQLf0ProAb/O1oln8Nek
9cI+k2SI6aD1+ShwxEtu9yMYexSSbVqdDPO6/eias5JratxugBAlXHgvRwoc
0mqmfb+fdJil44sgRngWzI3Lggj4t7xs+mcabNbwmb0cCiM23ww1kE94QZzi
XrXKbMWNpTj3Rf4OJfrhuYPeodcE3ROaqwbu1NbPfU089J2+oOUXSnw4sRr9
mPlbw1NyLwn7C/B9MCOM8Auk9Wfi1rL6z+QZ8YBye757Npy4GEXrk+i4Pvq+
cgChFN8SKP4ngujcQfOvo6ayd5u9iP1Tq6Z98iIJo8+0Pr/S25KsOl2JcP5j
530PRxG86rR1t83uX/Xj+HmiWHNI+/bzKCI7k9ancssGtZ4fuQTX7dnC5/dW
vp9byKsfiJgGtd0+IivjETH5lWrm4unEpTZJ9KJcn+t1f7KJtOdF2WJeWURZ
AAWdkS9y/OrvLEJ7Iknk5PNsYpWOMjprWOpXrsh0Yv7E7T4jbYLYOquAHpAn
J6vGlEoQWjNBalW5xAfnrehDTeE3ki0TCIlsCcGq3Dwiqk8NPUvsoMI/vffE
Ezr+sMN38gnpXFH0h8kObzJ+vCaWBDL47Zbyidfy4ugtXEt3rrY9IGK2Ove/
OlBA9DrT+rS2Kc6CWBXhpyh+1bqNIJ4eNDM9MjwFHT87rUV2VxCqj26rv2Qv
IIYonOisbd/YBSdLCeuqWvPaX0WEF8t5dKWuVWttTYqJmgimn5+DS4kKYUDf
KN77Vup2AUGV9AudGy0n9PmaTEinnIr9eNMyl5ALb6KDkUqi6r4u1jdMOE9t
FfxCqL7SpFf3/UbsETyOrsVo+bZJNpU46Zac1FxVTQhMqqJvUZM6OMXygWAu
Vziy2qSG2OFsia7pWqL7rPIpIWxlt+1eYA0hlncYPS+5kkXnbgPh6r/kpXeu
mKh3Xfu4sHwS3oSarqETrieaXb9QG7+VE/vbJtGnpzZMGHjVEkXqYRxZfd8I
UZsX6KeXUy2f2VYTR+JXefvp1BLP5EWekK5fH/fI+EwFcW68omXbZB1xtOop
1n/Oyxd+w1hCRB471fbXqIE41Xsb601s/gQpnMonJH+HP4lybiT0a3dg/Y9f
60ReLH0hFp6ezbtzsonYzJSA7hY1nuY/nEQETYvuzvzTRNQFFaMPrHM6+Mwh
hAC6mxEXdZuJ9LQM9Dv/7ZNU3tlG1E+FOaa+rCS0I15JSOmv/B50/tyltXYt
xHK/cXcGXy3xy6AE/ao1+43s003EbrbT1WwZ9YTdojl6WZq+ZQlrA9F2h5td
bEMTsf3FPknSzULCgz1caglNBTkjf4kWIvynNjrHoTWPN9hUEVz3dYQ+zrQS
pWZj2Gded8trsxslxAm2ap3mS+1Eam0Uuo/+94MBynlEokCP5FX/DqJemB29
yyTzUeGzNML8iIrloHEnEWpuh/0viT665t8TRgy++MFPJToJwvQR1svqKv/w
etANb5cZKOf31kJwh/hzuTtjxDvrTr1Z2044smD2Zul1A8xdYUVXu3WG9e5M
G2gkdxR6UJvhd+frYNJH5W28Zra3wOr9Fx6zB7eBweIm9NPZ2/23szVC2ayb
dhxvJ5xRPP+M9KEp5ofdKbWQ9mNdyAW1bpALfYE+1uvHdgUq4e9/fftU7vXA
3LUBXPf1xZa734lC+OFxivpOrhcYT0mga/5UXdu0lAWcmeV3vsb2gmf8dvQl
jZ5+D6MoeDMbW8s51gstC2ew/79nt/k+qPZBLEPtDPfUys/NnZ2yL4xHiMLZ
+Hf/GHuBzo174662ZjBsOILOy8zTskOuGwYfnD32z7kd8kdV0A0VXUW7Z9rh
UtO+AKaiLlhDXZIhvUFhVb+BZQuoFD2w+9XUAx3ff0qTzt7OEtTg0gA3K+nO
7OPug4vcO7F+6y2OfZ/MagD8vvtd4OuHZT3aupb/fLMq6Ethw/PiXZShfrgk
vBr91ua5rZ0bqfA0eHjT/qsDkM4wgv3dDp1tOPYwBnrDqboZXwcgMNQE64np
wwuzbgMwNqI+0GrTBJ6/HyXu6Bkk1lR9zCsQ7IetmTb5PFVtoPv2WgLpd3Yf
0ZN+2Atv2TJ1M3q6oOqoJNYb+9sX+Fzohkd+On2XtXuBbzOtzz9u939+Vu3w
RNZ9YXS8DzbctY4n3b+S5Rw/QzPsd9YNjyVzobu4kkiPD7PvpLOsg4Pn4gBu
D0LrGgX0b3WNLY1/y+FDvvkXZosheH9fHvswnW+UuzGUCwK3DRmU54fAIvo2
zsl8+4V6h/1HSJEc/zd4eBiqH0lhveIFrp++7kPQkz3bNvKsBTYURrAmbu8n
dDkckrctD4BJb7XYqUud8CCT5z/S59jqSq7t64dTlxUDb//sgXz74+tIZ90i
LMeo2gsjDmmjeX/64PareqzflLWod4ytC/62POMXKhoApUYN7H/KoV3nq3or
1BY6dU/oDYHzsRasv69+/b+TEg2QOJXbqv10GEoFS9FjPVv2JIRWAXsnG5+K
7wjonlJBDy2LZNrqnw/3dIlPCXtGwespBecpPFUzPUUfD8svuZW8no5CnKIz
egnjQiD5dx19U8F14jq9xOoStRsSN/dTQg9SAkifs6tnJZ2VNdJUaaMa5Yp0
Bf59qFM+Zw3pXVPqgvzmlpRiB3Xsszll33+k88HlN6WSxpSjviH+pB+pn15P
+vOTw+e8dZSpfvHbH5N+4xkXemnKxWwDz+PUb7t8nuC/I7w2yUZ6z+0gGRUD
OcrBL+/QN1uUo28osvh65rchha7HDfu82evHQbry415b48W5nC9tdejH7FnQ
x0//+LrTRppC7/ED+8itOox9ljf/4pCWVaJ4tp/Gf6eodk6TgfSjxnEpCVdt
KBBojH0e8n3DPo8ZUgPM6YSpfqrufqQ7rYvdQDrleGFP8d5D1J9a3tg/r3cK
+5s6lI86ch6gHJEg0N+df4V+5PCGmyWv1CiPyhewf4pKBTvpzbJWdTXPDlD8
jp7A+sRRBXSHxN+hp4WYKAqBN7E+JCME55mXrX+Z2LKJkpRejs7Cx41uXXlc
PIVPiFLea4RzGjydwDkf16es2fRVk9qa4Y73FVHbhfc+/TjwhP34AcqX7/N4
X/3VFngv1232CTaZ7aBaHzJF3/nMjJP07xzjfMsJhtR/uddx3cBHb3HdtTJr
uewLNSlndiigXw1chX18wvorjPuPUI+lW+O+cqwlcF81np+UbFVUKZJOa9Fb
a1LQtc+XfLu9R4IqJcOBbjoWi66bIuT2vWk/5d6ZQuyf/0sA1023U3q9JLWO
UunT4Et6+WDKRtIjdo5yCFIPUg9xVuPzzDr1A5/PfUu84vlKhpSNqlO4r3Bn
O5wzX2KVTe9GDmqxxyOsd0y3wOf/fkLrstILTcoBFXv0csIb/V/klaa6h3qU
zZIM6IbfpfB+12xReLj9hyq1VuIlzqnaZINzBqeal8rZL6kqfLLEfbUnyOC+
fOM2WIYelqQKjDRhffJWetr9Ro8erblwgML1/gD6a4Yx9KKidVe7yuUpxQmL
6I+0S7GP8y2zr7lLq6mDF0uw/w1/P5xHVyrmlvunvRQLsavovwY4sX51sLTD
Wg1dqtGT33gOmnOWeA69in5a40L61PpX+9Ethl3w3om3x58F9bNTB+2vom9u
k0HXO/1RiuOCGtVkF+05lBijPYfLcx3/FPcIUeTtjqH/LqpEF0teeGOorEq5
48WMHuWtgesmv3gv6KNNoTRtCsA5/6X24fw/42z5Kpi3UC+/z0SP8I1EV2gO
09M/DlS1Jtr9Nmd/x/v97PpJ6vioNsVTeDPWb7oUgvuVONt/eODpQcqfxLfo
gT5V2IdFbrUpV58Y9fKObHxfFMV08H3p0dQUus67m2pzfwbnrHnSRHs+l52d
0u9wUh75cGOfy4xh6Ek+wVI8a5Wox9N3o69zuoK+00hGnO8GHWVbxFl0nqZt
6Pa1fsnurXKUASER9MrzfugFFvU7+XklKO+CF3Ffb9Z+wn2d4eWMOTayl/LW
vQbrLc674fz37pz6UH5SgdLhXoj30uOSgOeZl7bHPjFZnWr/7xPOPzCjieff
/G2Q0dNzP1Ve7hS6Q0AOutzGmS+mE7soX75GopvVGqJL8OiY6cxwUCqHotCD
lvXRr1aNe1jEU6jJ/cPoxKcRnP/9Sw/fqO+KFBs5e5zT3oMH/ZPT3PFvFBVq
0NpL6L8v8qEXVTtcyd4iR7l9Yg7nz2w8jfN/fXbNx7hci6r7Zhr7n4juwHq9
u493bq/eTdlzfR268CJg/Ynt/FJTNyWoiuFHaV7ViHP2/FazbDslSf1Ml42+
iU4KXebO+Nj7rcrUjmeFOE/YXACe5+a7TTP5SsLURZNs9IDKt7SfO048jbJT
DFTtmfPoxspCOE/k9lV2Y2pcFDvNK9i/8kEU9k/QfasveFiBQgRy4OftwQlf
vMeHERH/6PLlqTzXv+B+s4SpOL/9/K+jX6oPUH8xncQ+66dysY/iw6cFTFv3
U/3pP+C6djdycJ7r2gdXnS6VpHQkGOBz8mN3Gn4ulb1wnuJL2kTdHEn7XF3w
p32uVrw8Nh2/fR3VhP43eu6lA9jnEnEraY8mDyVngQ99ZvE51j8+rLXd13E1
NfBlFLpIST7Wf/wvdEbMrUa1JakLffrIBfRH5su27OskqfzrL9Peu9rN2Od/
LZx5OFVdG8YzewvJkESpQ8YkU1LOeZQGUTI0mIUUkUpKFIqUlKG8GV6UDJki
SZNhb7NkykyIzAqHRBp9Z639/fu71vXsffZ13fvc+1nPur0H3051z6gyek2o
+nMc0ZjT9/4nI3BuG6H2+Bj+vcGGhfj3rmD2lgXmyjKeuYVjbu3hgXlrdesG
fT46MclzCNeR0NXFdT63NK3Z4chOOJ2SoZ5nXRp+L7F5KCad36HF+JzghJ/P
KcUo/Hy8830uM0/JEJIF1Pvhj18srhOsf89QxV+dEehP+ROlHsqfGH9t6H40
LcG4ZMiG63g4q+Hf+2aF649D69iImxPqlN+Ypf7v5uq5Q9+sl2JYZDTr+ep3
gYeKC7dc7iAwcp1CY1uHwStzheRiQDtY27+ezrz5CYziFst0DcZgObe0w8uL
LaBWu9t1jfFHkK8hk/jPf4Zvb+cNDyi/hz8+yVJ5ql3wKbJDdq/+BLyEWdn9
DjXQqgkdUNECssfqlzAqJiHxuI3q+dVloN+94UyBah08Yv9VazUwBX3N7Byd
6i/AYWFoICegBM6JJqduesiEK/YX5StKo0GX3ik57J8EfuoG1ecWmJDQVM8m
WpsE95fp/nq/KxvmnVIXhSaZ5FnV3Mh1D17DQENopq9SOXiE+mzR9GWSE+Gi
5v38neCQHvnSNnYAcs0ytoWvGoW26KKpUdk2uF/AftPdrB/4PdKEnzqPw7zr
+lMX/zRB6W9jI/v8bli2dHbMy/kLGN23IiW06iGyPsvz66l2qHkZeOf5ikkY
H+ixzROrgjf/kA90ut9Da51oJN1uCnS0tWcVTxTDu+zI817KVdDVZ5EpeIAJ
85eM+1uuPYFbaxpGxYl86A03yDXuZEKnN6nJN9sPbqm63QXSX2BPxdIb7i9v
wcqN9v2X7XJh58DvmLDZApA5wTYTV8ok922TbhitqoQ8qUFT9bn38I8jXXoP
67p+C78zE8ki6BjluLDgUAUW6424V7Ou694z9+nLpizIT4g1mVZ8ASd3qYZa
ojwrs8GuJ88vQ2V62PDBe0Gg6DKQh3KWLDhOcV/jfQYVYTu3id4sgLxhu9xU
1nW9dYTf0QVL4HCCceEZzhpwFnooOC/FJJ+tX7L9q0s1pMwGHQ3Y3gx2ixEj
dMkp0vV4uPMBnwYIc5XtyynsgJ+qHgoJbybIV79v/Qm53wzGdGeVMzM9wEW7
LWH1+zMpYCWwK/p5G2h2pXEojPSDSnHo3u7xMTJOwl43L6MZHq4YZjQL9sJh
YVvjM9OfYSwmdplGagP479u+Rqy3A4S2Fre3PZ0A/TS1mfGoasj32XHnBuv7
LNFz1vCNyBRIPVwx+5pRAq6KzMiW/TXwK1DWrRbtG8Z7vbM8+Qwu61XW1q8u
hPMHe4eGCCYc2TxZ49t+FW4UK5qraodA4qOOzfyLTHjmELyYdDaT/LCSEVAe
lU+6ZYvv8f/AhFrewkZRhyJyyUfazLrVVWRFzJRzlgmT9T+UTguIrSSz71hd
62p6T1oWyqzccmIKNr9qum5K1JGHx8Xgm307GaT1UfXb6km4/OuiPosDcBIi
LA5O+4voLE66fBExCaxoAi5B9bTBrG7gU1AUGDr/hdTZ/HJe/GsrHCOsGb4H
++HIBlJf+uI4mXSpPPgj67lkeNker74/AEZDfx5/khslfytf33LL7wPQGnwM
oh4NgYnNTVfjiCHynkSO9+vaHjixtE3J0W0ETG4+aQt99YnsbYq4zE98hKAp
qDk7PQqdA2MyEgd6SRvex/RkuX4wDPOr65MfhxulWct+1beTTw7ntSkW90Pm
8KtXJSs+g0D+iKkRN+t3l4rxFqz5BNOPLsTT0lk++u4DHm2xMjLVtA3Pl5ZM
OeI5UrotM9NlvwHDdXQHnl/dNpZMnQuoyJ1TddIlDF5l47nTe9nUuRWzh3lP
Z3foMTxpqZgnc73CfM59mY/LqlVEsYcRriNwORHX2eLqltcRocAwCtDBc7Pz
26m54tY/Yib+f+WJ+a98eL2qXR5ev8n+dANNRYFh+84Vz80uCTGl5t7rRHZa
/lUjDt3mxDy72B3zIOXMyEs5UowDA8LUnG3oKcyv1PVeoOdxExr91LzN43UZ
eN4mac8u9Sp3KeLvEWp+bHp7AZ4DeWuxuT72jwSjuIuaD+k/PYP5FG9A8w7h
meL2/89xGZ2l5rh0sh2dHV900tuXUXMv+WnNeO7lsVRdaVMSLyEjTs0RJd7+
B6+/edWugPlFkJhfQ83tKHZTc7CdfuOaepJsBN9Faj17XD+eR/reIfPzCedC
cXoItf7wnAJe3yZo2vSUc54u6Eld9/SbFGr+59yWcuaSyWLibP1upF/Ph1V7
kH4fuR44gPRr8SOcE+n3rN5ncaTfe8IjHUi//fq0EqTfYz2mDki/e/tKDyH9
mmofnkT6dbMeiEH67eVeifWrZPaaifQbyiMl0MDSrzRnxiDS7yGf+kNIvzVv
vd9uZumXv3MA63fM3lOFpV/Q2ik5VBaVD7t07PRZ+iV/jHjJrXQoApX5verr
V1fB4ZzK4yz9kpOBkaIs/UL4i3vnWPqFzJNjq1j6JU0sI08gna4aaldCOqUV
ntFCOvU5XpjReCsZtgnVCPWczgH/rkkihPVcxlOtr73RiyJpclV1TkcekVXr
LcHmBxM2VV6ysmrLJ0eicgUktEvI2PAvf0KSmLDE6leC4UQpGRbXXd05Xkv+
GFqTvXZsCoSemxbUataQM+mbk2IetZBtoeTLw3WTEOxR0y676j051GZSVCrY
RRr6qqT+ZzoBiSar1044tMCm066+hqofwepVPLev/2fST58Ue3+2HYzdOLgC
vD9B7LX6/anmY+SXDfMLtK1dcJJbUtI1eRB2ztFcqoaGydVX7sfx5XbDe4n1
Nb7hwzDoOF0aojRIusf3Uz7/eQD2G2KVK754hxgTUq+TsZ/JlanA/qS51/2p
VPsOhuq8GfYn7AKN2EexVTl1/3dZkshR08Z+RsC8G/uihY1Zsv6SKgxrTgtc
v7D/MPZRikunT+w7oU6Md3RgHmQVga+7/IZs4G6fdYTXxWu4/vDmYFz/dyyX
iaKCHOOtoS71nfLbFvurtbauahUJqsTWlquY9175ie9z6Uj0OdJxli7PWYnr
3Pu1FtcJVvnw9rvYasYupQBjlBdqZ2wziPJChdWTFkVsp0A8RjUG5Yv2qLtC
ukU52IQXYr7dpQrniB4VuJOFckTt6ATm0rUVXLnStaBX3OyBckStvmVhvqCs
cBzliHbtTQ5EOaImsd2YHxT/0Y1yRLXbxcJQjmjIjyrM5TjNR1GOaInD8TUo
RzSirR1zt8alpShHdI3Gg9soR5R40ID5dOMLl1aNbJDl+0JDOaLVvCWY/w67
a0PriAKe9uvlKEe0YqfiEpQjanicXefK1iYQbdS8aDJdAZ3WZfeQz+mSGl6h
M9YI69sjU8YT38KQ3QjmP73yH5T9rIObRl4pJEcdmNvWYG4nEWH+7lkNbOFu
eEYTaIRoGwJz7XJOrg9aZVDIW/pY5WcTPLbtw9yAt/LaD50iKEw0u3CsvxlK
NIWxv9qzvdZY7NlzUNO7b7gQ0sJ6Dh14vdbYKmmhhlQYPh96q+d7Cxyls+P1
VvMKEyjPVkYpoxPl2R4cWyaO8mDn9Bt0S7kqwP3e9AzKv716eynmBmF6nqFr
S8ErqOUryrk1+i6GecvDVrOGHAKi2J12qw+WgM5hfswnzwVtTyILwFFWPA3l
3PZKUvVDTb77s195CRzBXz+hnNtuU4pf6XTPQzm3naeNPfV+VsDuvWsxPzXo
/QDl3M4EyrahnNszawWo+/wexXE9IBE0ordfcTpbBR5RHJhridxeHtp8C5oX
+LNpZVUQObu4CnF+U4l4tB+tdMPEGO1H63OcwO/lHt5vc2j/OuvFw1q0f330
3THM/yZkeaJ9ar183UK0T50XaU3NI7KLPEf71JnX/BSfNGTClmJzzJ2u/TmL
9qn7lk+dQfvU3mLHMdfPf5OC9qml1C1y0D41TdcW8xJ2y3C0T+2YcPAp2qfm
t6auq3FulQfap06Xu5+A9qndCSfMa+u9+tA+9UmeXXNon/qjCbX+tq9CNNqn
7siPsUT71Pa0o5j3XTDLRf3w1cbflFA/PNb/Ne4nL9N7ifvnXHc6cP9cS5GJ
+9KyAt9rUZ9ce9nUTdQnVw50Xo64pPawBuqThze1DKM+uZbqPtwPl/F+twf1
yX9e4liH+uTWrZ0UL/h3H+qTn0iL+Ij65IZepgKIyxdECqI+uY27fCfqk0f0
puP1PLkfD6A++d3XtatQn1xpQgWvj9/Zpoz65M1fQ9NQn5y97Dzm2q+XpDE5
skGPtmsL6pNP2Slg/t3NGvuT9Ix47CuEBOmacYtChPTYSexn9ttvwH4mcnxo
YuKpKkHc/47PeT1JFcG+6KXZpQ6tDmOGsNFB7E9GTahzshVsfJyttuqEuwUb
Pr8T4fWLOh/X57X0qZQ0ER93C3NPaKfOb+6vc+XJk2YU3f2D62x8rYh5Sgpn
XrqhLFHz9hq+T4ML9zAv0lFuYNsqzhgWGcV8pIY6v9zwoUjGvnU5ofJtBPP2
ZooPya0UtAj8Ta+xoHzF9IFc7CtyGXvuaIX1FauayVJzDZ4XsY+aNhlOz19g
FnemUnO2PZY62OdwOjzgU7ZcyfAUpXxLzn82uM5YYg5z/i038W4p5aMO+VHn
IALmY1v2c4sQqqspzvbiM+Z56kfDxdzZCedOak5YaL0lnvvt8vpssJWbmwAn
6j61dmRR5wUOWlY8k56nn7Wn7ueEvyWus2Vjue22urHicFeqfmpXFeaP/k24
UfQtrjjvutsTpN9VlTFxSL9HOyi/6DK0SSaNpd8Jqb1MpN8uaynMxQyt7iD9
PhaVn0T6Xf6LRvm/uOk6pN/LF+8bI/0OcVJ1+Jc1xCL9vvf7Jx7p1+TEOswZ
+gkZSL/x++sHkX5TiqnnKxQf/Rzpd5NcTTXSb5AmVWeX2NICpN/k4P2vkH5z
RKnrBhIbl6ux9PtvxsRyc5Z+X9pRnCeaXtDI0q8Z27AH0m+kBcWb6q2KUF6T
5E3+RY2IBpIx/3cU5R3VJ+YcRflOz8AP5R2Ri9+cMY+sX1aFcpys8t2bUY6T
fHc85uXC1YEox0lXVZ0T5TjZFZmM4dynrI4fV7UbSa+sh7Uox2nIgQvzyOaM
TJTjRA810kc5TqfcbDEfuODoiHKcGgWFRsavdJI9X6/i+iuvBssFVhLkaKGu
CMpx+u9IMObcfLsqUY5Twm2tHSjHafcLNVxn34vTTqTMA/LRczselOP042MF
Xi+caIh9jmwqgX2Ocoj/tztSeozYTEvsQ6I+a2J/wj7Lpc5Zv5mYTtmNOU/6
EcztyzfejalYSTx+SPW3vwDV32a3dZtRXqtHJDr8xX1jA55B7JdiYt9HDosq
ECneanj9nAYH9kvvlmwp/rp5EzFjM415rVc3rq8mIL/pl/s6ovou5X/YpSj/
Exq3hNYerMJoEnmFud1TDczv5ou7XF+xlWGTYIHvczhUC9f5l0+HfkaBhzGX
JMzY/OA1dHdHf7ylVA6iBQ/DNHyZZP8GVd4elk92/dfIiMnyyYVC3KnIJ/f9
zH4nxPKZ1QZHHwSwfGZQf9PKQJbPvOGzG79/ArrH8PtB90moIp9hGd1XiNJR
pvgU1pFlxO44czo/YdjBJ4Xq3zov7o/qG9+tSUH1g55Wr7VnfXfvcNMtWWR9
d5urTojMsL67ae4x2E+uWzuLn4/agUB5t2Ql4n+cMVPH
            "]], {
          DisplayFunction -> Identity, Axes -> True, DisplayFunction :> 
           Identity, FaceGridsStyle -> Automatic, Method -> {}, 
           PlotRange -> {{-0.8067224167583664, 
            1.2443110347131079`}, {-0.9169525481046421, 
            1.4143330471708688`}, {3.138614994663922, 3.9360320548988814`}}, 
           PlotRangePadding -> {
             Scaled[0.02], 
             Scaled[0.02], 
             Scaled[0.02]}, Ticks -> {Automatic, Automatic, Automatic}}]}, 
      "ControllerVariables" :> {
        Hold[$CellContext`dof$$, $CellContext`dof$1075$$, 0], 
        Hold[$CellContext`r1$$, $CellContext`r1$1076$$, 0], 
        Hold[$CellContext`r2$$, $CellContext`r2$1077$$, 0], 
        Hold[$CellContext`r3$$, $CellContext`r3$1078$$, 0], 
        Hold[$CellContext`Type$$, $CellContext`Type$1079$$, False]}, 
      "OtherVariables" :> {
       Typeset`show$$, Typeset`bookmarkList$$, Typeset`bookmarkMode$$, 
        Typeset`animator$$, Typeset`animvar$$, Typeset`name$$, 
        Typeset`specs$$, Typeset`size$$, Typeset`update$$, Typeset`initDone$$,
         Typeset`skipInitDone$$}, "Body" :> 
      Module[{$CellContext`Ad$, $CellContext`Td$, $CellContext`a$, \
$CellContext`d$, $CellContext`theta$, $CellContext`alpha$, \
$CellContext`jointtype$, $CellContext`pm$}, $CellContext`pm$ = {
          Part[$CellContext`r1$$, 1] + 
           Part[$CellContext`params$$, 1] (Part[$CellContext`r1$$, 2] - 
             Part[$CellContext`r1$$, 1]), Part[$CellContext`r2$$, 1] + 
           Part[$CellContext`params$$, 2] (Part[$CellContext`r2$$, 2] - 
             Part[$CellContext`r2$$, 1]), Part[$CellContext`r3$$, 1] + 
           Part[$CellContext`params$$, 3] (Part[$CellContext`r3$$, 2] - 
             Part[$CellContext`r3$$, 1])}; Table[{
            $CellContext`jointtype$[$CellContext`i], 
            $CellContext`a$[$CellContext`i], 
            $CellContext`alpha$[$CellContext`i], 
            $CellContext`d$[$CellContext`i], 
            $CellContext`theta$[$CellContext`i]} = Part[$CellContext`Type$$, 
            Span[1, All], $CellContext`i], {$CellContext`i, 
           1, $CellContext`dof$$}]; If[
          
          Or[$CellContext`TypeOld$$ != $CellContext`Type$$, \
$CellContext`r1old$$ != $CellContext`r1$$, $CellContext`r2old$$ != \
$CellContext`r2$$, $CellContext`r3old$$ != $CellContext`r3$$], \
$CellContext`TypeOld$$ = $CellContext`Type$$; $CellContext`r1old$$ = \
$CellContext`r1$$; $CellContext`r2old$$ = $CellContext`r2$$; \
$CellContext`r3old$$ = $CellContext`r3$$; 
          Module[{$CellContext`o3coords$, $CellContext`p1$, $CellContext`p2$, \
$CellContext`p3$}, $CellContext`o3coords$[
               Pattern[$CellContext`p1$, 
                Blank[]], 
               Pattern[$CellContext`p2$, 
                Blank[]], 
               Pattern[$CellContext`p3$, 
                Blank[]]] = 
             Module[{$CellContext`p$ = {$CellContext`p1$, $CellContext`p2$, \
$CellContext`p3$}}, 
               Part[
                Apply[Dot, 
                 Table[
                  If["p" == $CellContext`jointtype$[$CellContext`i], 
                   $CellContext`dhTransform[
                    Part[$CellContext`p$, $CellContext`i], 
                    $CellContext`a$[$CellContext`i], 
                    $CellContext`theta$[$CellContext`i], 
                    $CellContext`alpha$[$CellContext`i]], 
                   $CellContext`dhTransform[
                    $CellContext`d$[$CellContext`i], 
                    $CellContext`a$[$CellContext`i], 
                    Part[$CellContext`p$, $CellContext`i], 
                    $CellContext`alpha$[$CellContext`i]]], {$CellContext`i, 
                   1, $CellContext`dof$$}]], 1, 
                Span[1, 3], 4]]; $CellContext`workspace3D$$ = 
             ParametricPlot3D[{
                $CellContext`o3coords$[
                Part[$CellContext`r1$$, 1] + $CellContext`a$ (
                   Part[$CellContext`r1$$, 2] - Part[$CellContext`r1$$, 1]), 
                 Part[$CellContext`r2$$, 1] + $CellContext`b (
                   Part[$CellContext`r2$$, 2] - Part[$CellContext`r2$$, 1]), 
                 Part[$CellContext`r3$$, 1] + 
                 0 (Part[$CellContext`r3$$, 2] - Part[$CellContext`r3$$, 1])], 
                $CellContext`o3coords$[
                Part[$CellContext`r1$$, 1] + $CellContext`a$ (
                   Part[$CellContext`r1$$, 2] - Part[$CellContext`r1$$, 1]), 
                 Part[$CellContext`r2$$, 1] + $CellContext`b (
                   Part[$CellContext`r2$$, 2] - Part[$CellContext`r2$$, 1]), 
                 Part[$CellContext`r3$$, 1] + 
                 1 (Part[$CellContext`r3$$, 2] - Part[$CellContext`r3$$, 1])], 
                $CellContext`o3coords$[
                Part[$CellContext`r1$$, 1] + $CellContext`a$ (
                   Part[$CellContext`r1$$, 2] - Part[$CellContext`r1$$, 1]), 
                 Part[$CellContext`r2$$, 1] + 
                 0 (Part[$CellContext`r2$$, 2] - Part[$CellContext`r2$$, 1]), 
                 Part[$CellContext`r3$$, 1] + $CellContext`b (
                   Part[$CellContext`r3$$, 2] - 
                   Part[$CellContext`r3$$, 1])], 
                $CellContext`o3coords$[
                Part[$CellContext`r1$$, 1] + $CellContext`a$ (
                   Part[$CellContext`r1$$, 2] - Part[$CellContext`r1$$, 1]), 
                 Part[$CellContext`r2$$, 1] + 
                 1 (Part[$CellContext`r2$$, 2] - Part[$CellContext`r2$$, 1]), 
                 Part[$CellContext`r3$$, 1] + $CellContext`b (
                   Part[$CellContext`r3$$, 2] - 
                   Part[$CellContext`r3$$, 1])], 
                $CellContext`o3coords$[
                Part[$CellContext`r1$$, 1] + 
                 0 (Part[$CellContext`r1$$, 2] - Part[$CellContext`r1$$, 1]), 
                 Part[$CellContext`r2$$, 1] + $CellContext`a$ (
                   Part[$CellContext`r2$$, 2] - Part[$CellContext`r2$$, 1]), 
                 Part[$CellContext`r3$$, 1] + $CellContext`b (
                   Part[$CellContext`r3$$, 2] - 
                   Part[$CellContext`r3$$, 1])], 
                $CellContext`o3coords$[
                Part[$CellContext`r1$$, 1] + 
                 1 (Part[$CellContext`r1$$, 2] - Part[$CellContext`r1$$, 1]), 
                 Part[$CellContext`r2$$, 1] + $CellContext`a$ (
                   Part[$CellContext`r2$$, 2] - Part[$CellContext`r2$$, 1]), 
                 Part[$CellContext`r3$$, 1] + $CellContext`b (
                   Part[$CellContext`r3$$, 2] - 
                   Part[$CellContext`r3$$, 1])], 
                $CellContext`o3coords$[
                Part[$CellContext`r1$$, 1] + $CellContext`a$ (
                   Part[$CellContext`r1$$, 2] - Part[$CellContext`r1$$, 1]), 
                 Part[$CellContext`r2$$, 1] + $CellContext`b (
                   Part[$CellContext`r2$$, 2] - Part[$CellContext`r2$$, 1]), 
                 Part[$CellContext`r3$$, 1] + (1/2) (
                   Part[$CellContext`r3$$, 2] - 
                   Part[$CellContext`r3$$, 1])], 
                $CellContext`o3coords$[
                Part[$CellContext`r1$$, 1] + $CellContext`a$ (
                   Part[$CellContext`r1$$, 2] - Part[$CellContext`r1$$, 1]), 
                 Part[$CellContext`r2$$, 1] + (1/2) (
                   Part[$CellContext`r2$$, 2] - Part[$CellContext`r2$$, 1]), 
                 Part[$CellContext`r3$$, 1] + $CellContext`b (
                   Part[$CellContext`r3$$, 2] - 
                   Part[$CellContext`r3$$, 1])], 
                $CellContext`o3coords$[
                Part[$CellContext`r1$$, 1] + (1/2) (
                   Part[$CellContext`r1$$, 2] - Part[$CellContext`r1$$, 1]), 
                 Part[$CellContext`r2$$, 1] + $CellContext`a$ (
                   Part[$CellContext`r2$$, 2] - Part[$CellContext`r2$$, 1]), 
                 Part[$CellContext`r3$$, 1] + $CellContext`b (
                   Part[$CellContext`r3$$, 2] - 
                   Part[$CellContext`r3$$, 1])]}, {$CellContext`a$, 0, 
                1}, {$CellContext`b, 0, 1}, Mesh -> 1, BoundaryStyle -> Blue, 
               PerformanceGoal -> "Speed", PlotStyle -> Directive[LightBlue, 
                 Opacity[0.5]]]]; Null]; $CellContext`Ad$ = Table[
           If["p" == $CellContext`jointtype$[$CellContext`i], 
            $CellContext`dhTransform[
             Part[$CellContext`pm$, $CellContext`i], 
             $CellContext`a$[$CellContext`i], 
             $CellContext`theta$[$CellContext`i], 
             $CellContext`alpha$[$CellContext`i]], 
            $CellContext`dhTransform[
             $CellContext`d$[$CellContext`i], 
             $CellContext`a$[$CellContext`i], 
             Part[$CellContext`pm$, $CellContext`i], 
             $CellContext`alpha$[$CellContext`i]]], {$CellContext`i, 
            1, $CellContext`dof$$}]; $CellContext`Td$[1] = 
         Part[$CellContext`Ad$, 1]; 
        Table[$CellContext`Td$[$CellContext`i] = Chop[
            Dot[
             $CellContext`Td$[$CellContext`i - 1], 
             Part[$CellContext`Ad$, $CellContext`i]]], {$CellContext`i, 
           2, $CellContext`dof$$}]; Show[{
           Graphics3D[{{LightBrown, 
              Cylinder[{{0, 0, (-2)/5}, {0, 0, (-1)/5 - 1/20}}, 2.2]}, 
             If["r" == $CellContext`jointtype$[1], 
              $CellContext`drawJoint[
               $CellContext`jointtype$[1], 
               $CellContext`d$[1], 
               $CellContext`a$[1], 
               Part[$CellContext`pm$, 1]], 
              $CellContext`drawJoint[
               $CellContext`jointtype$[1], 
               Part[$CellContext`pm$, 1], 
               $CellContext`a$[1], 
               $CellContext`theta$[1]]], 
             Table[
              If["r" == $CellContext`jointtype$[$CellContext`i], 
               GeometricTransformation[
                $CellContext`drawJoint[
                 $CellContext`jointtype$[$CellContext`i], 
                 $CellContext`d$[$CellContext`i], 
                 $CellContext`a$[$CellContext`i], 
                 Part[$CellContext`pm$, $CellContext`i]], 
                $CellContext`Td$[$CellContext`i - 1]], 
               GeometricTransformation[
                $CellContext`drawJoint[
                 $CellContext`jointtype$[$CellContext`i], 
                 Part[$CellContext`pm$, $CellContext`i], 
                 $CellContext`a$[$CellContext`i], 
                 $CellContext`theta$[$CellContext`i]], 
                $CellContext`Td$[$CellContext`i - 1]]], {$CellContext`i, 
               2, $CellContext`dof$$}], 
             GeometricTransformation[
              $CellContext`drawWristCenter[0], 
              Chop[
               $CellContext`Td$[$CellContext`dof$$]]]}, SphericalRegion -> 
            True, ImageSize -> 425, Boxed -> 
            False], $CellContext`workspace3D$$}]], 
      "Specifications" :> {{{$CellContext`dof$$, 3}, 1, 3, 1, ControlType -> 
         None}, {{$CellContext`workspace3D$$, 
          Graphics3D[
           GraphicsComplex[CompressedData["
1:eJzt3FVYVG/YNnykRFrEIEQkJERBBQmBewhRRAERKUFSBEVCUFEBkZYWAREB
aVGR7rrpjiGGjqG7JCzEF97v857/zux8e8/xfFsex29jmHHNrHXOdV6zjpvY
aNwhJSEhuU5OQkK28y/PJP/TrJk1cFPgedTX8kmQU2176exrasysp12MT+g3
oKkyvfCeag55Ocd16VyKVXBYTM1BZ3sR+aCaN+UltyUwMnJAJbNxBfk9a/1E
bMQcYGd00zlr/A25J7PAaeNbUwArMbz2pXQN+bat7YdbPaMgd7h9on1gHbnK
icoYsvP9QFhpsYi9YAP5qmpLf+A9LHjctvdbl9om8tZzeONDXwsBrkLAiy2O
4E3N9rcbi1ZBiQmjLJvhOPgeupr9opIGY8oaOTLotwJMLQ5bMQlPI1+9ovXE
9PoS2KS14OnJnUMeYs8QqXlyHqSnNPNcmllEXiG374BL+jSQnlsW4729gtyZ
YjpuZGYceLzN8Hsauoq89b4fprF2BLjTJTdeffMN+Xbz+5KX2j1g9bT23Wfa
a8iPfc45Sy/TAvDxP/6GDxN8ukU09PVQHhjv2Pz7/cQ6cv3HMhbYT0ug4iRJ
rWE5HkgfzcCkttJi8nr1vk9FL4DbPK8fadFOIO+5F4dPEJ8DvwdC2fewTCPv
ku51j3SZBjnHOX+VDc4iT6aOfGv6fAI45Qv44VQXkHOO81K4iY+CMWcbFv+7
S8gpN30qXj0bAGXTYvRcjCvI/SK5i68Jd4Hz9zIZ6goJzmmaH5B2uQEYnlV6
FH16FXnDFZHEgZpsMHskXF/0AcG/7RtUk3aeBUGlhSUv+wYAR96qtGUWHcYq
9fOrooZpoPvB+bHqZzzyiMZBMo6mScAuqawie24c+dIeoeEs73HQMLfqFvRi
Erno8PqjGMpRwN6U/pHEZxq5Jsly6usPg2BDWzT4hNIscvKNcQ/vnm6gJO6V
4Jozhzy4v17FsA4LdLqVJXxb55E/yPh88Dl5DXhWHt1bH7yA3Npa8EyBTQY4
Pewq0/OD4BXUSU9Ok0wAtSskulr9OEAqYBw/fY8e0/PbQN/+8xignBawuM7X
j9xVQcy7gWUUXNPPLSw8Pox8tefbvELyMHAb6/3xYhiPfGyJSUqkoB/I+W15
4x+PIeebf2le/qgbuHrUntnoGkc+MXFa/O5CO2i9zZRqtT6B/Pqdk5+WNBvB
Te4K/eL2SeRKr2y4FpzLQYOarN+Je1PIj4x8nzZs+gRCpry3m4sJnnTfeGXD
exBcpbQP0hFqA0XqZ9bwG/SYjwqGc89i+8Hc3SJe+48dyK2kNqKWzHoBTlK5
qssZh3zfQ15/g0EckIxurLrF2Is8ndLPvI6+EzSLj1DMavUjt+7UqKagwAJx
ntxL164MIk+4ftfK6HUDYBkl7TacGEI+Fn3h5++6SvA8g257P+cIckvdP6xv
vfJBV4gWxYufBOdgDf05fCceLFHLS22dxyMvKfn2+lkuFvSaXfGNkKsE9zhO
W8ZcZ8B064t9+i3cBta/ZF407KtBbmUgivUNaQaV1z8YCgo1IDfrYBWeiGwA
qrqLac+Fm5FLfG0UMteoBes36b639bUiL/R5amVdVQnWGG463n6NRc64Scn3
8Fsp4Dgcs+j5sh059dlHcc/+5gKNUz3HLaQ6kFdaVWtyBH4BbScO6pcWEpx2
hTstyD8MrAqSvF37Q/CMn14pPwqywNVtXpNOh0SAjaMz3HZiwKzE5MkyP80E
BZGdTx8+T0Ee4dAkTUGaAUiUzhbUiKYin3mHDenJSAU+OQbSNGHpyGPP/5Rk
IPsEVpiy+FouZyIXpYgfVqVMApeflHm6BWchv4w5p3CqLhZY2CYxvo/ORi5S
WfecNzkCBIuFn66xykH+I5Ey+AI+EDgeJGt9+Ps/7uEkYMzrCFz3pD8evZSL
nP/i3lcHP5TCIgrOTymnM+CVa/NjT57sHBeVIw9+JhbDRDHx7VWDbOTlrcoR
YTcKYdFv3omD6nnIc0IfeFzIyoOOTgx6db8LCE49yCnZkA359U41vtUpRo7H
6UeWxWVAiZKtlP2GpchDKXwy3fw+Q6t0blIsFURu/SyXrfBFAow8rKepzF9O
eJwN4/Eyqwi4nel9h+8dwUmyz8gNsrjDZLp9A9E4gqu0s3ndNe+EN6VutV8/
WwMvSiSbs8vtvE8W9vQus3bAx9OHJy/cbkAudDPUTzUcC2PnhFnppVqQqxy4
JBdd1AKZ/Tk7m/WxyEndx4fK5xrgRki67+xMO3I2H8d1xoYaKI93D30m3Yn8
87uZiQiTCrjnbViv2o0u5P22gbbPfAvhVY1v2z8EcMh9g8kYwXY6pNE5q4It
JfhU2IoqHcU7KH4X/CCl60auIMXf+HmuD/B16ZAxXW8G2BaZBlZ2GoxYqpzS
gHAv4OvtD+tfxSLfdpcLSuPvBhO8ea6pnp3IDU/rtvoNdIKbcf1+AwM45Owc
Bxny1dpBoU4i697FHuSn77tEO7C2ALPpPw4pWX3IT2S+FYuQrAPt9rImfHwD
yD/xzrBbMFSAgC6qzvmLg8gfvjZgybudC8zE7ppY0Qwhb3M/JXYpLxYI0em4
Sj4iuE9xck8OWzvQeft0WzmzEpxi2tTQs6fBGCmKTjYutQF8ZR07rWEt8hRe
VRU9uRZggmfyF6ptQH4k8tZvP/FGsHXpzsrCQDPyE/Ymad/7a4H+j5i+oPA2
5MFnDOylzlcB0eGFiuJNLHJ+iTjdz5/KwIxSrWfsznXpn1+14nNh9MoDIfo1
I4ufOpBTkuK8s8+nggWyD9yqQp3IFcF3FzPGcJAseiOD9yHB2bkjRR6fKgcr
rp+u/U7NBOEOa0MPk2kwsT4ig6xny0BX4NdSN75c5AupLB5rn4oBj8vLwxjD
AuScjluFDo0FgF/o53vqK8XIBzmOxmVE5QJ80oJz6nAp8iOXLZ9nCGYB/Xzh
3y+oy5Fj+s3WPm3vPP+AaRfyNYJnDDyuu3s1CfgvFKRSJFcQnudnDqbWr5Eg
Jd/klurxSoJXS9hzDXmAwXfHVXVN/+PLUNHbKRiWK3i33iZzg1x/4OGqnJ2/
m9rFn14TCEnEeBKmAj2Rk2g+I+M/4w9jT/1YM+j2QR4rT+Yfo/AKlt8e17vX
5kd4HJGfUtcpvSBJmuDeT7aByPEFWobP3N0gZnBYeEonmPD43PUfDf2dIV71
2NGBi68Jfhm3/SLnEcREkQle2iJ4ucZfibMi9yEJDbvp3NMQ5K4Bed2sCjch
CfOloxKlBBcqkdQ7pFQN+c8psuafyIM5p162HY3Zeb9NT3dnzFdCZuc/N4vc
ipBb39SLDrxZATNs5cWp/cqQl8w0jpruh7C7Kq30EUsFcv3bPmVvDhZDK7PY
z5b5lQRfUYzGVOdBnqtKyxdZq5FXR/hepD2ZBcmHuAU/y9Qgv4r7O7KZ8Rl2
aZfrbx+tRV6W9WEkKD8WltNdVXQpILjOtAypJ68vTLGoOyhPU4d85UJg3lWO
TsijLqTwarwaamUleTSa0GDumbqTJvW0w1j617Lf/tYjvznQyJ2ig4Uefx/s
e9DSjNxBffgNx9UW+CBbmXHrGBa5j27vyQe+DTDwrFygTnI7chJGZv0jjjVw
86yZ9+/1DuQWQmb7fh+vgIwGZxyq93YhTzn6QMz/ZCGMKX5brTZA8CXdv1ua
3unQ/xjTmz0PcMgDL71vok2JgDm2BqN0xQQ3vTNXfmZoAKY5FL+osmuFx7b4
PT3IaDBO3JPW7Hv6YXB9S3CzeAfyWaOaPwsdPTAopiLu7J8u5HPXFCs8dXHQ
FMspqunagzzhiLZPXlQHPJljUClf2Ie8nC5y6tKVNuhr2Ub9K3YA+UMNCk2d
H/WQkWTu/OMzQ8hD6BKDQ7QrYab6eG/tnWHkPIc8HOt+5kE73jZ3DqkR5Af5
GUTh9zjYEbPZPPyF4KxZ43X0XGMw74fR4+W2TvjmmViM40tqzMMzh4alF/Aw
ol1Lni2vB/n+8Nu0v/aMQJF34ph71weQa10/JR3aMABVP+BuPA0fRk7Fn8Rh
L9kLM921e5+V4pEPFKp5dOt3wYTy1YLhX6PIH31fyFsGWBieVi7u8ncMeVZr
8MajkTr4LYfxOVf9OPLTpFhpvelSuDVtuk9DYwI5KX+zU6nJR8gd9+ydThTB
hUdG3iiyT8ET57912/D3QhJfW3O54X0Y0/ux4c7xE1C7oZE3YHYQebLAFefg
6TEoaCn2K+MjHvmRgpGctRU8PDRjMkyRPIY84BuvRM6rISia93XPM8sJ5O+P
XByT7eqFrs4Z1E4Lk8h/PTl48w62E/o/CjiiJDiN3Hos+vdPthYouZTVtcAx
gzyy5WWE3IVKqF+V0HG/juB+8rFCU+6psFA+61D68VnkgqnR3l+ws9BC08zu
s+ggLPRJPjXGtg9j3HPgXMThGWjL5q44gMcjt+lVv+jANAX5XIWuH344jlz+
mdLz2LpxmKP24/pSzSThcTLSw3VlR+HkaDHjXtw08k8Z81QKfwYh231l49GI
WeQGTHwCV7l6IFdp/Mc5mnnkD74HnFDZxsJ1d07bIIEF5GurBhZKyjVw/fKe
MeUlguN7/JPdkzKg3lblcKr2IvIaddKM3flVxe+9jrvzq18JKmd35zzOSmHt
u/OrMAPpid351T9vrrWw3J1faSZwe+7Or/75Y2pj9d35lYlb5e3d+dU/vzYj
0L87v0ren/pod371z18ttV7dnV8dvvjkSGrpGnIGDtfE3flVOm84fcfAOvJe
MtWHu/Mre33r6d351T83jJp9tDu/ErIo4sCpbSK3+eMxf+BrIcgnc8jYnV/9
cx9uy3bNgG/gJfgVw3NxAoSIRY0o7KHBQLdFKj31VbBySNPDWH4G+TJZ4tN7
hcugmUuwiHJtHvkfE++33x8sgNpP5bUPVJaRT/Z8NqusnwFnhV3vuHOuIqdd
1Ja9hpsA53g+LErf+YZcmi0iaH8sHkiTBYIxmzXkm1eihzKsdr5XJ6U5C8is
I5eTTEiTj2gFSkGLvr6NBJ+KiQg8tJgP1INI9106tIGcnYmuRYVjFXw9EKDD
kTMGqBx8S7P0dq4L2MfXY9eWgfa5FnIbkynkXmynN+suLgJx2z3XDzTOIle+
HDbrfmEObGmPYlTwC8h7ve/rPBieAkeoh+e03y8jp81fW+CSHAcVe95yXPu5
gtwRvxSomjYMqm/uZdNeXkUef/6Y6tNX3UD+TRJ7Reo35BjKEtwPqWbwSSmN
XUx4DTm1CO2L4gN5YL91x4XDjwiuIftyVhqzDEhwwbX+TKNgv+A9G7MoGkzy
1QgtZcFFEPb+WG669QTyezcpRjEFc8CoOPSQuu80ctX8QqbB1WnAnqq3FH1r
DnngpdDWoZEJoCmhNJ3dt4DcuZT643e/UcAYnmaZvbWEvPmOo5fWnkFwVWG7
cOn2CnIyEjZzjZAuMH3jw1onzSryj0kCVnnVDaB/Xs5t2o3gM8sZ0n0ncsCZ
hlG7zEqCp7pmtVMKL4C526YBQk3DYC9Vqt+rGhpMDk7q+vNrO587PxmLaw/G
kDMJVIRMHJkBGoKvhrqlJpFHNH05LhkxufP9Xey8bs00cgY6hjLDzjFw1d4k
rpV8DnnhfEGXUPIIwEwwy3N8m0dOHzIe/1WuDxhuOL89672I3MhSt8A2oAOI
fLuTRQuXkOs19d+Qk6kDD2OZf3eGLSM/yUG17C6UBe5c3hC0I19BTlrL8yCR
aRZsSx43or4zAGJD8gVjcTt5w4ZyCphPA6wVyd8oMzxyX3+XzF82k8Aicolp
bWwMeflG6TvK8+OAsaV5XwrvJHIKVkHW+xAPrG5fqIngnUZ+NHz+1jDfILD9
fWbNYXAGOW7qgscex24wlnX0T4fiHHJWwzgR0ZdY8CDFg0NObx55W9jB4rDU
ajAUc+HBDZYF5CMOrKK/BDNA/fBIctoLgpPn/WCfWZ8EUw1BstZ/eoA+H8PL
R800GKmgN0kVzhOgLOcajjlvEDnWKZZ2tmYMWJho5lH74pHjrydSNHXgAbuB
CFunxxjygo+5zRw3h4BNcGcQ98758J/Th3Zb06T0grGbiokONZPIM/VU7h77
2gl89vduUu2ZRu5smDnFN9MMNll4Tm6tEXzBy3mqmbUS6PWXnVN7P4Oc3N65
g+t6KvilSFG/uk7whbjHXikjY0Br4DTNkQ9dAAqlvzjxmQZThCtlE44eBTeY
SFuo0nqRi4dpsbBw40HvyOpvOptB5F8SGiw5FwZBKunp+9e6R5D/VRD/W3q+
DzQF2Z72sBpFfr6d2bT8DA7canuaP5w3htxVM+xW7ywWkHPOqXLXjiMfvpoy
faCvHgz9kX/xM3wCOQ+Jt+xeTgg+Z+svt5yYRN5ldVRHQTsFTDw55tFvT3BG
fX5KhcIRoPf2nvFnzXbAxJLmeMyGBhPp0JB44f0QSDWNvo4t7ULe2i1bV3Rs
AIxvfBx9OtmD3L1gbihctRfobc0yZ1b0IzenCyvMEcGBn5szXNbKQ8j1LTce
z7e1AzeKU7+yHUaQf+d/0d/4oBmUcHiUaMbgketFvCH7M1oNOBxuePVcG0Ue
suEg9JK/CGwHMkkW1BJc+JZu0rhDIkicahF5uncM+f+2eU65y0tjC/NOSPGd
z1TjbA108HlhvDvXetecl7jC2gHXuZNzpW83IPe6anFRLRwLlcMjeBikWpBz
RPh5xxS1QP2hiw9b9LHIOXXJVirmGmDOO1XM3Ew78oXMjOb9DTVwYqtQ/7l0
J3KWQ4Eu70wq4DOlE+nqN7qQT3MVHHvuWwgduPRGfgrgkEuS7DfYndeN0RaL
tJcSnP10QRkNxTuYuc08TEbXjby4ifWy2HwvrHSqvL0u2QSP1XjzcvbQY0yo
Sb0eyvXAD0YcsV1XsMgdVA5cVlDFQXipq0S2pgM5ObdZZNvhTqh+UcLzDjkO
+fg5vrkDSVgoOxfKfGZPD/JkCd2lsN9NMLJpz95bub3I1Wx/9X7WrIUlj/iU
3dj6kXd7SfXL3i2HMhWkzNLCA8g/KI3H7ffPgbffUo0cXyT4lLXWc//9sbBR
J559UGMQufCx8yqNzMPQlkn594Y1Fn69y7b1TJseMxFQxtjJNAiDmTNppz93
IjcM+HtJsqAPap7mzIEp3ciPHUt4cZ+2B94X//6LzrAP+dwwy8few10w0c23
Qa5mAPlPdg1r3PjO++QU60pd5xDy8HWW2D/rjTCERXU/l8cI8vftKZdEW6pg
Yrbua2cLPHLBn552zS0FEHsDc5x9gODO972Wyu8kQNOZkm63I6PIv508af3A
chSmPZllFaPshFtSf6ZgFR2m9KEMyxLAw0DXd75fq7uR09Cr58rVDsGantn7
Chf6kbPmjk8mpfVDW1Wl0L2GQ8i1u3T2N8v3wJVKOkGqA3jktUokLi8DOqH5
tSfGzQqjyO2mzOM2Rdsg+U9fcXrZMeTVkheETBdqoUpZl3/+NsGf134IGckr
geHRQywW7uPIRRQiI2MdkmHfzMdlp0aCpxYIr573G4dYmuter/RwsG3igkbF
UTpMRsDhG1dkxqD6gO2GQWAf8n79jJi6XDwUd8D5udkNIecQFu2lWBuC7uR2
Gb3P8cjZZjUbF3fel+SFiRST1GPIXUTLV82/4eA938PGv4zGkf+gZ7MJc2+H
HjP6BfinE8i96pLVV4oa4OW4P0LHNSaRM8wcPPP0aDkMjfXeYpwmuLnuk1p3
0U/QWvPMQt/5KeQpZq0vKC0nIVO1/vMIs53jcDb8ht9dWoydotK+87/H4eSH
AdKTfweQP3Ggfq+mOgbpbaFnKiceeZ2Qq270XTy8JqOl1ks/htz81olaEDkI
c65fSSapHkd+rvhR4SZ1L7wgeUizQmoSedyVe+vVwp3wCru8wHPrKeRf339i
/CvcDI+92g41uTWNPKdrPujRxwpomBQV67dNcHExBtaSqC/wFBWDwNrNGeRb
VSavvm9NQYZwmrzVo33QSKD9nEECDebQopneGYdJqGxZSGvEMIw8ai78V9rH
cTgjbca2pj6K3Ls8Uo8jehQmpLkp+5qOI6///iV9LHcYSim81Z+TmERu+7y7
NgS78zmvJzFXbppC/vlD4Z+Kd13QtsqGLPPgDPJxu6ybLQqtMGu4bR898yzy
rUj6Pz7rlXBDpt1Eporg4UX9lBlJX+GsiP36weNzyCVDzPPo5Wegi6FjNffL
fji1fVcrvpMa00qjTReeMAXvMl6687RgBLk5w2P1hNIJ+Hfws17XxTHkycvu
FaLhY/CCaNBm0aMJ5DHnC4wPi+LhjS9U9VR3ppDzTm2sxssMQN8sJ7J4uhnk
ukFqKQd3rivpDkosV+7NIj9Eb3ewebkNvjt/Rjbn8RzyqTsOw29JquGz/NXt
YOF55MYzD2u/XU6HDB8rsiNCCG6vR5qgLDq7c3lROd/yagCGh2yFzf3ahxHW
YgPJntNQLeWMQ9QzPPJD09XU0H8SqljqcqhsjSG3KBbkbFUbhweaWD9SyU8i
n7xqbTmBw0OMgKJ+iuI08n1+y/HJ1wahtXT/w/Y/M8g51rYF+MO7oQrDJuMB
iznkTKtOZHpvsbCeRvv5hss8cvlLm/gb9dVwZcFKhAosIL+YX3k9FZMB735U
r15IIvj/P7/6f+Y8LIdtAnf3lEr3f2Lf3VO6KHM1aXef5xCeMm93n0ciUil5
d58n/EKi+O7ei1Jgpvfu3svPl1ouu3svg8nWErv7Ic2LLuG7+yF6P8ZYdvdD
XtOci9vdozA2Svu+u0fRhlel292j+NJMtb67b/AOx2W0u2+w3/UX6e6+QXlO
yr3dXl5TiPfnZkEWkJ44LdK+k4v3m5b93/465d0q+5EPpdC6fKXh0+kMeLzJ
dPT/9tREciOx+VUqxQHfCpUVMDt5oZu5dRSUPJdyX2KjxZz+9YDi8dd5wJwf
yh8lMww+rLrPGjDSYeKwYzaUVlNg4nyES+iLXtDhkaEZOECH4Tq3eqZ5HA9s
OmMXzjl0AMFwWcz2M3qMOUZL2/lqN/hAItR4zKAemBsbXclb2MlRejLmmwdr
wMRFO4meffmAu+iypZowAwbTVOhN7hwGY5ean9g+84ZmaqU633ee/2PGp14l
Vo3Q59HsZeGREjj8Yu2MPQ/x3EhsfmXLG2DK/34RSH0Eiu5CeFB9hlUs1oAW
4yv9YRFEzYDt+9386eP9IKythsJdkg6zefAmTaLaOPixX+v6QxYcuM9QspY4
T4dRVvKtOuY/ANJqOcKFWVrBlGB4eKcdPUbiiIbg9nEscOO+Z2ofWwEazUmU
7zbSY4IP3c3MOZAFZu6Xc8mVJ4DHyZdMLq/TY6h+Le2Pf1cCe5Mm2D6CdHic
/Nqnb0v0GFEcTuBkcQd0xzDESj6phj4lvzceFxHPjcTmVy13y1S+vZoDRmQV
Pix/B0ERg/71Gi9aDP7l/ldDLyYB86XQ3/7OPeC0XnviO2U6zC8KgeLpM3gw
Hlct3J3ZDvgSD1x5Nka38/3IKzzWGAdCZvWF3fXrgLLI+TAPtZ28asR6CS9e
DZjFq46JHM4DR2/8lTMKosfEVt75lREZCjE/q6z43L2g6K2eLIU4eszmIcHu
e1oN0OI2n+fh9GLYjLn5YNODHvORf1WofKIHRs8Mi59ha4RHDIOa26WI50Zi
8yuuryLTQQ+mwZNhl1vfI/rAjci7KwPhtJgxu0mJ4yZj4E83af/KwS4wt8T/
2xqzk/fGrymrS/aDZPO6H3+Tm4Hvp+zLJ8t38oyV2zHe/FYQbtw18LGuHESx
iwU6M9NjXKVEYwzFMgGeLEzuZks86N8bFHJLmh4TkbjdOyJZDDEH3zdch1/h
8NuRx5fE6TGrt0WsG7vaobq2qZ778yqoNz6jcJ1i5zxgvL8MGz8Ij2Yfe5Ku
3AbNz9rlH/pIPDcSm19xR5O/7VSbAPrWi5K3znQDemGolBlAiwktrlWsejcM
rGfikiPzseDaev3PYxx0mO3fPvLy57rA52HZJB6mWjAUliXH40mHoRJw+7jO
UgWo6DMkGodzgF579bBjBR0mdrJqRe3JG4i5qpndqOgJPRXfahl20GEkfuZ+
Y1mpg/h+K+YIviJoJWV/LT6fDvP5rfrf/pBuiD2e+G7P53qYktd4OtyODmMh
Qzd2xh4P0x98PyH0sx1KDKZzHdlDPDcSm1/RvnIjCZUaBS0v/CkpOjvAQ1HD
iyb3aTHNnXtkc9/0guHLxT91+xtBLL7u25FxWox+zn46zYZmIEHl2VRlD0Hk
FfmQY2fpMPwcH3JyOTMAP8WmG+2HOEBO3xLvpE2HwVyKoeL3KIQ+2Qdo949+
gQemBU8VadBhNMWe+x7Nw8Il8SfKV+QrYRHdTYlp7p3PRaYuhXV3Pzxwxd72
s0YLdDLaF/m2aSevzn6x3XN9DK7ykrFfIOuCeoLUPGNXiOdGYvMrdUHMLxab
QRD/JLjw9P42UMN5W/aWyM7jZO4hPVrTATSWZKUOu1UDgzBtBeZgWowR6wgo
ghWgYEut64VZNrjofUWLu37n814mYfj0UAjEPOCyu1TlDkMyjJ729NBiGMOC
TojJ1sJqaz9yz2sF0KSI9sjfsp0cruoRnPGzC541UuJcm6uFgflvEmacaTEH
9r3logsZhlcNLp4yy8bC6DTT98YHaTGFlzHPvk3s5PLvmVqhJTjYyHmRPMeL
eG4kNr96SYUpexnbDZiOMv+aL6gHbrjFQK0hGgxJI/Z8rGsj4Gm/7Dy3XgKo
Ogz3Hjmx87pa4qYwWV8Bia6x8nHJWNAlMCzkcpkWE8GkFoV3zofqNSp9jZ8/
Qby7vBm7Ii1mvS7u7K2aVhg/wqx7v60cipqpuJOw0GJgcsVXtZ3v/zlYA+r9
fxthOsVSYlYdDWZ4fCTy+yAeKguv3y216oC0STHPz12n2TmPKX7cJzgJ9Ss4
7tQe64FqmwMnfuYRz43E5lfE9naI7rcQ2QMhti9BbK+AWP9OrKcm1ucSy43E
erSi3hLjoKE8kJueyPPjxDrKV0sZdkV9NdnAu0P36e5e8b98NWh5OzTPJgOk
llwx3d2//ZevqF5aqxk0fQKcnvZ0LcVTKF9p4Bf2Dt2JB6kp+0R29zn/5Ss1
nMvxAP8wcEAu4Nru3uO/fJUR0mWpz+sIaOGLpyOXclG+KlAVGh1gcYe2c/wK
H3DlKF8Rm18R69FGLEU0RMlzwaNAB+16z28oXzXbHnL83psJrt7quB39aBnl
KxmR+wGVc6ngULEdTstpFuWrh6ff9CnXJ4FjnRa3350dR/lKsPPexKHKKFB8
9Ha8i04fylcTr511o874gd5r7n6vvtehfIWfFc70yLsF8SfPeaRaRaB8dax7
EXuC+jXMuJdBsX9fK8pXxOZXxHq0MgEcg8bhbBA26x5g0rmC8tVR2QFNxdJ0
kK/7E8v+cx7lq49DQYfTp1MA8767J7jXJlG+yssfUHCsigOxD9n/FqiNoHyF
rf28rN0aCkLdQ1jVtTpQvkoJ72e/oP4EZOx/5kTln4PyVbkeTkvezg3mTNLd
2CtXjvIV1nOMSpQ/AloH3akwuoJD+YrY/IpYj0aiI1S5xb7z+Ww/HaAouITy
lff832GRoi8g5+PS8VmbGZSv5sx7Xw6NJYJLNv6XzC3GUL5iNv0ecubNe+DY
q/7E91ovylfMly4ynzngC4Qqh6dZf9eifGX0mjEkLkQPkhTf4LK6/xblK1oD
Q9M0rWCIf8ePzW9uRvnqp4v8chQ2Bt7bK6Phyz+A8hWx+RWxHs3CwJl+6nwa
eH2MZ5gxbg7lq2aHy8qGuR/BFB2zgtDkBMpXZUaOe7dnYoGZ4rW9Ma1DKF81
AptTD1vfgF+5X7F1Du0oXzF2nvzocuYxwLqnXdEIykb5yt/uZLx14EvIr91I
Kv2tDOUraS2ofPhvOFypnnf0VOxC+erc4Vzu00/i4SmPffJACY/yFbH5FbEe
7etUjSPz2c+g0lP5741D0yhfRVvPtL4oTgAdmNN/mc1GUb4SibH4JE0XCSaO
fzYa2Ogm5Csu4ciH8T4gY75z+GheDcpX+LT3Y72suhATwlYmxRmO8tWC6bw1
ZiQQhrIxc9uJNaF8lUwxHdfNHg1Haio+P03pI+SrUyFq+8WTYMVyiGxU1hjK
V8TmV8R6NBf8nW1RiWSQVmBaLB02jvLVSuFlCUzWB8BjWKh9d3kA5at7y4wN
ZMEhoNrY/AWXPhblKx1PP82MZgdAYvOwql8zC+UrUYsDs0IOrtCneuJbDkkp
yldGxtFBhgFhMI/eqCp1uwPlK+p3WCdV1Ti4N7or4dzLYZSvpPYJJih/+Ahd
E2yf57ZPoHxFbH5FrEfra41Twd2MBw/4mCwG+PEoXwnRrGrvFYsApB8ePU67
iUP5yvGFBNfiuBfQZMh6GSpUjfJVrHP7gZYb2pCk+uvfU0GhKF9R1Zm8+5Lu
DznNOIbkLzegfNUaoD3NS/ce4icflpj39KB81e9ggfXKS4AKpPX02YajKF9F
Bm4EyaR8gtLNXUq9UVMoXxGbXxHr0b4tzefZnY4GRYH4pT/5fShfPbu1dezX
odeg6xXWJvVYK8pXEqyrn8+fsAcZspTlbvgMlK8Yj75JWlFxgQUOCpeisotQ
vloKus8T1fMGSuyLvzvg1I7yle1F78DwoQ/Q4Nrp8+8ODqJ8JfXrXOkzmAS3
BYX6cYLjKF+ZlASouap8gcby3JS36GdQviI2vyLWoxHbHya2Z0tsH5XY3iax
/UZie4DE9uWI7ZURm1/RCOKfxuycf5Vy2uc2jwyB406aucVYGsydCHX6d/0z
QIikz9PRchQ5RbikAVvNFGi8RKPoxDCBXFaAsqrw7gTQOlGwV/DxFPLHzyvD
5htGQT7rAc7FsBnkSnzsvNPTQyBUl7KK0WIOeeRW4Wu/oR6Qr+kpiZuaR05K
Uv6l27sdWHSzU7rRLSJ32avK+3qjBpDrP2x26CX4YMzBcIxQJlC6kNgodHEJ
ecbNx6pgeBJQUtDLpU70gLtfT3VZh9FiuARU/r63mADmdtJu9hGDyPM26jRe
ZY2BsS5D/p+OeOQp3Sqm1hV4QHsvyD/Pbgx5ZFmkTt/5IWA07aEtITKB/Mqd
C7ffBfYCtzGy1q20SeSxyl8Z2yM6QQnvu4cqs1PI3+OUpjQam0EgNeuiwsA0
8g236sqjVJVARCj/0tzLGeTWop6ps+dSwdbrbT2RAYL/nRBnw+HwgHy7oGrV
rAOce9vIcnfnvLrHfIDX0GoE2KexeX0l60bu3VlEPdwwCKZzFQ/M6Pch99Pm
cHRs7gNdJ3VX6M0Hkaeyqf6Mce4GaaYkj/wPjiDfwJ9UrJruAF7Zh/faZ+KR
/0hWfmEt1ApuRUS8vJ8wivzcoQ/+TIK1wO35uZxsgzHkludFdRyrioEXJvn5
3yGCG1m0n7iUlwSebHLWF3KNE56POI+1t0gPIHWi/H1JqwFojcUt5STRYZRU
QuXVE3GA0dPtU7NGK3KuDssf1Z2dwFMkl7fxSTvyWTk7mdjqdvDi4gmbvUGd
yDvWbtdV/2oF/NUuah4WOORlB2xyn1I3ApM2gUHL7W7kQ4pRa5t11SCKyex1
nkov4e/2Sfqme5eB5ChlTtWrfciT+o/y0R3LBpF+UpaUfwhuXmIcJjwTDaKW
o55m3upH7n/rI/+t3nqQUkx+4xZvMaA3yL+0sEiHCb1052W/cB3gcpWUeilb
jhxjHzbHI1sDpBMvGtPkVyLfmtmoM9hXBTJe5+57sV6NXMQoV8Ypohzom3xI
bViuRe6xqHTn3lQxsBoxT/oUX488Nctjs0YhD0Q99vhWsbcR+Yr4kQ2cXgbQ
UbXh+cLahDyCu+S0l1Mi0D8kenFPL8FVPR7eN6MIBhOHO/ytZZqR490fsGXt
iwDlmFskQQq+oBvzm1uAeifHVsbZblmHA3ybNnOtdRByjIl09cOAUMDoxVtt
yBaCnNNZ/j7F/RBQPljldEYplPA4hp3v7OiDAafSd1eSM+HIY/V9JXS2fUF5
+8vzEPcWefmQ6Ls3vp6AU9FWQkb4HeFxcgOlIpJeANdowUJxyUjk+EmbAI4O
e2DkaLJpvkJwEoPyUHkSQ4BfuBPqo/EeOfOnIon7I5Ww2vk4Iy4mB7r58ekz
/aLDtFLbTm5EVcDY87cTCr4WINecP5I+wVUOMdptsZnWJciDR+86Vs6XwMsr
3KRTOIjcB+fDYnW+EPqnng9XsapAvvL0i9/zM7mQfUlyKS6vEnn988b7cbMZ
MGMmkHm8pgp5xgWxs1W9KbB+MzQmL7waOVWoD2/8sQ+QmcLtwqsTNcjLxa5f
XtfygdiLe7pD7Qn+qqP89gX5LpjB513gylULi+KkSvpLd3Lg1FrR4ZkO6DVd
Wpju3Ih8qepgqbxGO5SuvqhywbMVeXnSRcvnNG1wypJNRexQO/IMFkWxB4xN
0ONu7ov+7A7kdqpnlRbgzt+bbrxsf6gLeaXuZtNbvkpY8HiJJVYSh5zDH+c4
/qUIlnG+T2hg6SY8n8rf1CY5GbDrYqaAUA7Bk0Vij/lzRkJ+s3UZPGUPcjMm
36XGrmFICz+L9PdgoZjPIVOMFh2G8TWHAFntIFSdoe+z4e9C7q9AfcRUvx/i
vGuhkWQPcqPe6EKfDz3Q/1pb4jZJP/Kcat32IxFdUPd3Y73b40HkUreyohPU
2qHgtFe8QMgw8qju/Kt0cU2Q/gpDZtIJPPKrC1PkESrV8HWs4DOJdoLL2t/6
ftSqEJ6Sm47LUhpFrnimxCB4NAFqdeIwcZ4ET+Znct/jPgHfar+3V7Lshgz0
4J5vNS2mURzL68czDtlvhBww7ehHXqbRGZL4ahQW3LjQi50aRm4oViFaZj0C
42iLOXqujiKvPJFWsmE+ACPMsePJy2PIa32iND9R9cCBGwx2X7UmkDsnChYl
GHfA+t7Hss7Ok8j5G3/5Ce5pgv2/+F/aG00h5zzce6hpthxuYfmSq7YILs1p
9PWgw2fIXVzM76o+jZyqRrhxgH8UzL1uK0mBHeDD7YDGc3n0mFD1tK9fyPCA
ur2xR9a2G3nalZ/YHu0hkIhT/L7V14f8BWtCYLZKP+j1nXVi/zaIfIlC5uur
hW4gpFn6vDdtBLmHqpfZX8VOQBvBdLmWbBQ5SWb/kHRFK5Bcesps8YvgIdge
ceO3tcBC5suGUsEY8kXDcgazqyVAIn3cnubCOHJSI9dKvePJwPVlZuaRlwRn
O+CgzHVwJz856QuPpbWCsz553F8K6TGD9GnC18X6geCca7Chfgfya04U3BWk
vWAxhQl7kw+HPPvFBWUTDxz4+PGat1t6D/IOwf2czJUdwPmpsOL0Qh/y9aYq
BZZXbeC+02yaXe8A8mkKdt+ngg2A1ylkaL/VEHKVWGa9O06VQK1k/klL/DDy
xtia1Gm+fFCR1GTd5jiCnH78dVQ7WzwYOB100GGe4LI+Yh+yRnCAP9p7XW6q
DnBnzeZMJtBjjn2o9t607AKkY350upHNyA83TjTXZneA4yMOA4zPsMh7BjMn
fCuxQMr8T+Hyww7kjtFP7jRItICyeg+uoDNdyN3USLWlguvBNXhtyyoDh7z/
1xj2aGQVcJEpfYqd60aeJWhZfqG5BDwe7ibpG+xBfnBDZFltXxZQND9m6eve
i7x6f75/vVgU8J2WtegeJLjT+5pECt82oOMdUTIgVQGoL3dL8T+ix6TO8Jnz
2rSABWkGx8WYauTVNE2DWt8bgW+8uPZERR3ytA6lFjPBelAZHXe2710jcrfX
vxpO7K8B1crXctqPtiDXM7h7+U5uBeDZeyBj8GIbclYZEqdHCiWgi9T6fZgn
FvnDhhmVicoc0MFRTn1AtB25y1isecCez6D1Z24pSCX4aRYt0wdaocCJ79Pl
3d+Z/nOSAzlamRlVwKn5j4WTey4glba8RHmaHpPTqGWoaFQJrH7XTRvCQuQR
5wLHO3DlIKVaqvPHp1LkVFSlzx2EyoBIaeegw/ty5Iyj/ef+XisCjBQVD1xO
VSJnH39N58yeBzSrl+u4vaqQx9KV6J9MygRC/tgGnrhq5M0XKUouLXwCVF8k
Vsie1xAe5/cRMyPpWMBjf//W/v21yLdcyu/1mb0CjobjV+yNCW4xXf0mPjMD
MFJ3sd2RiQdXZsdsjNp2rpvbyRRt/OlghiJbVacrCXmszDWBH36p4Iejbqnh
iU/IXetq5EnCPgH8e5aUv/ypyBmpgl4rXEsGwUkipPkdach/fOnQOgrjweXf
5YWBARnI1TmfXwycjwYSOlTfXJwzka+4z9fd+xUGeo1yYs6fz0JuxK2SWOvt
B6hCGQKscwn+w4FFL8L7EZgxEGMO+0lwErcfHBqtIRD/ZzvmhL8HjMLXehvr
7eTM0wFLJAGvYaxd4F/9tz4ET1WTVmIOhhgAjJc1/ZG7cn2dC2z3hxj3p8/o
C4OQc97aGx167BU0enw8z03/NeFxPt6QvczhCY365+u+JoUgLy++t6Da7wpj
h1ZO/8x+g5zk4UYQptIRup556MPgEUpwFfYyGzIbaETxvdfwQBhyI10ezDRG
B2L6/JN5jQjOH7RZmutbAPmLJ/k9sZ/h1DWz1WfYne+hdZTQdDoPbg0fpy2l
yUD+41HLu4rfOVBTWM4k0CkLuc9TDveRliyI7RUY35Odg9x29Gl/jFkGdAw/
z8WWkoe8YHFhVCLoC8R8kqNKvVmAnEqwhOTMdhK00MrQssgrRM7+vVi6kC8W
ijjZGW/WFCEXSeH/8UI6FPqsHGvaci1GrjNY7pCU/AIW3J4/fGqS4IPnaF8G
XqmB9XePnFRVyIeJBe8YjM7RYrZ+lcfWDldB8k635zd6ipF7xHwIkhethCTx
xTSF+8qRs0tuzSmql0PNr1fCeDcqkNMqUwb42ZXA4NiB1pGPVchtDa0+wpl8
GLWX+bQfew3ykvdbDE+Ys2FojsLhINVa5Dqv39OO7U2FPtYXjHil6gh/l8ph
82ZgHEzty5cyHyS4rSv1R9tHftAqvcLo+rl65FHVefZzV1tgAX/RYPWecjgj
dn6axJMG00Xbp3BFpwmGiLz9KsFfhTzRatFBk64BykseTBVhqkXeO3lIVMm+
FrYWFCQ8zatHfiKy7K5WSBWkdjxlUkLbhHw9Saz8vf1OLvKPoo2ga0E+8eY1
DV6uCHr4gPuFWa3If3iU/olTzoZWxomjS5ttyF3fqsj94U+BVwuv3GO/j0Uu
WEtKMTwRAr8NLfPMJxO8Uu5mZZ37DDg3S2X78xDhd3yycVR6rV2TIKvvnQEl
ySrqByc/txdM48eAdQ97fYv5IuoH376qH2o2GQFX8Cv4ItsZ1A8Oqns8yYrt
BZJx99adT42jfjCcDER2B7eDZjk6bstbA6gfbO8IAtRvqkF28u/xxxRY1A/q
zNL319F9BBgvL+XStUzUD1rxMt3IlM6CZkOaA/R3S1A/ONe/Iaw7XgdHGFY/
0ER2oH5wMWopmK53Cpj8eVsz1UD4fR/5C5qTg4wTYEr4lZMwZhn1g+T6Efbq
1KPg3vc9k1Ov5lA/6NMmmLn8fgDA3qCrXS8mUT8os1p++UBzF6h/TFOLP4NH
/SA7MPvG+qgJaFKR3n1rjEP9oGPzM4fSzUIQMeH6tke8mrB/ZX90Iv6RL8RE
6nCmRIaifnCTzuZxv2I5bP3gt2ao1YD6wZKur8p/0lqhiw/XbO4EYa8++9mq
MRvzJIgvlj/xw5LwOz4ukY5JRakxMKZm/e6y6QLqB3/+CSmqujYM8Mux6iTv
plE/OGNz1029rgeopolwiHuMoX5QnC6Hr3EDC1hzaLDLlv2oH/Tt2P/AZqgK
9H8MiD6y0or6QazGyM0r8snAgv3O0KWbmagfnEhU28x/nwmrfXoP3LIqRv1g
416+YBfGOuhx+bt8/0Y76gfptkbf83l2QGsJn/OzPYOoH+w4WcDuITMO5N/e
s9z3fRH1g2w33NW6tfBA+zIdhcnDWdQP6se0+fE87Qe2NXJcptETqB8c89Mz
3rvaCfi4tVyj6EdQP9j99WDjzI1GkPcCm2/m2oX6QSq3iGClyQLgdB2uT5hU
EfavLMYer91+BTkPPypMrHiD+kGPCChAcg7CWDE1djeVetQP8p6UccHeb4F2
P/SD6Wa7UT+oyd8bHGOLg2mjyzdJswh79Y2qll4H9UZBftA+HBcl4Xd8YUoK
tnR9g2DqaFp39eMp1A/eKKq8NxDfDXL5pRWfxY6ifvDtzcyYM7xYULbEarGf
tw/1gxrs6knss5WA/FNHbZh9C+oHqQ6/W/QUSQIZmJKhI64ZqB8swFyNP1GV
ATH83DeY2IpQPxi3vEHasV4D4ZWAW+tH2lE/OFjBLGoY1w7vuOdQCDwaQP0g
azBX+BfGXnhlW+SRVilhr35dhmHYb2QYKN35nOxJTfgdX6DJebVq0j4g+DLl
x8DTcdQP7vNgSaHo6QB7jsrYWu58L/zXDyayRB95O1IP/Kuy3zy53Yn6QXb5
rgwxz3yAf7KKKVWvRP0giRzbpMAPb8jpfYJd7nMI6gcdvR1dGp+WQvJ1/erl
tVrUD4peHluATU0wAzeBZUjCoX4wXsBQ0kquC0aWnFds9xtB/eClmNwecoN+
OONonuXkPYH6wQVd0lL5fQPAtz6xPoqO8Ds+RaEA/6XzOLA55Wt1yBOP+kHf
xw9OAoVW0LyKe98W3IP6QXOOfJWDhRVg66ZX3KHIJtQPrkTV/+ofSACxrFJF
8YtpqB8sN/hwRuN+OvQxjnUyeFKA+sGrUfwCnW+q4Rq5l/YBSizqB0f8in8v
eGLhFP1dhoLiPtQPNot/e1YT0A0XetVH6INHUT9oQXm21kJ1EKqcvBncSk3Y
n0/g1v1lJdMDtE57S/ccIfyOz8VU8JnPy3aQH59G/kZhAPWDl5973y5lrwOy
upM6pn/bUT/Io7xtOtafCwrMuBkqyStQP4ipBJtZF70gvmHKeFn7NaEfNEpZ
8DAthilKf1Mmb9SgfrD/fum5xc4GKChLFV7M3oX6QZpIVRmljg7ITd7xilx9
CPWDyx+sLQVe9EI1P1BqdGAc9YMjjZ/+5gwPQfwG01lP+WnUDw7rbU5T+nSC
J2c+JjLfH0b9YFmTNV/QYBMQoC6rOJGFQ/2gw8s2RZbr5QBTcffYQ8sG1A/W
kycyOF+OB64HjNofPE1F/SBeQ+4Oz2IqlBju+WrplYf6wcC/M66Dq5WQqZDs
xPmXLagfFPEZz/tg1QrNz9kzUmX2oH5QiLTp5OeqLuiQ08OleRKP+kEzs6n4
Rwz9cOzmn9uHVSZQP9hRn/P1J9UIfL1/2qZekLA/71JESeY/2wYWI/KMU/p6
UT+41J5HN89YAwJe+zs63SXcv0i05OHXwPlsUF8pse8wJUT9YPkjMaO5qx6Q
hNHuTn50EOoHnS43XqrdLNh5367c8LStQv3gGBsDeZBvHaxqtwqY1iTcX0VC
aKkhm68dCkaoN3/J70f9YHyt/J3NG92Q6kVYNbUp4X4d2iar6kWSAzDUrj1K
7xzhvhbf5tPYXjWMwJial7+KymdQP5jwrvLtbr4qODimupuv/t2HU/aCsNhu
vmqjnMHs5qt/96t8rMIXtZuv6kkc3+/mq3/3dVRv3y7YzVeT8irVu/nq3/0P
86fe39vNV/yOBnO7+erffQLD+xI+7uYrOb1Z8d189e9+egyJfa9389WFdAae
JxSE+9pxkuaTN+7kK5/rtj/L1gj3hWMW0WtL38lXV4ddXtDu5Kt/9zFTtOaS
0tnJV3c5PPDUO/nq3/2+JO7OvHokNg3ybMg4Epi+gYUbto4Kp2kwe/F7T86Y
TABmHqlL3+EyYGWZGLW7RouppCuw+6M2Cq7uNzS4tzkHSE1ni+sv0u3kT4vc
TtpBULnfs7+kZxKwWKlsiO9cdzokhObZj+MAybbMyBEHPJqbJcubvjCabAIM
RkeCyAtxIGQio61EgAHjNFVxbsaoCHzO9Wpw960Gj+YmZM0f7OQrxfgLDaM7
+crDPnxkKRQyJpx11HNlwDBdHVM29C6HuZG1tT9KG2DJ1/UEAxOG/3XvW2Lf
C4jlZ2I5k1geI5ZbiF3fiV0HiV0viJ1XiR1HYt8XiOVqYvmTWE4jlmeIXfeJ
XR+JXUeInW+JHV9ic3Vi82dic1pi80xicz9i8zFicyRi8xZicwli39+JzduJ
zaWJzW+JzTmJzQOJzc2IzZeIzWGIzSuIfa8/dTjM1PIOHnCyu3Qxv5pF+xt1
xr4btxb7AeOTgNSx74T9CkMScc7Roi5QRlXe5MRD2DdgahjUnwpoBrEkNSpA
mtDLS3dGfq19WwZKzD4ZxXfUoX65PPGXneq7AFAe3qH2jJbQ82ZMbTkfl8mH
weshDlFHCH2oYk3Md8pv9ZCc7PFGh1Un6vVimhccDX53QFfLoYtkwUOo/9Kx
/CbfINcH7b7YVC19Hkc9EbHrQrSpjjfPlXaQgHlDr7bzuv8d3ynNVSHe5Uaw
yNyeq/ST0AtwPBPRX71XBRLNbCfqZVoJ72eRwZB22QIgkcxDrzRKmDPPtDEO
qAYmgoKe/W/q9hDmrnicdv0Bfm9oVFnyRJyKMJ+MCMioEZRIgxGMJyZP++Sj
45vzBBq5ihfB3qeVny7eqybMtfgnqFmoK2Fyl82I/JFmdHz/t73eVKZ70bt9
36spKUrwn74vUSCvYPc8pnazYeu/57FDrufydntAcc2Ug3/+0wMOxiQ77faA
3U8xAf/tAdnpvybv9oBiA06u/+0Bz2WF3N/tAbPzDyj9twdco/KllqloBRFD
WQz/7QHPL5l67vaAeHz78n97QGfmtqXdHjCwrNL6vz3gkpIMlcHOeSnc7mj6
f89LwWRnDDqj8KC8nsKGJbYLuZRo4UxR8TCQ8Brm+HK6F/mFgOTK2sEBsMrA
eXvcdgB5bYldbn9SL9DS0Nd21B9G3uiZAj7ptoMPR0w3Sb0Iz7+LweHWYF4j
KM/LiH1nRHj+5mvflOjNKwFO8XCx4B7C8+Tx3GzXyMsGJpodGsV3CE5sXkrs
/Pw/fY5KbN+P2H7g//Q9QGL7BsT2E/6n7yEQ23skdnyJ7Q0S268jtodGbF+L
2F4Tsf0fYnsyxI4XsX1IYu8HYvuExPbuiO2nEdvjIrbvRGwviNj+DLHje/nh
+gFSqiJgkdDqHSedBVQzHg+H7Nm5DlYc/RI2UQzqZTSiu5+mIQ/25Xgr25cP
utJfwyrSXOShffCWsGEuIHFRmwzyykfulCOqkByXBWaGFnUOlhciL5i6Yheq
kQ50XutuXUspRl6f8BN+304BRu9jjEQlS5HP9P5dt9GPByK+4fXdD8oIz+fV
3mcjf8OBhA1fjbk8RG4RQX3afeslCDZvlLyYQ/D/ba+XWO4llpP/p+dhYjmK
2Osl9jjE3g/E/p8n/t/zQ/nDvsGKsqL/z/5/ADZ7cbQ=
            "], {{{
               EdgeForm[], Lighting -> Automatic, 
               RGBColor[0.87, 0.94, 1], 
               Opacity[0.5], 
               GraphicsGroup[{
                 Polygon[CompressedData["
1:eJxNlGtfjUEUxZ8acr8VIpVSTq7dRIhKUrrTiRSR8ppPoaRyy+UL+HDud6V3
1vrN//mdXqyzz8yeWXvPzFpP5dSj4Yf5SZK8F4KwVcgTPFfMeJuwnfw6xvmM
H+vnubBe/3eQ9/99a/btFbbAtUsoEDYI+8l7XyXrXPMQ0RwHhJ3sKyUWCoeF
EvK7hU3CZmFGvbwIkbNcKCJfxj6Pq+B2/QphD7Wd2whXIT16PCu+lyGurxOq
qV9PzAgNxBrhJGdwnSfa9yrE/yNCj9ALfxlnWlH+u3Be///x39yXhDNCs3Cc
Xn1PR9hbTr1Sxu3CafZkuBvnTrDPPc2JeynEHluFU0KT0Eb0/mPCQeoVUcf8
jdRz/cvCWeGcMC3cEsaEq8JFuLuEFuGCcCWJ52vhDlqpuaBe3gp9cAwIg8KU
MMrcJt4wfZ+iNe9TiCY66cU15sX3JsT7M18H/Wbpz/Vv8A59xDbub5C1ndTv
h6OPvDmHyPtM14hdvN2PEM9oH1iD1re1b31ZZ/fQgeuPw2UO+8l+sUfuCNeF
YWFRfO9CXP9U8XWIergv3KTHB9y9+fLgcM0C7sb+ucv5R1g7SG33Z12nfnVM
fV9M3xP0kk1yPrS2ZvFaCTU85/8BXvPN8H1wflXxZ4h68F1UJFFn1uPXEPWU
+r8SLvvLfnuCB6upkyFnDnvjGL1Xsd96tU5rqOH8UXhPsMfrTjKew6e1nLkS
7kX06bteVvwWos5K4c4kOf/X06u/EbVJ7htRx9stcUbva2TPL819DLG3XnTh
N1pAw9af/WzdtSfRn47W6zx6sL6tOWsw9ZmjNWp/22v2pGtb215/jrzHqf/T
dfZIN+Mexp3oJfWH4wC9pB7qYK4/yXljiP1ZNNTD+bLU7GWum7x1/0dn+hTi
G5nP34BRdPKFb5fH/kakPnCcFJ7hl0nq2G/W/m/NfQhRB1nmrOsV3tRvtIp/
GzjDKNzuyZ68zbnH6Okv/TTzjuZvopb7P81ZPof4ht5nr05z1gl4fUfja3in
OdsyvqhlfTd3+RdO6/Y/lv+LLQ==
                  "]], 
                 Polygon[CompressedData["
1:eJwtkckyg0EURrs0tmzZGTbEEETEsIltskoVW8oD8BRmYuYVY0xMwc756n6L
U//5q7vvOLKz19jtSSmNQi+85ZR+YAVfhiv8EIbxcXjBb+AElvi/5nsM0/iY
7x/l8CGf619vZ+ABb8IWvgGvjqcYOr90Pr0dgFvnquDrcIef5vA63ONnOXwT
LvCDHG/7nO88x9k2POIfUMIXfH8f+vFBaPl8Ap+Ed/wP5vB59/8NUyl6fsK/
oIwvuj7VU8Orrk/51WvN/ak+5dJMnx1Psyw73meO3AXvo+vZzEIb/4XVFDtq
+b5ylzwvzUe5FbPj+8UUPXTcz1qKGG3vW7GL3ofyafcV16f+VIt6/gcbsEOr

                  "]]}]}, {
               EdgeForm[], Lighting -> Automatic, 
               RGBColor[0.87, 0.94, 1], 
               Opacity[0.5], 
               GraphicsGroup[{
                 Polygon[CompressedData["
1:eJxFlNlWU0EQRS+5+RF/h0c/waU4AQHBAQ2DSphkDmgGkhAMxoFAEEGGKE6o
3+QAnmNvlw973brV3dVVdbr73IXU+e5EFEUXRVIciiNxIr6IS6JD9IiUmBCT
IisW8feKe+KuuCH6xKBIiynxSCyLJbEiSqIunolyrP84+P2fE3nxVKyJgiiK
dVHzv+bmRb/soSh8b4ph/m3fEiP4PDcnrjHX3+vM68fuZI19V8RVanAtXaJb
3BG3nb9iFePQh0XW2X+fPXtZl6YnKXrk3gzQuyl6kWXvLmI41hN64NqroiJW
xUvxQjznuyNeY3vsDb5XYkPsiV3xU7n+iENMxz6Lg9AltPit/9M49LjAeHsy
1On6fsVhTgFd6uTgvbfRdlo8RuOG2BT74i25NLD30LXGHPvL1PivthpabzGn
Si82qG2NGA18CeUaJ0OPHMe1Ouc8fXStbcmwj2u27TWV6L+vzN51dLDWD9B0
AO1GxUNs65rBd0p/5mUvEN97ZtHYvfScBc6Ox87obZb+OudZ2XPoYZ/teeqx
hjNRmFORvSoO6JHtqvgcBV9Mfcto0s5+f++dvi1934lv4itnxmfnPX7f/RZj
J+i8w1gLDa3lR/EBv9d+J+Yuczx2zDlwXs7vE2v3sR2jSj1+d/ze+D1wjVuc
gwT6LXG+ctx/30/fU9t+E/ze9KFJhl7Ootko9gyaWlufWZ/dMTGOdu735Si8
Ab7Dvssd+Gz3oH2adRkYY+0cejj/ZhRqKpKf36YJ9HONfuuaxBokB8cZIUfn
5ndiGw1aaON4K5wnrynwHo0Tp0R8967I+fQZKHHHm8Q8Ir9h9vRefps2OVcH
6OR75Lvve+l7N8SaSepxHXViOvYfWA3ufw==
                  "]]}]}, {
               EdgeForm[], Lighting -> Automatic, 
               RGBColor[0.87, 0.94, 1], 
               Opacity[0.5], 
               GraphicsGroup[{
                 Polygon[CompressedData["
1:eJwtlNlWU0EQRfsml4RMCkYGgQTirPgnvvkJLp/1nwAn+BIVBxRBHBAwIAhB
QSCoDJ6zej/ster27erqOlXVjbsP7tzPhBDuiVQ0khAuiltiVExkQxgXH/Rv
RbwTs2JRfMF+L76yZntOLLH2UXwSq6KJj/eti+8hnrsgvnH+In4b7LHvZ7HG
GT57XiwTY4EYTc7YFj/FH3EoOpVDQVTFOZFP4prtblERZ0S/6BNnRZcYEBfE
U+X+RBRlnxclURa9oieJsX6Jv8QssqeH/WVi9OEzoz1vycm5dXGPQWK+1tob
cnJuXh8SV8WVJOryQ+yJ3yH6DPLvchI1sTab7GuhibVoi0fK5aHYkn2Axhuc
tUtN1rF30HUVTbfRuIndIoZj7XOnLdbbnO9Y7qEV/j0XL+iTWXKdQQvn7Fxr
4ho5T2vtJTWfw3ca22e8ClGzBXqpLp9hcUNcT2IvLXFP5+leHstGP+9/xp18
F/d2h4YgFUey/4VYM9fOczFCTwyg9SXxmP5wj52SrzXeR5e8zsqlUV/rM4H+
m9TJsRxzB819lnuuSt9Oyp7KRj1q5OZ73CTHTvneTuM9HH+Eu46yZ4z5nUfH
ceqxjKb29Rn79Ia/C34IyK3I9wE1rcgup7HX3fOlNO5p02P+57VD5iGHnrv0
mHNzTjVm0rGt0R491M1sDtHXU+yvU89j7Tlhrj3Ltp13gbnuEDlm2LOcFSkz
7jksoJdzdU45zunGZxL9Pbt1ZrgH3ev0Qj9vZIN/vdRkmLtXqZVzKKJHRnaS
xFla483wTBXQz/+seYY7V3g7EtbKvCuBNdt+a9wvrlPg231rjfLo4B4+Ik9r
4/q4hilxcuh/gl9Kvx7jV6LeWe6Rp19OqcMKb0KL+f4PoBDKiQ==
                  
                  "]]}]}, {
               EdgeForm[], Lighting -> Automatic, 
               RGBColor[0.87, 0.94, 1], 
               Opacity[0.5], 
               GraphicsGroup[{
                 Polygon[CompressedData["
1:eJwtlNlWU0EQRe+VPCsg6LMf4IxTAFEMxGiYEgUCYQphkEAIgkFADDI5ix+C
OPJ/nrPuftisom91d9U51blUWM6UzwRBUBQx0aw/TWI0DIK86BAPxBOREnfF
PfFIdIp2cV88FknRxprjbr51sNc5dzjDex+C47R4Ki6LK+Q4t0Xc4gyfdVIX
BD/qont8fly0ii6RoLY4sWv8q9w/oqz4tVgVa+Kt2BaLYkmsi6pYFhWxKTaI
V8QWa2VyNjivXlo1iILiKXEe/WYUF/k/LX6phhF6u0qd7rFEDb77Fd+u0YNz
3OuxuIG2SXTMiAF06xFDYjCM7vkp+sPovl7RJ3JiGM2S7O1nby/ffIb3WuNu
vqfw/pnIoltNfBQf0DjBGT7rbCzSxFpMinOxaM3xRBj1cZMZaSNuwdt2vL4d
RjNnz61to5gOI407ubOH3hN430cNWWodF2P0aa08y6P0NEA8gtbXmaE4Xr8U
b/B8DV9q9P5CLLC+Sr/u8Tn6N9D/EJr+Zv48Y+7BsWfS8+a1RubDedPUmqX2
PP7bjy5qnGTOStRxIA7Fd3Ek/in3VHxW/C6MzvYMFqnlmPfTSt/22zOTQ8ML
seg3IE8NTewfQbMT5qsP3dP4k2P2LsaiM8bwoB7/B5kv9+4aXds6/VuzYWZ0
Cp8X6XGCnhfQvoCOS+S4L781v0m/zSrn+vwd9pXw0J7NiXk8XkEv1+S3vEte
lb323Ot+63t8d+zfgX3WtpiVQ7zYZO2AnG3mxm/lfRi93RQeZzjDed/EV+7Y
I/4iZqnZtVaYH/tYpN8ZcipoYO3twTz7dtDC8/AJP+3vLHv3qfWIGmrsca7f
eDN+znHPKf7tUp9/s/0O7a/fwH/lNZih
                  "]]}]}, {
               EdgeForm[], Lighting -> Automatic, 
               RGBColor[0.87, 0.94, 1], 
               Opacity[0.5], 
               GraphicsGroup[{
                 Polygon[CompressedData["
1:eJwtlFlWk0EQhdMmi1AiIAvyxSV4fNa1KCoiYEIcIjIjgRCSIIpgBARjEAQE
hwDihNMCvPf09/Cdv/7q6urqrtvdcfHKhcunEonEJZES10Ii0Sn6RK94J7ZF
U3wWW/hsfyLujngo8mJIDItpMSW6xW1xT9wlZx+xD0QPPtv3PS+peeKm7Kw4
r6JaxQvZs/KP6zshyv4XYyH6bJfEKD7bM9QyIorUNEiNrq0gTit3i3guewGf
x56IOWzPm8c3Ta6n+FzLY1GhppJqnBGTsqshfgvk8n+H1jqXinOd4xE1FYj9
Io7FX/FHHOGz/dt5lLsqDmX/CtGuiN0Qff6+Z94hfTsQJ+InY3vkdMy++CC+
sq57vcOcJj12r3+I79hNctm3iSYc95E1j6jV9Z3VXtOpeDY+oy5xS+REv2ij
v1XOyGfTnopnPU9sN9rJ0a8zqdhb99i9s6+EBqwXr1mhF87lNeboX5p+z9Kv
Y/b+jzPPsdYwOunHN0SfsvgG6Z01Y+2siVch6raLGMcWk1HTWeKvixsiE+K9
8dxFsU6OZ+R0rlXGlsRrYtrRz7Lsl5yXNbUSos93pJyM8Z5XRh875LBerKET
elxEr9bwgLga4hvgO9lDjRnGfMcr6O8AnUyxvwz7su2cA8xzL9wT1+V6vJb7
4v54zU7OxOv4XWgl3jFV/t2/WohvQBv68N5r7M+5PVYPURvWwwJn2YL+Fjnr
DfE2RM1b+3n25Xs8jj1AbRPU6/24d5Now2/ZCBrxm+W3y2/OKHaeXGPU6Fob
4k2Id8R3xXfpG3123/2mbtHDFWrcYO4ydoMzqJHL+02j9yW0UmfM93wX2/P2
8DXItY9vDV1tU8M6OtnBV0FPdfKtUrNr3SSHc/ke+U35D5XaGhQ=
                  
                  "]]}]}, {
               EdgeForm[], Lighting -> Automatic, 
               RGBColor[0.87, 0.94, 1], 
               Opacity[0.5], 
               GraphicsGroup[{
                 Polygon[CompressedData["
1:eJwtlNlWU0EQRe9kVJAoGkMIIiYhQIgBFTARNCAogpgog+izy2f9RRVx+CDn
8ZzV+2Gv1be7uqvqVNWtvHi99yqJouilyMTHNIo+iW1troo5MS9uiY64K9bF
I7Ej1tjz+iG218Qyd2y7IXrcuSna3PHdRbGEr664Le6IB2JTrLDn9X2xwJ0u
dtfFDWxWWC9w5r2cOCmGxTknKmIxKAY4OyXOY5OJE+KsyIt30uJtGvbznOV4
yzYzEq2RBZ1aYlrrvnivO1V9f42j6FscfNq39f2QBh2c/2niKIgLxO3cttCg
je7raOZ912Zf7GGzxXoXnfviuXgmrhJXG+1b1LTD3g51ORRPxWfq71oeUNMd
zg6w7fH2ITVxbZ6Ix87fmlAH18bazGahL+bohXvE2EMva9gi1i69sE1em+S4
i49Z0aRn3AcN9hapu301s9CD88TS4MwxWfvjNPiyHs7VNVkjV9cpEWfokX9x
qNsAtRrkrCguikn5qorLWk/wnjXcJwf3wpSoJKEnnKtzdm/UknBWF1eSYGPf
qRjCTx97x+u4C/i1L/v8qfh+xaEvM/S2D789iR7e87qeBG285/WUOEpDXPY9
Qjz2aV9N9LI+q2h+TP8uUYc3zIdj/hGHefGbI+RgbWpinHidqzVz/M7Zd31n
iLw9AwVsx8m1iK3veEY9q5fEWBJm0LNYFqPit2L4E4e5zlG3Qd4poOkQufpd
a+c7trWGvvs3Dv8Ev+F/2DI9u0G8ztm6W2/P9vc41M2947V1SKllnfo36dMv
cfgnRPTWGLlMUZ8yedWp3xH6+N9TItdhbGybR4NRzovkVkGzEmfuhyr96pgc
i+eiRv96TqbpUfssYV+lXrad4a0ysfnNDhpZG/+j/gPZeGyZ
                  "]]}]}, {
               EdgeForm[], Lighting -> Automatic, 
               RGBColor[0.87, 0.94, 1], 
               Opacity[0.5], 
               GraphicsGroup[{
                 Polygon[CompressedData["
1:eJwtlNtSU0EQRU+dnB/xXUVQv8Mq37wgipGbQkgEAQNBJAYQASEiGq9cRUED
ioroFyAoyg+xd816WJVOn5np7t3Tc6wxc74jjqIoLRIxrT8z4qV4IWbFU/FW
vMHnbytiGfuVeIfP9muxiq8snuDzt/1UFP0RD2TniVXmm896Jp6LRbFA7Dkx
Tw5/tfcgFdZMiVNK+lwS/FPk6HUfxHtxVVwTN0Wb2BSfxS/xU/SLATEiSuKK
aBCtooWzHHuNM+fJax1fAzHa2PNPefwXH6nB9qH4HgffHDUucFZBDIoxMSoq
aGxtl8ReKmhmrbJxqLVWPEaLS/q9LJrEDXExDj7bab7Vi2bWrNAX67BBDMeq
ik9xOHeaHCrk75qW+L7MGRvsqWLviB/it9bupoKO1u8COTmX6+hmHazHN9a7
xiw5HhBvkXtwiJ6OvY3m6+z9So/dly3xhRyr5LJNrav0fJO1a+zdol+OWUEL
9+KeeEhPTkjr4+KR7HHuSoFe+c6c0bfTSfjv9bbrkvDNvjr6VWJ9mt5kRAf9
9B32PHgubNeIYhx8OXGbuP1omGZvuzhJfvdlD3O2+9xJjCZ0zeKz1ta8hbP9
20qMHLbvchc+256dbny2b4k7+NrJpVf0YDtuH74Mce/i6ySXPL59+p+jPs9q
IzEcy7W5B0NxqNHauGbXWiTHLnoywHp/n4hDz2rQ12/DpDibhJ6N0eM+8iii
Xy++YeL14LM9RI+cs3Mtob9j+OwJzsrTuyI52O+591u2i/7uST05OTffdb8L
niXfU79NO9wn5+s3cgbNrb3v6CC1d2MXuG/e47We41ruo23Pts+yBn6Ty+Q2
SWznOE7OznWW/b6jrtv1elb87vgsz4zfMc+qZ9pzeQRBr9oR
                  "]]}]}, {
               EdgeForm[], Lighting -> Automatic, 
               RGBColor[0.87, 0.94, 1], 
               Opacity[0.5], 
               GraphicsGroup[{
                 Polygon[CompressedData["
1:eJw1lOdWU0EUhS/3UiMQI2CvLyESBV4hy0dw+Vuf0N4oIkIKkAIBRIgGKQqR
orj3ms8f38rJzJk5Zfa5dx49ffgkjqLosWgX1SSKauKayIiSKIqPcirF4XdG
LPDf9iexyNoCZ7xXF1NiWhRFQbwUr1jzXlm+S+KX7L/itXjDvY71gjP2nSSm
46xy/wKx11grEGtFLIsHKuq++Cb7u7gn+674KntTJIrdLvpFnxjV3phoae9Q
zIl5URUVMSs+O2+xJPLEdKwavnnsKr2ZxXcRbK+Tc5tixqJXnEtC7W85N0PN
U8TIc+6L+CG249C3A3FG/1zbsNiIQ42uPScWk9CDOn1yL/x/mH7syt6Jg5/f
sM5+jdq26NcyvW2w5v66Z4fk4XheOyC3EdlZ8VP2PrW9oyeu8YOYoG9z2JPU
Ok+vytS8zhtUqW0Du0ZuXsvx3o5tXfm9nYNj79Efr9neJZf3vKlz6tCZTnFe
pNFGB3Y/e13Mhn18r2v7I07jMDueoQviOr8D4gb/bQ+Km6ylueequEKMNPZl
9jLMo312yPtYHFHTPrFP4nD3kLhFDNsXxW3WnsdhpibpteevQk2uocT7rzBH
Bebf8+q5Lf7/JsRhzT0uc9Z98FlrqIFO7O87PNee5wr9yRAziz59j89nea8t
NJZDPw20+SwONUygnVH0Zj03mQHPQo98u5OgyxZzFiVBl16zfYav7VQSzthu
YxZT5FrhLr/lCPrZRP89+A3ytt3cM4B/hf44L++tMvdNZtC5/Y7DN8Bxj3nH
DrTnb9F4e/Bp8fZ7+Nh3jPlr8j0Y5/tl29+IPjRlLV2irl40YW2coJ1OdH6K
lrvQeoozQ9R4RFzn5m/nGnO5TU3/AK9f99k=
                  "]]}]}, {
               EdgeForm[], Lighting -> Automatic, 
               RGBColor[0.87, 0.94, 1], 
               Opacity[0.5], 
               GraphicsGroup[{
                 Polygon[CompressedData["
1:eJxNlNdak0EQhvO4uRGsWKkSRZB0iiAGDGDESAgoREnUiGDFdp1CAONt+H3u
e+DBC5P5p+3szPastxaaZxKJxIZIiin9mRQPQiJREnnJJXEieVnsi/fih/gu
3ok98U18FX9EF71/2+9U1PD3t9/iJTan/H6BzaE4ErdFn+iIYzEncmJHNKlj
jzg7yI73VuyKA/ElxLP4TI8lV0SR81l+JG6Qx/luhWg7LVZD9GkT07E+izfo
LH+ilhY92Ud+JT6ge42PbT9ytg5ncc7r1ODcKXzs+5Med+npAed6LraI6dib
4hk5nXsDXYvaquKp2MbP/xucqU3vfQfr9LJOjCZ9te8aPtvY1flm+yfk2KI2
92yVmlzHQ1EmTpWeV8hR/6+mNmdqMAu71LiCT53crvUEvX8vhThXNepcJGeV
2irUtEnOMj5r1PQrxJnzDFwQ/WJA3BGj3Nch+lG+DYoxbLLcpedzFv24mBRF
5LtiCt2ESIsZMU1MxymIfIizcYQ+z7cxYhXAsvdzPsTd6GB7n9gZcY8cGWqc
Ref77tJf99n7UEhGX8fzblg3T4605Ewy1up4fg9yyRjv305KziZjbOss22eG
fN5L74XfCr8RfkscY47ap9lPvzULIdZim2POk8N+mXvLkn+Ju0yTv8y9XxZX
xLAYEmfFuRD37JroQWf5KlgeETeRbZtCN0icce6hQH88kyv4jND3CWyHuXP7
9FLTELE8dzV636C28yG+Ac7rWfNMXgxx5orMTYn7yDMD8/QnQ7999kVsp+il
ffyu+J3JMQcpdFlmw3kuhTjX/ci91DrA2VLYpqnVe9JHzSXmx73wXv4FycKa
HQ==
                  
                  "]]}]}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}}, {{
               Directive[
                RGBColor[0, 0, 1]], 
               
               Line[{2, 1, 11, 21, 31, 41, 1001, 51, 61, 71, 81, 91, 92, 93, 
                94, 95, 910, 96, 97, 98, 99, 100, 90, 80, 70, 60, 1009, 50, 
                40, 30, 20, 10, 9, 8, 7, 6, 901, 5, 4, 3, 2}]}, {
               Directive[
                RGBColor[0, 0, 1]], 
               
               Line[{102, 101, 111, 121, 131, 141, 1019, 151, 161, 171, 181, 
                191, 192, 193, 194, 195, 929, 196, 197, 198, 199, 200, 190, 
                180, 170, 160, 1027, 150, 140, 130, 120, 110, 109, 108, 107, 
                106, 920, 105, 104, 103, 102}]}, {
               Directive[
                RGBColor[0, 0, 1]], 
               
               Line[{202, 201, 211, 221, 231, 241, 1029, 251, 261, 271, 281, 
                291, 292, 293, 294, 295, 939, 296, 297, 298, 299, 300, 290, 
                280, 270, 260, 1037, 250, 240, 230, 220, 210, 209, 208, 207, 
                206, 930, 205, 204, 203, 202}]}, {
               Directive[
                RGBColor[0, 0, 1]], 
               
               Line[{302, 301, 311, 321, 331, 341, 1039, 351, 361, 371, 381, 
                391, 392, 393, 394, 395, 949, 396, 397, 398, 399, 400, 390, 
                380, 370, 360, 1047, 350, 340, 330, 320, 310, 309, 308, 307, 
                306, 940, 305, 304, 303, 302}]}, {
               Directive[
                RGBColor[0, 0, 1]], 
               
               Line[{402, 401, 411, 421, 431, 441, 1049, 451, 461, 471, 481, 
                491, 492, 493, 494, 495, 959, 496, 497, 498, 499, 500, 490, 
                480, 470, 460, 1057, 450, 440, 430, 420, 410, 409, 408, 407, 
                406, 950, 405, 404, 403, 402}]}, {
               Directive[
                RGBColor[0, 0, 1]], 
               
               Line[{502, 501, 511, 521, 531, 541, 1059, 551, 561, 571, 581, 
                591, 592, 593, 594, 595, 969, 596, 597, 598, 599, 600, 590, 
                580, 570, 560, 1067, 550, 540, 530, 520, 510, 509, 508, 507, 
                506, 960, 505, 504, 503, 502}]}, {
               Directive[
                RGBColor[0, 0, 1]], 
               
               Line[{602, 601, 611, 621, 631, 641, 1069, 651, 661, 671, 681, 
                691, 692, 693, 694, 695, 979, 696, 697, 698, 699, 700, 690, 
                680, 670, 660, 1077, 650, 640, 630, 620, 610, 609, 608, 607, 
                606, 970, 605, 604, 603, 602}]}, {
               Directive[
                RGBColor[0, 0, 1]], 
               
               Line[{702, 701, 711, 721, 731, 741, 1079, 751, 761, 771, 781, 
                791, 792, 793, 794, 795, 989, 796, 797, 798, 799, 800, 790, 
                780, 770, 760, 1087, 750, 740, 730, 720, 710, 709, 708, 707, 
                706, 980, 705, 704, 703, 702}]}, {
               Directive[
                RGBColor[0, 0, 1]], 
               
               Line[{802, 801, 811, 821, 831, 841, 1089, 851, 861, 871, 881, 
                891, 892, 893, 894, 895, 999, 896, 897, 898, 899, 900, 890, 
                880, 870, 860, 1097, 850, 840, 830, 820, 810, 809, 808, 807, 
                806, 990, 805, 804, 803, 
                802}]}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, \
{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, \
{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, \
{}, {}, {}, {
               GrayLevel[0.2], 
               
               Line[{901, 911, 902, 912, 903, 913, 904, 914, 905, 915, 906, 
                916, 907, 917, 908, 918, 909, 919, 910}], 
               Line[{920, 921, 922, 923, 924, 1098, 925, 926, 927, 928, 929}], 
               Line[{930, 931, 932, 933, 934, 1099, 935, 936, 937, 938, 939}], 
               Line[{940, 941, 942, 943, 944, 1100, 945, 946, 947, 948, 949}], 
               Line[{950, 951, 952, 953, 954, 1101, 955, 956, 957, 958, 959}], 
               Line[{960, 961, 962, 963, 964, 1102, 965, 966, 967, 968, 969}], 
               Line[{970, 971, 972, 973, 974, 1103, 975, 976, 977, 978, 979}], 
               Line[{980, 981, 982, 983, 984, 1104, 985, 986, 987, 988, 989}], 
               
               Line[{990, 991, 992, 993, 994, 1105, 995, 996, 997, 998, 
                999}]}, {
               GrayLevel[0.2], 
               
               Line[{1001, 1010, 1000, 1011, 1002, 1012, 1003, 1013, 1004, 
                915, 1005, 1014, 1006, 1015, 1007, 1016, 1008, 1017, 1009}], 
               
               Line[{1019, 1018, 1020, 1021, 1022, 1098, 1023, 1024, 1025, 
                1026, 1027}], 
               
               Line[{1029, 1028, 1030, 1031, 1032, 1099, 1033, 1034, 1035, 
                1036, 1037}], 
               
               Line[{1039, 1038, 1040, 1041, 1042, 1100, 1043, 1044, 1045, 
                1046, 1047}], 
               
               Line[{1049, 1048, 1050, 1051, 1052, 1101, 1053, 1054, 1055, 
                1056, 1057}], 
               
               Line[{1059, 1058, 1060, 1061, 1062, 1102, 1063, 1064, 1065, 
                1066, 1067}], 
               
               Line[{1069, 1068, 1070, 1071, 1072, 1103, 1073, 1074, 1075, 
                1076, 1077}], 
               
               Line[{1079, 1078, 1080, 1081, 1082, 1104, 1083, 1084, 1085, 
                1086, 1087}], 
               
               Line[{1089, 1088, 1090, 1091, 1092, 1105, 1093, 1094, 1095, 
                1096, 1097}]}}}, VertexNormals -> CompressedData["
1:eJws23c4lu0bB/AyQr1GCimyklFWlJDnTEZRZGWUFGkp0iRCNBCKlEolMylb
VvHc9pa9995UJIn83Ofz++s9js97Hud1Xtd134+H45uQ9RXDs3SrVq0yYFi1
in7lv8kHjXyU/AageaL2svn1Jgh96621WD0Ip4I2fBSV6ocWeZVrV7vaYI+J
MHpCv5ntw8BeeHb65IvPs13Q6bxZk/S30X2lXXe6YXTE7etb416IJ/IOkS7+
pydM63o7xNHlZkrM98Gz2BjsE7AwHk+/oRkY2CXe1Z4aAP0Zfw3Sv2vdrxm6
WAf1txXGRL0GQfP+Zqx3FAhZq8daAeF3W2Q8bYeA02we652dQyLO/siFoStD
zzcxDYO0aC/O88h+PfOi+0d4o2Oas3B8GNYyRdP2lXhjwvZIHxBsTHJ7/9VD
+A8dZ2mtEZgc2bK/aEMvMBjHZLhPNgO94RP0p6n2MRd1umGs/3OgSmA73GoU
vUN6mLXrare1HeC4J5phqrML1jSLuJK+8xC/7NprLcC2Z4kzeKAH2pdeYR+9
m03cF/waYJvjwbPd2/rAf8kc+8y4lmQNnq0BbkHzz907+uEH7EIXvHF2gwpP
KfBIKceZzfUDdRNg/2uyr7/ukaLCrPTKaXgMwPleG+y/V9PN2OpVDOz6kuPv
VjkA3E85XUiXrFcajYvuBrZV3rd/a9fCj1k2xpwrY5A4LSQx690JwSfO69Al
N0AupRz9ppxA1l+2duBb9hfKamoGvRBxetJr1zoyPYcWuCYzqbQzuQ12h3Jg
fSFXle87kUZY/rbezFyxE4RHz2E9n+RXykRhLfwnfdX4kFU3/LfqKbomqBaq
WlRCGpexKOezHrg4HsBAeuCSXNKmxkKYW/QOFFfvBfaj57H/zv6ZKyc2fYGm
HWciqV97wUB4Bv3O8rSwxoUosN27HGG62AteAT7oHEd8GV/ptQGDB2+7GrUS
4gZ/SzZoTkBi1kX+rf4tYKgs0iMrXwuX90rvID3WY5+3gFsTTMoc/vGzph6K
bsuhNxql7eQXb4A7edevbZNtgtmvuehrBNM2OD6tBSWTGqtD2i3gJNKPfvxV
gF/coyqIrUqqp3K1gZNRGfqZ1925okElEJ3CytD2pB3sTzWia4R6OMoa5MES
x0H57ykd0PhFGD1x29wfvuQ0iKi4rWfr0glnp26ga/ofZMuaC4MQK5nT1UOd
cGPdy504v17q5VtRDfDf/nWTDf7FwKqU9yUmfxLi9rNfLFSvh+R2nT0i/8pB
3f8z+vqKyTB4Xwv5XaVOe3mrYe33CPRRul/76SOqIcqqT5D9Yi38SU1AFzrM
3x8TVAG+V9P++rDVw0yt1FfSpbNFPFSkS6DYalCgzbEBHkgWY31TgtPhd+75
kORcX/worBFEV7Nh/S2VvSyKUl/B4pMkzNxvgjMK37B+oqGuyooxGRItuU4e
EG6Gha5udHWZXcWq70JgvFTnutetZvjx5D72Kfu3u0XvWhVUunkE7eLOhRvZ
F08+6J0C4vtMlYxLBTisz3L5tL8A7F8fQtfb1zN4RKoMrl3JvHFCuhimyu+h
q89Yx008KYZv5gIO9E2lELRsgd5r/stF/1MBlB9oDFGTqYA0PwN075PT90K8
c+GAADX3064q8Ja9jb53Zuiq9ZkvEHVum99Q6zeYNjiNrst46w5Yp0LWw7iu
DvYamJe8i57glrW1/uAHaNKXMuK4WwOV+ifQJXbtkl1cfLry/nVa7ciugUCn
YPR/nz+8yqTkgXmwi6RfcwrUx5mZ94dOA/+a8t+cm3JBoFv6Y6NfOrznV0VX
eke9qeyaA7qKmrMTC1nwg8EU/dDd/fIKdl/gGdeVjCXuHBCVNEdPjBWPoLJk
gH8fvcOeZgIsqZbosqBm63cyFTwFXkxc580DEV1anzNmFeKXRxIgc9l+8uj6
fBC/aIH+nvCVkPz8HpTT7n0WrMkH7jW66EN77LivO7wBj/FLtb5GBfBkTAM9
vbpK84nqQzAOazT8+aoAEs8cQj/y5GZ8rmEkKNlwLLXwPYfXu5eqLs1PQ53H
tcyXzBHgOG7u4KPxCuZ1af6dtWCrx4MwEHWumr7G9xbspWjud8X9p/ubt5Bx
SDdU6UgY/Hi2iL7f/OHauzWvILlgw+aipnCIkqXVVzcFn11OeQ6zCwW7/4lH
Qu91mpcyeb1e1A+EtlNmfzMpURBQQ+tj+zCf97WDDzywskzx3hgNru60ekPf
3bONne7g/F4QymOiYQuV5iy3AsZSxy+A+c29SXFz0aA6TPO9hTG7zjxNAIET
4UFBqWFgfHOCg2FymqgzbWNe6x0HV605HRiXIuFc+yS6WPWefW9EPsLa316x
J5hiwJxrCj21l+1GzOUYUL9VWLRYGgvmWrT6Z8PvU3IuRUHYqlzm3Upx4GY2
jS7Hdzv7uGQ4nOuM9TPQT4C5cVq9pKy2u+/Z1+BKiF3uoE8CVoLWX2uUd7FX
4BmoX+RvWI5LgguWtPpDXJymJ3f7QLmtwu87Qsnw3ZVWf+W9xM7c9OtQtmP4
v4FTyWD6jVY/0CxwL5exEBwl++9c7kmD98mtQjV3pgnu8w/2vk/MB0F6f8UQ
6S8g7VaFrpE5W+23NQ/6AzsFtkpSYTG2Ef2v54BiVQIVDI845e7qz4X/0hrQ
f9u/MAonvoDO1up1jHfywaC0Fl2Cvb1k9Z10EK/8t8elrQBYq2l9LgwLSJz9
kQz3xNVzDywUgu79JvSvgTwcGhYf4WyZDVd1cxGEx9Hq6wQUxu55hoHKOcVM
G4diODpUgX5z4Mu/x3U+UNZ3fVgovxjy0+rRHxw+FlyVVgvmaSNDMeYFYG3l
Rc9lOQV3NAa1d3vVgK5Z3hs/8RJ4xDuEziZxVbTj0Dfo13A5a5RQDm6eb9Dp
r/yaSxSpgJ51pSqd3VVgv6kPfW9/g0HDhxLIoQpfeWReAw3enuiLcDNHs78A
mG8lu3o9qYW+uLPoot843rbmEFAuU8y64XEdaAs5oEu+FDz0XT8DnrEsu7AY
1sM6Diq6cP6Tm/UK8eCs95t9VWs9fHhVgB4lyJEj2BwMfS+sko8INUAfmzP6
hrUlo4RVCbD8lrjM7pcFQ1rmqwKPTEO9NRRBXREM+tQ9lAiiwlsVA/TLdvtu
tC0VwGuHV6mdb/Lg4M2b6PyFjo0mY3lQ+1GfzTezAII/O6H3hjSxP9xHwAU2
senZ50VQMW6Gvvq1WxCh9gXcN576qyhTAo/lT6DnfRJK55z9DFbWb8UPuZdC
hrEG+ptss7A7pQlQ3pZovMqzDP4FUdDf1bENRhyMBD6QSZNRKIeAUBv09gpB
gY9ij6E4Ui41MqgcqkZprv3wcKkIXzpYUtYvHhb/AD5thWdUWqahjbN02jLi
M6QV3rS0Ho2D+qUi9AfcrHY3hlPg2GtbnZcxSZAVQPPV5Y7ebd+TgN89U3Us
OgUePqS51WmZM94+8VCT/yvrxMXPsFxbgk7w+UptbvgAspoTLicm0sCppQB9
awbdx4M1kTDBF1ksIpkBrSeq0TdPlqR92/IWClQT1pVtzYQnJyvRm0Ys725X
eQpSD6TWHyzJhP49tPndc+9qfL13B5YOPK3xEcqCfx9y0d1+7t0k7/0QfrQo
nDoZ6gQHbWpkTyxPE92roocUmR4As2vtvqRbruB1lOZP5epftqjfg4vCMxf+
zd8F9f/Xs3K217xX9wCdfQyLrKvvgb0pzUMfBZ8c5HWD3lr7If6q+7DmGM3V
bOdvXNe9DZuqdXd7mD4EbX2az5tPWPLL3YC+0Q39x197wdQ5mgdVXpDg+mgH
xpdHH3565g0j/183y4r3r9ozaxi+qCdhtd8HdprQPPrii9KN7LrgureI/+Qb
H4j9/7rru7+ePpL2FW6HK3nt1EqArQp3+27kTROvAu7m9LRnQdF/j6bS+VOg
LMcXXcpg8+OapAyokl+nJNr9GRwi/dB9r4crzCqnwWmupwvxlhnAaR6IfpPy
1prDMwW66z3m7J6t/LxO80b/ayz4zpk/EWrTQmdtb3+FmgO0PkoiVu0vVu49
Vv5uZCxTDuS4vUR/FLzF9ahqBLi8UW/dr0wFn1MB6GsKQ/J7W4Ih5/Hc/Qus
BDxzc0W/MiYRzNDlAe8+S089cCNAPdcDPVjuudU6oXJwLZzKdvHLBruq8dHD
Aiuf82yFRSHWpWCQELb1AlceCL9qRR+lqy/YaVoMEa+8L0QkFMDS6d/oPhO/
+3evfM73RzA8zWQphpfUOvTxve/cWW7kQRhD1nllgVK4oNqJnna5Zei+aQ5E
6wt7B42UgdSmZnSe3Jf/GdlmgBh/KZ3g6Qr44/oDvfa0xDN5+WRIoZcUp3hU
gkVEE7pH2KSPn280JJ2e3HTyUBXM5HahH5OldJq2BoBHXYYCc2oV7N1XhR5j
QTfotLYeei+G3uiuKoTr/6z39WyeIooFvv8WKaqFc0EzUwxDpTAZVYNuOeOi
8/lgDXjy5Yk/TqwEcflA9NDInqPZUlWwLGvz98DK9729Bl/RvcP/qny/UgZf
2BLer31WC68FXqDfELW0a7AqAsWP008Ne+vg9btr6HZMFpZ7OPKg3uTIw38/
6+F6si964JlN76+uz4KdV3WvJxQ1gBrvKfSx0t9aEecSgeHDI7Eyk5Xfwz47
ohvVvvF1fvwSDOcbPO9HNYKYPYF+lHv/Yb3SFoBMVca6DRVwOa5o3YWMCYL3
zd2ebsFm2HAgtYxOoQa+P7v2H+nvd2vUXJJtBDkTDd3Zj3Vw9OsgOmPQb8/1
S3Xg1yTi82uoAfQGNNCPnRCKLfCvgZbd9Q7zPU2gWOSJfp9vWNemoQJYqF13
iKctcKk+C9c1PJHSfGZ3MTQc1ub+MdcKohtssP6kmYW4pn4unFVvShJnaYfO
I2/QI6NCuXWufIbHFuyeDMXtIMAigC7GvXSZa+4dsHJGHpDd0QEhfw6he3rd
ZR5U7IJH/w7fFvGsAYmz82maf8aIK6Zjq4LlO0A25tmXtrx6aMygyyD9g/TH
5X1NrZDKXbNFrqAJtgRPpZO+rSd0t4FYM8i+2fUh9k4rGETqo3/cKWZ/WKYB
3sJPtY+D7RDFZYL9N9jk6Pj+roGopZpvwXOdoBOyCfvvHwlYuLaxAkT1vN1H
P3SDfrg39onnejdzdKAAdmQTbpEuPaB/eBL9/L/5W9SxTJDV0KwrmeoB3YsU
9D+pauFn7kSC4Ko8mVs7e2HLaAqu+2D1v2aWqF4QWlRYA1r1UDBUnPNncITY
lcPKE36rB77aRlQarG+GAutSKunRRq7XXCS7QEz76IaAoDbwyDmArvJt7/db
zO1gPqfWPZjXCXlmE9mk3+BUb9gU2AyHNgazbNPugb1btmN9Qml4tWF1PTCJ
RYiMufbCk8o4XNdc49UXutBqyD1pfuaWSx80HhbEPsxtxzb4K5WA3dB9ygdK
P7TdW4v1iuybYr14cmB1reA1nqJ+qNn9C5335fwuxdz3wJDn7reFdQDUm1Rw
3R1r9/tFmHWBl3m9xZpnNSDU2vLKeHIMQuWvim4z7ICdAU9yDzTXQ1rm7Zek
r9bXM1D52QpNHQVxbe1N4K5w9znpvd93q53f3wzslmt7tV62Qlxo1QvSxZjk
GCQ1G+AnT/lsP30HvOcIwz6vhJNPTLLUwn6tjeGsPF2gL+CD9ZeOvGIrl6sA
tR8f+vdUdIPOrsBg0pvGU491/imAY773TNz9e+A/tj/PSDdQ+fEuhzELfh3x
sxVc3Qvdm1LR67ctpt8NjIT51w+C10MvrDaOwf5ss/YaO8ZbgE9O1n5UqQIe
FO7c/zp+Aqii0tsuqjVDNstO7m86NeCuWahGumf132xlvUao3sMlrVhUBx/r
BoD0i93c/SU89fB9sz7XSYZG+MYcgvVb4sdkWaNrYMFyS5PY6mZovG2H/mZ6
Y8SjvxWg0NDPY5DWAjeV1NANBoz7w42LoW+Tf7XjljY48pgJ+8tUee/Ze37l
9+Uu3m1yMu2gEW6F9aeWdFRZ/D6D8d7aCO7JdrjccRz9Q0DR3wfrw2A+UfVL
zcq9Pdx4DPc1P3FfhUmmHhwUbtvU/CqEt13T6SYbpuAq3bs0teFaCGvdqFLH
VQbRISXojoXO2b/O1cDrycKKsKFKUJdLQZfxuiaVd7YKfrYms22SroE3O25k
kN6rZWtUHFYGWR8PCJevfE8/JEDB+v2cfPa9j4qA8yr7cCBjPRyX3oP1NyKE
sudl80BtofjAYd4GUBe8i+4yf2H/U7UsqBmmi3sy2QC7GR+jX+T5ZTwekQhV
ra3zue6NMHxjHPvb2d9peZfzEqqJ/Y/Xf2sEyc9uWM/wQMEp8nI5PFeenNvQ
nQ1Vekfu82+ehvOTg5pisaXgw36lsUErD77bUdDPqOWPf3tRDMsanFlBwwWQ
qyyFPtbw/L0QFMK80M5YZ+1iuGQliH50oxuT9/s8CInp/0N/thRqe8bukZ5/
z/XKhbgcEBhdpeUtXw4VPdpYL7mHWWV9QgYIRV7VNkqsgPiYdKxXFtx0e84m
Gd441Nel1VdCVaMt1p86Efu4tywaxA9YdW98WwVfTGewvmjIO457bSC0/uzx
m2X+BgLXz2B9k4bE3+dHs2Eob2coMZUADQeZ5pKp0yC9jk+y69IX4My0Wv9i
5fuRY9MmdBWHGbmvCpnwWeWm60mjNFDv+v2LdIWi/eE7ktOg8Km1WXXPyvcv
ulH0CuHsowKjKdDBEuXTKfxl5bnjwD7Bf0Jas8sToSnr2cbznNmg2LqI9UpK
Q9ztlrFw+XXUqaOxOfBPlB7rjZadL3ZHREDKW9XswD7qyucLD/pgjeiuzWYv
4O0fC1GDLwSY3WVAZ4/R4Lmn5gmDdqJKP7lzgfHtAPYXnn0YttnGF5xfRxTd
nHQFdr1wh/+Wp4Ex4/C+zQE+QBlx6gwb8YBAe5qrn3/uZ3PJC9qMW1kWw+/D
9eM0tzQUH81ZvA+MfvWZuzZ7AZc7zV/Eiyo3HPYE8wCTBidtH7gZTXPfb9Vb
dfPd4H3UVNiIuC90OdE89sijlAZfJ7jVEnX6ZYofOIXSfHLh9/sOpmvQaBco
Ht/rD/73aP7l2dl7U8/PgZeoGIttymOQc6C5iOtHamysAbxNPrmoIvwEFExo
Xn3iV/QpjzSi0OJaxy73GOLRu1VlVm3ToNtvID29/jNhUDGQtFogjpiqXigl
fU3gzwXBMymE1Idd2fTyScQR3v+wfuaaOkXTOYlQ17fztxJJIcSUv2N9xuee
nOIvccRfPQNGl7ZUIm2CAesPnxo9orf9AzFC2dczaJhGaF6dxfrb07xZwZqR
BH3LuU9LXumEpewP9GzrJiFW/TdEqsdz9583MwijH3/QE79Ysfl8DSQKD1lz
NfJmEpNZ/9Br3g27CWe7EMO/mZbeXc0kXjxnx3WDy5fm47lKiJD0L39/7M0i
whtsi0QNpiFow6Cnmm8RIcU4VJAkQyU2hvmg32GM4a3JLiAecce/DjiTRxR7
30EfeyZHZU3KI7pKmSWtnQqI5v+M0VMrioyGmqgEB5TVfzlaRFjXu6DXz8Vz
Gk1kEZ9SG66U9xcTBVZG6Ld8sw2HEz8Tu4X7FZ7LlBJhrqXoz3M8RE1sEojo
spg0epkyIrryGvo1wTM+o8yRhCPVzHK6q4xg57yJ7j1s8eNxkT/hV3+5fqdq
OaGaaYc+Y2nJNxhQS9j+qHE2lisgfhW8jmI5OwVjPZIOBTY1xOXmTA/t+WLC
cJMZeobOFmGC5RuxTea/v5m3yoknssHoWmIpS4Lj5cSZlyYsr8OriPMfrqDv
XBRveuBYQghftstzVKwhrAQd0Nf+nXGpzyogop48LZuyrSVGf95H1xg8vS/V
hSC4Op51nz9fR6zbU4he1OHMaS6YQQjsDBkrkqgnlB/6olcNmv749TuOmJy9
rxKZWE9kW/uge/+ZdvKKCV75HsQrb/67nqi5tR79uMiDM66nmgkNujp44VNG
GP5X2De3aRL01IPrRWoaiWTnR3S7A74R6oMX+0l/+qqnZvxvPaGWqtYr8bqW
EEnzRNfYtvrJ65FaQs5rgScyfeX/j6min3+QpeKhVE3cW3WvuiuokShP7MX+
8x8jW3dDOaH3+qHpws5mYjQmCf1b49pj//0sJKKK1VeP3Wkh3jraob9aZ1fu
WUQlKKU7pH3cWwmzUBvsH9yb9b1IM5UQfCNtEiPXRuQmMaEf4vHUzt0WSrxx
fbCFM7CNaPH4TutTYqGwsl/Iji3kXtkv/Gu+M7XixMGKw4kr+4UsgZM5CgHf
YCEqD/2P/+yHlf2ChcJi4cp+QelQFLqgcrfDyn7hdCX7VER6PTwzWUAfHQ2d
vqtUDWJ0lE8r+4Wq9DR0DkIzfmW/EBmmJbayX2i/X4kumuohuLJf2MGoWT96
pwUKBx3R6f2zo1f2CyeEDP95u7eCpq0L+qzky9yV/cJF2RjRlf1Cnch19PK0
j5LEtlDYUag2uj6wDZomUtH3JmdK6Qx0gFjfZVGlwGpYX3ntD+PVceLF056w
6Lk24KuXSavprwNPfVF0J0XHnO7UFijjLlo8XdAIYRH66J/jJeq2iDTB/u5m
bhXLFhDbeQ49tavF5fqRehhe0+MvGtcGntSL6FNlF3507K2BCfeAgdboDmCq
+Yq+9KfEl3+gDBb8bnps0ewC7ewd6IwvNr/LZyiAv/5TOhX3u4GqVz5P+ra7
A3UZdRnAP+I+6yLbA10mG7E+NnRQ8Fx4BOTcuCFw81IPMMXS+rTdsAieXtML
FZIcp16G1EGHYJTx1WujhKqByPY1pd0Q8uOIQL96E1yQ23OM9Fv5+7Ma+Tvh
5EW2X73vWyG9hQ/rj9nJfM7gaYPEkwNe/Zkd8JrbAeuPsTju2ks0QYbKJs09
Dt3QvpsR6wd9354c3loPL7JSzov09YDMV1msN/Clmjjc/wabDk+HdTb1Qn/X
JfT62AHOTNtiYHt4TEwzqA/S5sRNSLc7ohd+bWM2fBD4KcLE1Q/zI2+x/5ap
g6yvf0XD3merB3yO98OVJC/sM/Y8cOD32X7wfFl2r1muEYLs+RfuiwwT8Mj8
g9fmPjgcTsTFnmyFnY8Y/pB+wlWbd/p5D2z5ZJd2VqkT9qabzJMup1sv/jOr
E7rFIoPtLXrg7gMB9MK46tgdY63wMqrH1nOkF7IGdmOf/Qyjt4NKG0E60bVl
3b7+ld+LX/0m/cLvvx8VrGthTuNfa5vhADQ+UMM+1Od9/w2vvG/0To6S7hKD
wM12FfucmtxV8WCJgIHoYBGjnEGQaZdFb/IV0XZmiIXcbR4M9/8bgrql07gv
jdPl1nlegyB3OGdyx8OV9+hk18dHvgPE/Rr/rn6eAfjUwsB5XGzl+zaTdyzp
KlHtrc8v9AFjl4FVnWIPBOgmx5Nu9V8ok4R7Dwgs7HIsEO8Dz9LyONJftowZ
6xZ0wOxPIY1TPf0rP78vfiJ99qLWsJNUC7CIJ35TMh+E9KI57CM8c6BI73A9
AHXQMP7xEFw/W4B9rO9ziNwwqwSGiOqCkTvD0F8ZgvWXLm40qMzNg3h+meW/
giNg5H4K6xurmCtPFn6CLQs5Cz1OI/BpqhJ9zenUyYCzw8BsE9KnHNAK2Tvc
Pqek9BI/gS2ounFljrVHecRedkEay+VU0oPddOr4Vw8A3XPeiJx3vZDXEphO
+oGvHNfVJ1d+jx2q4i742g+r9IWx/oSrsP47tW7wNrDtcX41CMGDlWmk130e
i2fWaoObpt55q+SH4TMrA647WLbzsNvvBqim7so/eG8Elo1+YB+/CvF8z6pv
wFjuLiX5cHTl92ZBrD9ssTm0VKcAhGb/ht3aOwbpPfQ4T8fwPFPwzgTgy8/a
vfBiDN7AT6z//Unmj8efEVinO7n9mXY7aHfwWsyqdxKMPtoj5XrDwG5zgVXa
uAcc9jVYky7xrkTe9fgg8OwIUmGo6YMG9mtWpM9qbnMsFO6H5x7WbDc5BmGr
bcl50ks0hATCE3rgVQ79Kxu2YZBc7YJ91ig8/hy1rgPOTZxr8y4YgW/12uak
8439MHaxaoLQGrNQV8kxiPpZjPUFUj8efXCogcbA8KhGyji42gyfJH1fiMX6
jheFIKQ8fk58aRyM2WVOk84pfbK/cHMS8LgJ0fOfmQCGeOpZ0nc2vJZ4RRkD
6RYF8eK4DniifdmOv7iJkByueCnjNgIvCrOyv4j3gjPL31ukp4qm+151GIK/
Gi8dBwv7Ic6T5jHZoa1tWwYg/65PXrTwEBgc17pBOpXpp2iJVy8Eb7Vnuqc8
ArwlS1dJj9biHfjs1An5lTdu9jKOQUeQpD3py+ePMlTfbwZ/3vWp3O7joFTG
jv1vQv9YjFYteOYZNLuET8Cck6Mj6coD3t1jkUUAeVcZ7lychE9br1whPZbn
sfq5iSR4K/Ij/13DJJzw/459FDSYNzbIjkODX/SbK2mdUJmccfnUqhqC3oih
DE6PgmVmtPvmkl6QzPS7QXqtiqC6PwzD9yOcYOEyABIN8vakv10PKjcaB2Cz
Ta39zNQQmHxa50D62oQ2qWcSffC2QPG18OZRyIswv0T60VypkEzvLmAsEO7l
mBuDzsixa6RvFwx+0HuyBZhngz5235kAXSujq6QLWWqPbRmqBUXto5w5sZPg
uJbPmXQNSdbv7y8Vg9LV6/IzTlNwjz78OunRmYPCr7yT4UB7Y/3M8BTM9ytc
Id2E99P87qpx+HRauPHI6S5w7mPT38uRTwRXb8mV+zkKV45c1zGX6oO7FQmX
SX9cu8H9QNEwWPl+thf/MQCH55WsST8Ln4O0dAfhR1rsltZrw3A6U9aYdKpg
tinL0z7I0ovT9/g4Cr+7TE+QnmaxesF4bzf0J3jUCgaOw7WoRVx3fCLShXGs
Bfp3yqtnCE4C/auRo6QXPJZby6pbBwfzw5hNjadAyeGyEemTr3LsX00Uw6SR
56CC+Mr3ezkhE9J92Zc9HSaTIUPWcjL11TTYfWU+RXpCy2vLkYQu6K48+J84
1wTxvo1erSjemMr7R8SB9PEOVybS39U1mfozq1BOeY+dIz1hgG4t6QcY7W60
zZtT7ZyULpHe9a6RmfTr2mnpEjIm1HbnPFfSW8uL6El3cFq7hpHXnHJZcvQq
6dGcdNi/wEJPT2fPBoqgosl10idOhK4h/eAgz+ONdhRqhsMo+ty2W+i2ebNb
2K4rUibd1l8j3Y3tN/qBOh7e8gI5imd7zE3SbyssMpLOrDu1d72zNMU8Pe4W
6bkzn9F/yifNvzITp/Cr7bxPOuN6RjrSkykPNdflmlDm7Vfbkd6zwRz3dbIu
jnLFQo+6z8jQmXT16TQG0p8xC7e4BBpR5Lm5cV0q4zach0HINTBEUIcamWmH
+00rF8H9hkcEnheNkaSuvWmK9a8kObGeX2TysDfLIWpLu8cN0ls8j6DzisXp
BDfuoPZXpuM5sC7cQ5+dyLjwdJssZdX8EazX+HUcfVSqPOflGmHK+io3R9K/
B7jgfm2c39xtt99JyctKxvsN32aK8yRqZuTsd6OjhnA/wP0m8u/G/U4qWGSK
qx6g7rqmiOeclzRBO//SRTGjVlFKbtk67H/pbwj2dwnJ0I7nUqPURx3Fc846
9Q19R1Q8l7K9JuXm+WO43x1faPt9Wl0wMactTC3+twfn//brNO0eFfNYM9bs
o6wWt8c5fV5dxDlfFjM7HC4Rp+yeMMPz3HhbAt3GvsPWg+UAxeqcMq5LkWrA
dYXfzpSEJPBSJYZfos9YfEG/03lP+N5LfUqhGOC+4p6P4brdx97vYpuSp3A2
0M7/933a+V+USljfYSZP3cBm70R68h5l7PP97kLJi1FhasOeDqx/3iCP9VyZ
6qVaXxmoj9atwvvyGkxDrzstbuIg8FeVkUMbPWYmHL3piRn7vQB16snITTgn
77lO7L9B69vj/WKbqE/zVHHOe79pc/aUbEtPqZeimnaH4flsJU7gOdQ0dti8
kD5AjR5gxTn/OBtjn6p3dPzl00M545Ml2N//fjw6EWT586m4HoWj/Tq6n0sh
usXLW9tbvPdRz7VM4Jwde27iujFz27uKGaQo61mTcB4KUYFe0J38cIRvO2VN
20a8Xz4L2vPvE2G/ZzJWjEpfoY/1xoWD6Ed2Tf5U3raPsq8oinZufw6gm1KN
7hdVslDDWS5gH7lIVtr5r+bvmq8WpEx/l8Y+zvTT6Krsf5fefpxWPfTroz3p
x1oG8Bw2Dl0OCg0QogaqirmQXlHsgu+p/8m83dtDDCkb8ySxj5r4d+xzrOlA
wbuL26i32rjwPNUzPbCPdXzRPpba/VRRyTNY/4HooT0nWUP62w8KUa5qD+H8
Khvl0KnvQ6RsJrdRnxSnoMN2QG8Xi+e5wSFDfWwjgeepGPIRPeLvU+t/k5zU
nK6ztH3R0fr3TEkraFpuoGalDeO9QFMU3gu7weKdQXUVylTgAvYf8tmB9V8U
e+WuB4hQ7K/XY5/WDfnoH4MyGIxNpalXrV7iuvLafuhXtetjyj//yPFJscY+
bYzH0I/O3DrBJbWZai2fge+XdyY7ngP91sOiF74AxVFPBeuNSk9h/a6lUmYn
wV2U/A56XLepZAE9fJXWxRLz3ZTVyen4HDqe2o7zlzBHmnDZ7KEmPJjB+t0G
WVi/PujE3HwmhVJW4Yp+8XYrulswb/u0ghxVfCgY1w0s0ELvfXA0oDdBieJ6
2Qp9zJI2v4IN0bO1XYh6xqcB55dMY8L5x/pMlMvKxagvs8/jPJ7fVXAeU5Wb
HcdKV+635Qreu6sI7XNGzW9xp0jSeur5cxvw801k+CXWH9g7+pNPW5lq/dQT
57y5owXXlVfu1Bll3kOtVvmI8zxxUUNPv+vUGDh7gDo9rnWFdEPKF+x/UntO
VmtwO6Us1Qj7J5t4Yf8bzFfUQsLFKdxSyjg/T7cc1id5Do5/juOj3JTegnMm
qt9Fv5R3j/KGR5mazdWE92tn74LrLvoWdrOwylPeuN/HObOjmtHVE+45TTkI
UNa+uI/v15smRvTiuJoEugkByo57HVi/J4RAnzx2mOPB6d0UaUY9rL9L3Yje
qLdBxMWFl6ohU4Pr0q9zRX88naz//NUuSuGpLOxjIF+GXtS49ViAgCwlS5i2
7pk52rp859cypwpJU7YfPYR9TMQj0L91yEbsvLWeosKjgee5wfokuveXmcIX
japUmUDa8zkSSns+tR/fHzzDLkrlTZrCPgzpN7D+7Za1lzTjxCkeG5WwD4MN
7efL5N1FzhS6XdTYnBM4D8cyB3plYHnX5xE+6j2zbLwXgzxbvBemWav99Wq7
qJIvefF9VBKhfT4z+Z3YmF1DR/XxvID77dXuxj63TJgmGs7vpDS9lXvtPD4N
GpF3dBu3pBBOh2q9V+UPq0be10J3StXSIF0nYtiPTW87ZeCtBHrqZnoD0rVC
U1LD7i7kDL/hQ/+Pm2pC+t2T0UclP0lTuYdF0Wd53huS3j1w5+7PTkaqYSvN
TSXi0G1ZRYO0f0tQn27dQZsn2Fyf9ECH60oHmASpWg9k0I9zXdQjPURGnKkn
ZDBnn7AkelJpKdbbXhFbrxhJTxFLl0ZnOVGI9eXe05xtu1mpNUVi6G3C9Ti/
hTNXuIEeE0VJjBe96LSGGemKKelD6Q2C1Ow1+9FfsoocJF1og4X1112SlEUt
2jw3vO9j//Qzd8Sa/UQoJWFK6Amts9qkBzwISRlkl6fsZpVClwt1P0p6ojWL
7BmNjdSfF2l9FARvYh9Dg6xwAeWNlEtBtHqD2xpY/+tj8yqgMlDnw2nnX6O3
GuePu71scJpxVtV0hHaexx5F43m27v+zQ4iem7KQq4wu03oT5zmjdGKIc4Gf
wq68C92iQhHvPSU0OljbdQPFSJFWf9IzBesdr2dn5YrJUp5do52zqlU0nrPh
rbdvg7fwUVqSaX1209FjH3Uzt+NDJQuq4avl0X1au4+QzrY7b1nBkY3Kr0eb
f1htO84/rjOzxXMTC+XEFtp9+Qcy436Lou/VJ57cSGE9R/PdMcN4PvJjG4ZL
EydVNW7tRD8llI/1oxbPdSTZVlO8dFTRY9+YHiK9b4tfccAeQcqiKu08hQVs
sT4tYLUk25Y+VcYU2rl9cc7Bc+uYctfNCi5V5Q2kPYfWW8Vwv/tNnwfEs/JT
zdVo6ypeb8U+kS3vlVmWBlWN9tLcxq4TvSxqyK42P011/xOaOwpkoD/yO+wU
KsNKHZGguX9JP7q94uUhzyhOqtY6mmcd/IGuf0f12dledqrdpm3oF/5cNSLd
zNDlkpPdNorsLyF0esEzxqRvv9E/ayo+rzp9g/Zc1RxywHPz61wnu82dlRK0
SZz2vH1MwPNPjbr8qcufmcKbSnsvjKza0ZklA3w/r3zvdZGj3XsLRz6eg3Rf
0J/Cr+upskO0ewyk9KN3/+oRlH/zN4c5ltbfYvUV7CPY+v6PSh0DlbqVVm8m
rIJeEuv6uWxgRPVhCu1+E2aLcE6i8wP9ecMFVbEQ2jlwG6TgOfT1Naz6bjqa
syuZNo9+mQ+uu+7MgN5Djv+okRTaPbIbfcd7fJP+mMU8mZPKJ097Dh+sK8fn
sHlvgVXayvNQ7Uhbd21DN66rf65fb+DZOsqmAdpzUjsuiOs2rg2Lb9Dnpsro
0eoddvzE+usTqo+52JiopUy0fZXt18R9PeWWfqOgzEQlPtGenw2nt+Ccu8pu
JvxrWFY9zkHb19a109j/L8OeZP0nLNRf+2i+y7od/SXLpb1RL4dVufbvQf82
cPUwznN49SqpP0IUeea96MTgBx28RxdRz0MneSmPGGmf59MEge9jdcmnradY
+CnnxmjzWx2Lx/nHuJe04g1ZKX9aaPfemVSO8zv+DN19jnkVdcaaVn+LcRTr
ldYp7BGUpaNqp9D2dd9tE+5rYhcf270NvarvKmTRWzVbcd24m83b+qnjOX4l
tOfhz2Mr7L/G2H+7Rdec6olJWp8Ht/7hfv8kfvihvmsmZ38L7X6tg5ywfyyz
R1T2PlaKtBbtXt56nsP6ygUfHmWP1dRcIZpvD7iFrnpr6qm3HxclQJ7Wf6OJ
PvZJtlIIcR9cT629R+t/pPQt+oua1a8eBKylHrxA+xzLsBPB+emLhucKLRtU
E4xp79FDdzc8h71fX8l37VlN3fIf7XxiddhxXV691BeLH9ZQrexp5yn88Q/u
16jw1mbV5U2Urfwi6M8zevD93XpBwS3LhoPifJI2vzjFAvvU8ih17jjOQt0k
Rnueh1IW8HlmNb5rcmH1cs6xhP8/b+1/cX6N2z7XCl6vp9zKpZ1zcoINrnt2
f2ebqQ0d9QwT7RyKqs5gvbVip2vE17WUcVba89bH9x3XVQ7ZmVLe3pezfTvN
a64MoltcGL4tvYWBsjSqgA7RzPgeVW/vu8syOaPKx0Wbp+U04LoDGpY/WzwH
VVk+0J4H+tkRPM/kO4ZZjCOs1PoZ2vlfu3wZ57nxSebu4yBGyj0O2vy2iinY
RzU+Rq1WbTP16yjNh90M0W8bzyyozrFSjmvQzk3B4Dztc7i9gqM+bx21uZN2
/hx+pVjf6Fi0Rqt3OUdnLa2+fOgu7f2Sd+JLoStR/WxL81u2Juj0BMNjGYm+
nMZl2vPAHtWD+7XVusG7w2wddZ8P7Xx8i7KwnqV5ps3r5H/UNdto792Efgru
t7/oupfDhuGcTx7bwsn8lfmrVsxfMS/OBpE5JdujS3Jk/kogyr+EzF/xW/Wj
865OO0rmr/7EmKWT+Su9/W3oxzP4NMn8VXOYyTiZv9rufw9zUDy7HP4j81cH
U38vkfkr+7eJ6Gw9apZk/uq8xpsYMn9VKKSHOa4H8R4cZP6qec/SKJm/kud5
hvUO+o3GZP5qec8DMzJ/dfhNCa47Myf4ksxfvftreonMX/27dxTr/aRMs8j8
1a5PAW/I/NU3ohjrR2eEVMj81U+27vNk/krse+ABMqfUPKYlRuav/t47w07m
r5JlNqNnLRsmkPkrB7t/MmT+yrNUF/3BaN4Amb+6F5bDQeav1l6QQE8yer2d
zF/16cti/uo0ZQT9B2/1BzJ/ZdSjv57MX21IOYDeZ9XYTeavtj81LSDzV1Sn
BcxTpUccVifzV+eLrwmT+avFLQboTKLfZMn81bJfZwiZvwrba69O+larH0Nk
/urnfb0kMn/FndCA/eluXGQj81dBwoxKZP6qqIIxl8wpnf864Uvmrwz/bmUg
81fTIa0E6Wx0UUFk/ur9pOZTMn+lF5qBvtn+bW/u2SooXfrzk0e6BgTdatD7
3bZxkPmrqU8p82VptUD5+AP9YZHVUTJ/xfOnPZvMX+U+OoD+e2uvP5m/uvVp
Bw+Zv7ol5YDzOMVYcJD5K94NZz3J/NW24stY3/Ake2AsIhFcP+SUkvmrGc8F
9GaGVQOhOS+h+7rsKTJ/5Z63F/tMf++5TeavhCwrfpP5KwmTXZhTevKX/hCZ
vyo33FZP5q/+TSuif4sOmyDzV+m2Zulk/iprHaCXTY5j/mo/x5doMn9VbaWB
/kBzbMnrfR6s+3RumcxfTX5axHzUzXVfLpH5q33EBR0yfxXfzof19JFPDpD5
K7ljehQyf2VynJb7yj9v7U3mr2bmZRrI/NV7BwX0rz1ViWT+yjT9fiuZv4q5
tBs92exVGJm/sn0+FkjmryyBB/33k0rN4KPZ8OWgaBeZv5pKos6SOaXxcxkm
ZP7qUImkBZm/GhxuRI/PEjtD5q9SO3nTyPxVrGYl+vft7QVk/mr7Gt7nZP6q
IL8evUXNwovMXyk/fFdM5q8+NOejq342WibzVwV/NCzI/NWIWyv6YKYhkPkr
Y5uOGDJ/xTDVgH6j5gJB5q88UlsYnvZRYZ18ObrAmuslZP6qeVLUjsxfOa6n
ef46FkYyf+XhOXmCzF/d2l2BPk1fkcVr4wuFYYcWyfzVUmsc5pSuKvKb8gb4
QPtSaBeZvxpopPnZ3805Zy55wTiPTuvf8PsQWkjzj7m/w8j8VcSLfavJ/NW7
HJq3nNuTV3/YE/IWN+uQ+av2CporhcjsO5LvBmqWpevI/NVS2f/Xnd/zmMxf
fb3CGfkixQ/OJND8Vai2Apm/UidOu8T1+sNsOM13PSSMxp+fg4mLYVcvpjyG
0SKarxFVsY2MNYD8OX4DMn/16x3NT47NLFt6pEHxtfdSu9xjoK1brcKqbZp4
obTrydT6z+BN59WxSiAOHE0PorOcKLMXPJMCX9+WH6CXT4LyVC30SSk6QsM5
CcabQhZOi6SA9G9N9LUilR1FX+KAjan4inNbKsgXaqAfyGvz193+AcRfeCkN
GqZBaLI6ule/9YZgzUiwyRhfXPRKBylBmrN0d9/5T/8NqFWlFv24mQEJygfQ
W6X7A72/BsKmTT3HG3gzoeUBrf+jo47JQtkuYGJYfPDd1UxwyaegOzAHqyZw
lQCVy0nn594sOCumkSdqME2821JZruZbBDL+X1mTZagw9/AY+su6RNOa7ALY
Mj3yLeBMHkCNOjqn6ugwa1IeRP1hsrd2KgDXj3vQj6v2vB9qokJrh8jmr0eL
4PxNmpdVsV8xmsgCwrg4vry/GI5s0kX/nE0fMpz4GcTiWu4/lymFTAFZdDsh
+i8mNgmw43vYEr1MGQxF0uZ88L00epQ5ElZzsyVNd5XB0/od6EzGlr5PivzB
U4JfSEq1HAw2G6EniVxmHwyoheGnzOeN5QrAzuVADMvZKaJBUNSqwKYGjNIp
V7Xni2GpTxT98YIuE8HyDVYtLA9m3ioHVfkz6K2iFj2C4+VQsan1Z0h4FYiL
26OnSXbmPnAsgbp331MdFWvgJ6GO3vRim2N9VgEEd/tkTNnWQu7hI+hPeGKk
U10I0L24v+b8+Tq4su0o+mRCKru5YAb85PdqKZKoh82H7dCjbQI//vodB2tj
23ZEJtbDA1kT9AR7/6CHMcHQx1QpYP67HmwKz6O735o7TOaR1szHyZN5JLNo
iRHMC7G15pN5pEZPsVEyj1QVrYhOaVHPIfNIudK3W8g80sFBBfTFnUN3yTzS
KcmtjJHpKz/fDF+jx+s/2OihVA3Hsq4QZB5pINsV/db1O4VkHslRVk6NzCPt
uWCMfjnZVYnMI1Gqro2QeaTwy/3oExfflZB5pNhuOW4f91b4J5KMbn/1bSeZ
R1oVwg9kHilQ/RJ6SrLVOTKPdOTjHwbOwDZYW6qPrhPJFvS+NBF6H79+xXky
HOacuvB7mMArCeWXsfHwZ+rdpp6SKBjXHkTX4xaT6N/5CULF39uu/xEDuUat
6I9twdLq+AfgDkto/PTtI/Ak9dD+DlLaLruoHA2SG5/xRprGA78grX+l6mmr
Jy3hUDBQd3DuXiLYCo2in58rVXju8gbuvX7oYvM+CQLf0ProAb/O1oln8Nek
9cI+k2SI6aD1+ShwxEtu9yMYexSSbVqdDPO6/eias5JratxugBAlXHgvRwoc
0mqmfb+fdJil44sgRngWzI3Lggj4t7xs+mcabNbwmb0cCiM23ww1kE94QZzi
XrXKbMWNpTj3Rf4OJfrhuYPeodcE3ROaqwbu1NbPfU089J2+oOUXSnw4sRr9
mPlbw1NyLwn7C/B9MCOM8Auk9Wfi1rL6z+QZ8YBye757Npy4GEXrk+i4Pvq+
cgChFN8SKP4ngujcQfOvo6ayd5u9iP1Tq6Z98iIJo8+0Pr/S25KsOl2JcP5j
530PRxG86rR1t83uX/Xj+HmiWHNI+/bzKCI7k9ancssGtZ4fuQTX7dnC5/dW
vp9byKsfiJgGtd0+IivjETH5lWrm4unEpTZJ9KJcn+t1f7KJtOdF2WJeWURZ
AAWdkS9y/OrvLEJ7Iknk5PNsYpWOMjprWOpXrsh0Yv7E7T4jbYLYOquAHpAn
J6vGlEoQWjNBalW5xAfnrehDTeE3ki0TCIlsCcGq3Dwiqk8NPUvsoMI/vffE
Ezr+sMN38gnpXFH0h8kObzJ+vCaWBDL47Zbyidfy4ugtXEt3rrY9IGK2Ove/
OlBA9DrT+rS2Kc6CWBXhpyh+1bqNIJ4eNDM9MjwFHT87rUV2VxCqj26rv2Qv
IIYonOisbd/YBSdLCeuqWvPaX0WEF8t5dKWuVWttTYqJmgimn5+DS4kKYUDf
KN77Vup2AUGV9AudGy0n9PmaTEinnIr9eNMyl5ALb6KDkUqi6r4u1jdMOE9t
FfxCqL7SpFf3/UbsETyOrsVo+bZJNpU46Zac1FxVTQhMqqJvUZM6OMXygWAu
Vziy2qSG2OFsia7pWqL7rPIpIWxlt+1eYA0hlncYPS+5kkXnbgPh6r/kpXeu
mKh3Xfu4sHwS3oSarqETrieaXb9QG7+VE/vbJtGnpzZMGHjVEkXqYRxZfd8I
UZsX6KeXUy2f2VYTR+JXefvp1BLP5EWekK5fH/fI+EwFcW68omXbZB1xtOop
1n/Oyxd+w1hCRB471fbXqIE41Xsb601s/gQpnMonJH+HP4lybiT0a3dg/Y9f
60ReLH0hFp6ezbtzsonYzJSA7hY1nuY/nEQETYvuzvzTRNQFFaMPrHM6+Mwh
hAC6mxEXdZuJ9LQM9Dv/7ZNU3tlG1E+FOaa+rCS0I15JSOmv/B50/tyltXYt
xHK/cXcGXy3xy6AE/ao1+43s003EbrbT1WwZ9YTdojl6WZq+ZQlrA9F2h5td
bEMTsf3FPknSzULCgz1caglNBTkjf4kWIvynNjrHoTWPN9hUEVz3dYQ+zrQS
pWZj2Gded8trsxslxAm2ap3mS+1Eam0Uuo/+94MBynlEokCP5FX/DqJemB29
yyTzUeGzNML8iIrloHEnEWpuh/0viT665t8TRgy++MFPJToJwvQR1svqKv/w
etANb5cZKOf31kJwh/hzuTtjxDvrTr1Z2044smD2Zul1A8xdYUVXu3WG9e5M
G2gkdxR6UJvhd+frYNJH5W28Zra3wOr9Fx6zB7eBweIm9NPZ2/23szVC2ayb
dhxvJ5xRPP+M9KEp5ofdKbWQ9mNdyAW1bpALfYE+1uvHdgUq4e9/fftU7vXA
3LUBXPf1xZa734lC+OFxivpOrhcYT0mga/5UXdu0lAWcmeV3vsb2gmf8dvQl
jZ5+D6MoeDMbW8s51gstC2ew/79nt/k+qPZBLEPtDPfUys/NnZ2yL4xHiMLZ
+Hf/GHuBzo174662ZjBsOILOy8zTskOuGwYfnD32z7kd8kdV0A0VXUW7Z9rh
UtO+AKaiLlhDXZIhvUFhVb+BZQuoFD2w+9XUAx3ff0qTzt7OEtTg0gA3K+nO
7OPug4vcO7F+6y2OfZ/MagD8vvtd4OuHZT3aupb/fLMq6Ethw/PiXZShfrgk
vBr91ua5rZ0bqfA0eHjT/qsDkM4wgv3dDp1tOPYwBnrDqboZXwcgMNQE64np
wwuzbgMwNqI+0GrTBJ6/HyXu6Bkk1lR9zCsQ7IetmTb5PFVtoPv2WgLpd3Yf
0ZN+2Atv2TJ1M3q6oOqoJNYb+9sX+Fzohkd+On2XtXuBbzOtzz9u939+Vu3w
RNZ9YXS8DzbctY4n3b+S5Rw/QzPsd9YNjyVzobu4kkiPD7PvpLOsg4Pn4gBu
D0LrGgX0b3WNLY1/y+FDvvkXZosheH9fHvswnW+UuzGUCwK3DRmU54fAIvo2
zsl8+4V6h/1HSJEc/zd4eBiqH0lhveIFrp++7kPQkz3bNvKsBTYURrAmbu8n
dDkckrctD4BJb7XYqUud8CCT5z/S59jqSq7t64dTlxUDb//sgXz74+tIZ90i
LMeo2gsjDmmjeX/64PareqzflLWod4ytC/62POMXKhoApUYN7H/KoV3nq3or
1BY6dU/oDYHzsRasv69+/b+TEg2QOJXbqv10GEoFS9FjPVv2JIRWAXsnG5+K
7wjonlJBDy2LZNrqnw/3dIlPCXtGwespBecpPFUzPUUfD8svuZW8no5CnKIz
egnjQiD5dx19U8F14jq9xOoStRsSN/dTQg9SAkifs6tnJZ2VNdJUaaMa5Yp0
Bf59qFM+Zw3pXVPqgvzmlpRiB3Xsszll33+k88HlN6WSxpSjviH+pB+pn15P
+vOTw+e8dZSpfvHbH5N+4xkXemnKxWwDz+PUb7t8nuC/I7w2yUZ6z+0gGRUD
OcrBL+/QN1uUo28osvh65rchha7HDfu82evHQbry415b48W5nC9tdejH7FnQ
x0//+LrTRppC7/ED+8itOox9ljf/4pCWVaJ4tp/Gf6eodk6TgfSjxnEpCVdt
KBBojH0e8n3DPo8ZUgPM6YSpfqrufqQ7rYvdQDrleGFP8d5D1J9a3tg/r3cK
+5s6lI86ch6gHJEg0N+df4V+5PCGmyWv1CiPyhewf4pKBTvpzbJWdTXPDlD8
jp7A+sRRBXSHxN+hp4WYKAqBN7E+JCME55mXrX+Z2LKJkpRejs7Cx41uXXlc
PIVPiFLea4RzGjydwDkf16es2fRVk9qa4Y73FVHbhfc+/TjwhP34AcqX7/N4
X/3VFngv1232CTaZ7aBaHzJF3/nMjJP07xzjfMsJhtR/uddx3cBHb3HdtTJr
uewLNSlndiigXw1chX18wvorjPuPUI+lW+O+cqwlcF81np+UbFVUKZJOa9Fb
a1LQtc+XfLu9R4IqJcOBbjoWi66bIuT2vWk/5d6ZQuyf/0sA1023U3q9JLWO
UunT4Et6+WDKRtIjdo5yCFIPUg9xVuPzzDr1A5/PfUu84vlKhpSNqlO4r3Bn
O5wzX2KVTe9GDmqxxyOsd0y3wOf/fkLrstILTcoBFXv0csIb/V/klaa6h3qU
zZIM6IbfpfB+12xReLj9hyq1VuIlzqnaZINzBqeal8rZL6kqfLLEfbUnyOC+
fOM2WIYelqQKjDRhffJWetr9Ro8erblwgML1/gD6a4Yx9KKidVe7yuUpxQmL
6I+0S7GP8y2zr7lLq6mDF0uw/w1/P5xHVyrmlvunvRQLsavovwY4sX51sLTD
Wg1dqtGT33gOmnOWeA69in5a40L61PpX+9Ethl3w3om3x58F9bNTB+2vom9u
k0HXO/1RiuOCGtVkF+05lBijPYfLcx3/FPcIUeTtjqH/LqpEF0teeGOorEq5
48WMHuWtgesmv3gv6KNNoTRtCsA5/6X24fw/42z5Kpi3UC+/z0SP8I1EV2gO
09M/DlS1Jtr9Nmd/x/v97PpJ6vioNsVTeDPWb7oUgvuVONt/eODpQcqfxLfo
gT5V2IdFbrUpV58Y9fKObHxfFMV08H3p0dQUus67m2pzfwbnrHnSRHs+l52d
0u9wUh75cGOfy4xh6Ek+wVI8a5Wox9N3o69zuoK+00hGnO8GHWVbxFl0nqZt
6Pa1fsnurXKUASER9MrzfugFFvU7+XklKO+CF3Ffb9Z+wn2d4eWMOTayl/LW
vQbrLc674fz37pz6UH5SgdLhXoj30uOSgOeZl7bHPjFZnWr/7xPOPzCjieff
/G2Q0dNzP1Ve7hS6Q0AOutzGmS+mE7soX75GopvVGqJL8OiY6cxwUCqHotCD
lvXRr1aNe1jEU6jJ/cPoxKcRnP/9Sw/fqO+KFBs5e5zT3oMH/ZPT3PFvFBVq
0NpL6L8v8qEXVTtcyd4iR7l9Yg7nz2w8jfN/fXbNx7hci6r7Zhr7n4juwHq9
u493bq/eTdlzfR268CJg/Ynt/FJTNyWoiuFHaV7ViHP2/FazbDslSf1Ml42+
iU4KXebO+Nj7rcrUjmeFOE/YXACe5+a7TTP5SsLURZNs9IDKt7SfO048jbJT
DFTtmfPoxspCOE/k9lV2Y2pcFDvNK9i/8kEU9k/QfasveFiBQgRy4OftwQlf
vMeHERH/6PLlqTzXv+B+s4SpOL/9/K+jX6oPUH8xncQ+66dysY/iw6cFTFv3
U/3pP+C6djdycJ7r2gdXnS6VpHQkGOBz8mN3Gn4ulb1wnuJL2kTdHEn7XF3w
p32uVrw8Nh2/fR3VhP43eu6lA9jnEnEraY8mDyVngQ99ZvE51j8+rLXd13E1
NfBlFLpIST7Wf/wvdEbMrUa1JakLffrIBfRH5su27OskqfzrL9Peu9rN2Od/
LZx5OFVdG8YzewvJkESpQ8YkU1LOeZQGUTI0mIUUkUpKFIqUlKG8GV6UDJki
SZNhb7NkykyIzAqHRBp9Z639/fu71vXsffZ13fvc+1nPur0H3051z6gyek2o
+nMc0ZjT9/4nI3BuG6H2+Bj+vcGGhfj3rmD2lgXmyjKeuYVjbu3hgXlrdesG
fT46MclzCNeR0NXFdT63NK3Z4chOOJ2SoZ5nXRp+L7F5KCad36HF+JzghJ/P
KcUo/Hy8830uM0/JEJIF1Pvhj18srhOsf89QxV+dEehP+ROlHsqfGH9t6H40
LcG4ZMiG63g4q+Hf+2aF649D69iImxPqlN+Ypf7v5uq5Q9+sl2JYZDTr+ep3
gYeKC7dc7iAwcp1CY1uHwStzheRiQDtY27+ezrz5CYziFst0DcZgObe0w8uL
LaBWu9t1jfFHkK8hk/jPf4Zvb+cNDyi/hz8+yVJ5ql3wKbJDdq/+BLyEWdn9
DjXQqgkdUNECssfqlzAqJiHxuI3q+dVloN+94UyBah08Yv9VazUwBX3N7Byd
6i/AYWFoICegBM6JJqduesiEK/YX5StKo0GX3ik57J8EfuoG1ecWmJDQVM8m
WpsE95fp/nq/KxvmnVIXhSaZ5FnV3Mh1D17DQENopq9SOXiE+mzR9GWSE+Gi
5v38neCQHvnSNnYAcs0ytoWvGoW26KKpUdk2uF/AftPdrB/4PdKEnzqPw7zr
+lMX/zRB6W9jI/v8bli2dHbMy/kLGN23IiW06iGyPsvz66l2qHkZeOf5ikkY
H+ixzROrgjf/kA90ut9Da51oJN1uCnS0tWcVTxTDu+zI817KVdDVZ5EpeIAJ
85eM+1uuPYFbaxpGxYl86A03yDXuZEKnN6nJN9sPbqm63QXSX2BPxdIb7i9v
wcqN9v2X7XJh58DvmLDZApA5wTYTV8ok922TbhitqoQ8qUFT9bn38I8jXXoP
67p+C78zE8ki6BjluLDgUAUW6424V7Ou694z9+nLpizIT4g1mVZ8ASd3qYZa
ojwrs8GuJ88vQ2V62PDBe0Gg6DKQh3KWLDhOcV/jfQYVYTu3id4sgLxhu9xU
1nW9dYTf0QVL4HCCceEZzhpwFnooOC/FJJ+tX7L9q0s1pMwGHQ3Y3gx2ixEj
dMkp0vV4uPMBnwYIc5XtyynsgJ+qHgoJbybIV79v/Qm53wzGdGeVMzM9wEW7
LWH1+zMpYCWwK/p5G2h2pXEojPSDSnHo3u7xMTJOwl43L6MZHq4YZjQL9sJh
YVvjM9OfYSwmdplGagP479u+Rqy3A4S2Fre3PZ0A/TS1mfGoasj32XHnBuv7
LNFz1vCNyBRIPVwx+5pRAq6KzMiW/TXwK1DWrRbtG8Z7vbM8+Qwu61XW1q8u
hPMHe4eGCCYc2TxZ49t+FW4UK5qraodA4qOOzfyLTHjmELyYdDaT/LCSEVAe
lU+6ZYvv8f/AhFrewkZRhyJyyUfazLrVVWRFzJRzlgmT9T+UTguIrSSz71hd
62p6T1oWyqzccmIKNr9qum5K1JGHx8Xgm307GaT1UfXb6km4/OuiPosDcBIi
LA5O+4voLE66fBExCaxoAi5B9bTBrG7gU1AUGDr/hdTZ/HJe/GsrHCOsGb4H
++HIBlJf+uI4mXSpPPgj67lkeNker74/AEZDfx5/khslfytf33LL7wPQGnwM
oh4NgYnNTVfjiCHynkSO9+vaHjixtE3J0W0ETG4+aQt99YnsbYq4zE98hKAp
qDk7PQqdA2MyEgd6SRvex/RkuX4wDPOr65MfhxulWct+1beTTw7ntSkW90Pm
8KtXJSs+g0D+iKkRN+t3l4rxFqz5BNOPLsTT0lk++u4DHm2xMjLVtA3Pl5ZM
OeI5UrotM9NlvwHDdXQHnl/dNpZMnQuoyJ1TddIlDF5l47nTe9nUuRWzh3lP
Z3foMTxpqZgnc73CfM59mY/LqlVEsYcRriNwORHX2eLqltcRocAwCtDBc7Pz
26m54tY/Yib+f+WJ+a98eL2qXR5ev8n+dANNRYFh+84Vz80uCTGl5t7rRHZa
/lUjDt3mxDy72B3zIOXMyEs5UowDA8LUnG3oKcyv1PVeoOdxExr91LzN43UZ
eN4mac8u9Sp3KeLvEWp+bHp7AZ4DeWuxuT72jwSjuIuaD+k/PYP5FG9A8w7h
meL2/89xGZ2l5rh0sh2dHV900tuXUXMv+WnNeO7lsVRdaVMSLyEjTs0RJd7+
B6+/edWugPlFkJhfQ83tKHZTc7CdfuOaepJsBN9Faj17XD+eR/reIfPzCedC
cXoItf7wnAJe3yZo2vSUc54u6Eld9/SbFGr+59yWcuaSyWLibP1upF/Ph1V7
kH4fuR44gPRr8SOcE+n3rN5ncaTfe8IjHUi//fq0EqTfYz2mDki/e/tKDyH9
mmofnkT6dbMeiEH67eVeifWrZPaaifQbyiMl0MDSrzRnxiDS7yGf+kNIvzVv
vd9uZumXv3MA63fM3lOFpV/Q2ik5VBaVD7t07PRZ+iV/jHjJrXQoApX5verr
V1fB4ZzK4yz9kpOBkaIs/UL4i3vnWPqFzJNjq1j6JU0sI08gna4aaldCOqUV
ntFCOvU5XpjReCsZtgnVCPWczgH/rkkihPVcxlOtr73RiyJpclV1TkcekVXr
LcHmBxM2VV6ysmrLJ0eicgUktEvI2PAvf0KSmLDE6leC4UQpGRbXXd05Xkv+
GFqTvXZsCoSemxbUataQM+mbk2IetZBtoeTLw3WTEOxR0y676j051GZSVCrY
RRr6qqT+ZzoBiSar1044tMCm066+hqofwepVPLev/2fST58Ue3+2HYzdOLgC
vD9B7LX6/anmY+SXDfMLtK1dcJJbUtI1eRB2ztFcqoaGydVX7sfx5XbDe4n1
Nb7hwzDoOF0aojRIusf3Uz7/eQD2G2KVK754hxgTUq+TsZ/JlanA/qS51/2p
VPsOhuq8GfYn7AKN2EexVTl1/3dZkshR08Z+RsC8G/uihY1Zsv6SKgxrTgtc
v7D/MPZRikunT+w7oU6Md3RgHmQVga+7/IZs4G6fdYTXxWu4/vDmYFz/dyyX
iaKCHOOtoS71nfLbFvurtbauahUJqsTWlquY9175ie9z6Uj0OdJxli7PWYnr
3Pu1FtcJVvnw9rvYasYupQBjlBdqZ2wziPJChdWTFkVsp0A8RjUG5Yv2qLtC
ukU52IQXYr7dpQrniB4VuJOFckTt6ATm0rUVXLnStaBX3OyBckStvmVhvqCs
cBzliHbtTQ5EOaImsd2YHxT/0Y1yRLXbxcJQjmjIjyrM5TjNR1GOaInD8TUo
RzSirR1zt8alpShHdI3Gg9soR5R40ID5dOMLl1aNbJDl+0JDOaLVvCWY/w67
a0PriAKe9uvlKEe0YqfiEpQjanicXefK1iYQbdS8aDJdAZ3WZfeQz+mSGl6h
M9YI69sjU8YT38KQ3QjmP73yH5T9rIObRl4pJEcdmNvWYG4nEWH+7lkNbOFu
eEYTaIRoGwJz7XJOrg9aZVDIW/pY5WcTPLbtw9yAt/LaD50iKEw0u3CsvxlK
NIWxv9qzvdZY7NlzUNO7b7gQ0sJ6Dh14vdbYKmmhhlQYPh96q+d7Cxyls+P1
VvMKEyjPVkYpoxPl2R4cWyaO8mDn9Bt0S7kqwP3e9AzKv716eynmBmF6nqFr
S8ErqOUryrk1+i6GecvDVrOGHAKi2J12qw+WgM5hfswnzwVtTyILwFFWPA3l
3PZKUvVDTb77s195CRzBXz+hnNtuU4pf6XTPQzm3naeNPfV+VsDuvWsxPzXo
/QDl3M4EyrahnNszawWo+/wexXE9IBE0ordfcTpbBR5RHJhridxeHtp8C5oX
+LNpZVUQObu4CnF+U4l4tB+tdMPEGO1H63OcwO/lHt5vc2j/OuvFw1q0f330
3THM/yZkeaJ9ar183UK0T50XaU3NI7KLPEf71JnX/BSfNGTClmJzzJ2u/TmL
9qn7lk+dQfvU3mLHMdfPf5OC9qml1C1y0D41TdcW8xJ2y3C0T+2YcPAp2qfm
t6auq3FulQfap06Xu5+A9qndCSfMa+u9+tA+9UmeXXNon/qjCbX+tq9CNNqn
7siPsUT71Pa0o5j3XTDLRf3w1cbflFA/PNb/Ne4nL9N7ifvnXHc6cP9cS5GJ
+9KyAt9rUZ9ce9nUTdQnVw50Xo64pPawBuqThze1DKM+uZbqPtwPl/F+twf1
yX9e4liH+uTWrZ0UL/h3H+qTn0iL+Ij65IZepgKIyxdECqI+uY27fCfqk0f0
puP1PLkfD6A++d3XtatQn1xpQgWvj9/Zpoz65M1fQ9NQn5y97Dzm2q+XpDE5
skGPtmsL6pNP2Slg/t3NGvuT9Ix47CuEBOmacYtChPTYSexn9ttvwH4mcnxo
YuKpKkHc/47PeT1JFcG+6KXZpQ6tDmOGsNFB7E9GTahzshVsfJyttuqEuwUb
Pr8T4fWLOh/X57X0qZQ0ER93C3NPaKfOb+6vc+XJk2YU3f2D62x8rYh5Sgpn
XrqhLFHz9hq+T4ML9zAv0lFuYNsqzhgWGcV8pIY6v9zwoUjGvnU5ofJtBPP2
ZooPya0UtAj8Ta+xoHzF9IFc7CtyGXvuaIX1FauayVJzDZ4XsY+aNhlOz19g
FnemUnO2PZY62OdwOjzgU7ZcyfAUpXxLzn82uM5YYg5z/i038W4p5aMO+VHn
IALmY1v2c4sQqqspzvbiM+Z56kfDxdzZCedOak5YaL0lnvvt8vpssJWbmwAn
6j61dmRR5wUOWlY8k56nn7Wn7ueEvyWus2Vjue22urHicFeqfmpXFeaP/k24
UfQtrjjvutsTpN9VlTFxSL9HOyi/6DK0SSaNpd8Jqb1MpN8uaynMxQyt7iD9
PhaVn0T6Xf6LRvm/uOk6pN/LF+8bI/0OcVJ1+Jc1xCL9vvf7Jx7p1+TEOswZ
+gkZSL/x++sHkX5TiqnnKxQf/Rzpd5NcTTXSb5AmVWeX2NICpN/k4P2vkH5z
RKnrBhIbl6ux9PtvxsRyc5Z+X9pRnCeaXtDI0q8Z27AH0m+kBcWb6q2KUF6T
5E3+RY2IBpIx/3cU5R3VJ+YcRflOz8AP5R2Ri9+cMY+sX1aFcpys8t2bUY6T
fHc85uXC1YEox0lXVZ0T5TjZFZmM4dynrI4fV7UbSa+sh7Uox2nIgQvzyOaM
TJTjRA810kc5TqfcbDEfuODoiHKcGgWFRsavdJI9X6/i+iuvBssFVhLkaKGu
CMpx+u9IMObcfLsqUY5Twm2tHSjHafcLNVxn34vTTqTMA/LRczselOP042MF
Xi+caIh9jmwqgX2Ocoj/tztSeozYTEvsQ6I+a2J/wj7Lpc5Zv5mYTtmNOU/6
EcztyzfejalYSTx+SPW3vwDV32a3dZtRXqtHJDr8xX1jA55B7JdiYt9HDosq
ECneanj9nAYH9kvvlmwp/rp5EzFjM415rVc3rq8mIL/pl/s6ovou5X/YpSj/
Exq3hNYerMJoEnmFud1TDczv5ou7XF+xlWGTYIHvczhUC9f5l0+HfkaBhzGX
JMzY/OA1dHdHf7ylVA6iBQ/DNHyZZP8GVd4elk92/dfIiMnyyYVC3KnIJ/f9
zH4nxPKZ1QZHHwSwfGZQf9PKQJbPvOGzG79/ArrH8PtB90moIp9hGd1XiNJR
pvgU1pFlxO44czo/YdjBJ4Xq3zov7o/qG9+tSUH1g55Wr7VnfXfvcNMtWWR9
d5urTojMsL67ae4x2E+uWzuLn4/agUB5t2Ql4n+cMVPH
             "]], {
           DisplayFunction -> Identity, Axes -> True, DisplayFunction :> 
            Identity, FaceGridsStyle -> Automatic, Method -> {}, 
            PlotRange -> {{-0.8067224167583664, 
             1.2443110347131079`}, {-0.9169525481046421, 
             1.4143330471708688`}, {3.138614994663922, 3.9360320548988814`}}, 
            PlotRangePadding -> {
              Scaled[0.02], 
              Scaled[0.02], 
              Scaled[0.02]}, Ticks -> {Automatic, Automatic, Automatic}}]}, 
         Graphics3D[
          Point[{0, 0, 0}]], ControlType -> 
         None}, {{$CellContext`TypeOld$$, {{"r", "r", "r"}, {0, 1, 1}, {
           Rational[1, 2] Pi, 0, 0}, {2, 0, 0}, {
            TextCell[
             RawBoxes[
              Cell[
               BoxData[
                FormBox[
                 SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
             "InlineMath", $CellContext`ExpressionUUID -> 
             "ac4494d5-7022-4b04-9d21-ee2100588f50"], 
            TextCell[
             RawBoxes[
              Cell[
               BoxData[
                FormBox[
                 SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
             "InlineMath", $CellContext`ExpressionUUID -> 
             "61f66885-76ae-4040-8977-edc2197c65ae"], 
            TextCell[
             RawBoxes[
              Cell[
               BoxData[
                FormBox[
                 SubscriptBox["q", "3"], TraditionalForm]], "InlineMath"]], 
             "InlineMath", $CellContext`ExpressionUUID -> 
             "17945a6d-caf3-4555-ade9-6fdfb1395e36"]}}}, 0, ControlType -> 
         None}, {{$CellContext`params$$, {0.5, 0.5, 0.5}}, ControlType -> 
         None}, 
        Dynamic[
         Grid[
          Table[
           With[{$CellContext`i$ = $CellContext`i}, 
            If["p" == Part[$CellContext`Type$$, 1, $CellContext`i$], {
              Subscript["d", $CellContext`i$], 
              Slider[
              Part[$CellContext`params$$, $CellContext`i$] = 0.5; Dynamic[
                 Part[$CellContext`params$$, $CellContext`i$]], {0, 1, 0.01}, 
               ImageSize -> 160]}, {
              Subscript["\[Theta]", $CellContext`i$], 
              Slider[
              Part[$CellContext`params$$, $CellContext`i$] = 0.5; Dynamic[
                 Part[$CellContext`params$$, $CellContext`i$]], {0, 1, 0.01}, 
               ImageSize -> 160]}]], {$CellContext`i, $CellContext`dof$$}]]], 
        Delimiter, {{$CellContext`r1$$, {0.5, 1.5}, 
          "\!\(\*SubscriptBox[\(r\), \(1\)]\)"}, -3.1, 3.1, 0.1, ControlType -> 
         IntervalSlider, Method -> "Push", MinIntervalSize -> 0.1, ImageSize -> 
         160}, {{$CellContext`r2$$, {0.5, 1.5}, 
          "\!\(\*SubscriptBox[\(r\), \(2\)]\)"}, -3.1, 3.1, 0.1, ControlType -> 
         IntervalSlider, Method -> "Push", MinIntervalSize -> 0.1, ImageSize -> 
         160}, {{$CellContext`r3$$, {0.5, 1.5}, 
          "\!\(\*SubscriptBox[\(r\), \(3\)]\)"}, -3.1, 3.1, 0.1, ControlType -> 
         IntervalSlider, Method -> "Push", MinIntervalSize -> 0.1, ImageSize -> 
         160}, {{$CellContext`r1old$$, {0.5, 1.5}}, ControlType -> 
         None}, {{$CellContext`r2old$$, {0.5, 1.5}}, ControlType -> 
         None}, {{$CellContext`r3old$$, {0.5, 1.5}}, ControlType -> None}, 
        Delimiter, {{$CellContext`Type$$, {{"r", "r", "r"}, {0, 1, 1}, {
           Rational[1, 2] Pi, 0, 0}, {2, 0, 0}, {
            TextCell[
             RawBoxes[
              Cell[
               BoxData[
                FormBox[
                 SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
             "InlineMath", $CellContext`ExpressionUUID -> 
             "ac4494d5-7022-4b04-9d21-ee2100588f50"], 
            TextCell[
             RawBoxes[
              Cell[
               BoxData[
                FormBox[
                 SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
             "InlineMath", $CellContext`ExpressionUUID -> 
             "61f66885-76ae-4040-8977-edc2197c65ae"], 
            TextCell[
             RawBoxes[
              Cell[
               BoxData[
                FormBox[
                 SubscriptBox["q", "3"], TraditionalForm]], "InlineMath"]], 
             "InlineMath", $CellContext`ExpressionUUID -> 
             "17945a6d-caf3-4555-ade9-6fdfb1395e36"]}}, 
          ""}, {{{"r", "r", "r"}, {1, 1, 1}, {0, 0, 0}, {0, 0, 0}, {
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "75a08159-001e-417c-b04c-4d8833c6c8c1"], 
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "85feb9af-fac7-4a31-96bc-02c9fa353adb"], 
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "ac226dba-08cd-4a3a-be9c-d0195bccdfd6"]}} -> 
          "planar robot arm", {{"r", "r", "r"}, {0, 1, 1}, {
            Rational[1, 2] Pi, 0, 0}, {2, 0, 0}, {
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "783e4ade-c5c0-4011-b7a6-c4f92ff43638"], 
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "f6cbe890-1676-4bde-9a07-e77f4fb4380e"], 
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "3"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "a1f6ecb4-a5e4-493f-963e-c45e92e4e9c3"]}} -> 
          "elbow robot arm", {{"r", "r", "r"}, {0, 1, 1}, {
            Rational[1, 2] Pi, 0, 0}, {2, 
             Rational[1, 3], 
             Rational[-1, 3]}, {
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "d18c7dfd-4993-4fea-8f30-5601ed07adc1"], 
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "65d49791-8247-491f-af42-235bb864d46d"], 
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "3"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "8f8a5e1f-905f-4876-b1e3-8755dcb0d157"]}} -> 
          "PUMA robot arm", {{"r", "r", "r"}, {0, 1, 1}, {
            Rational[1, 2] Pi, 0, 0}, {2, 
             Rational[1, 3], 
             Rational[1, 3]}, {
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "97c6b72b-7c13-43d3-9939-826b35321e0b"], 
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "6ba5e64c-8837-4658-85b8-7c421888a628"], 
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "3"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "b302060c-ab1c-49bf-a77a-70fab5ffd8c5"]}} -> 
          "offset PUMA arm", {{"r", "r", "p"}, {0, 0, 0}, {
            Rational[-1, 2] Pi, Rational[1, 2] Pi, 0}, {1, 1, 
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "3"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "30d84090-bbf0-4c36-92cd-6fa49f441749"]}, {
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "404e5879-03b2-45b0-8ae1-82793a684e31"], 
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "0bfa1c86-ec86-42ec-a81a-affde2f63536"], 0}} -> 
          "Stanford robot arm", {{"r", "p", "p"}, {0, 0, 0}, {
            0, Rational[-1, 2] Pi, 0}, {1, 
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "c2d2263d-5dc7-4f9e-8004-1ec48d64d401"], 
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "3"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "15691925-19b5-4369-b2a0-6ad121af4092"]}, {
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "b3ee7ad4-cb2a-4fe3-9b8a-1ef786aa800f"], 0, 0}} -> 
          "cylindrical robot arm", {{"r", "r", "p"}, {0, 0, 0}, {
            Rational[-1, 2] Pi, Rational[1, 2] Pi, 0}, {1, 0, 
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "3"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "43429225-23d6-452a-9db4-d5b9f2eafa87"]}, {
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "eb0be0cd-61f3-4f2a-bc08-320a4fa03a9a"], 
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "ca3ad2cd-03cc-48f6-820b-8f0717450f39"], 0}} -> 
          "spherical robot arm", {{"r", "r", "p"}, {0, 1, 0}, {
            Rational[-1, 2] Pi, 0, 0}, {1, 0, 
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "3"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "d27b1fce-af6b-4633-83c2-d25f6bfd6aa8"]}, {
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "3a04ad8d-fd68-4dc7-ba17-9c0555256829"], 
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "3346b394-321e-4396-b7da-8e1eae823eed"], 0}} -> 
          "offset spherical arm", {{"r", "r", "p"}, {0, 1, 0}, {
            Rational[-1, 2] Pi, Rational[1, 2] Pi, 0}, {1, 0, 
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "3"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "f545a84e-d885-4723-a911-5cf6565754c1"]}, {
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "279ea711-2f69-4fb7-9346-4c1bb2d1cc6d"], 
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "89f673fc-286e-44de-a783-b92f9162dd49"], 0}} -> 
          "twisted spherical", {{"r", "r", "p"}, {1.2, 0.6, 0}, {0, Pi, 0}, {
            1.5, 0, 
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "3"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "472057d8-b7f8-4b2a-b8c9-0a8987f32d9b"]}, {
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "3305ad2b-ea05-44ba-bc2f-2ed114fe44e4"], 
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "71a45250-720c-48f2-8c74-61297be636e3"], 0}} -> 
          "SCARA robot arm", {{"r", "p", "r"}, {1, 1, 1}, {
            Rational[1, 2] Pi, Rational[1, 2] Pi, 0}, {0, 
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "18797ff3-4302-4e50-ae39-512d28599863"], 0}, {
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "fac033f6-dd0a-4191-b8fe-642c1a88dce5"], 0, 
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "d25f4409-df56-4da0-ae08-fa6d5f2d5544"]}} -> 
          "planar RPR arm", {{"p", "p", "p"}, {0, 0, 0}, {
            Rational[-1, 2] Pi, Rational[1, 2] Pi, 0}, {
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "f25b47c6-4d41-455b-ac7f-e1c2cad6903d"], 
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "18868fbf-5381-47b4-a008-26da7ea70dd8"], 
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "3"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "efaddb6f-2b58-46a6-9708-307ecc29f113"]}, {
            Rational[-1, 2] Pi, Rational[-1, 2] Pi, 0}} -> 
          "CNC robot arm", {{"p", "p", "p"}, {0, 0, 0}, {
            Rational[-1, 2] Pi, Rational[-1, 2] Pi, 0}, {
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "c3056f93-3fad-42f7-8ec5-84e1318a3b3b"], 
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "caf662aa-42f7-48dc-9407-ded68dedd5cd"], 
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "3"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "a1ce9786-c98f-467a-af45-20c625ffd093"]}, {0, 0, 0}} -> 
          "Cartesian robot arm", {{"p", "p", "p"}, {0, 0, 0}, {0, 0, 0}, {
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "1"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "7a6ea635-61aa-4be6-94eb-9323296d28b0"], 
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "2"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "22733fc5-48a1-40a7-8c48-2d06f3710442"], 
             TextCell[
              RawBoxes[
               Cell[
                BoxData[
                 FormBox[
                  SubscriptBox["q", "3"], TraditionalForm]], "InlineMath"]], 
              "InlineMath", $CellContext`ExpressionUUID -> 
              "ad9e1ee6-b2d1-4fc8-a5f0-adcc3053b08e"]}, {0, 0, 0}} -> 
          "linear robot arm"}, ControlType -> PopupMenu}}, 
      "Options" :> {ControlPlacement -> Left}, 
      "DefaultOptions" :> {ControllerLinking -> True}],
     ImageSizeCache->{650., {240., 245.}},
     SingleEvaluation->True],
    Deinitialization:>None,
    DynamicModuleValues:>{},
    Initialization:>(({$CellContext`dhTransform[
          Pattern[$CellContext`d, 
           Blank[]], 
          Pattern[$CellContext`r, 
           Blank[]], 
          Pattern[$CellContext`\[Theta], 
           Blank[]], 
          Pattern[$CellContext`\[Alpha], 
           Blank[]]] := Dot[
          RotationTransform[$CellContext`\[Theta], {0, 0, 1}], 
          TranslationTransform[{0, 0, $CellContext`d}], 
          TranslationTransform[{$CellContext`r, 0, 0}], 
          
          RotationTransform[$CellContext`\[Alpha], {1, 0, 
           0}]], $CellContext`drawJoint[
          Pattern[$CellContext`j, 
           Blank[]], 
          Pattern[$CellContext`d, 
           Blank[]], 
          Pattern[$CellContext`r, 
           Blank[]], 
          Pattern[$CellContext`\[Theta], 
           Blank[]]] := 
        Module[{$CellContext`jr = 1/5, $CellContext`ar = 
           1/20, $CellContext`pr = 1/7, $CellContext`vr = 1/6}, {
           $CellContext`drawCoordAxes[$CellContext`jr], {Gray, 
            If["p" == $CellContext`j, 
             
             Cuboid[{-$CellContext`ar, -$CellContext`ar, -$CellContext`jr - 
               0.01}, {$CellContext`ar, $CellContext`ar, $CellContext`d + 
               0.01}], 
             
             Cylinder[{{
               0, 0, Min[-$CellContext`ar, $CellContext`d - $CellContext`jr] - 
                0.01}, {
               0, 0, Max[$CellContext`ar, $CellContext`d] + 
                0.01}}, $CellContext`ar]]}, {LightBlue, 
            If[$CellContext`j == "p", {
              
              Cuboid[{-$CellContext`jr, -$CellContext`jr, -$CellContext`jr}, \
{$CellContext`jr, $CellContext`jr, Plus[$CellContext`jr] - 0.1}], 
              Cuboid[{-$CellContext`jr, -$CellContext`jr, 
                Plus[$CellContext`jr]}, {$CellContext`jr, $CellContext`jr, 
                Plus[$CellContext`jr] + 0.05}]}, {
              
              Cylinder[{{0, 0, -$CellContext`jr - 0.05}, {
                0, 0, Plus[$CellContext`jr] + 0.05}}, 0.7 $CellContext`jr]}]}, 
           If[$CellContext`r != 0, 
            Rotate[{Gray, 
              
              Cuboid[{-$CellContext`ar, -$CellContext`ar, $CellContext`d - \
$CellContext`ar}, {$CellContext`r, $CellContext`ar, $CellContext`d + \
$CellContext`ar}]}, $CellContext`\[Theta], {0, 0, 
             1}]]}], $CellContext`drawCoordAxes[
          Pattern[$CellContext`jr, 
           Blank[]]] := {Thick, {Red, 
           $CellContext`drawZArrow[$CellContext`jr]}, {Blue, 
           Rotate[
            $CellContext`drawZArrow[$CellContext`jr], Pi/2, {0, 1, 0}]}, {
          Green, 
           Rotate[
            $CellContext`drawZArrow[$CellContext`jr], -(Pi/2), {1, 0, 
            0}]}}, $CellContext`drawZArrow[
          Pattern[$CellContext`jr, 
           Blank[]]] := 
        Line[{{{0, 0, 0}, {0, 0, 2 $CellContext`jr}}, {{
            0, 0, 2 $CellContext`jr}, {1/32, 0, (3/2) $CellContext`jr}}, {{
            0, 0, 2 $CellContext`jr}, {(-1)/32, 0, (3/2) $CellContext`jr}}, {{
            0, 0, 2 $CellContext`jr}, {0, 1/32, (3/2) $CellContext`jr}}, {{
            0, 0, 2 $CellContext`jr}, {
            0, (-1)/32, (3/
              2) $CellContext`jr}}}], $CellContext`drawWristCenter[
          Pattern[$CellContext`r, 
           Blank[]]] := 
        Module[{$CellContext`jr = 1/5, $CellContext`ar = 1/20}, {
           $CellContext`drawCoordAxes[$CellContext`jr], {
            PointSize[0.02], Orange, 
            Point[{0, 0, 0}]}}], Attributes[Subscript] = {NHoldRest}}; 
      Typeset`initDone$$ = True); ReleaseHold[
       HoldComplete[{($CellContext`drawZArrow[
             Pattern[$CellContext`jr, 
              Blank[]]] := 
           Line[{{{0, 0, 0}, {0, 0, 2 $CellContext`jr}}, {{
               0, 0, 2 $CellContext`jr}, {1/32, 0, (3/2) $CellContext`jr}}, {{
               0, 0, 2 $CellContext`jr}, {(-1)/32, 
                0, (3/2) $CellContext`jr}}, {{0, 0, 2 $CellContext`jr}, {
               0, 1/32, (3/2) $CellContext`jr}}, {{0, 0, 2 $CellContext`jr}, {
               0, (-1)/32, (3/2) $CellContext`jr}}}]; 
          Null) ($CellContext`drawCoordAxes[
            Pattern[$CellContext`jr, 
             Blank[]]] := {Thick, {Red, 
             $CellContext`drawZArrow[$CellContext`jr]}, {Blue, 
             Rotate[
              $CellContext`drawZArrow[$CellContext`jr], Pi/2, {0, 1, 0}]}, {
            Green, 
             Rotate[
              $CellContext`drawZArrow[$CellContext`jr], (-Pi)/2, {1, 0, 
              0}]}}) ($CellContext`drawJoint[
             Pattern[$CellContext`j, 
              Blank[]], 
             Pattern[$CellContext`d, 
              Blank[]], 
             Pattern[$CellContext`r, 
              Blank[]], 
             Pattern[$CellContext`\[Theta], 
              Blank[]]] := 
           Module[{$CellContext`jr = 1/5, $CellContext`ar = 
              1/20, $CellContext`pr = 1/7, $CellContext`vr = 1/6}, {
              $CellContext`drawCoordAxes[$CellContext`jr], {Gray, 
               If["p" == $CellContext`j, 
                
                Cuboid[{-$CellContext`ar, -$CellContext`ar, -$CellContext`jr - 
                  0.01}, {$CellContext`ar, $CellContext`ar, $CellContext`d + 
                  0.01}], 
                
                Cylinder[{{
                  0, 0, Min[-$CellContext`ar, $CellContext`d - \
$CellContext`jr] - 0.01}, {
                  0, 0, Max[$CellContext`ar, $CellContext`d] + 
                   0.01}}, $CellContext`ar]]}, {LightBlue, 
               If[$CellContext`j == "p", {
                 
                 Cuboid[{-$CellContext`jr, -$CellContext`jr, \
-$CellContext`jr}, {$CellContext`jr, $CellContext`jr, Plus[$CellContext`jr] - 
                   0.1}], 
                 Cuboid[{-$CellContext`jr, -$CellContext`jr, 
                   Plus[$CellContext`jr]}, {$CellContext`jr, $CellContext`jr, 
                   Plus[$CellContext`jr] + 0.05}]}, {
                 
                 Cylinder[{{0, 0, -$CellContext`jr - 0.05}, {
                   0, 0, Plus[$CellContext`jr] + 0.05}}, 
                  0.7 $CellContext`jr]}]}, 
              If[$CellContext`r != 0, 
               Rotate[{Gray, 
                 
                 Cuboid[{-$CellContext`ar, -$CellContext`ar, $CellContext`d - \
$CellContext`ar}, {$CellContext`r, $CellContext`ar, $CellContext`d + \
$CellContext`ar}]}, $CellContext`\[Theta], {0, 0, 1}]]}]; 
          Null) ($CellContext`drawWristCenter[
             Pattern[$CellContext`r, 
              Blank[]]] := 
           Module[{$CellContext`jr = 1/5, $CellContext`ar = 1/20}, {
              $CellContext`drawCoordAxes[$CellContext`jr], {
               PointSize[0.02], Orange, 
               Point[{0, 0, 0}]}}]; Null) ($CellContext`dhTransform[
             Pattern[$CellContext`d, 
              Blank[]], 
             Pattern[$CellContext`r, 
              Blank[]], 
             Pattern[$CellContext`\[Theta], 
              Blank[]], 
             Pattern[$CellContext`\[Alpha], 
              Blank[]]] := Dot[
             RotationTransform[$CellContext`\[Theta], {0, 0, 1}], 
             TranslationTransform[{0, 0, $CellContext`d}], 
             TranslationTransform[{$CellContext`r, 0, 0}], 
             RotationTransform[$CellContext`\[Alpha], {1, 0, 0}]]; Null)}]]; 
     Typeset`initDone$$ = True),
    SynchronousInitialization->True,
    UndoTrackedVariables:>{Typeset`show$$, Typeset`bookmarkMode$$},
    UnsavedVariables:>{Typeset`initDone$$},
    UntrackedVariables:>{Typeset`size$$}], "Manipulate",
   Deployed->True,
   StripOnInput->False],
  Manipulate`InterpretManipulate[1]]], "Output",
 CellID->96089594],

Cell[TextData[{
 "A robot's ",
 StyleBox["workspace",
  FontSlant->"Italic"],
 " is the total volume swept out by the end effector as the manipulator \
executes all possible motions. The shape of the workspace dictates the \
applications for which each design can be used. This Demonstration lets you \
load several robot designs to compare their workspaces. You can set the \
position and range for each joint using sliders."
}], "ManipulateCaption",
 CellID->80008708],

Cell["THINGS TO TRY", "ManipulateCaption",
 CellFrame->{{0, 0}, {1, 0}},
 CellFrameColor->RGBColor[0.87, 0.87, 0.87],
 FontFamily->"Helvetica",
 FontSize->12,
 FontWeight->"Bold",
 FontColor->RGBColor[0.597406, 0, 0.0527047],
 CellTags->"ControlSuggestions"],

Cell[TextData[{
 Cell[BoxData[
  TooltipBox[
   PaneSelectorBox[{False->Cell[TextData[StyleBox["Resize Images",
     FontFamily->"Verdana"]]], True->Cell[TextData[StyleBox["Resize Images",
     FontFamily->"Verdana",
     FontColor->GrayLevel[0.5]]]]}, Dynamic[
     CurrentValue["MouseOver"]]],
   "\"Click inside an image to reveal its orange resize frame.\\nDrag any of \
the orange resize handles to resize the image.\"",
   TooltipStyle->{
    FontFamily -> "Verdana", FontSize -> 10, FontColor -> GrayLevel[0.35], 
     Background -> GrayLevel[0.98]}]]],
 StyleBox["\[NonBreakingSpace]\[FilledVerySmallSquare]\[NonBreakingSpace]",
  FontColor->RGBColor[0.928786, 0.43122, 0.104662]],
 Cell[BoxData[
  TooltipBox[
   PaneSelectorBox[{False->Cell[TextData[StyleBox["Rotate and Zoom in 3D",
     FontFamily->"Verdana"]]], True->Cell[TextData[StyleBox[
    "Rotate and Zoom in 3D",
     FontFamily->"Verdana",
     FontColor->GrayLevel[0.5]]]]}, Dynamic[
     CurrentValue["MouseOver"]]],
   RowBox[{
    "\"Drag a 3D graphic to rotate it. Starting the drag near the center \
tumbles\\nthe graphic; starting near a corner turns it parallel to the plane \
of the screen.\\nHold down \"", 
     FrameBox[
     "Ctrl", Background -> GrayLevel[0.9], FrameMargins -> 2, FrameStyle -> 
      GrayLevel[0.9]], "\" (or \"", 
     FrameBox[
     "Cmd", Background -> GrayLevel[0.9], FrameMargins -> 2, FrameStyle -> 
      GrayLevel[0.9]], "\" on Mac) and drag up and down to zoom.\""}],
   TooltipStyle->{
    FontFamily -> "Verdana", FontSize -> 10, FontColor -> GrayLevel[0.35], 
     Background -> GrayLevel[0.98]}]]],
 StyleBox["\[NonBreakingSpace]\[FilledVerySmallSquare]\[NonBreakingSpace]",
  FontColor->RGBColor[0.928786, 0.43122, 0.104662]],
 Cell[BoxData[
  TooltipBox[
   PaneSelectorBox[{False->Cell[TextData[StyleBox["Slider Zoom",
     FontFamily->"Verdana"]]], True->Cell[TextData[StyleBox["Slider Zoom",
     FontFamily->"Verdana",
     FontColor->GrayLevel[0.5]]]]}, Dynamic[
     CurrentValue["MouseOver"]]],
   RowBox[{"\"Hold down the \"", 
     FrameBox[
     "Alt", Background -> GrayLevel[0.9], FrameMargins -> 2, FrameStyle -> 
      GrayLevel[0.9]], 
     "\" key while moving a slider to make fine adjustments in the slider \
value.\\nHold \"", 
     FrameBox[
     "Ctrl", Background -> GrayLevel[0.9], FrameMargins -> 2, FrameStyle -> 
      GrayLevel[0.9]], "\" and/or \"", 
     FrameBox[
     "Shift", Background -> GrayLevel[0.9], FrameMargins -> 2, FrameStyle -> 
      GrayLevel[0.9]], "\" at the same time as \"", 
     FrameBox[
     "Alt", Background -> GrayLevel[0.9], FrameMargins -> 2, FrameStyle -> 
      GrayLevel[0.9]], "\" to make ever finer adjustments.\""}],
   TooltipStyle->{
    FontFamily -> "Verdana", FontSize -> 10, FontColor -> GrayLevel[0.35], 
     Background -> GrayLevel[0.98]}]]],
 StyleBox["\[NonBreakingSpace]\[FilledVerySmallSquare]\[NonBreakingSpace]",
  FontColor->RGBColor[0.928786, 0.43122, 0.104662]],
 Cell[BoxData[
  TooltipBox[
   PaneSelectorBox[{False->Cell[TextData[StyleBox["Gamepad Controls",
     FontFamily->"Verdana"]]], True->Cell[TextData[StyleBox["Gamepad Controls",
     FontFamily->"Verdana",
     FontColor->GrayLevel[0.5]]]]}, Dynamic[
     CurrentValue["MouseOver"]]],
   "\"Control this Demonstration with a gamepad or other\\nhuman interface \
device connected to your computer.\"",
   TooltipStyle->{
    FontFamily -> "Verdana", FontSize -> 10, FontColor -> GrayLevel[0.35], 
     Background -> GrayLevel[0.98]}]]],
 StyleBox["\[NonBreakingSpace]\[FilledVerySmallSquare]\[NonBreakingSpace]",
  FontColor->RGBColor[0.928786, 0.43122, 0.104662]],
 Cell[BoxData[
  TooltipBox[
   PaneSelectorBox[{False->Cell[TextData[StyleBox["Automatic Animation",
     FontFamily->"Verdana"]]], True->Cell[TextData[StyleBox[
    "Automatic Animation",
     FontFamily->"Verdana",
     FontColor->GrayLevel[0.5]]]]}, Dynamic[
     CurrentValue["MouseOver"]]],
   RowBox[{"\"Animate a slider in this Demonstration by clicking the\"", 
     AdjustmentBox[
      Cell[
       GraphicsData[
       "CompressedBitmap", 
        "eJzzTSzJSM1NLMlMTlRwL0osyMhMLlZwyy8CCjEzMjAwcIKwAgOI/R/IhBKc\n\
/4EAyGAG0f+nTZsGwgysIJIRKsWKLAXGIHFmEpUgLADxWUAkI24jZs+eTaEt\n\
IG+wQKRmzJgBlYf5lhEA30OqWA=="], "Graphics", ImageSize -> {9, 9}, ImageMargins -> 
       0, CellBaseline -> Baseline], BoxBaselineShift -> 0.1839080459770115, 
      BoxMargins -> {{0., 0.}, {-0.1839080459770115, 0.1839080459770115}}], 
     "\"button\\nnext to the slider, and then clicking the play button that \
appears.\\nAnimate all controls by selecting \"", 
     StyleBox["Autorun", FontWeight -> "Bold"], "\" from the\"", 
     AdjustmentBox[
      Cell[
       GraphicsData[
       "CompressedBitmap", 
        "eJyNULENwyAQfEySIlMwTVJlCGRFsosokeNtqBmDBagoaZjAI1C8/8GUUUC6\n\
57h7cQ8PvU7Pl17nUav7oj/TPH7V7b2QJAUAXBkKmCPRowxICy64bRvGGNF7\n\
X8CctGoDSN4xhIDGGDhzFXwUh3/ClBKrDQPmnGXtI6u0OOd+tZBVUqy1xSaH\n\
UqiK6pPe4XdEdAz6563tx/gejuORGMxJaz8mdpJn7hc="], "Graphics", 
       ImageSize -> {10, 10}, ImageMargins -> 0, CellBaseline -> Baseline], 
      BoxBaselineShift -> 0.1839080459770115, 
      BoxMargins -> {{0., 0.}, {-0.1839080459770115, 0.1839080459770115}}], 
     "\"menu.\""}],
   TooltipStyle->{
    FontFamily -> "Verdana", FontSize -> 10, FontColor -> GrayLevel[0.35], 
     Background -> GrayLevel[0.98]}]]]
}], "ManipulateCaption",
 CellMargins->{{Inherited, Inherited}, {0, 0}},
 Deployed->True,
 FontFamily->"Verdana",
 CellTags->"ControlSuggestions"],

Cell["DETAILS", "DetailsSection"],

Cell["\<\
This Demonstration creates a serial-link robot arm with three joints (three \
degrees of freedom) and displays the robot workspace (the volume swept out by \
the end effector as the manipulator executes all possible motions). You can \
manipulate each joint using the sliders and select from a variety of robot \
types. The workspaces vary greatly and determine the tasks the robot is \
suited for.\
\>", "DetailNotes",
 CellID->447453696],

Cell[TextData[{
 "In this Demonstration, prismatic joints and revolute joints are limited by \
the range sliders ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["r", "1"], TraditionalForm]], "InlineMath"],
 ",",
 Cell[BoxData[
  FormBox[
   RowBox[{" ", 
    SubscriptBox["r", "2"]}], TraditionalForm]], "InlineMath"],
 " and ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["r", "3"], TraditionalForm]], "InlineMath"],
 ". The range is between ",
 Cell[BoxData[
  FormBox[
   RowBox[{"-", "\[Pi]"}], TraditionalForm]], "InlineMath"],
 " and ",
 Cell[BoxData[
  FormBox["\[Pi]", TraditionalForm]], "InlineMath"],
 ". Changes to these range sliders or to the type of robot require \
recomputing the workspace, which is time-consuming. Changing the current \
position of the robot arm updates the graphic quickly. "
}], "DetailNotes",
 CellID->558642309],

Cell[TextData[{
 "The shape of the workspace also indicates where the robots experience \
Jacobian singularities. Snapshot 1 shows the spherical manipulator at its \
default position, with the end effector pointing along the ",
 Cell[BoxData[
  FormBox["z", TraditionalForm]], "InlineMath"],
 " axis. The manipulator can generate instantaneous motion in the ",
 Cell[BoxData[
  FormBox[
   RowBox[{"\[PlusMinus]", "x"}], TraditionalForm]], "InlineMath"],
 " (blue arrow) and ",
 Cell[BoxData[
  FormBox[
   RowBox[{"\[PlusMinus]", "z"}], TraditionalForm]], "InlineMath"],
 " (red arrow) axes but cannot move along the ",
 Cell[BoxData[
  FormBox[
   RowBox[{"\[PlusMinus]", "y"}], TraditionalForm]], "InlineMath"],
 " (green arrow) axis."
}], "DetailNotes",
 CellID->1398432875],

Cell[TextData[{
 "The workspaces are generated by combining multiple 2D parametric plots. \
These plots correspond to the parameter ",
 Cell[BoxData[
  FormBox[
   RowBox[{"i", "\[Element]", 
    RowBox[{"{", 
     RowBox[{"1", ",", "2", ",", "3"}], "}"}]}], TraditionalForm]], 
  "InlineMath"],
 " being set to its minimum, middle and maximum values, while the other \
parameters vary along their full ranges. For the Cartesian manipulator, this \
results in a cube that is divided into eight smaller cubes. For other types \
of robots, for instance the Stanford robot arm, the relation is less \
intuitive."
}], "DetailNotes",
 CellID->994441909],

Cell["Reference", "DetailNotes",
 CellID->470230901],

Cell[TextData[{
 "[1] M. W. Spong, S. Hutchinson and M. Vidyasagar, ",
 StyleBox["Robot Modeling and Control",
  FontSlant->"Italic"],
 ", Hoboken, NJ: John Wiley and Sons, 2006."
}], "DetailNotes",
 CellID->54221902],

Cell["RELATED LINKS", "RelatedLinksSection"],

Cell[TextData[{
 ButtonBox["Parametric Equations",
  BaseStyle->"Hyperlink",
  ButtonData->{
    URL["http://mathworld.wolfram.com/ParametricEquations.html"], None},
  ButtonNote->"http://mathworld.wolfram.com/ParametricEquations.html"],
 " (",
 ButtonBox["Wolfram",
  BaseStyle->"SiteLink",
  ButtonData->{
    URL["http://mathworld.wolfram.com/"], None},
  ButtonNote->"http://mathworld.wolfram.com/"],
 " ",
 StyleBox[ButtonBox["MathWorld",
  BaseStyle->"SiteLink",
  ButtonData->{
    URL["http://mathworld.wolfram.com/"], None},
  ButtonNote->"http://mathworld.wolfram.com/"],
  FontSlant->"Italic"],
 ")"
}], "RelatedLinks",
 CellID->1860385687],

Cell[TextData[{
 ButtonBox["ParametricPlot3D",
  BaseStyle->"Hyperlink",
  ButtonData->{
    URL["http://reference.wolfram.com/language/ref/ParametricPlot3D.html"], 
    None},
  ButtonNote->
   "http://reference.wolfram.com/language/ref/ParametricPlot3D.html"],
 " (",
 ButtonBox["Wolfram",
  BaseStyle->"SiteLink",
  ButtonData->{
    URL["http://reference.wolfram.com/"], None},
  ButtonNote->"http://reference.wolfram.com/"],
 " ",
 StyleBox[ButtonBox["Mathematica",
  BaseStyle->"SiteLink",
  ButtonData->{
    URL["http://reference.wolfram.com/"], None},
  ButtonNote->"http://reference.wolfram.com/"],
  FontSlant->"Italic"],
 " ",
 ButtonBox["Documentation Center",
  BaseStyle->"SiteLink",
  ButtonData->{
    URL["http://reference.wolfram.com/"], None},
  ButtonNote->"http://reference.wolfram.com/"],
 ")"
}], "RelatedLinks",
 CellID->140341089],

Cell[TextData[{
 ButtonBox["Denavit\[Dash]Hartenberg Parameters for a Three-Link Robot",
  BaseStyle->"Hyperlink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/\
DenavitHartenbergParametersForAThreeLinkRobot/"], None},
  ButtonNote->
   "http://demonstrations.wolfram.com/\
DenavitHartenbergParametersForAThreeLinkRobot/"],
 " (",
 ButtonBox["Wolfram Demonstrations Project",
  BaseStyle->"SiteLink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/"], None},
  ButtonNote->"http://demonstrations.wolfram.com/"],
 ")"
}], "RelatedLinks",
 CellID->285307411],

Cell[TextData[{
 ButtonBox["Forward and Inverse Kinematics for Two-Link Arm",
  BaseStyle->"Hyperlink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/\
ForwardAndInverseKinematicsForTwoLinkArm/"], None},
  ButtonNote->
   "http://demonstrations.wolfram.com/\
ForwardAndInverseKinematicsForTwoLinkArm/"],
 " (",
 ButtonBox["Wolfram Demonstrations Project",
  BaseStyle->"SiteLink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/"], None},
  ButtonNote->"http://demonstrations.wolfram.com/"],
 ")"
}], "RelatedLinks",
 CellID->15861152],

Cell[TextData[{
 ButtonBox["Forward and Inverse Kinematics of the SCARA Robot",
  BaseStyle->"Hyperlink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/\
ForwardAndInverseKinematicsOfTheSCARARobot/"], None},
  ButtonNote->
   "http://demonstrations.wolfram.com/\
ForwardAndInverseKinematicsOfTheSCARARobot/"],
 " (",
 ButtonBox["Wolfram Demonstrations Project",
  BaseStyle->"SiteLink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/"], None},
  ButtonNote->"http://demonstrations.wolfram.com/"],
 ")"
}], "RelatedLinks",
 CellID->419985252],

Cell[TextData[{
 ButtonBox["Inverse Kinematics for a Robot Manipulator with Six Degrees of \
Freedom",
  BaseStyle->"Hyperlink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/\
InverseKinematicsForARobotManipulatorWithSixDegreesOfFreedom/"], None},
  ButtonNote->
   "http://demonstrations.wolfram.com/\
InverseKinematicsForARobotManipulatorWithSixDegreesOfFreedom/"],
 " (",
 ButtonBox["Wolfram Demonstrations Project",
  BaseStyle->"SiteLink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/"], None},
  ButtonNote->"http://demonstrations.wolfram.com/"],
 ")"
}], "RelatedLinks",
 CellID->475238986],

Cell[TextData[{
 ButtonBox["Kinematics of a Redundant Anthropomorphic Arm with Seven Degrees \
of Freedom",
  BaseStyle->"Hyperlink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/\
KinematicsOfARedundantAnthropomorphicArmWithSevenDegreesOfFr/"], None},
  ButtonNote->
   "http://demonstrations.wolfram.com/\
KinematicsOfARedundantAnthropomorphicArmWithSevenDegreesOfFr/"],
 " (",
 ButtonBox["Wolfram Demonstrations Project",
  BaseStyle->"SiteLink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/"], None},
  ButtonNote->"http://demonstrations.wolfram.com/"],
 ")"
}], "RelatedLinks",
 CellID->63190210],

Cell[TextData[{
 ButtonBox["Forward Kinematics of Humanoid Robots",
  BaseStyle->"Hyperlink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/ForwardKinematicsOfHumanoidRobots/\
"], None},
  ButtonNote->
   "http://demonstrations.wolfram.com/ForwardKinematicsOfHumanoidRobots/"],
 " (",
 ButtonBox["Wolfram Demonstrations Project",
  BaseStyle->"SiteLink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/"], None},
  ButtonNote->"http://demonstrations.wolfram.com/"],
 ")"
}], "RelatedLinks",
 CellID->188192833],

Cell[TextData[{
 ButtonBox["Snake-Arm Robot",
  BaseStyle->"Hyperlink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/SnakeArmRobot/"], None},
  ButtonNote->"http://demonstrations.wolfram.com/SnakeArmRobot/"],
 " (",
 ButtonBox["Wolfram Demonstrations Project",
  BaseStyle->"SiteLink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/"], None},
  ButtonNote->"http://demonstrations.wolfram.com/"],
 ")"
}], "RelatedLinks",
 CellID->114694354],

Cell[TextData[{
 ButtonBox["Model of an Industrial Robot Arm",
  BaseStyle->"Hyperlink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/ModelOfAnIndustrialRobotArm/"], 
    None},
  ButtonNote->
   "http://demonstrations.wolfram.com/ModelOfAnIndustrialRobotArm/"],
 " (",
 ButtonBox["Wolfram Demonstrations Project",
  BaseStyle->"SiteLink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/"], None},
  ButtonNote->"http://demonstrations.wolfram.com/"],
 ")"
}], "RelatedLinks",
 CellID->708933422],

Cell[TextData[{
 ButtonBox["Rotation Matrix",
  BaseStyle->"Hyperlink",
  ButtonData->{
    URL["http://mathworld.wolfram.com/RotationMatrix.html"], None},
  ButtonNote->"http://mathworld.wolfram.com/RotationMatrix.html"],
 " (",
 ButtonBox["Wolfram",
  BaseStyle->"SiteLink",
  ButtonData->{
    URL["http://mathworld.wolfram.com/"], None},
  ButtonNote->"http://mathworld.wolfram.com/"],
 " ",
 StyleBox[ButtonBox["MathWorld",
  BaseStyle->"SiteLink",
  ButtonData->{
    URL["http://mathworld.wolfram.com/"], None},
  ButtonNote->"http://mathworld.wolfram.com/"],
  FontSlant->"Italic"],
 ")"
}], "RelatedLinks",
 CellID->105557710],

Cell[TextData[{
 ButtonBox["Forward Kinematics",
  BaseStyle->"Hyperlink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/ForwardKinematics/"], None},
  ButtonNote->"http://demonstrations.wolfram.com/ForwardKinematics/"],
 " (",
 ButtonBox["Wolfram Demonstrations Project",
  BaseStyle->"SiteLink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/"], None},
  ButtonNote->"http://demonstrations.wolfram.com/"],
 ")"
}], "RelatedLinks",
 CellID->417986324],

Cell[TextData[{
 ButtonBox["A Model of the SCARA Robot",
  BaseStyle->"Hyperlink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/AModelOfTheSCARARobot/"], None},
  ButtonNote->"http://demonstrations.wolfram.com/AModelOfTheSCARARobot/"],
 " (",
 ButtonBox["Wolfram Demonstrations Project",
  BaseStyle->"SiteLink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/"], None},
  ButtonNote->"http://demonstrations.wolfram.com/"],
 ")"
}], "RelatedLinks",
 CellID->857510713],

Cell[TextData[{
 ButtonBox["Inverse Kinematics",
  BaseStyle->"Hyperlink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/InverseKinematics/"], None},
  ButtonNote->"http://demonstrations.wolfram.com/InverseKinematics/"],
 " (",
 ButtonBox["Wolfram Demonstrations Project",
  BaseStyle->"SiteLink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/"], None},
  ButtonNote->"http://demonstrations.wolfram.com/"],
 ")"
}], "RelatedLinks",
 CellID->419798374],

Cell[BoxData[
 ButtonBox[
  PaneSelectorBox[{False->Cell[BoxData[
    GraphicsBox[RasterBox[CompressedData["
1:eJztmrtPnFcQxa2kSZkqff6LtCnTOopbZCsEpyFgRwrQQQd0QMWjMVBhQ2Ej
CoiBCMxD4iWEzSMEJ85DCstiS7gBWeSXPfJkcr/HfsvaQZG/I3k199y5M+fO
PbuyLH9845urX7135cqV2x/wcfX6d5/eunW9+fMPWXxRf/vruvraLz+r/7a2
rvbWJzfeh/yIPz/w5+/4/Pz81atXp6enJycnz58/Pz4+LhaLxyUUSyB4UQK7
xggwLxyCZRKS0tKPB9qSECSUlWQJPvB3T6of20if2goGlaJTg025Quyj+LOG
aOV0ZJlh7C5nMQy2wTznJRQKhYODgx9z5KgQ2AbzyEX7+/uXLSfH/xWYRy6y
ZY4cWSC32Kf9FuXIcWHkLhLm5uYmJyf5vGwh/2Bra2uyhMsWUh5y0U+Vo6mp
6ZoDy/v379suE2hvb6+pqWGrtbWV17EtSJIVa9fX7O3tVTw1NcWuD1LKQkoG
wcjICAGZ8Eiqq6tj2dXV5Yt7UEQ5ArGVTWqHJB1hV5XtOmxZZdopraGhwYZD
wBKSspyVzijYMklkcikvGDHiEWAV4PUofHpVZTty8M6dO8r0j5gdctGBA+RB
BiCyo6Pj+9cgRieB7TJA7v7gwQNi9D9+/BieZU0JxCTrgbSlU319fYpjXWRl
uazKMhMmoDmTqeFrgDrIUxJo7FEXcZyz7K6srLDkk3ehRUo7eEjeBf2o1ZdC
mv0EtMVZGKlaXV0lliEJ2CKBuUVna2eJGQ5LTjE6LdWRJQkoUXfxqqx88Rk7
ch1OoZAEu0IKApNEXZQR/sUF5CGGADF+nrqgZtJRAsv5+fnBwUE9k+YT1NT1
fZBUljoUMStSXwO0QCA5EAw4K0t7qEvKLTzvu3geSRT3w9F3zUtiCHZ3A418
nWC2coJdVsNBasBjm9ghxHa0OuwyIj/MjJCLnpbA8mlmNDc309Ezjx49Qsna
2pokxSajcHx8nOXdu3e5KaR8Fa1pRaJBUPbJkyf6xaZ482tEFUYFA9Qi2zPq
AplyC72ySBi1C3gf+woq4hHkMB/8ENUJH7TzjQLellk6CjwHYySge3d3d2xO
EryLKkLso+hGmj+Pa7x+MGUzeJlHP55+aFlcFC2rmOKU0lfJBuin4ZOj4Dg/
HRyhhVyd0i6ji/TuwdC81WX+ICfqk2BXby0gz1zkeS5uQyjb8WnJpbwCX209
R/DNKgu56OfKgaT+/v6AROHDhw8J5GeRCINfWFiAaWtrgyFHf+1UAsnkqCb8
9vY28dDQkBJI9pnRsuvr6/DqCzRAVbBqKhIVDKhgP2X4x+oktfPXBNRUO/GW
z02NJ5lS6EQSvSQJ0E4D8fAFY9XKn3ZZKhsvedSHtCGU7Si1gDSGwJGk7kmQ
i36pHHKRZ5gSF+GT+N69e8Q3b97UA3V2dkKyhFcyJBUUd5agmuSTpt8EkdPT
08TKjC2rCiooXl12dnZYap58cioQbBcBi4uL0a2kdsSoUqxHN55MK6im/i5q
J0lsEUT70hQ+ZfK6rAZll/VD4LhkZOwIg4UYF6WwZUrrJMhFzyoHqgYGBjzT
09ODTltubGwMDw+Tg0gxMzMzu7u7iiFJsEzliCSNU6Ojo3aK4aSUFSYmJgZK
IN9I2lEHklNRwQLF/RFiMtPb+YuYeONNA7HOoi0qlS0r4gEZSHoWmTYdVcFm
6CtrgP4W6R2rRzUuwvwzr6EvQjCuN4LARRdGkovg+SbqOfhsbGxkWX27asD3
kR8NfY9ky6iv0hG46G1DLvq1crS0tFxzYPhY6AJ1ykIuqr4OgpltlF9aWkK8
XYS0vb296ttVCaTqH9P0r4VjY2OVHucib0lbFHLRb+88lpeXZ2dnNzc3L1vI
vzBbQsbk/8wzIGiduyhH9ZCLfs+RowqYi/7IkeNCMBcVCgWZKiUzd1qOAHKF
/Y/Zs7Ozly9fFovFw8PDP+MAf3R0lLSb4x0EZsAw2AbzYKG/AGc0UCI=
      "], {{0, 0}, {
      194, 22}}, {0, 255},
      ColorFunction->RGBColor],
     ImageSize->{194, 22},
     PlotRange->{{0, 194}, {0, 22}}]]], True->Cell[BoxData[
    GraphicsBox[RasterBox[CompressedData["
1:eJztmrtPZVUUxok2llb2/he2lrZjtCUzEQcaDHdMjFBCCXRAx6MCOh4VhAYE
Cp4Nr4J3olxHjdwLyS2A4M/7heWafR73Xo5I1PMlc7L22muv9a21v3MgZD5+
9c2Lr99ramp68wGPFy+/+7RQePn95x+y+KL9Tdvr9pavPmv/tuV1S+GTV+/j
/Ih/S/z7076/v7+7u7u5ualUKtfX11dXV+Vy+aqKchUY1w8wj3D9z8JzS0IQ
kKVQbC0/nKRC2goGlcLTjiRljr0Uf9YQzVyz05ozjN3lLIJBNojnvopSqXRx
cfFjjhwNAtkgHqnoubnk+HcjV1GO7MhVlCM7chUJa2tri4uLPJ+byF84PDxc
rOK5idSGVPRT4+js7PzSgeXc3JztHh0d9fb2Njc3s9Xd3b2+vm5bOAmWrV2f
c3h4WPbS0hK73khJi1M0MKampjCIxA+l1tZWlgMDAz65B0kUI2Bb2qRyUNIR
dpXZ2mHLMlNOYR0dHTYcDJY4SctZ8YyCLaNEJE15wpCRHwKWAb8uhadnVbMi
B8fHxxXpL7F+SEUXDjgv6gAk+/r6fngANjwxbJcB0vv8/Dw2/I+Pj/GzbK4C
m2BdkLZ0amRkRHasiiwtzSotM2ECmjORGr4GqINcJYbGHlURxznL7s7ODkue
3AslUsrhx8m9wB+2einE2U9AW5zFMzg4yNbu7i62BInBFgHMLTpbO4vNcFhy
itFpqYosCYCJqsuvzIqXv86KtMMpGBJgLaQgEElURXXC37gAPchgQMbPUw1q
Jn1VsNzY2OD2dU2aT5BT7XsjKS15SGJSJL8GaIZAcEAYSIGBU1VSuvB+X8X7
oURyPxy9a54SQ7DeDRTyeYLZSgnWrIYD1cCPbGKHEFvR8rDLiPww64RUVKyC
ZbFudHV1UdF7Njc3YbK3tydKscHSDMvp6emenh6c0lU0pyWJGkHak5MTfbFJ
3vWAKMMoYQBbaHuPquBM6UK3LCcelQv83vYZlMQjiNHnOsoTf1DOFwr8tqyn
osB1MEYMqvPxjI1JgldRQ4i9FHWk+XO55tcHUzLDL/Ho4+mHVo+Komllk5xU
epVsgH4aPjgKjk9MTHCEElJ1Srk6VaR7D4bmpS7xBzFRnQS7umsBeqYi76dx
G0LNisWqSrkFXm1dR/Bm1cRTqKj4rp4hprcbD4IvVi9Iv3YqgGANXC3r4rjT
6CcoNq0mYNdnAySDZVOSWBWRwT5l+rljW7Hliqkqsng6Nb9+A4QnlKhlyqSc
BuKRriK9iaZPyukdlF/0yI/ThlCzotgCwhgCR5KqJ0Eq+rlxQHJ0dNR79vf3
aYQn9szMjO6lUCjo7cbJEr+CcZJBtn6oKSfxenfs1PLyMrYiY9MqgxLKryqn
p6csNU+enAoIWyOA+Ue3ksphw0o2Oa0R/ERaQhX1vaicKLGltz5aFH/K5NWs
BmXN+iFwXDTqrIgHCTEuUg0NDaWUToJU9LZxSEXeA4G2tjZbIqfJyUlitra2
5FlZWTk7O5ONkwCLVIychHGKjuwUw0lJKywsLIxWQbw5KUcenJyKEhZI7o9g
E5lezjdi5M1vHLB1Fm5RqmxZEg+cAaW3kWlTURlshj6zBui7SK+YHVlU1N/f
v/IAbHoPxvW3IFDRo5GkIvy8iboOnrzFLLOXywLeRz4aeo8ky6iu0hGo6Kkh
Ff3SOPShNjB8JPSIPDWxurpK/ux5IDw2Nhb1b29v6weWQNj5+Xn2chkBVf0x
TX8tnJ2dbfQ4jTwRtygeraL/GNAScj04OHhuIu9gtYrnZlEbuYpyZIdU9GuO
HBlgKvotR45HwVRUKpUkqpTIXGk5AkgV9j9mb29vK5VKuVy+vLz8PQ74CU7a
zfE/BGJAMMgG8SChPwC1foQX
      "], {{0, 0}, {194, 22}}, {0, 255},
      ColorFunction->RGBColor],
     ImageSize->{194, 22},
     PlotRange->{{0, 194}, {0, 22}}]]]}, Dynamic[
    CurrentValue["MouseOver"]]],
  BaseStyle->"Hyperlink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/versions/source.jsp?id=\
RobotManipulatorWorkspaces&version=0009"], None},
  ButtonNote->
   "http://demonstrations.wolfram.com/RobotManipulatorWorkspaces/\
RobotManipulatorWorkspaces-source.nb"]], "DemoSourceNotebookSection",
 CellFrame->{{0, 0}, {1, 1}},
 ShowCellBracket->False,
 CellMargins->{{48, 48}, {28, 28}},
 CellGroupingRules->{"SectionGrouping", 25},
 CellFrameMargins->{{48, 48}, {6, 8}},
 CellFrameColor->RGBColor[0.87, 0.87, 0.87]],

Cell["PERMANENT CITATION", "CitationSection"],

Cell[TextData[{
 "\[NonBreakingSpace]",
 ButtonBox["Aaron T. Becker",
  BaseStyle->"Hyperlink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/author.html?author=Aaron+T.+\
Becker"], None},
  ButtonNote->
   "http://demonstrations.wolfram.com/author.html?author=Aaron+T.+Becker"],
 ", ",
 ButtonBox["Benedict Isichei",
  BaseStyle->"Hyperlink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/author.html?author=Benedict+\
Isichei"], None},
  ButtonNote->
   "http://demonstrations.wolfram.com/author.html?author=Benedict+Isichei"],
 ", ",
 ButtonBox["Muhammad Sultan",
  BaseStyle->"Hyperlink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/author.html?author=Muhammad+\
Sultan"], None},
  ButtonNote->
   "http://demonstrations.wolfram.com/author.html?author=Muhammad+Sultan"],
 " and ",
 ButtonBox["Maruthi S. Chemudupati",
  BaseStyle->"Hyperlink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/author.html?author=Maruthi+S.+\
Chemudupati"], None},
  ButtonNote->
   "http://demonstrations.wolfram.com/author.html?author=Maruthi+S.+\
Chemudupati"],
 " "
}], "Author",
 FontColor->GrayLevel[0.6],
 CellID->422655669],

Cell[TextData[{
 "\"",
 ButtonBox["Robot Manipulator Workspaces",
  BaseStyle->"SiteLink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/RobotManipulatorWorkspaces/"], 
    None},
  ButtonNote->"http://demonstrations.wolfram.com/RobotManipulatorWorkspaces/"],
 "\"",
 "\[ParagraphSeparator]\[NonBreakingSpace]",
 ButtonBox["http://demonstrations.wolfram.com/RobotManipulatorWorkspaces/",
  BaseStyle->"SiteLink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/RobotManipulatorWorkspaces/"], 
    None},
  ButtonNote->"http://demonstrations.wolfram.com/RobotManipulatorWorkspaces/"],
 "\[ParagraphSeparator]\[NonBreakingSpace]",
 ButtonBox["Wolfram Demonstrations Project",
  BaseStyle->"SiteLink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/"], None},
  ButtonNote->"http://demonstrations.wolfram.com/"],
 "\[ParagraphSeparator]\[NonBreakingSpace]",
 "Published: ",
 "December 13, 2017"
}], "Citations"],

Cell[TextData[{
 "\[Copyright] ",
 StyleBox[ButtonBox["Wolfram Demonstrations Project & Contributors",
  BaseStyle->"Hyperlink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/"], None},
  ButtonNote->"http://demonstrations.wolfram.com/"],
  FontColor->GrayLevel[0.6]],
 "\[ThickSpace]\[ThickSpace]\[ThickSpace]|\[ThickSpace]\[ThickSpace]\
\[ThickSpace]",
 StyleBox[ButtonBox["Terms of Use",
  BaseStyle->"Hyperlink",
  ButtonData->{
    URL["http://demonstrations.wolfram.com/termsofuse.html"], None},
  ButtonNote->"http://demonstrations.wolfram.com/termsofuse.html"],
  FontColor->GrayLevel[0.6]]
}], "Text",
 CellFrame->{{0, 0}, {0, 0.5}},
 CellMargins->{{48, 48}, {20, 50}},
 CellFrameColor->GrayLevel[0.45098],
 FontFamily->"Verdana",
 FontSize->9,
 FontColor->GrayLevel[0.6],
 CellTags->"Copyright"]
},
Editable->False,
Saveable->False,
ScreenStyleEnvironment->"Working",
CellInsertionPointCell->None,
CellGrouping->Manual,
WindowSize->{750, 650},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
WindowElements->{
 "StatusArea", "MemoryMonitor", "MagnificationPopUp", "VerticalScrollBar", 
  "MenuBar"},
WindowTitle->"Robot Manipulator Workspaces",
DockedCells->{},
CellContext->Notebook,
FrontEndVersion->"11.3 for Mac OS X x86 (32-bit, 64-bit Kernel) (March 14, \
2018)",
StyleDefinitions->Notebook[{
   Cell[
    CellGroupData[{
      Cell[
      "Demonstration Styles", "Title", 
       CellChangeTimes -> {
        3.3509184553711*^9, {3.36928902713192*^9, 3.36928902738193*^9}, {
         3.3754479092466917`*^9, 3.3754479095123196`*^9}, {
         3.375558447161495*^9, 3.375558447395873*^9}, {3.37572892702972*^9, 
         3.375728927639103*^9}}], 
      Cell[
       StyleData[StyleDefinitions -> "Default.nb"]], 
      Cell[
       CellGroupData[{
         Cell[
         "Style Environment Names", "Section", 
          CellChangeTimes -> {{3.369277974278112*^9, 3.369277974396138*^9}}], 
         
         Cell[
          StyleData[All, "Working"], ShowCellBracket -> False]}, Closed]], 
      Cell[
       CellGroupData[{
         Cell[
         "Notebook Options", "Section", 
          CellChangeTimes -> {{3.374865264950812*^9, 3.374865265419568*^9}}], 
         
         Cell[
         "  The options defined for the style below will be used at the \
Notebook level.  ", "Text"], 
         Cell[
          StyleData["Notebook"], Editable -> True, 
          PageHeaders -> {{None, None, None}, {None, None, None}}, 
          PageFooters -> {{None, None, None}, {None, None, None}}, 
          PageHeaderLines -> {False, False}, 
          PageFooterLines -> {False, False}, 
          PrintingOptions -> {
           "FacingPages" -> False, "FirstPageFooter" -> False, 
            "RestPagesFooter" -> False}, CreateCellID -> True, 
          CellFrameLabelMargins -> 6, DefaultNewInlineCellStyle -> 
          "InlineMath", DefaultInlineFormatType -> 
          "DefaultTextInlineFormatType", ShowStringCharacters -> True, 
          CacheGraphics -> False, StyleMenuListing -> None, 
          DemonstrationSite`Private`TrackCellChangeTimes -> False]}, Closed]], 
      Cell[
       CellGroupData[{
         Cell[
         "Input/Output", "Section", 
          CellChangeTimes -> {{3.3756313297791014`*^9, 
           3.3756313299509783`*^9}}], 
         Cell[
         "The cells in this section define styles used for input and output \
to the kernel.  Be careful when modifying, renaming, or removing these \
styles, because the front end associates special meanings with these style \
names.    ", "Text"], 
         Cell[
          StyleData["Input"], CellMargins -> {{48, 4}, {6, 4}}], 
         Cell[
          StyleData["Output"], CellMargins -> {{48, 4}, {6, 4}}], 
         Cell[
          StyleData["DemonstrationHeader"], Deletable -> False, 
          CellFrame -> {{0, 0}, {0, 0}}, ShowCellBracket -> False, 
          CellMargins -> {{0, 0}, {30, 0}}, 
          CellGroupingRules -> {"SectionGrouping", 20}, 
          CellHorizontalScrolling -> True, 
          CellFrameMargins -> {{0, 0}, {0, 0}}, CellFrameColor -> 
          RGBColor[0, 0, 0], StyleMenuListing -> None, Background -> 
          RGBColor[0, 0, 0]], 
         Cell[
          StyleData["ShowSource"], CellFrame -> {{0, 0}, {0, 1}}, 
          ShowCellBracket -> False, CellMargins -> {{48, 48}, {8, 28}}, 
          CellFrameMargins -> {{48, 48}, {6, 8}}, CellFrameColor -> 
          RGBColor[0.87, 0.87, 0.87], StyleMenuListing -> None, FontFamily -> 
          "Helvetica", FontSize -> 10, FontWeight -> "Bold", FontSlant -> 
          "Plain", FontColor -> RGBColor[1, 0.42, 0]]}, Closed]], 
      Cell[
       CellGroupData[{
         Cell[
         "Basic Styles", "Section", 
          CellChangeTimes -> {{3.34971724802035*^9, 3.34971724966638*^9}, {
           3.35091840608065*^9, 3.35091840781999*^9}, {3.35091845122987*^9, 
           3.35091845356607*^9}, {3.35686681885432*^9, 3.35686681945788*^9}, {
           3.375657418186455*^9, 3.375657418452083*^9}}], 
         Cell[
          StyleData["Hyperlink"], StyleMenuListing -> None, FontColor -> 
          GrayLevel[0]], 
         Cell[
          StyleData["SiteLink"], StyleMenuListing -> None, 
          ButtonStyleMenuListing -> Automatic, FontColor -> 
          GrayLevel[0.45098], 
          ButtonBoxOptions -> {
           Active -> True, Appearance -> {Automatic, None}, 
            ButtonFunction :> (FrontEndExecute[{
               NotebookLocate[#2]}]& ), ButtonNote -> ButtonData}], 
         Cell[
          StyleData["Link"], FontColor -> GrayLevel[0.45098]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData["DemoNotes"], CellFrame -> True, 
             CellMargins -> {{0, 0}, {0, 0}}, 
             CellFrameMargins -> {{48, 48}, {4, 4}}, CellFrameColor -> 
             GrayLevel[0.99], StyleMenuListing -> None, FontFamily -> 
             "Verdana", FontSize -> 10, FontColor -> GrayLevel[0.45098], 
             DemonstrationSite`Private`ReturnCreatesNewCell -> True], 
            Cell[
             StyleData["DemoNotes", "Printout"], 
             CellMargins -> {{24, 0}, {0, 10}}, FontSize -> 9]}, Open]], 
         Cell[
          StyleData["SnapshotsSection"], CellFrame -> {{0, 0}, {0, 2}}, 
          ShowCellBracket -> False, ShowGroupOpener -> True, 
          CellMargins -> {{48, 48}, {10, 30}}, 
          CellGroupingRules -> {"SectionGrouping", 30}, 
          CellFrameMargins -> {{8, 8}, {8, 2}}, CellFrameColor -> 
          RGBColor[0.870588, 0.521569, 0.121569], DefaultNewCellStyle -> 
          "SnapshotCaption", StyleMenuListing -> None, FontFamily -> 
          "Verdana", FontSize -> 12, FontColor -> GrayLevel[0.45098], 
          PrivateCellOptions -> {"DefaultCellGroupOpen" -> False}], 
         Cell[
          StyleData[
          "SnapshotCaption", StyleDefinitions -> StyleData["DemoNotes"]], 
          ShowCellBracket -> False], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData["SnapshotOutput"], ShowCellBracket -> False, 
             CellMargins -> {{48, 10}, {5, 7}}, Evaluatable -> True, 
             CellGroupingRules -> "InputGrouping", PageBreakWithin -> False, 
             GroupPageBreakWithin -> False, DefaultFormatType -> 
             DefaultInputFormatType, ShowAutoStyles -> True, 
             "TwoByteSyntaxCharacterAutoReplacement" -> True, 
             HyphenationOptions -> {
              "HyphenationCharacter" -> "\[Continuation]"}, 
             AutoItalicWords -> {}, LanguageCategory -> "Mathematica", 
             FormatType -> InputForm, NumberMarks -> True, 
             LinebreakAdjustments -> {0.85, 2, 10, 0, 1}, CounterIncrements -> 
             "Input", MenuSortingValue -> 1500, MenuCommandKey -> "9", 
             DemonstrationSite`Private`StripStyleOnPaste -> True], 
            Cell[
             StyleData["SnapshotOuput", "Printout"], 
             CellMargins -> {{39, 0}, {4, 6}}, 
             LinebreakAdjustments -> {0.85, 2, 10, 1, 1}]}, Open]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData["DemoTitle"], Deletable -> False, ShowCellBracket -> 
             False, CellMargins -> {{48, 48}, {22, 10}}, 
             CellGroupingRules -> {"SectionGrouping", 20}, StyleMenuListing -> 
             None, FontFamily -> "Verdana", FontSize -> 20, FontWeight -> 
             "Bold", FontColor -> RGBColor[0.597406, 0, 0.0527047], 
             Background -> GrayLevel[1]], 
            Cell[
             StyleData["DemoName", "Printout"], 
             CellMargins -> {{24, 8}, {8, 27}}, 
             HyphenationOptions -> {"HyphenationCharacter" -> "-"}, FontSize -> 
             16]}, Open]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData["DetailsSection"], CellFrame -> {{0, 0}, {1, 0}}, 
             ShowCellBracket -> False, CellMargins -> {{48, 48}, {8, 28}}, 
             CellGroupingRules -> {"SectionGrouping", 25}, 
             CellFrameMargins -> {{48, 48}, {6, 8}}, CellFrameColor -> 
             RGBColor[0.87, 0.87, 0.87], StyleMenuListing -> None, FontFamily -> 
             "Helvetica", FontSize -> 12, FontWeight -> "Bold", FontColor -> 
             RGBColor[0.597406, 0, 0.0527047]], 
            Cell[
             StyleData["DetailsSection", "Printout"], 
             CellMargins -> {{12, 0}, {0, 16}}, PageBreakBelow -> False, 
             FontSize -> 12]}, Open]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData["DemoSection"], CellFrame -> {{0, 0}, {1, 0}}, 
             ShowCellBracket -> False, CellMargins -> {{48, 48}, {8, 28}}, 
             CellGroupingRules -> {"SectionGrouping", 30}, 
             CellFrameMargins -> {{48, 48}, {6, 8}}, CellFrameColor -> 
             RGBColor[0.87, 0.87, 0.87], StyleMenuListing -> None, FontFamily -> 
             "Helvetica", FontSize -> 12, FontWeight -> "Bold", FontColor -> 
             RGBColor[0.597406, 0, 0.0527047]], 
            Cell[
             StyleData["DemoSection", "Printout"], 
             CellMargins -> {{12, 0}, {0, 16}}, PageBreakBelow -> False, 
             FontSize -> 12]}, Open]], 
         Cell[
          StyleData["ManipulateSection"], CellFrame -> {{0, 0}, {0, 2}}, 
          ShowCellBracket -> False, CellMargins -> {{48, 48}, {10, 30}}, 
          CellGroupingRules -> {"SectionGrouping", 30}, 
          CellFrameMargins -> {{8, 8}, {8, 2}}, CellFrameColor -> 
          RGBColor[0.870588, 0.521569, 0.121569], StyleMenuListing -> None, 
          FontFamily -> "Verdana", FontSize -> 12], 
         Cell[
          StyleData["ManipulateCaptionSection"], 
          CellFrame -> {{0, 0}, {0, 2}}, ShowCellBracket -> False, 
          CellMargins -> {{48, 48}, {10, 30}}, 
          CellGroupingRules -> {"SectionGrouping", 30}, 
          CellFrameMargins -> {{8, 8}, {8, 2}}, CellFrameColor -> 
          RGBColor[0.870588, 0.521569, 0.121569], DefaultNewCellStyle -> 
          "ManipulateCaption", StyleMenuListing -> None, FontFamily -> 
          "Verdana", FontSize -> 12, FontColor -> GrayLevel[0.45098]], 
         Cell[
          StyleData["ManipulateCaption"], ShowCellBracket -> False, 
          CellMargins -> {{48, 48}, {10, 16}}, StyleMenuListing -> None, 
          FontFamily -> "Verdana", FontSize -> 12, FontColor -> GrayLevel[0], 
          DemonstrationSite`Private`ReturnCreatesNewCell -> True], 
         Cell[
          StyleData[
          "SeeAlsoSection", StyleDefinitions -> StyleData["DemoSection"]], 
          ShowCellBracket -> False, DefaultNewCellStyle -> "SeeAlso"], 
         Cell[
          StyleData["SeeAlso", StyleDefinitions -> StyleData["DemoNotes"]], 
          CellDingbat -> 
          Cell["\[FilledSmallSquare]", FontColor -> 
            RGBColor[0.928786, 0.43122, 0.104662]], ShowCellBracket -> False, 
          FontColor -> GrayLevel[0.45098]], 
         Cell[
          StyleData[
          "RelatedLinksSection", StyleDefinitions -> 
           StyleData["DemoSection"]], ShowCellBracket -> False, 
          DefaultNewCellStyle -> "RelatedLinks"], 
         Cell[
          StyleData[
          "RelatedLinks", StyleDefinitions -> StyleData["DemoNotes"], 
           FontColor -> RGBColor[0.928786, 0.43122, 0.104662]], 
          ShowCellBracket -> False, FontColor -> GrayLevel[0.45098]], 
         Cell[
          StyleData[
          "CategoriesSection", StyleDefinitions -> StyleData["DemoSection"]], 
          ShowCellBracket -> False, DefaultNewCellStyle -> "Categories"], 
         Cell[
          StyleData["Categories", StyleDefinitions -> StyleData["DemoNotes"]],
           ShowCellBracket -> False], 
         Cell[
          StyleData[
          "AuthorSection", StyleDefinitions -> StyleData["DemoSection"]], 
          ShowCellBracket -> False, CellMargins -> {{48, 48}, {4, 18}}, 
          CellElementSpacings -> {"CellMinHeight" -> 3}, 
          CellFrameMargins -> {{48, 48}, {6, 3}}, DefaultNewCellStyle -> 
          "Author", FontSize -> 1, FontColor -> GrayLevel[1]], 
         Cell[
          StyleData[
          "Author", StyleDefinitions -> StyleData["DemoNotes"], FontColor -> 
           GrayLevel[0.64]], ShowCellBracket -> False], 
         Cell[
          StyleData[
          "DetailNotes", StyleDefinitions -> StyleData["DemoNotes"]], 
          ShowCellBracket -> False, FontColor -> GrayLevel[0]], 
         Cell[
          StyleData[
          "CitationSection", StyleDefinitions -> StyleData["DemoSection"]], 
          ShowCellBracket -> False, CellMargins -> {{48, 48}, {8, 14}}, 
          DefaultNewCellStyle -> "Categories"], 
         Cell[
          StyleData["Citations", StyleDefinitions -> StyleData["DemoNotes"]], 
          ShowCellBracket -> False, ParagraphSpacing -> {0, 6}], 
         Cell[
          StyleData[
          "RevisionSection", StyleDefinitions -> StyleData["DemoSection"]], 
          DefaultNewCellStyle -> "RevisionNotes"], 
         Cell[
          StyleData[
          "RevisionNotes", StyleDefinitions -> StyleData["DemoNotes"]], 
          ShowCellBracket -> False]}, Closed]], 
      Cell[
       CellGroupData[{
         Cell[
         "Specific Styles", "Section", 
          CellChangeTimes -> {{3.34971724802035*^9, 3.34971724966638*^9}, {
           3.35091840608065*^9, 3.35091840781999*^9}, {3.35091845122987*^9, 
           3.35091845356607*^9}, {3.36230868322317*^9, 3.36230868335672*^9}, {
           3.36928857618576*^9, 3.36928857640452*^9}, {3.3737586217185173`*^9,
            3.373758622077897*^9}}], 
         Cell[
          StyleData["InitializationSection"], CellFrame -> {{0, 0}, {0, 2}}, 
          ShowCellBracket -> False, CellMargins -> {{48, 48}, {10, 30}}, 
          CellGroupingRules -> {"SectionGrouping", 30}, 
          CellFrameMargins -> {{8, 8}, {8, 2}}, CellFrameColor -> 
          RGBColor[0.870588, 0.521569, 0.121569], StyleMenuListing -> None, 
          FontFamily -> "Verdana", FontSize -> 12, FontColor -> 
          GrayLevel[0.45098]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData["AnchorBar"], ShowCellBracket -> False, 
             CellMargins -> {{48, 44}, {3, 6}}, StyleMenuListing -> None, 
             FontFamily -> "Verdana", FontSize -> 9, FontColor -> 
             GrayLevel[0.5]], 
            Cell[
             StyleData["AnchorBar", "Presentation"], FontSize -> 18], 
            Cell[
             StyleData["AnchorBar", "SlideShow"], StyleMenuListing -> None], 
            Cell[
             StyleData["AnchorBar", "Printout"], FontSize -> 9]}, Open]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData["AnchorLink"], StyleMenuListing -> None, 
             ButtonStyleMenuListing -> Automatic, FontColor -> 
             RGBColor[0.5, 0.5, 0.5], 
             ButtonBoxOptions -> {
              Active -> True, ButtonFunction :> (FrontEndExecute[{
                  FrontEnd`NotebookLocate[#2]}]& ), ButtonNote -> 
               ButtonData}], 
            Cell[
             StyleData["AnchorLink", "Printout"], 
             FontVariations -> {"Underline" -> False}, FontColor -> 
             GrayLevel[0]]}, Open]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData["GamePadStatus"], ShowCellBracket -> False, 
             CellMargins -> {{48, 48}, {5, 5}}, StyleMenuListing -> None, 
             FontFamily -> "Verdana", FontSize -> 10], 
            Cell[
             StyleData["GamePadStatus", "Printout"], 
             CellMargins -> {{24, 0}, {0, 10}}, FontSize -> 9]}, Open]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData["DemoInstruction"], CellMargins -> {{48, 48}, {5, 5}}, 
             CellFrameLabelMargins -> 2, MenuSortingValue -> 800, 
             MenuCommandKey -> "8", StyleMenuListing -> None, FontFamily -> 
             "Verdana", FontSize -> 11, Background -> RGBColor[1, 0.85, 0.5], 
             DemonstrationSite`Private`ReturnCreatesNewCell -> True], 
            Cell[
             StyleData["DemoInstruction", "Printout"], 
             CellMargins -> {{24, 0}, {0, 10}}, Hyphenation -> True, 
             HyphenationOptions -> {"HyphenationCharacter" -> "-"}, 
             LineSpacing -> {1., 2, 2.}, FontSize -> 9]}, Open]], 
         Cell[
          StyleData[
          "ImplementationSection", StyleDefinitions -> 
           StyleData["DemoSection"]], Deletable -> True, DefaultNewCellStyle -> 
          "ImplementationNotes"], 
         Cell[
          StyleData[
          "ImplementationNotes", StyleDefinitions -> StyleData["DemoNotes"]]], 
         Cell[
          StyleData[
          "StatusSection", StyleDefinitions -> StyleData["DemoSection"]], 
          DefaultNewCellStyle -> "StatusNotes"], 
         Cell[
          StyleData[
          "StatusNotes", StyleDefinitions -> StyleData["DemoNotes"]]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData["SectionGloss"], StyleMenuListing -> None, FontSize -> 
             0.85 Inherited, FontWeight -> "Plain", FontColor -> 
             GrayLevel[0.6]], 
            Cell[
             StyleData["SectionGloss", "Printout"]]}, Open]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData["InlineFormula"], 
             "TwoByteSyntaxCharacterAutoReplacement" -> True, 
             HyphenationOptions -> {
              "HyphenationCharacter" -> "\[Continuation]"}, LanguageCategory -> 
             "Formula", AutoSpacing -> True, ScriptLevel -> 1, 
             AutoMultiplicationSymbol -> False, SingleLetterItalics -> False, 
             SpanMaxSize -> 1, StyleMenuListing -> None, FontFamily -> 
             "Courier", FontSize -> 1.05 Inherited, 
             ButtonBoxOptions -> {Appearance -> {Automatic, None}}, 
             FractionBoxOptions -> {BaseStyle -> {SpanMaxSize -> Automatic}}, 
             GridBoxOptions -> {
              GridBoxItemSize -> {
                "Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, 
                 "Rows" -> {{1.}}, "RowsIndexed" -> {}}}], 
            Cell[
             StyleData["InlineFormula", "Printout"], 
             CellMargins -> {{2, 0}, {0, 8}}]}, Open]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData["InlineOutput"], CellHorizontalScrolling -> True, 
             "TwoByteSyntaxCharacterAutoReplacement" -> True, 
             HyphenationOptions -> {
              "HyphenationCharacter" -> "\[Continuation]"}, LanguageCategory -> 
             None, AutoMultiplicationSymbol -> False, StyleMenuListing -> 
             None, FontFamily -> "Courier", FontSize -> 1.05 Inherited], 
            Cell[
             StyleData["InlineOutput", "Printout"], 
             CellMargins -> {{2, 0}, {0, 8}}]}, Open]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData["InlineMath"], DefaultFormatType -> 
             "DefaultTextFormatType", DefaultInlineFormatType -> 
             "TraditionalForm", LanguageCategory -> "Formula", AutoSpacing -> 
             True, ScriptLevel -> 1, AutoMultiplicationSymbol -> False, 
             SingleLetterItalics -> True, SpanMaxSize -> DirectedInfinity[1], 
             StyleMenuListing -> None, FontFamily -> "Times", FontSize -> 
             1.05 Inherited, 
             ButtonBoxOptions -> {Appearance -> {Automatic, None}}, 
             GridBoxOptions -> {
              GridBoxItemSize -> {
                "Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, 
                 "Rows" -> {{1.}}, "RowsIndexed" -> {}}}], 
            Cell[
             StyleData["InlineMath", "Printout"], 
             CellMargins -> {{2, 0}, {0, 8}}]}, Open]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData["TableBase"], CellMargins -> {{48, 48}, {4, 4}}, 
             SpanMaxSize -> 1, StyleMenuListing -> None, FontFamily -> 
             "Courier", FontSize -> 11, 
             ButtonBoxOptions -> {Appearance -> {Automatic, None}}, 
             GridBoxOptions -> {
              GridBoxAlignment -> {
                "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, 
                 "Rows" -> {{Baseline}}, "RowsIndexed" -> {}}}], 
            Cell[
             StyleData["TableBase", "Printout"], 
             CellMargins -> {{2, 0}, {0, 8}}, FontSize -> 9]}, Open]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData[
             "1ColumnTableMod", StyleDefinitions -> StyleData["TableBase"]], 
             GridBoxOptions -> {GridBoxItemSize -> {"Columns" -> {
                   Scaled[0.04], {
                    Scaled[0.966]}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}},
                  "RowsIndexed" -> {}}, GridBoxSpacings -> {"Columns" -> {
                   Offset[0.28], 
                   Offset[0.126], {
                    Offset[0.77]}, 
                   Offset[0.28]}, "ColumnsIndexed" -> {}, "Rows" -> {
                   Offset[0.2], {
                    Offset[0.4]}, 
                   Offset[0.2]}, "RowsIndexed" -> {}}}], 
            Cell[
             StyleData[
             "1ColumnTableMod", "Printout", StyleDefinitions -> 
              StyleData["TableBase", "Printout"]], 
             GridBoxOptions -> {GridBoxItemSize -> {"Columns" -> {
                   Scaled[0.078], {
                    Scaled[0.922]}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}},
                  "RowsIndexed" -> {}}, GridBoxSpacings -> {"Columns" -> {
                   Offset[0.28], {
                    Offset[0.56]}, 
                   Offset[0.28]}, "ColumnsIndexed" -> {}, "Rows" -> {
                   Offset[0.2], {
                    Offset[0.56]}, 
                   Offset[0.2]}, "RowsIndexed" -> {}}}]}, Open]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData[
             "2ColumnTableMod", StyleDefinitions -> StyleData["TableBase"]], 
             GridBoxOptions -> {GridBoxItemSize -> {"Columns" -> {
                   Scaled[0.05], 
                   Scaled[0.41], {
                    Scaled[0.565]}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}},
                  "RowsIndexed" -> {}}, GridBoxSpacings -> {"Columns" -> {
                   Offset[0.28], 
                   Offset[0.14], {
                    Offset[0.77]}, 
                   Offset[0.28]}, "ColumnsIndexed" -> {}, "Rows" -> {
                   Offset[0.2], {
                    Offset[0.4]}, 
                   Offset[0.2]}, "RowsIndexed" -> {}}}], 
            Cell[
             StyleData[
             "2ColumnTableMod", "Printout", StyleDefinitions -> 
              StyleData["TableBase", "Printout"]], 
             GridBoxOptions -> {GridBoxItemSize -> {"Columns" -> {
                   Scaled[0.079], 
                   Scaled[0.363], {
                    Scaled[0.558]}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}},
                  "RowsIndexed" -> {}}, GridBoxSpacings -> {"Columns" -> {
                   Offset[0.28], {
                    Offset[0.56]}, 
                   Offset[0.28]}, "ColumnsIndexed" -> {}, "Rows" -> {
                   Offset[0.2], {
                    Offset[0.56]}, 
                   Offset[0.2]}, "RowsIndexed" -> {}}}]}, Open]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData[
             "3ColumnTableMod", StyleDefinitions -> StyleData["TableBase"]], 
             GridBoxOptions -> {GridBoxItemSize -> {"Columns" -> {
                   Scaled[0.04], 
                   Scaled[0.266], 
                   Scaled[0.26], {
                    Scaled[0.44]}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, 
                 "RowsIndexed" -> {}}, GridBoxSpacings -> {"Columns" -> {
                   Offset[0.28], 
                   Offset[0.14], {
                    Offset[0.77]}, 
                   Offset[0.28]}, "ColumnsIndexed" -> {}, "Rows" -> {
                   Offset[0.2], {
                    Offset[0.4]}, 
                   Offset[0.2]}, "RowsIndexed" -> {}}}], 
            Cell[
             StyleData[
             "3ColumnTableMod", "Printout", StyleDefinitions -> 
              StyleData["TableBase", "Printout"]], 
             GridBoxOptions -> {GridBoxItemSize -> {"Columns" -> {
                   Scaled[0.08], 
                   Scaled[0.25], 
                   Scaled[0.25], {
                    Scaled[0.42]}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, 
                 "RowsIndexed" -> {}}, GridBoxSpacings -> {"Columns" -> {
                   Offset[0.28], {
                    Offset[0.56]}, 
                   Offset[0.28]}, "ColumnsIndexed" -> {}, "Rows" -> {
                   Offset[0.2], {
                    Offset[0.56]}, 
                   Offset[0.2]}, "RowsIndexed" -> {}}}]}, Open]], 
         Cell[
          CellGroupData[{
            Cell[
             StyleData["TableText"], Deletable -> False, StyleMenuListing -> 
             None, FontFamily -> "Verdana", FontSize -> 0.952 Inherited], 
            Cell[
             StyleData["TableText", "Printout"], 
             CellMargins -> {{24, 0}, {0, 8}}, Hyphenation -> True, 
             HyphenationOptions -> {"HyphenationCharacter" -> "-"}, 
             LineSpacing -> {1., 2, 2.}]}, Open]], 
         Cell[
          StyleData["Continuation"], FontColor -> GrayLevel[1]]}, Closed]]}, 
     Open]]}, Visible -> False, FrontEndVersion -> 
  "11.3 for Mac OS X x86 (32-bit, 64-bit Kernel) (March 14, 2018)", 
  StyleDefinitions -> "Default.nb"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "ControlSuggestions"->{
  Cell[364461, 6610, 258, 7, 70, "ManipulateCaption",ExpressionUUID->"737cd4cf-fa7c-470b-8b08-b7091465f268",
   CellTags->"ControlSuggestions"],
  Cell[364722, 6619, 5495, 119, 70, "ManipulateCaption",ExpressionUUID->"4af48fa3-d145-4650-accc-2d176aad34e3",
   CellTags->"ControlSuggestions"]},
 "Copyright"->{
  Cell[388563, 7268, 818, 23, 70, "Text",ExpressionUUID->"6b74adb4-0c30-4b02-9893-567abff63996",
   CellTags->"Copyright"]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"ControlSuggestions", 415257, 7840},
 {"Copyright", 415567, 7845}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[1530, 34, 34394, 569, 70, "DemonstrationHeader",ExpressionUUID->"d4fd0da3-4a44-4bec-8149-73dbd60c59b4"],
Cell[35927, 605, 49, 0, 70, "DemoTitle",ExpressionUUID->"225930b4-f559-4b69-8b7b-7b077c8987a3"],
Cell[35979, 607, 328006, 5989, 70, "Output",ExpressionUUID->"200e8e08-d8e6-4330-8171-d3d997cf5278",
 CellID->96089594],
Cell[363988, 6598, 470, 10, 70, "ManipulateCaption",ExpressionUUID->"8a58011b-b0c0-4233-9447-fb6cdcc00371",
 CellID->80008708],
Cell[364461, 6610, 258, 7, 70, "ManipulateCaption",ExpressionUUID->"737cd4cf-fa7c-470b-8b08-b7091465f268",
 CellTags->"ControlSuggestions"],
Cell[364722, 6619, 5495, 119, 70, "ManipulateCaption",ExpressionUUID->"4af48fa3-d145-4650-accc-2d176aad34e3",
 CellTags->"ControlSuggestions"],
Cell[370220, 6740, 33, 0, 70, "DetailsSection",ExpressionUUID->"2194f910-745d-4096-a3ad-20ed7c525f72"],
Cell[370256, 6742, 449, 8, 70, "DetailNotes",ExpressionUUID->"f5810f20-1074-41d3-9304-1eec110abc6e",
 CellID->447453696],
Cell[370708, 6752, 845, 26, 70, "DetailNotes",ExpressionUUID->"f02700fc-1e62-480f-8158-6765125cc8d0",
 CellID->558642309],
Cell[371556, 6780, 778, 20, 70, "DetailNotes",ExpressionUUID->"ca6563d3-abdd-402d-af11-f6188479902c",
 CellID->1398432875],
Cell[372337, 6802, 648, 15, 70, "DetailNotes",ExpressionUUID->"bd6180a9-b2f0-4836-9483-733fe42596fa",
 CellID->994441909],
Cell[372988, 6819, 52, 1, 70, "DetailNotes",ExpressionUUID->"90250e17-fb46-471d-bd84-55a6bdbb8950",
 CellID->470230901],
Cell[373043, 6822, 217, 6, 70, "DetailNotes",ExpressionUUID->"23f8d76b-68b2-4549-b7ce-4713f3c4e260",
 CellID->54221902],
Cell[373263, 6830, 44, 0, 70, "RelatedLinksSection",ExpressionUUID->"3bc94578-638b-48d5-8e12-0eefc417b759"],
Cell[373310, 6832, 651, 21, 70, "RelatedLinks",ExpressionUUID->"688119f5-b504-4eaf-9b05-0fdc5aff96f9",
 CellID->1860385687],
Cell[373964, 6855, 856, 29, 70, "RelatedLinks",ExpressionUUID->"371c5942-d286-4ef1-8a20-9b638930ac30",
 CellID->140341089],
Cell[374823, 6886, 582, 17, 70, "RelatedLinks",ExpressionUUID->"8b3095a8-c510-472a-9bba-e3d629f17a43",
 CellID->285307411],
Cell[375408, 6905, 560, 17, 70, "RelatedLinks",ExpressionUUID->"977d5bfc-c135-490b-ad50-b06825b4bd3d",
 CellID->15861152],
Cell[375971, 6924, 567, 17, 70, "RelatedLinks",ExpressionUUID->"684db157-a32e-40b4-80b6-1d244d3db4e5",
 CellID->419985252],
Cell[376541, 6943, 626, 18, 70, "RelatedLinks",ExpressionUUID->"1dac14e6-d014-4747-93ae-5a7123d48c5a",
 CellID->475238986],
Cell[377170, 6963, 630, 18, 70, "RelatedLinks",ExpressionUUID->"92aac8ce-1b68-47d5-ba28-953921886eba",
 CellID->63190210],
Cell[377803, 6983, 535, 16, 70, "RelatedLinks",ExpressionUUID->"b037a808-8c02-4fc1-b0ea-5cfe12da0793",
 CellID->188192833],
Cell[378341, 7001, 467, 14, 70, "RelatedLinks",ExpressionUUID->"031af0bf-b794-4aad-8b4c-0b5ffd22dfa0",
 CellID->114694354],
Cell[378811, 7017, 521, 16, 70, "RelatedLinks",ExpressionUUID->"4ca0a8d4-3c35-42fe-aacb-3a026e8f0024",
 CellID->708933422],
Cell[379335, 7035, 635, 21, 70, "RelatedLinks",ExpressionUUID->"d04376c9-d0ac-414c-a3f0-81b666ef5d7c",
 CellID->105557710],
Cell[379973, 7058, 478, 14, 70, "RelatedLinks",ExpressionUUID->"ae3f8e4a-2b25-4301-b483-7c0086a23c38",
 CellID->417986324],
Cell[380454, 7074, 494, 14, 70, "RelatedLinks",ExpressionUUID->"190fe68f-aa83-435a-b268-7377ef9b2944",
 CellID->857510713],
Cell[380951, 7090, 478, 14, 70, "RelatedLinks",ExpressionUUID->"d3b3f36a-cd0a-448d-a926-565d9082bb21",
 CellID->419798374],
Cell[381432, 7106, 4962, 92, 70, "DemoSourceNotebookSection",ExpressionUUID->"78784d0b-4058-4d90-b617-97ff3a482e14",
 CellGroupingRules->{"SectionGrouping", 25}],
Cell[386397, 7200, 45, 0, 70, "CitationSection",ExpressionUUID->"c791b8ea-8e46-4bcc-b98c-7a219655be7e"],
Cell[386445, 7202, 1168, 37, 70, "Author",ExpressionUUID->"7072aa23-2e59-4387-847b-dde9aeed6d45",
 CellID->422655669],
Cell[387616, 7241, 944, 25, 70, "Citations",ExpressionUUID->"85e860a1-8049-47bc-b473-827ffb7b3e14"],
Cell[388563, 7268, 818, 23, 70, "Text",ExpressionUUID->"6b74adb4-0c30-4b02-9893-567abff63996",
 CellTags->"Copyright"]
}
]
*)

(* End of internal cache information *)
(* NotebookSignature 3uDcvV8tIS9ebB1tmgmwVYug *)
